<?php

if(!$sitemap = cache::get('sitemap')) {
	tpl::load('sitemap');
	db::query('select title_url from blog order by title_url');
	foreach(db::fetchAll() as $post) {
		tpl::push($post);
		tpl::make('sitemap-yamiblog-post');
	}
	$delivery_cities = json::get('delivery-cities');
	foreach($delivery_cities as $letter => $list) {
		foreach($list as $city_url => $cityname) {
			tpl::set('city-url', $city_url);
			tpl::make('sitemap-delivery-city');
		}
	}
	db::query('select url from categories order by url');
	foreach(db::fetchAll() as $cat) {
		tpl::set('cat-url', $cat['url']);
		tpl::make('sitemap-catalog-category');
	}
	$brands = shop::get_brands();
	foreach($brands as $brand) {
		tpl::set('brand-url', $brand['url']);
		tpl::make('sitemap-brand');
	}
	db::query('select url_name from product where status > -1 and url_name <> \'\' order by url_name');
	$pages = ceil(db::count() / PAGE_LIMIT_CATALOG);
	for($p = 2; $p < $pages - 3; $p++) {
		tpl::set('pnum', $p);
		tpl::make('sitemap-catalog-page');
	}
	while($product = db::fetchArray()) {
		tpl::set('product-url-name', $product['url_name']);
		tpl::make('sitemap-catalog-product');
	}
	$sitemap = tpl::make('sitemap');
	cache::set('sitemap', $sitemap);
}
header('Content-Type: text/xml');
echo $sitemap;
exit;

?>