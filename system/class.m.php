<?php

class M {
	
	static function layout() {
		tpl::load('mobile-layout');
		$css_mtime = filemtime(ROOT.'/ui/css/mobile.css');
		tpl::set('css-mtime', $css_mtime);
		if(account::$admin) {
			tpl::make('mobile-admin-link');
		}
		echo tpl::make('mobile-layout');
	}
	
}

?>