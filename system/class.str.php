<?php

class Str {

	static function startsWith($haystack, $needle, $case=true) {
		return ($case)
			? strpos($haystack, $needle, 0) === 0
			: stripos($haystack, $needle, 0) === 0;
	}

	static function endsWith($haystack, $needle, $case=true) {
		$expectedPosition = strlen($haystack) - strlen($needle);
		return ($case)
			? strrpos($haystack, $needle, 0) === $expectedPosition
			: strripos($haystack, $needle, 0) === $expectedPosition;
	}
	
	static function numberof($numberof, $value, $suffix) {
		$keys = array(2, 0, 1, 1, 1, 2);
		$mod = $numberof % 100;
		$suffix_key = $mod > 4 && $mod < 20 ? 2 : $keys[min($mod%10, 5)];
		return $value . $suffix[$suffix_key];
	}
	
	static function rus2translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',
			'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
			'и' => 'i',   'й' => 'y',   'к' => 'k',
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'h',   'ц' => 'c',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
			'ь' => "'",  'ы' => 'y',   'ъ' => "'",
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
			'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
			'И' => 'I',   'Й' => 'Y',   'К' => 'K',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
			'Ь' => "'",  'Ы' => 'Y',   'Ъ' => "'",
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		return strtr($string, $converter);
	}
	
	static function filter_phone_number($phone_number) {
		$str = preg_replace('/[^0-9]/s', '', $phone_number);
		$length = strlen($str);
		if($length < 10) return false;
		if($length > 11) return false;
		if($length == 11) {
			$first_cipher = substr($str, 0, 1);
			if($first_cipher == '8') {
				$str = substr_replace($str, '7', 0, 1);
			}
			elseif($first_cipher !== '7') return false;
		}
		else {
			$str = '7'.$str;
		}
		return substr($str, 0, 2) == '79' ? $str : false;
	}
	
	static function utf8($str) {
		return mb_convert_encoding($str, 'UTF-8');
	}
	
	static function phoneNumber($number) {
		$number = trim($number);
		if(is_numeric($number)) {
			$ln = strlen($number);
			if($ln == 10) {
				$number = '+7'.$number;
			}
		}
		return $number;
	}
	
	static function str2url($str) {
		$str = str::rus2translit($str);
		$str = mb_strtolower($str);
		$str = preg_replace('#[^\w ]+#', '', $str);
		$str = preg_replace('#\s+#', '-', $str);
		return $str;
	}
	
	static function str2url_ajax() {
		$str = GET('str');
		if(!$str) die('EMPTY STRING');
		$str = self::str2url($str);
		die($str);
	}
	
	static function generatePassword($lenght = 0) {
		if(!$length) $length = rand(8, 11);
		return substr(md5(microtime()), 0, $length);
	}
	
	static function generateCoupon($numbers = true, $letters = true) {
		if(!$numbers && !$letters) $numbers = true;
		$chars = ''; $N = '0123456789'; $L = 'ABCDEFGHJKMNPQRSTUVWXYZ';
		if($numbers) $chars .= $N;
		if($letters) $chars .= $L;
		if($numbers && $letters) {
			$numbers = substr(str_shuffle($N), 0, 4);
			$letters = substr(str_shuffle($L), 0, 4);
			return str_shuffle($numbers.$letters);
		}
		else {
			$chars = str_shuffle($chars);
			return substr($chars, 0, 8);
		}
	}
	
	static function address2str($address, $complete = true) {
		$str = '';
		if(!is_array($address)) return $str;
		if(isset($address['building']) && isset($address['building-add']) && !empty($address['building-add'])) {
			$address['building'] .= '/'.$address['building-add'];
			$address['building-add'] = '';
		}
		if(isset($address['apt']) && !empty($address['apt'])) $address['apt'] = 'кв. '.$address['apt'];
		$address = array_filter($address, function($val){return !!$val;});
		if(!$complete) unset($address['zipcode'], $address['city']);
		return implode(', ', $address);
	}
	
	static function valid_email($email) {
		$email = trim($email);
		$apos = strrpos($email, '@');
		return ($apos > 0 && $apos < strlen($email) - 1) ? $email : false;
	}
	
	static function xmltoarray($xml) {
		$xml = simplexml_load_string($xml);
		$xml = json::encode($xml, JSON_UNESCAPED_UNICODE);
		$array = json::decode($xml);
		return $array;
	}
	
	static function is_moscow($city) {
		$city = mb_strtolower($city, 'utf8');
		$city = mb_substr($city, 0, 6, 'utf8');
		return ($city == 'москва' || $city == 'moscow' || $city == 'москов' || $city == 'moskva');
	}
	
	static function is_central($city, $region) {
		$city = mb_strtolower($city, 'utf8');
		if($city == 'москва' || $city == 'moscow' || $city == 'moskva') return true;
		if($city == 'санкт-петербург' || $city == 'санкт петербург') return true;
		$region = mb_strtolower($region, 'utf8');
		$region = mb_substr($region, 0, 10, 'utf8');
		if($region == 'московская') return true;
		return false;
	}
	
	static function file_to_utf8($file) {
		$content = file_get_contents($file);
		if(!mb_check_encoding($content, 'UTF-8')) {
			$content = mb_convert_encoding($content, 'UTF-8', 'CP1251');
			file_put_contents($file, '\xEF\xBB\xBF'.$content);
		}
	}

}

?>