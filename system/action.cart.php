<?php

make_title('Корзина');

shop::makeBreadcrumbs();

function makeProduct($product, $option = NULL) {
	if($option) {
		$remove_href = '/remove/'.$product['id'].'/'.$option['id'];
		$product['barcode'] = $option['barcode'];
		$product['price'] = $option['price'];
		$product['price_old'] = $option['price_old'];
		$product_count = cart::$options[$option['id']];
		tpl::set('number-input-name', 'option['.$option['id'].']');
		tpl::set('option-name', $option['name']);
		tpl::make('option-label');
	}
	else {
		$remove_href = '/remove/'.$product['id'];
		$product_count = cart::$products[$product['id']];
		tpl::set('number-input-name', 'product['.$product['id'].']');
		tpl::clear('option-label');
	}
	if($product['quantity'] < 5 && $product['stock'] > 2) $product['quantity'] = 5;
	tpl::push($product);
	$image = product::getImage($product);
	tpl::set('product-img', $image);
	tpl::set('product-href', '/product/'.$product['url_name']);
	tpl::set('product-count', $product_count);
	tpl::set('remove-href', $remove_href);
	if($product['price_old']) {
		tpl::make('price-old');
	}
	tpl::set('product-sum', $product_count * $product['price']);
	tpl::make('cart-item');
	tpl::clear('price-old', 'option-label');
}

function make_cart_offer($products, $options) {
	/* $cat_ids = shop::concat_categories(2);
	$sql = 'select '.CATALOG_SELECT.' from product p where status > 0 and stock > 1 and id in (select product_id from product_category where not exists(select 1 from product_option where product_id = p.id) and category_id in ('.$cat_ids.'))';
	if(cart::$products) {
		$sql .= ' and id not in('.implode(',', array_keys(cart::$products)).')';
	}
	$sql .= ' order by utime desc limit 10';
	db::query($sql);
	while($offer = db::fetchArray()) {
		tpl::push($offer);
		$image = product::getImage($offer);
		tpl::set('product-id', $offer['id']);
		tpl::set('product-img', $image);
		tpl::set('product-href', '/product/'.$offer['url_name']);
		if($offer['price_old']) {
			tpl::make('price-old');
		}
		tpl::make('cart-offer-item');
	}
	tpl::make('cart-offer', 'main'); */
	$rr_pids = [];
	foreach($products as $product) {
		$rr_pids[] = $product['id'];
	}
	if($options) {
		foreach($options as $id => $opts) {
			foreach($opts as $option) {
				$rr_pids[] = $option['product_id'].'00'.$option['id'];
			}
		}
	}
	tpl::set('rr-products-id', implode(',', $rr_pids));
	tpl::make('cart-rr-widget', 'main');
}

$products = cart::getProducts();
foreach($products as $product) {
	makeProduct($product);
}

if($options = cart::getOptions('id')) {
	$option_products = shop::getOptionProducts(array_keys($options));
	foreach($options as $id => $opts) {
		foreach($opts as $option) {
			makeProduct($option_products[$option['product_id']], $option);
		}
	}
}

// Предлагаем случайные товары, если корзина пуста
if(empty(cart::$products) && empty(cart::$options)) {
	db::query('select '.CATALOG_SELECT.' from product p where status = 1 and quantity > 0 order by rand() limit 10');
	foreach(db::fetchAll() as $product) {
		shop::makeProduct($product, 'product');
	}
	tpl::make('cart-empty', 'main');
	make_cart_offer($products, $options);
	return;
}

tpl::set('total-sum', cart::$sum);

$bonus_discount = 0;
$cum_discount = 0;
$cumulative = 0;
$bonus = 0;

if(account::$auth) {
	$bonus = account::$info['bonus'];
	$cumulative = account::getCumulative();
	$cum_discount = round((cart::$sum / 100) * $cumulative);
}

foreach(cart::$gifts as $gift) {
	if(!$gift) continue;
	tpl::push($gift);
	tpl::make('cart-gift');
}

tpl::set('bonus', $bonus);

$max_bonus = cart::getMaxBonus();
if($bonus < $max_bonus) $max_bonus = $bonus;
tpl::set('max-bonus', $max_bonus);

if(REQUEST_METHOD == 'POST') {
	// пересчет кол-ва товара
	if(is_array($product = POST('product'))) {
		$products = array_group($products, 'id');
		foreach($product as $productId => $num) {
			if(!cart::hasProduct($productId)) continue;
			$quantity = $products[$productId][0]['quantity'];
			$max_quantity = $quantity > 0 ? $quantity : 5;
			if($num < 1) $num = 1;
			if($num > $max_quantity) $num = $max_quantity;
			cart::$products[$productId] = $num;
		}
	}
	if(is_array($option = POST('option'))) {
		foreach($option as $optionId => $num) {
			if(!cart::hasOption($optionId)) continue;
			$quantity = $options[$optionId][0]['quantity'];
			$max_quantity = $quantity > 0 ? $quantity : 5;
			if($num < 1) $num = 1;
			if($num > $max_quantity) $num = $max_quantity;
			cart::$options[$optionId] = $num;
		}
	}
	cart::save();
	// yamibonus
	if($bonus < $max_bonus) $max_bonus = $bonus;
	if($bonus_spend = absint(POST('bonus'))) {
		if($bonus_spend > $max_bonus) {
			$bonus_spend = $max_bonus;
		}
		$_SESSION['bonus-spend'] = $bonus_spend;
	}
	else {
		unset($_SESSION['bonus-spend']);
	}
	// promocode
	if($code = trim(POST('code'))) {
		db::query('select * from coupon where status = 1 and quantity > 0 and threshold < ? and code like ?', cart::$sum, $code);
		if(!$coupon = db::fetchArray()) {
			unset($_SESSION['coupon'], $_SESSION['coupon-discount']);
			die('Купон недоступен');
		}
		$coupon_discount = $coupon['amount'];
		if($coupon['pc']) $coupon_discount = round(cart::$sum / 100 * $coupon['amount']);
		$_SESSION['coupon'] = strtoupper($code);
		$_SESSION['coupon-discount'] = $coupon_discount;
	}
	else {
		unset($_SESSION['coupon'], $_SESSION['coupon-discount']);
	}
	if(AJAX) die('ok');
	return redirect('/cart');
}

$cart_sum = cart::$sum - SESSION('bonus-spend');
$bonus_discount += SESSION('bonus-spend');
$coupon_code = SESSION('coupon');
$coupon_discount = intval(SESSION('coupon-discount'));

tpl::set('cart-sum', cart::$sum);
tpl::set('cumulative', $cumulative);
tpl::set('cumulative-discount', $cum_discount);
tpl::set('bonus-discount', $bonus_discount);
tpl::set('total-sum', cart::$sum - $bonus_discount - $cum_discount - $coupon_discount);

tpl::set('coupon', $coupon_code);
tpl::set('coupon-discount', $coupon_discount);

tpl::set('bonus-spend', SESSION('bonus-spend'));
if(account::$auth) {
	tpl::make('bonus-input');
	tpl::push(account::$info);
	if(account::$info['address']) {
		tpl::push(json::decode(account::$info['address']));
	}
}

tpl::make('oneclick-form');
tpl::make('cart-list', 'main');

// accessories offer
make_cart_offer($products, $options);

?>