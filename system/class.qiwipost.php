<?php

class qiwipost {
	
	static $api_url = 'http://api.qiwipost.ru/';
	
	static function get_points() {
		$url = self::$api_url . '?do=listmachines_csv';
		$response = web::http_request($url, 'GET');
		file_put_contents(FILES.'/points/qiwipost.csv', $response);
		return $response;
	}
	
	static function import_points($update_file = true) {
		if($update_file) self::get_points();
		$file = FILES.'/points/qiwipost.csv';
		$filesize = filesize($file);
		if($filesize < 5*1024) {
			shop::sendEmail('wwweb.sinet@gmail.com', 'Ошибка импорта точек QiwiPost', 'Размер файла: '.$filesize);
			return ['import_points' => 'ERROR! FILE TOO SMALL!'];
		}
		db::query('truncate table points_qp');
		$email_notice = true;
		$num = 0;
		$sql = 'replace into points_qp values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
		db::prepare($sql);
		$f = fopen($file, 'r');
		while($point = fgetcsv($f, 9999, ';')) {
			if(count($point) != 11) continue;
			// if(count($point) != 12) continue;
			list($citycode) = explode('-', $point[0]);
			$point[] = $citycode;
			$point[] = 300;
			$point[] = NULL;
			db::set($point);
			db::execute();
			if(!db::count()) {
				if(x::config('DEBUG')) {
					var_dump($point);
					abort(db::error());
				}
				elseif($email_notice) {
					shop::sendEmail('wwweb.sinet@gmail.com', 'Ошибка импорта точек QiwiPost', db::error());
					$email_notice = false;
				}
			}
			else {
				$num++;
			}
		}
		db::query('update points_qp qp set delivery_price = ifnull((select distinct qp_pvz from delivery where qp_id = qp.citycode), 300)');
		// update days
		db::query('update points_qp qp set days = (select max(qp_dt) from delivery where qp_id = qp.citycode)');
		return ['import_points' => $num];
	}
	
}

?>