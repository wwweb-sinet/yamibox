<?php

class Shop {
	
	const DIAMONDS_LIMIT = 20;
	const JEWELRY_LIMIT = 24;
	const MODEL_ITEMS_LIMIT = 10;
	const ERROR = 'Внутренняя ошибка сервера. Страница недоступна.';
	
	static function paginator($num_records, $pagelimit, $all_link = false, $paginator_class = 'paginator') {
		$num_pages = intval(ceil($num_records / $pagelimit));
		if($num_pages < 2) return;
		function make_link($i, $text = NULL) {
			if(!$text) $text = $i;
			tpl::set('pnum', $text);
			tpl::set('phref', ($i > 1 || $i == 'all') ? qs::set('p',$i) : qs::remove('p'));
			if(CURRENT_PAGE == $i) {
				tpl::set('class', 'current');
				tpl::make('pspan', 'plink');
			}
			else tpl::make('plink');
		}
		
		function make_delimiter() {
			tpl::set('pnum', '...');
			tpl::set('class', 'disabled');
			tpl::make('pspan', 'plink');
		}
		
		tpl::set('current_page', (CURRENT_PAGE > $num_pages) ? $num_pages : CURRENT_PAGE);
		if($num_pages <= 13) {
			for($i = 1; $i <= $num_pages; $i++) make_link($i);
		}
		else {
			$middle_start = CURRENT_PAGE - 3;
			$middle_end = CURRENT_PAGE + 3;
			if($middle_start < 3) $middle_start = 3;
			if($middle_end > $num_pages - 3) $middle_end = $num_pages;
			if(CURRENT_PAGE < 6)
				$middle_end += (6 - CURRENT_PAGE);
			if(CURRENT_PAGE + 6 > $num_pages)
				$middle_start -= (CURRENT_PAGE + 6 - $num_pages);
			for($i = 1; $i <= 2; $i++) make_link($i);
			if($middle_start > 3) make_delimiter();
			for($i = $middle_start; $i <= $middle_end; $i++) make_link($i);
			if($num_pages - $middle_end > 1) make_delimiter();
			if($num_pages - CURRENT_PAGE >= 6) {
				for($i = $num_pages - 1; $i <= $num_pages; $i++) make_link($i);
			}
		}
		if($all_link) {
			make_link('all', 'Показать все');
			if(GET('p') == 'all') tpl::append('paginator', '<p class="center pink bold">Показаны все товары категории</p>');
		}
		tpl::set('paginator-class', $paginator_class);
		tpl::make('paginator');
		qs::reset();
	}
	
	static function getCategories() {
		if(!$categories = cache::get('categories')) {
			$categories = [];
			db::query('select *, (select group_concat(id) from categories where parent_id = c.id) as children, (select url from categories where id = c.parent_id) as parent_url, (select count(*) from product_category pc where (select `status` = 1 and stock > 1 from product where id = pc.product_id) and category_id = c.id) as product_count from categories c where status = 1 order by sort, id');
			while($cat = db::fetchArray()) {
				$categories[$cat['id']] = $cat;
			}
			// print_r($categories);
			foreach($categories as $cat) {
				if($cat['children'] && $cat['parent_id']) {
					$num = db::querySingle('select count(*) from product_category pc where (select `status` = 1 and stock > 1 from product where id = pc.product_id) and category_id in('.$cat['children'].')');
					$categories[$cat['parent_id']]['product_count'] += $num;
				}
			}
			foreach($categories as $cat) {
				if($cat['parent_id']) $categories[$cat['parent_id']]['product_count'] += $cat['product_count'];
			}
			cache::set('categories', $categories, 300);
		}
		return $categories;
	}
	
	// список id всех подкатегорий выбранной категории через запятую (для sql-запроса)
	static function concat_categories($cid) {
		$categories = shop::getCategories();
		if(!function_exists('_concat')) {
			function _concat(&$concat_str, &$category, &$categories) {
				if($category['children']) {
					$concat_str .= ','.$category['children'];
					$child_ids = explode(',', $category['children']);
					foreach($child_ids as $child_id) {
						if(isset($categories[$child_id])) {
							_concat($concat_str, $categories[$child_id], $categories);
						}
					}
				}
			}
		}
		$cat_ids = $cid;
		_concat($cat_ids, $categories[$cid], $categories);
		return $cat_ids;
	}
	
	static function makeCatalogMenu() {
		
		$categories = static::getCategories();
		$cat_by_parent = array_group($categories, 'parent_id');
		
		if(x::data('PATH') == '/page/yamibox') tpl::set('yamibox-page-is-current', ' current');
		if(x::data('PATH') == '/catalog' && x::data('discounts-selected')) tpl::set('menu-discounts-is-current', ' current');
		
		foreach($categories as $cat) {
			if($cat['parent_id'] == 0) {
				tpl::set('a-href', '/catalog'.$cat['url']);
				tpl::set('a-content', $cat['name']);
				tpl::set('a-class', $cat['id'] == x::data('menu-category-level1') ? 'a-level1 current' : 'a-level1');
				tpl::make('a', 'catalog-menu-1-content');
				if(isset($cat_by_parent[$cat['id']])) {
					foreach($cat_by_parent[$cat['id']] as $level2) {
						tpl::set('a-href', '/catalog'.$level2['url']);
						tpl::set('a-content', $level2['name']);
						tpl::set('a-class', $level2['id'] == x::data('menu-category') ? 'a-level2 current' : 'a-level2');
						tpl::make('a', 'catalog-menu-2-content');
						tpl::append('catalog-menu-2-content', '<br>');
						if(isset($cat_by_parent[$level2['id']])) {
							foreach($cat_by_parent[$level2['id']] as $level3) {
								tpl::set('a-href', '/catalog'.$level3['url']);
								tpl::set('a-content', $level3['name']);
								tpl::set('a-class', $level3['id'] == x::data('menu-category') ? 'a-level3 current' : 'a-level3');
								tpl::make('a', 'catalog-menu-3-content');
								tpl::append('catalog-menu-3-content', '<br>');
							}
							tpl::make('catalog-menu-3', 'catalog-menu-2-content');
							tpl::clear('catalog-menu-3-content');
						}
					}
					tpl::make('catalog-menu-2');
				}
				tpl::make('catalog-menu-1');
				tpl::clear('catalog-menu-2', 'catalog-menu-1-content', 'catalog-menu-2-content');
			}
		}
		tpl::make('catalog-menu');
		
	}
	
	static function newCatalogMenu() {
		$categories = static::getCategories();
		$cat_by_parent = array_group($categories, 'parent_id');
		$products = product::get_margintop(); // promo products for dropdown menu
		foreach($categories as $cat) {
			tpl::set('dropdown-class', '');
			if($cat['parent_id'] == 0) {
				tpl::set('a-href', '/catalog'.$cat['url']);
				tpl::set('a-content', $cat['name']);
				tpl::set('a-class', $cat['id'] == x::data('menu-category-level1') ? 'a-level1 current' : 'a-level1');
				tpl::make('a', 'menubar-level1-content');
				if(isset($cat_by_parent[$cat['id']])) {
					foreach($cat_by_parent[$cat['id']] as $level2) {
						tpl::set('a-href', '/catalog'.$level2['url']);
						tpl::set('a-content', $level2['name']);
						tpl::set('a-class', $level2['id'] == x::data('menu-category') ? 'a-level2 current' : 'a-level2');
						tpl::make('a', 'menubar-level2-content');
						if(isset($cat_by_parent[$level2['id']])) {
							tpl::append('menubar-level3-content', '<div class="level3-title">'.$level2['name'].'</div>');
							foreach($cat_by_parent[$level2['id']] as $level3) {
								tpl::set('a-href', '/catalog'.$level3['url']);
								tpl::set('a-content', $level3['name']);
								tpl::set('a-class', $level3['id'] == x::data('menu-category') ? 'a-level3 current' : 'a-level3');
								tpl::make('a', 'menubar-level3-content');
								tpl::append('menubar-level3-content', '<br>');
							}
							tpl::set('dropdown-class', ' wide');
						}
						if(@$product = $products[$level2['id']]) {
							shop::makeMargintop($product, 'menubar-level3-content');
						}
						tpl::make('menubar-level3', 'menubar-level2-content');
						tpl::clear('menubar-level3-content');
					}
					tpl::make('menubar-level2');
				}
				tpl::make('menubar-level1');
				tpl::clear('menubar-level2', 'menubar-level1-content', 'menubar-level2-content');
			}
		}
		tpl::make('menubar');
	}
	
	static function makeLeftMenu($category = NULL, $appendTo = NULL) {
		$categories = self::getCategories();
		$_categories = array_filter($categories, function($cat) use($category) {
			if($category) {
				return $cat['parent_id'] == $category['id'];
			}
			else {
				return $cat['parent_id'] == 0;
			}
		});
		if(empty($_categories)) {
			$_categories = array_filter($categories, function($cat) use($category) {
				return $category['parent_id'] == $cat['parent_id'];
			});
		}
		qs::remove('p');
		foreach($_categories as $cat) {
			qs::$params['path'] = '/catalog'.$cat['url'];
			tpl::set('href', qs::build());
			tpl::set('name', $cat['name'].' ('.$cat['product_count'].')');
			tpl::set('class', $category && $category['id'] == $cat['id'] ? ' active' : '');
			tpl::make('catalog-category');
		}
		qs::reset();
		tpl::make('catalog-categories', $appendTo);
	}
	
	static function makeProduct($product, $appendTo = NULL) {
		if($product['min_price']) $product['price'] = $product['min_price'];
		if($product['min_price_old']) $product['price_old'] = $product['min_price_old'];
		tpl::push($product);
		$image = product::getImage($product);
		tpl::set('product-img', $image);
		tpl::set('product-href', '/product/'.$product['url_name']);
		if(isset($product['rating_avg']) && $product['rating_avg'] > 0) {
			tpl::set('rating-value', round($product['rating_avg']));
			$review_count_text = $product['review_count'] . ' ' . str::numberof($product['review_count'], 'отзыв', ['','а','ов']);
			$rating_text = sprintf("%s, средняя оценка %1.1f", $review_count_text, $product['rating_avg']);
			tpl::set('rating-title', $product['rating_avg'] > 0 ? $rating_text : 'Оценок нет');
			tpl::make('rating', 'product-rating');
		}
		if($product['price_old'] && $product['price_old'] > $product['price']) {
			tpl::make('price-old');
		}
		if(isset($product['option_count']) && $product['option_count'] > 1) {
			if($product['status'] && $product['stock'] > 1) {
				tpl::set('add2cart-href', tpl::get('product-href'));
				tpl::clear('product-class');
				tpl::make('product-add2cart');
			}
			//tpl::make('product-option-available');
		}
		elseif($product['status'] && $product['stock'] > 1) {
			tpl::set('add2cart-href', '/add2cart/'.$product['id']);
			tpl::set('product-class', ' single');
			tpl::set('add2cart-attr', ' data-pid="'.$product['id'].'"');
			tpl::make('product-add2cart');
		}
		if(!$product['status'] || $product['stock'] < 2) {
			tpl::make('product-outofstock-text');
		}
		if($product['outofp'] && $product['stock'] < 2) tpl::make('product-outofp-sticker');
		$spf = $product['max_spf'] ?: $product['spf'];
		if($spf) {
			tpl::set('spf', str_replace(' ', '', trim($spf)));
			tpl::make('product-spf');
		}
		tpl::make('product', $appendTo);
		tpl::clear('product-rating', 'price-old', 'product-option-available', 'product-add2cart', 'product-outofstock-text', 'product-outofp-sticker', 'product-spf');
	}
	
	static function makeMargintop($product, $appendTo = NULL) {
		if($product['min_price']) $product['price'] = $product['min_price'];
		if($product['min_price_old']) $product['price_old'] = $product['min_price_old'];
		tpl::push($product);
		$image = product::getImage($product);
		tpl::set('product-img', $image);
		tpl::set('product-href', '/product/'.$product['url_name']);
		if($product['price_old'] && $product['price_old'] > $product['price']) {
			tpl::make('price-old');
		}
		if(isset($product['option_count']) && $product['option_count'] > 1) {
			if($product['status'] && $product['stock'] > 1) {
				tpl::set('add2cart-href', tpl::get('product-href'));
				tpl::clear('product-class');
				tpl::make('product-add2cart');
			}
		}
		elseif($product['status'] && $product['stock'] > 1) {
			tpl::set('add2cart-href', '/add2cart/'.$product['id']);
			tpl::set('product-class', ' single');
			tpl::set('add2cart-attr', ' data-pid="'.$product['id'].'"');
			tpl::make('product-add2cart');
		}
		tpl::make('margintop', $appendTo);
		tpl::clear('price-old', 'product-add2cart');
	}
	
	static function layout() {
		tpl::load('shop-layout');
		if(!tpl::get('document_meta_description')) {
			tpl::make('document_meta_description');
		}
		if(!tpl::get('document_meta_keywords')) {
			tpl::make('document_meta_keywords');
		}
		if(!tpl::get('document-title')) {
			tpl::make('document-title');
		}
		if(account::$auth) {
			tpl::set('account-email', account::$info['email']);
			if(account::$admin || account::$employee || account::$pnl) tpl::make('admin-link');
			tpl::make('account-auth-links');
			$phone = str::filter_phone_number(account::$info['phone']);
			$phone = $phone ? '+'.$phone : 'null';
			$rees46_user = [
				'id' => account::$info['id'],
				'email' => account::$info['email'],
				'phone' => $phone,
				'location' => null
			];
			tpl::set('rees46-user_info', json::encode($rees46_user, JSON_UNESCAPED_UNICODE));
		}
		else {
			tpl::make('account-noauth-links');
			tpl::set('rees46-user_info', 'null');
		}
		if(CURRENT_PAGE == 'all') {
			tpl::append('document-title', ' - Все товары');
		}
		elseif(CURRENT_PAGE > 1) {
			tpl::append('document-title', ' - Страница '.CURRENT_PAGE);
		}
		if(x::config('ENABLE_COUNTERS')) {
			tpl::load('counters');
			tpl::make('google-tag-manager');
			tpl::make('yandex-metrika');
			if(qs::$params['path'] !== '/account/feedback') tpl::make('freshdesk');
			tpl::make('yandex-goals');
			tpl::make('facebook-conversion-script');
			tpl::make('retail-rocket-main');
			tpl::make('retail-rocket-add2cart');
			tpl::make('rambler-top100');
			tpl::make('rees46-main');
		}
		tpl::set('cart-count', cart::count());
		tpl::set('cart-count-word', str::numberof(cart::count(), 'товар', ['','а','ов']));
		tpl::set('cart-sum', cart::$sum);
		tpl::set('cart-sum-with-discounts', cart::$sum_with_discounts);
		$css_mtime = filemtime(ROOT.'/ui/css/main.css');
		tpl::set('css-mtime', $css_mtime);
		shop::newCatalogMenu();
		if(tpl::get('breadcrumbs')) tpl::make('breadcrumbs-block');
		tpl::set('cart-sum-with-discounts', cart::$sum_with_discounts);
		$yml_count = self::count_yml_offers()[1];
		tpl::set('yml-count', $yml_count);
		$count_word = str::numberof($yml_count, 'товар', ['','а','ов']);
		tpl::set('yml-count-word', $count_word);
		tpl::set('instock-text', sprintf('В наличии %d %s', $yml_count, $count_word));
		$reviews = db::querySingle('select count(*) from product_review where status = 1');
		$reviews_word = str::numberof($reviews, 'отзыв', ['','а','ов']);
		tpl::set('reviews-text', sprintf("%d %s", $reviews, $reviews_word));
		echo tpl::make('shop-layout');
	}
	
	static function getOptionProducts($idList) {
		$products = array();
		if(is_array($idList)) {
			$idList = implode(',', $idList);
			db::query('select '.CATALOG_SELECT.' from product p where id in (select product_id from product_option where id in ('.$idList.'))');
			foreach(db::fetchAll() as $product) {
				$products[$product['id']] = $product;
			}
		}
		return $products;
	}
	
	static function getCoupons() {
		if(!$coupons = cache::get('coupons')) {
			db::query('select * from coupons order by id');
			$coupons = db::fetchAll();
			cache::set('coupons', $coupons);
		}
		return $coupons;
	}
	
	static function makeBreadcrumbs($add = []) {
		tpl::clear('breadcrumbs');
		$links = json::get('breadcrumbs');
		$bc = ['/'=>'Главная'];
		foreach($links as $href=>$title) {
			if(str::startsWith(qs::$params['path'], $href)) {
				$bc[$href] = $title;
			}
		}
		$bc += $add;
		uksort($bc, function($a, $b) {
			return strlen($a) > strlen($b) ? 1 : 0;
		});
		$last = end($bc);
		foreach($bc as $href=>$title) {
			if($title == $last) {
				tpl::append('breadcrumbs', $title);
			}
			else {
				tpl::set('a-href', $href);
				tpl::set('a-content', $title);
				tpl::make('a', 'breadcrumbs');
				tpl::append('breadcrumbs', '&nbsp;&nbsp;›&nbsp;&nbsp;');
			}
		}
	}
	
	static function makeOrderProductList($order, $for_admin = false) {
		$items = json::decode($order['cart_items']);
		foreach($items as $item) {
			if($item['option_id'] > 0) {
				tpl::set('option-name', $item['option_name']);
				tpl::make('order-form-cart-item-option-name');
			}
			tpl::push($item);
			tpl::set('product-img', product::getImage($item));
			tpl::set('product-href', '/product/'.$item['url_name']);
			tpl::set('product-count', $item['quantity']);
			tpl::set('product-sum', $item['quantity'] * $item['price']);
			if($for_admin) {
				if($item['stock'] > 2) tpl::make('order-form-cart-item-stock-sign');
			}
			tpl::make('order-form-cart-item');
			tpl::clear('order-form-cart-item-option-name', 'order-form-cart-item-stock-sign');
		}
		tpl::set('id', $order['id']);
		$result = tpl::get('order-form-cart-item');
		tpl::clear('order-form-cart-item');
		return $result;
	}
	
	static function sendOrderEmail($order) {
		tpl::load('email');
		tpl::push($order);
		tpl::walk('firstname', 'ucfirst');
		$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
		tpl::set('order-sum', $order_sum);
		$dtype = json::get('dtype');
		$ptype = json::get('ptype');
		if(!$order['delivery_info']) {
			tpl::set('delivery_info', @$dtype[$order['dtype']]['text'] ?: 'Не выбрано');
		}
		tpl::set('ptype', @$ptype[$order['ptype']]['text'] ?: 'Не выбрано');
		if($order['ptype'] > 3) {
			$login = x::config('ROBOX_LOGIN');
			$password = x::config('ROBOX_PASSWORD_1');
			$inv_desc = "Оплата заказа №".$order['id']." в интернет-магазине Yamibox.ru";
			$out_sum = number_format($order_sum, 2, '.', '');
			$crc = md5(sprintf('%s:%s:%s:%s', $login, $out_sum, $order['id'], $password));
			tpl::set('robox-login', $login);
			tpl::set('robox-description', $inv_desc);
			tpl::set('robox-sum', $out_sum);
			tpl::set('robox-crc', $crc);
			tpl::set('invId', $order['id']);
			tpl::make('robox-payment-link');
		}
		if($order['comment']) {
			tpl::walk('comment', 'html2chars');
			tpl::walk('comment', 'nl2br');
			tpl::make('email-order-comment');
		}
		tpl::clear('order-form-cart-item');
		tpl::set('order-form-cart-item', self::makeOrderProductList($order));
		$msg_client = tpl::make('email-order');
		tpl::clear('order-form-cart-item');
		tpl::set('order-form-cart-item', self::makeOrderProductList($order, true));
		$msg_admin = tpl::make('email-order-admin');
		shop::sendEmail($order['email'], 'Заказ № '.$order['id'].' принят в интернет-магазине Yamibox.Ru', $msg_client);
		$email = x::config('SHOP_EMAIL');
		shop::sendEmail($email, 'Заказ №'.$order['id'], $msg_admin);
		$email = 'zakazskilledcall@gmail.com';
		shop::sendEmail($email, 'Заказ №'.$order['id'], $msg_admin);
	}
	
	static function sendDeliveredEmail($order) {
		tpl::load('email');
		tpl::load('order-form');
		tpl::push($order);
		tpl::set('cart-sum', $order['total_sum']);
		$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
		tpl::set('order-sum', $order_sum);
		$dtype = json::get('dtype');
		$ptype = json::get('ptype');
		if(!$order['delivery_info']) {
			tpl::set('delivery_info', @$dtype[$order['dtype']]['text'] ?: 'Не выбрано');
		}
		tpl::set('ptype', @$ptype[$order['ptype']]['text'] ?: 'Не выбрано');
		if($order['comment']) {
			tpl::walk('comment', 'html2chars');
			tpl::walk('comment', 'nl2br');
			tpl::make('email-order-comment');
		}
		tpl::set('address', str::address2str($order['address']));
		tpl::set('order-form-cart-item', self::makeOrderProductList($order));
		$msg_client = tpl::make('email-order-delivered');
		shop::sendEmail($order['email'], mb_convert_case($order['firstname'], MB_CASE_TITLE).', спасибо за заказ! Оцените уровень сервиса Yamibox.Ru', $msg_client);
		tpl::clear('email-order-comment', 'email-order-delivered', 'order-form-cart-item');
	}
	
	static function sendIntransitEmail($order) {
		tpl::load('email');
		tpl::load('order-form');
		tpl::push($order);
		tpl::set('cart-sum', $order['total_sum']);
		$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
		tpl::set('order-sum', $order_sum);
		$dtype = json::get('dtype');
		$ptype = json::get('ptype');
		if(!$order['delivery_info']) {
			tpl::set('delivery_info', @$dtype[$order['dtype']]['text'] ?: 'Не выбрано');
		}
		tpl::set('ptype', @$ptype[$order['ptype']]['text'] ?: 'Не выбрано');
		if($order['comment']) {
			tpl::walk('comment', 'html2chars');
			tpl::walk('comment', 'nl2br');
			tpl::make('email-order-comment');
		}
		if($order['dtype'] == 3 && $order['tracking_id']) { // Почта России
			tpl::make('email-order-transit-pr');
		}
		tpl::set('address', str::address2str($order['address']));
		tpl::set('order-form-cart-item', self::makeOrderProductList($order));
		$msg_client = tpl::make('email-order-transit');
		shop::sendEmail($order['email'], 'Заказ на Yamibox.Ru! '.$order['firstname'].', Ваш заказ передан в службу доставки для отправки', $msg_client);
		tpl::clear('email-order-comment', 'email-order-transit', 'email-order-transit-pr', 'order-form-cart-item');
	}
	
	static function sendPickpointEmail($order) {
		tpl::load('email');
		tpl::load('order-form');
		tpl::push($order);
		tpl::set('cart-sum', $order['total_sum']);
		$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
		tpl::set('order-sum', $order_sum);
		$dtype = json::get('dtype');
		$ptype = json::get('ptype');
		if(!$order['delivery_info']) {
			tpl::set('delivery_info', @$dtype[$order['dtype']]['text'] ?: 'Не выбрано');
		}
		tpl::set('ptype', @$ptype[$order['ptype']]['text'] ?: 'Не выбрано');
		if($order['comment']) {
			tpl::walk('comment', 'html2chars');
			tpl::walk('comment', 'nl2br');
			tpl::make('email-order-comment');
		}
		tpl::set('address', str::address2str($order['address']));
		tpl::set('order-form-cart-item', self::makeOrderProductList($order));
		$msg_client = tpl::make('email-order-pickpoint');
		shop::sendEmail($order['email'], 'Заказ от Yamibox.Ru прибыл в Пункт выдачи', $msg_client);
		tpl::clear('email-order-comment', 'email-order-pickpoint', 'order-form-cart-item');
	}
	
	static function sendOrderReviewEmail($order) {
		tpl::load('email');
		tpl::push($order);
		$products = json::decode($order['cart_items']);
		foreach($products as $product) {
			tpl::set('product-img', product::getImage($product));
			tpl::set('url_name', $product['url_name']);
			tpl::set('name', $product['option_name'] ? $product['name'].' - '.$product['option_name'] : $product['name']);
			tpl::make('email-order-review-product');
		}
		$msg_client = tpl::make('email-order-review');
		tpl::clear('email-order-review', 'email-order-review-product');
		return shop::sendEmail($order['email'], 'Оставьте отзывы на товары из Вашего заказа на Yamibox.Ru', $msg_client);
	}
	
	static function sendOneclickOrderEmail($order) {
		tpl::load('email');
		tpl::push($order);
		$order_sum = $order['total_sum'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
		tpl::set('order-sum', $order_sum);
		tpl::walk('comment', 'html2chars');
		tpl::walk('comment', 'nl2br');
		if($email = filter_var($order['email'], FILTER_VALIDATE_EMAIL)) {
			tpl::clear('order-form-cart-item');
			tpl::set('order-form-cart-item', self::makeOrderProductList($order));
			$msg_client = tpl::make('email-oneclick-order');
			shop::sendEmail($email, 'Заказ в один клик. Интернет-магазин Yamibox.ru', $msg_client);
		}
		tpl::clear('order-form-cart-item');
		tpl::set('order-form-cart-item', self::makeOrderProductList($order, true));
		$msg_admin = tpl::make('email-oneclick-order-admin');
		shop::sendEmail(x::config('SHOP_EMAIL'), 'Заказ в один клик №'.$order['id'], $msg_admin);
		tpl::clear('order-form-cart-item');
		$email = 'zakazskilledcall@gmail.com';
		shop::sendEmail($email, 'Заказ в один клик №'.$order['id'], $msg_admin);
	}
	
	static function sendPaymentEmail($orderId, $sum) {
		tpl::set('order-id', $orderId);
		$sum = floatval($sum);
		tpl::set('order-sum', number_format($sum, 2));
		tpl::load('email');
		$msg_admin = tpl::make('email-order-payment');
		shop::sendEmail(x::config('SHOP_EMAIL'), 'Оплачен заказ №'.$orderId, $msg_admin);
	}
	
	static function sendReviewEmail($review) {
		tpl::load('email');
		tpl::push($review);
		tpl::set('content', nl2br(html2chars($review['content'])));
		$msg_admin = tpl::make('email-review-notice');
		shop::sendEmail(x::config('SHOP_EMAIL'), 'Новый отзыв', $msg_admin);
	}
	
	static function sendFeedbackEmail($text) {
		tpl::load('email');
		tpl::push(account::$info);
		$msg_admin = tpl::make('email-feedback');
		shop::sendEmail(x::config('FEEDBACK_EMAIL'), 'Запрос из личного кабинета', $msg_admin);
	}
	
	static function sendWishlistNoticeEmail($users) {
		tpl::load('email');
		$msg_client = tpl::make('email-wishlist-notice');
		foreach($users as $user) {
			shop::sendNoticeEmail($user['email'], 'Товары из вашего Wishlist в наличии на сайте Yamibox.ru', $msg_client);
			tpl::clear('email-wishlist-notice');
		}
	}
	
	static function sendCallRequest($order) {
		tpl::load('email');
		$msg_client = tpl::make('email-call-request');
		$subject = 'Менеджеры Yamibox.Ru не смогли до Вас дозвониться';
		return self::sendEmail($order['email'], $subject, $msg_client);
	}
	
	static function sendEmail($email_to, $subject = 'Интернет-магазин Yamibox.ru', $message, $options = []) {
		tpl::set('subject', $subject);
		tpl::set('body', $message);
		if(!isset($options['from'])) $options['from'] = x::config('SHOP_EMAIL');
		if(!isset($options['login'])) $options['login'] = x::config('EMAIL_LOGIN');
		if(!isset($options['password'])) $options['password'] = x::config('EMAIL_PASSWORD');
		if(!isset($options['layout'])) $options['layout'] = 'email-layout';
		$message = tpl::make($options['layout']);
		$mail = new phpmailer();
		if($options['login'] && $options['password']) {
			$mail->SMTPAuth = true;
			$mail->Username = $options['login'];
			$mail->Password = $options['password'];
		}
		$mail->CharSet = 'UTF-8';
		$mail->From = $options['from'];
		$mail->FromName = 'Интернет-магазин Yamibox.ru';
		if(x::config('DEBUG')) {
			$email_to = 'wwweb.sinet@gmail.com';
		}
		if(is_array($email_to)) {
			foreach($email_to as $email) $mail->AddAddress($email);
		}
		else {
			$mail->AddAddress($email_to);
		}
		$mail->Subject = $subject;
		$mail->Body = $message;
		$mail->ContentType = 'text/html';
		if(isset($options['attachment']) && $options['attachment']) {
			$mail->addAttachment($options['attachment']);
		}
		$send = $mail->Send();
		$mail = null;
		return $send;
	}
	
	static function sendNoticeEmail($email_to, $subject = 'Интернет-магазин Yamibox.ru', $message) {
		tpl::set('subject', $subject);
		tpl::set('body', $message);
		$message = tpl::make('email-layout');
		$mail = new phpmailer();
		if(($login = x::config('NOTICE_EMAIL_LOGIN')) && ($password = x::config('NOTICE_EMAIL_PASSWORD'))) {
			$mail->SMTPAuth = true;
			$mail->Username = $login;
			$mail->Password = $password;
		}
		$mail->CharSet = 'UTF-8';
		$mail->From = x::config('SHOP_EMAIL');
		$mail->FromName = 'Интернет-магазин Yamibox.ru';
		$mail->AddAddress($email_to);
		$mail->Subject = $subject;
		$mail->MsgHTML($message);
		$send = $mail->Send();
		$mail = null;
		return $send;
	}
	
	static function paymentResult($path, $result = NULL) {
		if($result) {
			return self::paymentResultPage($result);
		}
		$out_sum = REQUEST('OutSum');
		$inv_id = absint(REQUEST('InvId'));
		$crc = REQUEST('SignatureValue');
		file_put_contents(FILES.'/p.log', 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."<br>\r\n", FILE_APPEND);
		$password = x::config('ROBOX_PASSWORD_2');
		$myHash = md5(sprintf('%s:%s:%s', $out_sum, $inv_id, $password));
		if($inv_id && strtoupper($crc) == strtoupper($myHash)) {
			db::query('update orders set paid = 1 where id = ?', $inv_id);
			if(db::count()) shop::sendPaymentEmail($inv_id, $out_sum);
			exit('OK'.$inv_id);
		}
		else {
			tpl::set('main', $crc);
			tpl::append('main', '<br>'.$myHash);
		}
	}
	
	static function paymentResultPage($result) {
		make_title('Оплата заказа в ROBOKASSA.RU');
		if(account::$auth) {
			tpl::make('robox-payment-lk');
		}
		$out_sum = REQUEST('OutSum');
		$inv_id = REQUEST('InvId');
		$crc = REQUEST('SignatureValue');
		tpl::set('payment-sum', $out_sum);
		tpl::set('order-id', $inv_id);
		if($result == 'success') {
			tpl::make('payment-success');
			tpl::make('robox-payment-success', 'main');
		}
		elseif($result == 'fail') {
			$login = x::config('ROBOX_LOGIN');
			$password = x::config('ROBOX_PASSWORD_1');
			$inv_desc = "Оплата заказа №".$inv_id." в интернет-магазине Yamibox.ru";
			$crc = md5(sprintf('%s:%s:%s:%s', $login, $out_sum, $inv_id, $password));
			tpl::set('robox-login', $login);
			tpl::set('robox-description', $inv_desc);
			tpl::set('robox-sum', $out_sum);
			tpl::set('robox-crc', $crc);
			tpl::set('invId', $inv_id);
			tpl::make('robox-payment');
			tpl::make('robox-payment-fail', 'main');
		}
	}
	
	static function createCoupon($numbers, $letters, $data) {
		$pairs = [];
		$data['code'] = str::generateCoupon($numbers, $letters);
		foreach($data as $column => $value) {
			$pairs[] = '`'.$column.'` = :'.$column;
		}
		db::query('insert ignore into coupon set '.implode(', ', $pairs), $data);
		if(!db::count()) return self::createCoupon($numbers, $letters, $data);
		$id = db::lastInsertId();
		return db::querySingle('select * from coupon where id = '.$id, true);
	}
	
	static function get_sms_text($key) {
		$file = FILES.'/sms/'.$key.'.txt';
		return file_exists($file) ? file_get_contents($file) : '';
	}
	
	static function update_stock_from_import() {
		$c = 0;
		db::query('update product set stock = 1');
		$c += db::count();
		db::query('update product_option set stock = 1');
		$c += db::count();
		// в наличии на складе из тех, что нет в магазине
		db::query('update product p set stock = ifnull((select if((i.quantity > 2 and i.stock = 4) or i.quantity > 4 or (i.quantity > 0 and ((i.price_usd < 18 and i.price_usd * 1.9 >= 35) or i.price_usd * 1.9 >= 1500)), stock, 1) from product_import i where barcode = p.barcode group by barcode), if(p.quantity > 0, 2, 1))');
		$c += db::count();
		db::query('update product_option po set stock = ifnull((select if((i.quantity > 2 and i.stock = 4) or i.quantity > 4 or (i.quantity > 0 and ((i.price_usd < 18 and i.price_usd * 1.9 >= 35) or i.price_usd * 1.9 >= 1500)), stock, 1) from product_import i where barcode = po.barcode group by barcode), if(po.quantity > 0, 2, 1))');
		$c += db::count();
		// отключенные поставщики
		db::query('update product p set stock_status = ifnull((select status from product_stock where id = (select stock from brands where name = p.manufacturer)), stock_status)');
		$c += db::count();
		db::query('update product_option po set stock_status = ifnull((select status from product_stock where id = (select stock from brands where name = (select manufacturer from product where id = po.product_id))), stock_status)');
		$c += db::count();
		db::query('update product set stock = 1 where stock_status = 0');
		$c += db::count();
		db::query('update product_option set stock = 1 where stock_status = 0');
		$c += db::count();
		// в наличии в магазине
		db::query('update product set stock = 2 where quantity > 0');
		$c += db::count();
		db::query('update product_option set stock = 2 where quantity > 0');
		$c += db::count();
		// обновляем сток для товаров с опциями
		db::query('update product p set stock = (select max(stock) from product_option where product_id = p.id) where (select count(*) from product_option where product_id = p.id) > 1');
		$c += db::count();
		return ['shop::update_stock_from_import' => $c];
	}
	
	static function count_yml_offers() {
		$yml_log = file(FILES.'/yml.log');
		$yml_log_last = end($yml_log);
		preg_match('#(\d+)\|(\d+)$#', $yml_log_last, $match);
		return $match;
	}
	
	static function get_brands() {
		if($brands = cache::get('brands')) return $brands;
		$brands = [];
		db::query('select * from brands order by name');
		while($brand = db::fetchArray()) {
			$brands[$brand['id']] = $brand;
		}
		cache::set('brands', $brands);
		return $brands;
	}
	
	static function get_stock_list($cache = false) {
		if((!$stock_list = cache::get('stock_list')) || !$cache) {
			$stock_list = [];
			db::query('select * from product_stock order by id');
			while($stock = db::fetchArray()) {
				$stock_list[$stock['id']] = $stock;
			}
			cache::set('stock_list', $stock_list, 10000);
		}
		return $stock_list;
	}
	
	static function ajax($path, $action) {
		$response = '';
		if($action == 'call_me') {
			$phone = trim(POST('phone'));
			$subject = sprintf("Перезвоните мне! %s", $phone);
			$message = sprintf("Посетитель просит перезвонить по номеру %s", $phone);
			$send = shop::sendEmail(['help@yamibox.ru', 'zakazskilledcall@gmail.com'], $subject, $message, ['layout'=>'email-layout-clear']);
			$response = $send ? 'Спасибо! Ваш запрос принят. Мы вам скоро перезвоним!' : 'Ой! Что-то пошло не так :(';
		}
		elseif($action == 'subscribe') {
			if($email = trim(POST('email'))) {
				$pechkin = new phpechkin();
				$pechkin->lists_add_member(null, $email);
				$response = 'Спасибо! Ваш адрес добавлен в список рассылки.';
			}
			else {
				$response = 'Введите адрес электронной почты';
			}
		}
		die($response);
	}
	
	static function get_yml($path, $filename) {
		$file = FILES.'/'.$filename;
		if(!file_exists($file)) return status('404 Not Found');
		header('Content-Type: text/xml');
		readfile($file);
		exit;
	}
	
}

tpl::load('shop');

?>