<?php

class boxberry {
	
	static $api_url = 'http://api.boxberry.de/json.php';
	static $token = '11803.rnpqddab';
	
	static function get_points() {
		$url = self::$api_url . '?token='.self::$token.'&method=ListPoints';
		$response = web::http_request($url, 'GET');
		file_put_contents(FILES.'/points/boxberry.json', $response);
		return $response;
	}
	
	// static function import_points($json = NULL) {
		// if(!$json) $json = self::get_points();
		// $data = json::decode($json);
		// foreach($data as $point) {
			// print_r($point);
		// }
	// }
	
	static function import_points($json = NULL) {
		if(!$json) $json = self::get_points();
		$data = json::decode($json);
		if($data) {
			db::query('truncate table points_bb');
		}
		$email_notice = true;
		$num = 0;
		$sql = '';
		foreach($data as $point) {
			$params = $p = [];
			$point['delivery_price'] = 300;
			list($latitude, $longitude) = explode(',', $point['GPS']);
			$point['latitude'] = $latitude;
			$point['longitude'] = $longitude;
			unset($point['GPS'], $point['Acquiring'], $point['DigitalSignature'], $point['TypeOfOffice'], $point['NalKD']);
			foreach($point as $column => $value) {
				$column = strtolower($column);
				$p[$column] = $value;
				$params[] = $column.' = :'.$column;
			}
			if(!$sql) {
				$sql = 'replace into points_bb set ';
				$sql .= implode(', ', $params);
				db::prepare($sql);
			}
			db::set($p);
			db::execute();
			if(!db::count()) {
				if(x::config('DEBUG')) {
					var_dump($p);
					var_dump($params);
					var_dump($sql);
					abort(db::error());
				}
				elseif($email_notice) {
					shop::sendEmail('wwweb.sinet@gmail.com', 'Ошибка импорта точек BoxBerry', db::error());
					$email_notice = false;
				}
			}
			else {
				$num++;
			}
		}
		db::query('update points_bb bb set delivery_price = ifnull((select bb_pvz from delivery where bb_id = bb.citycode), 300)');
		// update days
		db::query('update points_bb bb set days = (select max(bb_dt) from delivery where bb_id = bb.citycode)');
		return ['import_points' => $num];
	}
}

?>