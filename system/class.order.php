<?php

class Order {
	
	static function save($data, $order_id = null) {
		$pairs = [];
		if($order_id = absint($order_id)) {
			$data['id'] = $order_id;
		}
		foreach($data as $column => $value) {
			$pairs[] = '`'.$column.'` = :'.$column;
		}
		if($order_id) {
			db::query('update orders set '.implode(', ', $pairs).' where id = :id', $data);
		}
		else {
			db::query('replace into orders set '.implode(', ', $pairs), $data);
		}
		return $order_id ?: db::lastInsertId();
	}
	
	static function update($orderId) {
		if(!AJAX) die('AJAX only');
		if(!$column = trim(POST('column'))) {
			die('no column');
		}
		$value = trim(POST('value'));
		$is_address = in_array($column, ['region','city','zipcode','street','building','building-add','apt']);
		if($value == '' && !$is_address && $column != 'tracking_id') {
			die('no value');
		}
		if($column == 'cstatus' && $value != 2) {
			db::query('delete from collecting_orders where order_id = ?', $orderId);
			$dtype = db::querySingle('select dtype from orders where id = '.$orderId);
			// collected and ready to pickup from office
			if($value == 3 && $dtype == 2) order::status_sms([$orderId], 'collected-yamibox');
		}
		if($column == 'status') {
			if($value == 2) { // СОГЛАСОВАН, записываем время согласования 
				db::query('update orders set stime = CURRENT_TIMESTAMP where id = ?', $orderId);
			}
			elseif($value == 4) { // ДОСТАВЛЕН
				db::query('update orders set dtime = CURRENT_TIMESTAMP where id = ?', $orderId);
				// 1% от стоимости покупки к бонусам (с учетом скидки от примененных бонусов)
				db::query('update user set bonus = bonus + (select ROUND((total_sum - bonus) / 100) from orders where id = ?) where id = (select user_id from orders where id = ?)', $orderId, $orderId);
				// активируем купон, если есть
				db::query('update coupon set status = 1 where order_id = ?', $orderId);
				$order = db::querySingle('select * from orders where id = '.$orderId, true);
				shop::sendDeliveredEmail($order);
			}
			elseif($value == 3) { // ЗАКАЗ В ПУТИ
				$order = db::querySingle('select * from orders where id = '.$orderId, true);
				shop::sendIntransitEmail($order);
			}
			elseif($value == 8) { // ПРИБЫЛ В ПВЗ
				$order = db::querySingle('select * from orders where id = '.$orderId, true);
				shop::sendPickpointEmail($order);
			}
			if($value == 5) { // ОТМЕНЕН
				order::returnItems($orderId); // заказ отменен, возврат товара на склад
				shop::update_stock_from_import();
			}
			else {
				order::takeItems($orderId); // заказ восстановлен, забираем товары со склада
				shop::update_stock_from_import();
			}
			if($value != 4) {
				// Если статус изменен с Доставлен на любой другой, отнимаем обратно бонусы
				// db::query('update user set bonus = bonus - (select ROUND((total_sum - bonus) / 100) from orders where id = ?) where id = (select user_id from orders where id = ? and status = 4)', $orderId, $orderId);
				// db::query('update user set bonus = 0 where bonus < 0');
			}
		}
		if($is_address) {
			$address = db::querySingle('select address from orders where id = '.$orderId);
			if($column == 'city') db::query('update orders set city = ? where id = ?', $value, $orderId);
			if($address = json::decode($address)) {
				$address[$column] = $value;
			}
			else {
				$address = [$column => $value];
			}
			$column = 'address';
			$value = json::encode($address);
		}
		if(!$value && !is_numeric($value)) $value = NULL;
		db::query('update orders set `'.$column.'` = ? where id = ?', $value, $orderId);
		if($column == 'tracking_id') {
			$send_email = REQUEST('send_email');
			if($send_email == 'true') {
				$order = db::querySingle('select * from orders where id = '.$orderId, true);
				shop::sendIntransitEmail($order);
				order::status_sms([$orderId], 'intransit-pr');
			}
		}
		die('ok');
	}
	
	static function update_products($orderId) {
		if(!$quantity = POST('quantity')) die('NO DATA!');
		if(!$order = db::querySingle('select * from orders where id = '.$orderId, true)) die('ЗАКАЗ НЕ НАЙДЕН!');
		$items = json::decode($order['cart_items']);
		foreach($quantity as $pid => $qty) {
			list($productId, $optionId) = explode('-', $pid);
			$productId = intval($productId);
			$optionId = intval($optionId);
			$qty = absint($qty); if($qty < 1) $qty = 1;
			foreach($items as $key => $item) {
				if($item['option_id'] && $optionId == $item['option_id']) {
					$items[$key]['quantity'] = $qty;
				}
				elseif(!$item['option_id'] && $item['id'] == $productId) {
					$items[$key]['quantity'] = $qty;
				}
			}
		}
		$total_sum = 0;
		foreach($items as $item) {
			$total_sum += $item['price'] * $item['quantity'];
		}
		$order['cart_items'] = json::encode(array_values($items));
		$order['total_sum'] = $total_sum;
		// discounts & delivery
		$order['delivery_price'] = absint(POST('delivery_price'));
		$order['cumulative'] = absint(POST('cumulative'));
		$order['bonus'] = absint(POST('bonus'));
		$order['coupon'] = absint(POST('coupon'));
		if(!order::save($order)) die('DB ERROR!');
		die('ok');
	}
	
	static function getList($params = []) {
		$sql = 'select count(*) from orders where true';
		if(preg_match_all('/[\w\@\.]+/u', $params['search'], $match)) {
			$words = array();
			$wcount = count($match[0]);
			foreach($match[0] as $word) {
				$word = db::escape('%'.$word.'%');
				$words[] = ' email LIKE '.$word;
				$words[] = ' firstname LIKE '.$word;
				$words[] = ' lastname LIKE '.$word;
			}
			if(empty($words))
				return alert('Недопустимые параметры поиска.', 'warning');
			$cnd = implode(' or ', $words);
			$sql .= ' and ('.$cnd.')';
		}
		// filter by user_id
		if($params['user_id']) {
			$sql .= ' and user_id = '.$params['user_id'];
		}
		// filter by status
		if(!is_null($params['status'])) {
			$sql .= ' and status = '.$params['status'];
		}
		// cstatus
		if($params['cstatus']) {
			$sql .= ' and cstatus = '.$params['cstatus'];
		}
		if(!is_null($params['paid'])) {
			$sql .= ' and paid = '.$params['paid'];
		}
		// filter by region
		if($params['region']) {
			if($params['region'] == 1) $sql .= ' and city like '. db::escape('%москва%');
			if($params['region'] == 2) $sql .= ' and city not like '. db::escape('%москва%');
		}
		// filter by dtype
		if($params['dtype']) {
			$sql .= ' and dtype = '.$params['dtype'];
		}
		$dservice = json::get('dservice');
		// filter by delivery service
		if($params['dservice']) {
			if(isset($dservice[$params['dservice']])) $sql .= ' and delivery_info like ' . db::escape('%'.$dservice[$params['dservice']]['name'].'%');
		}
		$total_orders = db::querySingle($sql);
		$sql = str_replace('count(*)', '*', $sql) . ' order by '.$params['order_by'];
		if($params['limit']) $sql .= ' limit '.offset($params['limit']).', '.$params['limit'];
		db::query($sql);
		$orders = db::fetchAll();
		return [$total_orders, $orders];
	}
	
	static function show($path, $orderId) {
		$OLD = is_numeric($orderId);
		if($OLD) {
			$orderId = intval($orderId);
			db::query('select * from orders where id = ?', $orderId);
		}
		else {
			db::query('select * from orders where uid = ?', $orderId);
		}
		if(!$order = db::fetchArray()) {
			return alert('Заказ не найден', 'warning', 'main');
		}
		if($OLD && $order['cookie'] !== account::$cookie && !account::$admin) {
			return alert('Это чужой заказ. Доступ ограничен.', 'warning', 'main');
		}
		// redirect to uid for new orders
		if($OLD && $orderId > 408) {
			return redirect('/order-info/'.$order['uid'], 301);
		}
		$action = REQUEST('action');
		if($action == 'tracking-widget') {
			if(!$order['tracking_id']) exit('no');
			$widget = shoplogistics::tracking_widget($order['tracking_id'], $order['dtype']);
			exit($widget ?: 'no');
		}
		shop::makeBreadcrumbs(['/does-not-matter' => 'Информация о заказе']);
		tpl::load('shop');
		make_title('Заказ №'.$order['id']);
		$dtype = json::get('dtype');
		$ptype = json::get('ptype');
		$status = json::get('order-status');
		$dservice = json::get('dservice');
		tpl::push($order);
		tpl::set('cdate', ts2date('Y-m-d H:i', $order['ctime']));
		if(!$order['delivery_info']) {
			tpl::set('delivery_info', @$dtype[$order['dtype']]['text'] ?: 'Не выбрано');
		}
		tpl::set('address', str::address2str(json::decode($order['address'])));
		tpl::set('ptype', @$ptype[$order['ptype']]['text'] ?: 'Не выбрано');
		tpl::set('paid-text', $order['paid'] ? ' Оплачен' : 'Не оплачен');
		tpl::set('status-text', $status[$order['status']]);
		$orderSum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
		tpl::set('order-sum', $orderSum);
		if($order['ptype'] > 3) {
			$login = x::config('ROBOX_LOGIN');
			$password = x::config('ROBOX_PASSWORD_1');
			$inv_desc = "Оплата заказа №".$order['id']." в интернет-магазине Yamibox.ru";
			$out_sum = number_format($orderSum, 2, '.', '');
			$crc = md5(sprintf('%s:%s:%s:%s', $login, $out_sum, $order['id'], $password));
			tpl::set('robox-login', $login);
			tpl::set('robox-description', $inv_desc);
			tpl::set('robox-sum', $out_sum);
			tpl::set('robox-crc', $crc);
			tpl::set('invId', $order['id']);
			tpl::make('robox-payment');
		}
		
		$order_dservice = NULL;
		$delivery_data = $order['delivery_data'] ? json::decode($order['delivery_data']) : NULL;
		if($order['dtype'] == 1 && $delivery_data) {
			// Multiship data
			if(@$delivery_data['sender_id'] == 637) {
				tpl::set('delivery-name', $delivery_data['delivery']['name']);
				$contact = [];
				if($delivery_data['delivery']['phone']) {
					$contact += explode(',', $delivery_data['delivery']['phone']);
				}
				if($delivery_data['delivery']['ticket_email']) {
					$contact += explode(',', $delivery_data['delivery']['ticket_email']);
				}
				tpl::set('delivery-contact', $contact ? implode(', ', $contact) : '--');
				foreach($dservice as $id => $service) {
					if(mb_strtolower($service['name'], 'utf-8') == mb_strtolower($delivery_data['delivery']['name'], 'utf-8')) {
						$order_dservice = $service;
						break;
					}
				}
			}
			// other data
			else {
				$order_dservice = $dservice[$order['dservice']];
			}
		}
		else {
			if($order['dtype'] == 3) $order_dservice = $dservice[8];
			if($order['dtype'] == 2) $order_dservice = $dservice[10];
			tpl::set('delivery-contact', '--');
		}
		if($order_dservice) {
			if($order['dtype'] == 3) {
				$order['tracking_id'] = $order['pr_id'];
				tpl::set('tracking_id', $order['pr_id']);
				tpl::set('tracking_url', str_replace('TRACKING_ID', $order['tracking_id'], $order_dservice['tracking_url']));
				if($order_dservice['tracking_url']) tpl::make('order-tracking-link');
			}
			else {
				tpl::set('tracking_url', 'http://shop-logistics.ru/');
				tpl::make('order-tracking-link');
			}
			tpl::set('delivery-name', $order_dservice['name']);
			tpl::set('delivery-phone', $order_dservice['phone']);
		}
		
		$status_check = 'check';
		$status_hidden = in_array($order['status'], [6,7]);
		$order_cancel = in_array($order['status'], [9,10,5]);
		foreach($status as $id => $text) {
			if(in_array($id, [1,6,7])) continue;
			$status_cancel = in_array($id, [9,10,5]);
			tpl::set('status-text', $text);
			tpl::set('status-check', $order_cancel ? '' : $status_check);
			if($status_cancel) {
				tpl::set('status-check', $id == $order['status'] ? 'cancel' : '');
			}
			if($status_hidden) tpl::set('status-check', '');
			if(!in_array($id, [11,12])) tpl::make('order-info-status', $status_cancel ? 'order-info-status-re' : NULL);
			if($id == $order['status'] || in_array($id, [11,12])) $status_check = '';
		}
		
		$products = json::decode($order['cart_items']);
		
		foreach($products as $product) {
			tpl::push($product);
			if($product['option_name']) {
				tpl::append('name', ' - '.$product['option_name']);
			}
			tpl::set('product-sum', $product['quantity'] * $product['price']);
			tpl::make('order-info-product');
		}
		
		// order confirmation trackers
		if(x::config('ENABLE_COUNTERS')) {
			// Google Tag Manager transaction tracking
			tpl::load('counters');
			tpl::set('order-id', $order['id']);
			$rr_products = $gtm_products = [];
			foreach($products as $product) {
				tpl::push($product);
				if($product['option_name']) {
					$product['name'] .= ' - '.$product['option_name'];
				}
				$rr_pid = $product['id'];
				if($oid = intval($product['option_id'])) {
					$rr_pid .= '00'.$oid;
				}
				db::query('select name from categories where id = (select max(category_id) from product_category where product_id = ?)', $product['id']);
				$product['category'] = db::fetchSingle();
				$rr_products[] = [
					'id' => $rr_pid,
					'qnt' => $product['quantity'],
					'price' => $product['price']
				];
				$gtm_products[] = [
					'sku' => ''.$product['barcode'],
					'name' => $product['name'],
					'category' => $product['category'],
					'price' => $product['price'],
					'quantity' => $product['quantity']
				];
			}
			if($gtm_order_id = SESSION('gtm_order_id')) {
				tpl::set('tr_status', 'new');
				unset($_SESSION['gtm_order_id']);
			}
			else {
				tpl::set('tr_status', 'old');
			}
			tpl::set('gtm-products', json::encode($gtm_products, JSON_UNESCAPED_UNICODE));
			tpl::make('gtm-transaction');
			tpl::set('rr-order-items', json::encode($rr_products));
			tpl::make('retail-rocket-order');
		}
		if($order['dtype'] == 3) {
			tpl::make('order-info-pr');
		}
		else {
			tpl::clear('order-info-pr');
		}
		tpl::make('order-info', 'main');
	}
	
	static function returnItems($orderId) {
		$cart_items = db::querySingle('select cart_items from orders where id = '.$orderId);
		$items = json::decode($cart_items);
		$products = $options = array();
		foreach($items as $item) {
			if($item['stock'] > 2) continue;
			if($item['option_id']) $options[] = $item;
			else $products[] = $item;
		}
		if(!empty($products)) {
			db::prepare('update product set quantity = quantity + :quantity where id = :id');
			foreach($products as $product) {
				db::set('id', $product['id']);
				db::set('quantity', $product['quantity'], PDO::PARAM_INT);
				db::execute();
			}
		}
		if(!empty($options)) {
			db::prepare('update product_option set quantity = (quantity + :quantity) where id = :id');
			foreach($options as $option) {
				db::set('id', $option['option_id']);
				db::set('quantity', $option['quantity'], PDO::PARAM_INT);
				db::execute();
			}
		}
	}
	
	static function takeItems($orderId) {
		$order = db::querySingle('select * from orders where id = '.$orderId, true);
		// предыдущий статус должен быть "отменен"
		if(intval($order['status']) !== 5) return;
		$items = json::decode($order['cart_items']);
		$products = $options = array();
		foreach($items as $item) {
			if($item['stock'] > 2) continue;
			if($item['option_id']) $options[] = $item;
			else $products[] = $item;
		}
		if(!empty($products)) {
			db::prepare('update product set quantity = if(quantity - :quantity < 0, 0, quantity - :quantity) where id = :id');
			foreach($products as $product) {
				db::set('id', $product['id']);
				db::set('quantity', $product['quantity'], PDO::PARAM_INT);
				db::execute();
			}
		}
		if(!empty($options)) {
			db::prepare('update product_option set quantity = if(quantity - :quantity < 0, 0, quantity - :quantity) where id = :id');
			foreach($options as $option) {
				db::set('id', $option['option_id']);
				db::set('quantity', $option['quantity'], PDO::PARAM_INT);
				db::execute();
			}
		}
	}
	
	static function get_semafor($order) {
		$cart_items = json::decode($order['cart_items']);
		$total_positions = count($cart_items);
		$in_stock = array_filter($cart_items, function($item) {
			return $item['stock'] > 2;
		});
		$in_stock = count($in_stock);
		return [$total_positions, $in_stock];
	}
	
	static function complete_collecting($order) {
		$items = json::decode($order['cart_items']);
		$citems = [];
		db::query('select * from collecting_items order by barcode');
		while($citem = db::fetchArray()) {
			$citems[$citem['barcode']] = $citem['quantity'];
		}
		// check all items
		foreach($items as $item) {
			$barcode = ''.$item['barcode'];
			// item not collected
			if(!isset($citems[$barcode])) {
				return false;
			}
			// not enough quantity
			if($citems[$barcode] < $item['quantity']) {
				var_dump($citems[$barcode]);
				return false;
			}
		}
		// remove collected items
		foreach($items as $item) {
			foreach($citems as $barcode => $quantity) {
				if($barcode == $item['barcode']) {
					$quantity -= $item['quantity'];
					if($quantity <= 0) {
						db::query('delete from collecting_items where barcode = ?', $barcode);
					}
					else {
						db::query('update collecting_items set quantity = ? where barcode = ?', $quantity, $barcode);
					}
				}
			}
		}
		// remove order from collecting
		db::query('update orders set cstatus = 3 where id = ?', $order['id']);
		db::query('delete from collecting_orders where order_id = ?', $order['id']);
		return true;
	}
	
	static function status_sms($id_array, $template) {
		$templates = json::get('sms');
		db::query('select *, (select code from coupon where order_id = o.id) as coupon_code from orders o where id in(' . implode(',', $id_array) . ')');
		if(!$orders = db::fetchAll()) return false;
		if(!isset($templates[$template])) return false;
		foreach($orders as $order) {
			if(!$phone = str::filter_phone_number($order['phone'])) continue;
			if(x::config('DEBUG')) $phone = '79248644480';
			$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['coupon'] - $order['cumulative'];
			if($template == 'order-delivered' && $order['coupon_code']) $template .= '-coupon';
			$message = $templates[$template];
			$message = str_replace(['ORDER_ID', 'SUM', 'TRACKING_ID', 'COUPON'], [$order['id'], $order_sum, $order['tracking_id'], $order['coupon_code']], $message);
			web::send_sms($phone, $message);
		}
	}
	
	static function get_buyin_sum($cart_items) {
		if(is_string($cart_items)) $cart_items = json::decode($cart_items);
		$sum = 0;
		foreach($cart_items as $item) {
			$sum += $item['quantity'] * (@$item['price_buyin'] ?: 0);
		}
		return $sum;
	}
	
	static function write_csv_for_1c($file, $orders) {
		$headers = [
			'id', 'status', 'ctime', 'utime', 'total_sum', 'delivery_price', 'firstname', 'lastname', 'email', 'phone', 'city', 'region', 'delivery_info', 'cart_items', 'address'
		];
		$f = fopen($file, 'w+');
		fputcsv($f, $headers, "\t");
		foreach($orders as $order) {
			$line = [
				$order['id'],
				$order['status'],
				$order['ctime'],
				$order['utime'],
				$order['total_sum'],
				$order['delivery_price'],
				$order['firstname'],
				$order['lastname'],
				$order['email'],
				$order['phone'],
				$order['city'],
				$order['region'],
				$order['delivery_info'],
				$order['cart_items'],
				$order['address']
			];
			fputcsv($f, $line, "\t");
		}
		fclose($f);
	}
	
	static function export_for_1c() {
		// new orders, confirmed orders
		db::query('SELECT * FROM orders WHERE status in (0,2)');
		$orders = db::fetchAll();
		$file = FILES.'/1c/orders.csv';
		self::write_csv_for_1c($file, $orders);
		$orders_processed = db::count();
		// cancelled orders
		db::query('SELECT * FROM orders WHERE status = 5');
		$file = FILES.'/1c/orders-cancelled.csv';
		$orders = db::fetchAll();
		self::write_csv_for_1c($file, $orders);
		$orders_cancelled = db::count();
		return ['order::export_for_1c' => [
			'orders_processed' => $orders_processed,
			'orders_cancelled' => $orders_cancelled
		]];
	}
	
	static function cancel_old_orders() {
		db::query('select id, ctime from orders where status = 0 and paid = 0 and unix_timestamp(CURRENT_TIMESTAMP) - unix_timestamp(ctime) >= 86400 * 4');
		$orders = db::fetchAll();
		$num = db::count();
		ob_start();
		db::query('update orders set status = 5 where status = 0 and paid = 0 and unix_timestamp(CURRENT_TIMESTAMP) - unix_timestamp(ctime) >= 86400 * 4');
		printf("%s Cancelled orders: %d\n", date('Y-m-d H:i:s'), $num);
		foreach($orders as $order) {
			printf("ORDER %d, CTIME: %s\n", $order['id'], $order['ctime']);
		}
		$log = ob_get_clean()."\n";
		file_put_contents(FILES.'/orders-cancel.log', $log, FILE_APPEND);
		return ['order::cancel_old_orders' => $num];
	}
	
}

?>