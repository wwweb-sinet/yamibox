<?php

tpl::load('product-details');

tpl::set('meta-robots-rule', 'INDEX,FOLLOW');
tpl::make('meta-robots');

tpl::set('main-class', 'wide');

$product_url_name = $args[1];

db::query('select *, (select max(category_id) from product_category where product_id = p.id) as cid from product p where url_name = ? and status >= 0', $product_url_name);

// Товар не найден в базе
if(!$product = db::fetchArray()) {
	return x::do_action('404.php');
}

tpl::set('document-title', $product['short_text'].' '.$product['name']);

tpl::set('catalog_url', SESSION('catalog_url') ?: '/catalog');

shop::makeBreadcrumbs(['/catalog' => 'Каталог', '/product/'.$product['url_name'] => $product['name']]);

$brands = shop::get_brands();

foreach($brands as $brand) {
	if($product['manufacturer'] == $brand['name']) {
		tpl::set('brand-href', '/catalog?filter='.$brand['filter_id']);
	}
}

// product images
$images = get_files(FILES.'/product/thumbs/', '#^('.$product['id'].')?(\-[0-9]+)?\.jpg$#i', 0, 5);
$img_out = 0;
if($images) {
	sort($images);
	$filename = basename($images[0]);
	tpl::set('image-big-src', '/files/product/'.$filename);
	tpl::set('image-big-href', '/files/product/'.$filename);
	foreach($images as $image) {
		$filename = basename($image);
		tpl::set('image-add-src', '/files/product/thumbs/'.$filename);
		tpl::set('image-add-href', '/files/product/'.$filename);
		tpl::make('image-add');
		$img_out++;
	}
}
else {
	tpl::set('image-big-src', product::DEFAULT_IMAGE);
	tpl::set('image-big-href', '');
}

$reviews = product::getReviewData($product['id']);
tpl::set('rating-avg', $reviews['avg']);
tpl::set('rating-value', round($reviews['avg']));
tpl::set('recommend-rate', $reviews['recommend_rate']);
tpl::set('review-count', $reviews['count']);
tpl::set('review-word', str::numberof($reviews['count'], 'отзыв', ['','а','ов']));
if($reviews['count'] > 0) {
	foreach($reviews['reviews'] as $key => $review) {
		tpl::set('review-id', $review['id']);
		tpl::set('author', $review['author']);
		tpl::set('content', nl2br(html2chars($review['content'])));
		tpl::set('cdate', ts2date('d.m.Y', $review['ctime']));
		tpl::set('rating-value', $review['rating']);
		if(account::$admin) {
			tpl::make('admin-review-delete');
		}
		if($review['recommend']) {
			tpl::make('review-recommend');
		}
		tpl::make('rating');
		tpl::make('review');
		tpl::clear('rating', 'admin-review-delete', 'review-recommend');
	}
	foreach($reviews['stats'] as $key => $value) {
		$width = round($value/$reviews['count']*300);
		tpl::set('row-key', $key);
		tpl::set('row-count', $value);
		tpl::set('bar-width', $width);
		tpl::make('review-stats-row');
	}
	tpl::set('name', $product['name']);
	tpl::make('review-list');
}

if($product['id'] == 550) { // yami box
	$product['quantity'] = 1;
}

$selected_option_id = intval(GET('option_id'));

if($options = product::getOptions($product['id'])) {
	$selected_option = NULL;
	foreach($options as $option) {
		if($option['status'] < 1) continue;
		if($option['stock'] > 2 && $option['quantity'] < 5) $option['quantity'] = 5;
		$img = '/files/variations/'.$option['option_id'].'.jpg';
		tpl::set('option-id', $option['id']);
		tpl::set('option-name', $option['name']);
		tpl::set('option-image', $img);
		tpl::set('option-class', $option['stock'] > 1 ? '' : ' disabled');
		if((!$selected_option_id && !$selected_option) || $selected_option_id == $option['id']) {
			tpl::make('product-buy-option');
			tpl::set('selected-option-id', $option['id']);
		}
		tpl::make('product-option');
		$qty_limit = $option['price_buyin'] >= 1500 ? 3 : 5;
		$available_qty = $option['stock_quantity'] - $qty_limit;
		if($available_qty < 0) $available_qty = 0;
		$available_qty += $option['quantity'];
		$options_json_data[$option['id']] = [
			'option_id' => $option['option_id'],
			'big_img' => '/files/options/'.$option['option_id'].'.jpg',
			'name' => $option['name'],
			'price' => $option['price'],
			'price_old' => $option['price_old'],
			'quantity' => $option['quantity'],
			'spf' => $option['spf'],
			'weight' => $option['weight'],
			'volume' => $option['volume'],
			'stock' => $option['stock'],
			'stock_quantity' => $option['stock_quantity'],
			'available' => $available_qty,
			'artikul' => $option['option_id']
		];
		if(!$selected_option) $selected_option = $option;
		if($option['id'] == $selected_option_id) $selected_option = $option;
		// option images as additional product images
		if($img_out < 5 && file_exists(FILES.'/options/thumbs/'.$option['option_id'].'.jpg')) {
			tpl::set('image-add-id', $option['id']);
			tpl::set('image-add-src', '/files/options/thumbs/'.$option['option_id'].'.jpg');
			tpl::set('image-add-href', '/files/options/'.$option['option_id'].'.jpg');
			tpl::make('image-add');
			$img_out++;
		}
	}
	tpl::set('product-options-data', json::encode($options_json_data));
	tpl::set('product-quantity', $product['quantity']);
	$product['spf'] = $selected_option['spf'];
	$product['price'] = $selected_option['price'];
	$product['price_old'] = $selected_option['price_old'];
	$product['weight'] = $selected_option['weight'];
	$product['volume'] = $selected_option['volume'];
	$product['stock'] = $selected_option['stock'];
	tpl::set('add2cart-option', $selected_option['id']);
	tpl::make('product-options');
}
else {
	tpl::set('selected-option-id', 0);
	tpl::set('product-options-data', 'null');
	tpl::set('add2cart-option', 0);
	if($product['quantity'] < 5 && $product['stock'] > 2) $product['quantity'] = 5;
	tpl::set('product-quantity', $product['quantity']);
}

$qty_limit = $product['price_buyin'] >= 1500 ? 3 : 5;
$available = $product['stock_quantity'] - $qty_limit;
if($available < 0) $available = 0;
$available += $product['quantity'];
if($available > 100) {
	$instock_text = 'В наличии много';
}
elseif($available > 30) {
	$instock_text = 'Есть в наличии';
}
elseif($available > 7) {
	$instock_text = 'В наличии мало';
}
elseif($available) {
	$instock_text = 'Осталась 1 штука';
}
else {
	$instock_text = 'Нет в наличии';
}
tpl::set('stock-text', $instock_text);
tpl::set('stock-color', $available ? 'green' : 'red');

if(!$product['weight']) {
	tpl::set('weight-class', 'hidden');
}
if(!$product['volume']) {
	tpl::set('volume-class', 'hidden');
}
if(!$product['spf']) {
	tpl::set('spf-class', 'hidden');
}

tpl::set('product-name', $product['name']);

if($video = product::getVideo($product['id'])) {
	foreach($video as $url) {
		$query = parse_url($url, PHP_URL_QUERY);
		parse_str($query, $parse_url);
		if(!isset($parse_url['v'])) continue;
		tpl::set('video-id', $parse_url['v']);
		tpl::make('product-video');
	}
}

if($files = get_files('files/descript/', '#^'.$product['id'].'\.(\w+)$#')) {
	foreach($files as $file) {
		tpl::set('description-img', '/'.$file);
		tpl::make('product-description-img');
	}
}

$product_rel = product::getRelatives($product['id']);
if(count($product_rel) > 1) {
	$rel_sum = 0;
	foreach($product_rel as $key => $rel) {
		if($rel['min_price']) $rel['price'] = $rel['min_price'];
		if($rel['min_price_old']) $rel['price_old'] = $rel['min_price_old'];
		tpl::push($rel);
		$image = product::getImage($rel);
		tpl::set('product-img', $image);
		tpl::set('product-href', '/product/'.$rel['url_name']);
		if($key) tpl::make('product-rel-plus');
		tpl::make('product-rel');
		tpl::clear('product-rel-plus');
		$rel_sum += $rel['price'];
	}
	tpl::set('product-rel-sum', $rel_sum);
	tpl::make('product-rel-list');
}

// if($recommended = product::get_recommended($product['id'])) {
	// foreach($recommended as $p) {
		// shop::makeProduct($p, 'recommended');
	// }
// }
if($viewed = product::get_viewed(account::$cookie, $product['id'])) {
	foreach($viewed as $p) {
		shop::makeProduct($p, 'viewed-product');
	}
	tpl::make('viewed-products');
}

tpl::push($product);
if($product['id'] == 540) tpl::set('manufacturer', '');
tpl::set('artikul', $product['id']);
tpl::set('product-id', $product['id']);
if(!$product['price_old']) {
	tpl::set('price-old-class', ' hidden');
}
tpl::set('add2cart-disabled', $product['status'] > 0 && $product['stock'] > 1 ? '' : ' disabled');

tpl::set('rating-avg', $reviews['avg']);
tpl::set('rating-value', round($reviews['avg']));
tpl::make('rating', 'product-rating');
tpl::make('yashare');

if($reviews['count'] > 0) {
	tpl::make('product-meta-rating');
}
// Не показывать бренд коробочки
if($product['id'] != 540) {
	tpl::make('product-manufacturer');
}

$meta_keywords = $product['meta_keywords'] ?: $product['name'].','.$product['manufacturer'].','.COMMON_KEYWORDS;
tpl::set('document_meta_keywords', $meta_keywords);
$meta_description = $product['meta_description'] ?: $product['short_text'].' '.$product['name'].' '.$product['price'].' руб';
tpl::set('document_meta_description', $meta_description);

if($product['outofp'] && $product['stock'] < 2) tpl::make('product-outofp');

if(account::$auth) {
	tpl::set('firstname', account::$info['firstname']);
	tpl::set('lastname', account::$info['lastname']);
	if(account::$admin) {
		tpl::load('admin');
		$status = array(-1 => 'DEL', 0 => 'OFF', 1 => 'ON');
		foreach($status as $value => $text) {
			tpl::set('status-value', $value);
			tpl::set('status-text', $text);
			tpl::set('status-selected', $product['status'] == $value ? ' selected' : '');
			tpl::make('admin-product-status');
		}
		tpl::set('outofp-checked', $product['outofp'] ? ' checked' : '');
		tpl::make('product-outofp-cb');
		tpl::make('product-admin-section');
		tpl::make('product-description-form');
	}
	if(REQUEST_METHOD == 'POST') {
		die_stupid_bot();
		$action = trim(POST('action'));
		if($action == 'addReview') {
			$response = account::addReview($product['id']);
			if(AJAX) die($response);
			redirect(SESSION('referer'));
		}
		if(account::$admin && $action == 'updateUtime') {
			db::query('update product set utime = CURRENT_TIMESTAMP where id = ?', $product['id']);
			die('ok');
		}
		if(account::$admin && $action == 'outofp') {
			$outofp = absint(POST('value'));
			if($outofp !== 0 && $outofp !== 1) die('WRONG!');
			db::query('update product set outofp = ? where id = ?', $outofp, $product['id']);
			die('ok');
		}
	}
	tpl::make('review-form');
}
else {
	tpl::make('review-form-disabled', 'review-form');
}

if(x::config('ENABLE_COUNTERS')) {
	$rr_pid = $product['id'];
	$option_id = 0;
	if($options) {
		if($selected_option_id) {
			$rr_pid .= '00' . $selected_option_id;
		}
		else {
			foreach($options as $option) {
				if(!$option['status']) continue;
				$rr_pid .= '00' . $option['id'];
				break;
			}
		}
	}
	tpl::set('rr-product-id', $rr_pid);
	tpl::make('retail-rocket-product');
	tpl::make('retail-rocket-add2cart-form');
	
	tpl::make('yandex-goal-wishlist');
}

tpl::make('product-details', 'main');

?>