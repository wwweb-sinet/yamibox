<?php

class Parikmag {
	
	const PAGE_LIMIT = 0; // used in fetch_recursive(), 0 for unlimited
	
	static $page_limit = 0; // used in fetch_recursive(), 0 for unlimited
	static $url = 'http://www.parikmag.ru/bitrix/php_interface/ajax/more_group.php';
	
	static function fetch($page = 1) {
		// return file_get_contents(FILES.'/parikmag/page-1.html');
		$file = FILES.'/parikmag/page-'.$page.'.html';
		// keep files for a day
		$now = time();
		if(file_exists($file) && $now - filemtime($file) > 86400) {
			return file_get_contents($file);
		}
		$response = web::http_request(self::$url, 'GET', ['page'=>$page]);
		file_put_contents($file, $response);
		return $response;
	}
	
	static function fetch_recursive($page = 1) {
		$html = self::fetch($page);
		if(preg_match('/<a class="more_tovar" href="(.*?)"/', $html, $match)) {
			$url = $match[1];
			$url = parse_url($url);
			parse_str($url['query'], $params);
			if($params['page']) {
				self::$page_limit--;
				if(self::$page_limit == 0) {
					self::$page_limit = self::PAGE_LIMIT;
					return;
				}
				self::fetch_recursive($params['page']);
			}
		}
	}
	
	static function get_products($html = '') {
		// $html = file_get_contents(FILES.'/parikmag/page-1.html');
		$html = mb_convert_encoding($html, 'utf-8', 'windows-1251');
		preg_match_all('/<li>.*?<\/li>/msi', $html, $match);
		// print_r($match);
		$products = [];
		foreach($match[0] as $product_html) {
			$product = [];
			if(!preg_match('/element_id="(\d+)"/', $product_html, $m)) continue;
			$product['id'] = $m[1];
			preg_match('/<a href="(.*?)" class="name".*>(.*?)<\/a>/', $product_html, $m);
			$product['url'] = $m[1];
			$product['name'] = $m[2];
			preg_match('/<span class="sArticle">Арт: (.*?)<\/span>/ui', $product_html, $m);
			$product['artikul'] = isset($m[1]) ? $m[1] : NULL;
			preg_match('/<span class="price">([\d\s]+)<sup>РУБ.<\/sup><\/span>/ui', $product_html, $m);
			$product['price'] = isset($m[1]) ? preg_replace('/[^\d]/', '', $m[1]) : 0;
			if(preg_match('/<span class="old_price">([\d\s]+)<\/span>/', $product_html, $m)) {
				$product['price_old'] = preg_replace('/[^\d]/', '', $m[1]);
			}
			else {
				$product['price_old'] = NULL;
			}
			preg_match('/<img.*src="(.*?)".*>/i', $product_html, $m);
			$product['img_src'] = strpos($m[1], 'no_image') ? NULL : $m[1];
			if($product['img_src'] && str::startsWith($product['img_src'], '/upload')) {
				$product['img_src'] = '//www.parikmag.ru.images.1c-bitrix-cdn.ru' . $product['img_src'];
			}
			// print_r($product);
			$products[] = $product;
		}
		return $products;
	}
	
	static function save_products($products) {
		$result = 0;
		db::prepare('replace into parikmag set id = :id, url = :url, name = :name, artikul = :artikul, price = :price, price_old = :price_old, img_src = :img_src');
		foreach($products as $product) {
			db::set($product);
			db::execute();
			if(db::count()) $result++;
		}
		return $result;
	}
	
	/*** uses database records ***/
	static function fetch_images() {
		db::query('select id, img_src from parikmag where img_src is not null order by id');
		while($product = db::fetchArray()) {
			$img_url = $product['img_src'];
			$url = parse_url($img_url);
			preg_match('/\.(\w{3,4})$/', $url['path'], $m);
			$img_file = FILES.'/parikmag/product_image/'.$product['id'].strtolower($m[0]);
			if(file_exists($img_file) && filesize($img_file) > 0) continue;
			if(str::startsWith($img_url, '//')) {
				$img_url = 'http:'.$img_url;
			}
			// remove query string
			$img_url = preg_replace('/\??(\w+)$/', '', $img_url);
			// remove size parameters to get original size
			$img_url = preg_replace('/_\d+_\d+(\.\w+)$/', '\1', $img_url);
			$img = file_get_contents($img_url);
			file_put_contents($img_file, $img);
		}
	}
	
	static function import() {
		self::fetch_recursive();
		$files = get_files(FILES.'/parikmag/', '/page-(\d+)\.html/');
		foreach($files as $file) {
			$html = file_get_contents($file);
			$products = self::get_products($html);
			self::save_products($products);
		}
		self::fetch_images();
		return db::querySingle('select count(*) from parikmag');
	}
	
}

?>