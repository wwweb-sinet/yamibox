<?php

set_time_limit(0);

$filename = 'popular100.xml';

db::query('select p.*, pt.*, (select max(category_id) from product_category where product_id = p.id) as cid, (select name from product_groups where rel = p.rel) as rel_name, (select video_url from product_video where product_id = p.id) as video_url from product p left join product_sum_top pt on p.id = pt.pid where summ > 0 order by available desc, summ desc');
$products = db::fetchAll();

$options = [];
db::query('select * from product_option where id in(select distinct oid from product_sum_top where oid > 0)');
while($option = db::fetchArray()) {
	$options[$option['id']] = $option;
}
if(!function_exists('make_yml_offer')) {
	function make_yml_offer($product, $images) {
		if($product['stock'] > 2) $product['quantity'] = intval($product['stock_quantity']);
		if(preg_match('#(\[.*?\]\s*)#', $product['name'], $match)) {
			$product['name'] = str_replace($match[1], '', $product['name']);
		}
		array_walk($product, function(&$val) {
			$val = html2chars($val);
		});
		tpl::push($product);
		if($product['weight']) tpl::make('yml-offer-weight');
		if($product['volume']) tpl::make('yml-offer-volume');
		if($product['spf']) tpl::make('yml-offer-spf');
		if(isset($product['group_id'])) {
			tpl::make('yml-offer-group');
		}
		$i = 0;
		foreach($images as $image) {
			tpl::set('picture', $image);
			tpl::make('yml-offer-picture');
			$i++; if($i == 10) break;
		}
		if($product['rel_name']) {
			tpl::make('yml-offer-rel');
		}
		if($product['price_old']) {
			tpl::make('yml-offer-oldprice');
		}
		if(!preg_match('#[а-я]#ui', $product['option_name']) && preg_match('#\d+#', $product['option_name'])) {
			tpl::make('yml-offer-color');
		}
		tpl::set('sex', $product['cid'] == 6 ? 'Мужской' : 'Женский');
		tpl::make('yml-offer');
		tpl::clear('yml-offer-picture', 'yml-offer-volume', 'yml-offer-weight', 'yml-offer-spf', 'yml-offer-group', 'yml-offer-rel', 'yml-offer-oldprice', 'yml-offer-color');
	}
}

function makeCategories($categories) {
	foreach($categories as $cat) {
		tpl::set('category-id', $cat['id']);
		tpl::set('category-name', $cat['name']);
		if($cat['parent_id']) {
			tpl::set('category-parent', $cat['parent_id']);
			tpl::make('yml-category-p');
		}
		tpl::make('yml-category');
		tpl::clear('yml-category-p');
	}
}

tpl::load('yml');
tpl::set('yml-date', date('Y-m-d H:i'));
$categories = shop::getCategories();
makeCategories($categories);
$DEFAULT_CATEGORY_MARKET = 'Все товары / Одежда, обувь и аксессуары / Красота';
$market_categories = json::get('yandex_categories');

// images
$images = get_files(FILES.'/product/', '#.*\.jpg$#');
sort($images);

$csv = fopen(FILES.'/popular100.csv', 'w+');
$csv_headers = [
	'Название товара',
	'ID товара',
	'URL товара',
	'фото 1',
	'фото 2',
	'фото 3',
	'фото 4',
	'фото 5',
	'Фото в описании',
	'Видеоролик'
];
fputcsv($csv, $csv_headers, "\t");

function put_csv($csv, $product, $imglist) {
	$photo1 = $photo2 = $photo3 = $photo4 = $photo5 = '';
	array_walk($imglist, function(&$val) {
		$val = basename($val);
	});
	if(file_exists(FILES.'/descript/'.$product['id'].'.jpg')) {
		$description_img = $product['id'].'.jpg';
	}
	else {
		$description_img = '';
	}
	$line = [
		$product['name'],
		$product['id'],
		x::config('SITE_URL').'/product/'.$product['url_name'],
		@$imglist[0],
		@$imglist[1],
		@$imglist[2],
		@$imglist[3],
		@$imglist[4],
		$description_img,
		$product['video_url']
	];
	fputcsv($csv, $line, "\t");
}

$num_offers = 0;
foreach($products as $product) {
	// if(!$cid = absint($product['cid'])) continue;
	// if(!isset($categories[$cid])) continue;
	$num_offers += 1;
	$market_category = @$market_categories[$cid] ?: $DEFAULT_CATEGORY_MARKET;
	$product['market_category'] = $market_category;
	$product['available'] = $num_offers > 300 ? 'false' : 'true';
	if(isset($options[$product['oid']])) {
		$option = $options[$product['oid']];
		$orig_id = $product['id'];
		$orig_name = $product['name'];
		$url_name = $product['url_name'];
		$img_dir = '/options/thumbs/';
		$img_path = $img_dir.$option['option_id'].'.jpg';
		if(!file_exists(FILES.$img_path)) continue;
		$imglist = [x::config('SITE_URL').'/files'.$img_path];
		$product['id'] = $orig_id.'00'.$option['id'];
		$product['spf'] = $option['spf'];
		$product['price'] = $option['price'];
		$product['price_old'] = $option['price_old'];
		$product['quantity'] = $option['quantity'];
		$product['weight'] = $option['weight'];
		$product['volume'] = $option['volume'];
		$product['name'] = $orig_name.' - '. ($option['name'] ?: 'M'.substr($option['barcode'], -4, 4));
		$product['text'] = preg_replace('#<.+?>#msi', '', $product['text']);
		$product['text'] = preg_replace('#&\wdash;#si', '-', $product['text']);
		$product['text'] = preg_replace('#&[a-z]+?;#si', ' ', $product['text']);
		$product['url_name'] = $url_name . '?option_id=' . $option['id'];
		$product['group_id'] = $orig_id.'00';
		$product['option_name'] = $option['name'];
		make_yml_offer($product, $imglist);
		put_csv($csv, $product, $imglist);
	}
	else {
		$imglist = [];
		foreach($images as $image) {
			$imgname = finfo($image, 'filename');
			if(!preg_match('#^('.$product['id'].')?(\-\d+)$#', $imgname)) continue;
			$img_dir = '/files/product/thumbs/';
			$imglist[] = x::config('SITE_URL').$img_dir.$imgname.'.jpg';
		}
		$product['text'] = preg_replace('#<.+?>#msi', '', $product['text']);
		$product['text'] = preg_replace('#&\wdash;#si', '-', $product['text']);
		$product['text'] = preg_replace('#&[a-z]+?;#si', ' ', $product['text']);
		$product['option_name'] = '';
		make_yml_offer($product, $imglist);
		put_csv($csv, $product, $imglist);
	}
}

$yml = tpl::make('yml');
$yml_file = FILES.'/'.$filename;
file_put_contents($yml_file, $yml);
// write log
$log_file = FILES.'/yml-top.log';
$log_line = sprintf("%s %s %s\n", date("Y-m-d H:i:s"), $filename, $num_offers);
file_put_contents($log_file, $log_line, FILE_APPEND);

if(!defined('ROUTINE')) { // Вызов по псевдо-ссылке, отдаем контент
	header('Content-Type: text/xml');
	echo $yml;
	exit;
}

?>