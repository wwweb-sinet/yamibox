<?php

class monitoring {
	
	static $filters = [
		1 => [
			'label' => 'В наличии',
			'sql' => 'stock > 1'
		],
		2 => [
			'label' => 'Совпадающие',
			'sql' => 'exists(select 1 from product_monitoring where local_id = p.id)'
		]
	];
	
	static $modes = [
		1 => [
			'text' => 'Показать дороже',
			'sql' => 'exists(select 1 from product_monitoring where local_id = p.id and price < p.price)'
		],
		2 => [
			'text' => 'Показать дешевле',
			'sql' => 'exists(select 1 from product_monitoring where local_id = p.id and price > p.price)'
		],
		3 => [
			'text' => 'Показать равные',
			'sql' => 'exists(select 1 from product_monitoring where local_id = p.id and price = p.price)'
		]
	];
	
	static function attachment_list() {
		$sql = "select count(*) from product p where status > -1";
		if($selected_filters = GET('filter')) {
			foreach($selected_filters as $filter_id) {
				if(!isset(self::$filters[$filter_id])) continue;
				$sql .= ' and '.self::$filters[$filter_id]['sql'];
			}
		}
		if($selected_mode = GET('mode')) {
			if(isset(self::$modes[$selected_mode])) {
				$sql .= ' and ' . self::$modes[$selected_mode]['sql'];
			}
		}
		if($selected_brand = GET('brand')) {
			$sql .= ' and name like \'%'.$selected_brand.'%\'';
		}
		foreach(self::$filters as $filter_id => $filter) {
			tpl::set('checkbox-name', 'filter[]');
			tpl::set('checkbox-text', $filter['label']);
			tpl::set('checkbox-value', $filter_id);
			tpl::set('checkbox-checked', @in_array($filter_id, $selected_filters) ? ' checked' : '');
			tpl::make('checkbox', 'monitoring-checkbox');
			tpl::append('monitoring-checkbox', '<br>');
		}
		foreach(self::$modes as $mode_id => $mode) {
			tpl::set('radio-name', 'mode');
			tpl::set('radio-text', $mode['text']);
			tpl::set('radio-value', $mode_id);
			tpl::set('radio-checked', $mode_id == $selected_mode ? ' checked' : '');
			tpl::make('radio', 'monitoring-radio');
			tpl::append('monitoring-radio', '<br>');
		}
		foreach(shop::get_brands() as $brand) {
			tpl::set('option-text', $brand['name']);
			tpl::set('option-value', $brand['name']);
			tpl::set('option-selected', $brand['name'] == $selected_brand ? ' selected' : '');
			tpl::make('option', 'brand-option');
		}
		$num_records = db::querySingle($sql);
		$sql = str_replace('count(*)', "id, name, url_name, price, price_old, (select group_concat(concat_ws('|',id,name,price,ifnull(price_old,'')) separator '$$') from product_monitoring where source = 'asiacream' and local_id = p.id) as asiacream, (select group_concat(concat_ws('|',id,name,price,ifnull(price_old,'')) separator '$$') from product_monitoring where source = 'bbcream' and local_id = p.id) as bbcream, (select group_concat(concat_ws('|',id,name,price,ifnull(price_old,'')) separator '$$') from product_monitoring where source = 'lunifera' and local_id = p.id) as lunifera", $sql);
		$sql .= ' order by id desc limit :offset, :limit';
		$params['limit'] = 50;
		$params['offset'] = offset(50);
		db::query($sql, $params);
		while($product = db::fetchArray()) {
			tpl::push($product);
			if($product['asiacream']) {
				self::make_source_products('asiacream', $product);
			}
			if($product['bbcream']) {
				self::make_source_products('bbcream', $product);
			}
			if($product['lunifera']) {
				self::make_source_products('lunifera', $product);
			}
			tpl::make('monitoring-product');
			tpl::clear('monitoring-product-match', 'match-asiacream', 'match-bbcream', 'match-lunifera');
		}
		shop::paginator($num_records, 50);
		tpl::make('admin-monitoring', 'main');
	}
	
	static function make_source_products($source, $product) {
		foreach(explode('$$', $product[$source]) as $match) {
			$m = explode('|', $match);
			// print_r($m);
			$mp = [];
			$mp['m-id'] = $m[0];
			$mp['m-name'] = $m[1];
			$mp['m-price'] = $m[2];
			$mp['m-price_old'] = $m[3];
			tpl::push($mp);
			$m_price_class = ' cgrey';
			if($m[2] > $product['price']) $m_price_class = ' green';
			if($m[2] < $product['price']) $m_price_class = ' red';
			tpl::set('m-price-class', $m_price_class);
			tpl::make('monitoring-product-match', 'match-'.$source);
		}
	}
	
}

admin::checkAuth();
tpl::load('monitoring');

?>