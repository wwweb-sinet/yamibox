<?php

class sdek {
	
	const ACCOUNT = '8369b83638a596d3f3b293aebbdb15b6';
	const PASSWORD = '644919b6b49680907335548480597371';
	const DATE_FORMAT = 'Y-m-d\TH:i:s';
	
	static $api_url = 'http://gw.edostavka.ru:11443';
	
	static function get_secure_key($date = NULL) {
		if(!$date) $date = date(self::DATE_FORMAT);
		if(!is_object($date) || get_class($date) !== 'DateTime') {
			$date = new DateTime($date);
			$date = $date->format(self::DATE_FORMAT);
		}
		$key = md5($date.'&'.self::PASSWORD);
		return $key;
	}
	
	static function get_points() {
		$url = self::$api_url.'/pvzlist.php';
		$response = web::http_request($url, 'GET');
		file_put_contents(FILES.'/points/sdek.xml', $response);
		return $response;
	}
	
	static function import_points($xml = NULL) {
		if(!$xml) $xml = self::get_points();
		$xml_data = str::xmltoarray($xml);
		// print_r($xml_data);
		if(!$xml_data) abort('NO XML DATA');
		$email_notice = true;
		$num = 0;
		db::prepare('replace into points_sdek set code = :code, name = :name, citycode = :citycode, city = :city, worktime = :worktime, address = :address, phone = :phone, note = :note, latitude = :latitude, longitude = :longitude, delivery_price = :delivery_price');
		foreach($xml_data['Pvz'] as $point) {
			$point = $point['@attributes'];
			$data = [
				'code' => $point['Code'],
				'name' => $point['Name'],
				'citycode' => $point['CityCode'],
				'city' => $point['City'],
				'worktime' => $point['WorkTime'],
				'address' => $point['Address'],
				'phone' => $point['Phone'],
				'note' => $point['Note'],
				'latitude' => $point['coordY'],
				'longitude' => $point['coordX'],
				'delivery_price' => 300
			];
			db::set($data);
			db::execute();
			if(!db::count()) {
				if(x::config('DEBUG')) {
					var_dump($p);
					var_dump($params);
					var_dump($sql);
					abort(db::error());
				}
				elseif($email_notice) {
					shop::sendEmail('wwweb.sinet@gmail.com', 'Ошибка импорта точек СДЭК', db::error());
					$email_notice = false;
				}
			}
			else {
				$num++;
			}
		}
		// update prices (static data)
		db::query('update points_sdek ps set delivery_price = ifnull((select sd_pvz from delivery where sd_id = ps.citycode), 300)');
		// update days
		db::query('update points_sdek sd set days = (select max(sd_dt) from delivery where sd_id = sd.citycode)');
		return ['import_points' => $num];
	}
	
	static function update_orders() {
		$date_first = date(self::DATE_FORMAT, strtotime('now - 2 months'));
		$secure = md5($date_first.'&'.self::PASSWORD);
		$data = [
			'account' => self::ACCOUNT,
			'secure' => $secure,
			'datefirst' => $date_first,
			'showhistory' => 0
		];
		$have_data = false;
		if($response = web::http_request(self::$api_url.'/status_report.php', 'GET', $data)) {
			file_put_contents(FILES.'/cache/orders_sdek.xml', $response);
			$have_data = strlen($response) > 192;
			if(!$have_data) {
				log_write('sdek::update_orders ОШИБКА: '.$response);
				return ['sdek::update_orders' => $response];
			}
		}
		else {
			log_write('sdek::update_orders ОШИБКА: данные не получены');
			return ['sdek::update_orders' => 'ОШИБКА: данные не получены'];
		}
		$num = 0;
		if($have_data) {
			$delivered = $intransit = $pickpoint = [];
			$os = [
				1 => 3,
				2 => 9,
				3 => 3,
				6 => 3,
				16 => 9,
				7 => 3,
				21 => 3,
				22 => 3,
				13 => 3,
				17 => 9,
				19 => 3,
				20 => 3,
				8 => 3,
				9 => 3,
				10 => 8,
				12 => 8,
				11 => 3,
				18 => 9,
				4 => 4,
				5 => 9
			];
			$response = str::xmltoarray($response);
			$orders = $response['Order'];
			// set tracking_id
			db::prepare('update orders set dservice = 9, tracking_id = ? where id = ?');
			foreach($orders as $order) {
				$order = $order['@attributes'];
				$order_id = intval($order['Number']);
				db::set([$order['DispatchNumber'], $order_id]);
				db::execute();
			}
			// обновление статусов заказов
			db::prepare('update orders set status = ?, dtime = ? where id = ? and status in(3,8,9)');
			foreach($orders as $order) {
				$order = $order['@attributes'];
				$status = intval($order['StatusCode']);
				$status = $os[$status];
				if(!array_key_exists($status, $os)) continue;
				$order_id = intval($order['Number']);
				$dtime = NULL;
				$ddate = in_array($status, [8,4,10]) ? ($order['DeliveryDate'] ?: $order['Date']) : NULL;
				if($ddate) {
					$dtime = str_replace('T', ' ', $ddate);
				}
				if(!$dtime || $status !== 4) $dtime = NULL;
				db::set([$status, $dtime, $order_id]);
				db::execute();
				if(!db::count()) continue;
				// add to notification list
				if($status == 4) {
					$delivered[] = $order_id;
					$num ++;
				}
				if($status == 3) {
					$intransit[] = $order_id;
				}
				if($status == 8) {
					$pickpoint[] = $order_id;
				}
			}
			if(!x::config('DEBUG')) {
				// email-уведомление о полученном заказе
				if($delivered) {
					delivered_email($delivered);
					delivered_sms($delivered);
					activate_coupons($delivered);
				}
				// email-уведомление о передаче в СД
				if($intransit) {
					intransit_email($intransit);
				}
				// email-уведомление о доставке в ПВЗ
				if($pickpoint) {
					pickpoint_email($pickpoint);
				}
			}
			else {
				activate_coupons($delivered);
			}
			if($num) log_write('SDEK: Обновлено заказов '.$num);
			else log_write('SDEK: Доставленных заказов нет');
			tpl::append('main', '<pre>'.ob_get_clean().'</pre>');
			return ['sdek::update_orders' => 'Доставлено заказов: '.$num];
		}
		// если данные не получены
		return ['sdek::update_orders' => 'Данные не получены'];
	}
}

?>