<?php

class dguru {
	
	const API_URL = 'http://api.dostavka.guru/client';
	// const API_UID = 9999;
	// const API_KEY = '827ccb0eea8a706c4c34a16891f84e7b';
	const API_UID = 2043;
	const API_KEY = '2ff0bbbc1c042453ada5697039763fd9';
	const USLUGA = '';
	
	static $actions = [
		'send_order' => '/in_up.php',
		'order_status' => '/order_info_post.php',
		'delivery_dates' => '/date_dost.php',
		'barcode' => '/barcode/barcode.php'
	];
	static $dtype = [
		'КурьерМСК', 'КурьерСПБ', 'Почта', 'ПВЗ МЭ'
	];
	static $pr_type = ['разное', '1 класс'];
	static $region_v = [
		'Москва, МО',
		'Санкт-Петербург, ЛО'
	];
	
	static function get_dtype($order) {
		
	}
	
	static function get_delivery_dates() {
		$url = self::API_URL . self::$actions['delivery_dates'];
		$data = [
			'partner_id' => self::API_UID,
			'key' => self::API_KEY
		];
		$response = web::http_request($url, 'POST', $data);
		return $response;
	}
	
	static function get_delivery_date() {
		$xml = self::get_delivery_dates();
		file_put_contents(FILES.'/dguru/delivery_dates.xml', $xml);
		if(preg_match('#<day>([\d\.]+)</day>#', $xml, $match)) {
			return $match[1];
		}
		return date('Y.m.d', strtotime('tomorrow'));
	}
	
	static function send_order($order) {
		if(is_numeric($order)) {
			$order_id = absint($order);
			$order = db::querySingle('select * from orders where id = '.$order_id, true);
		}
		$address = json::decode($order['address']);
		$address_str = str::address2str($address);
		$delivery_date = self::get_delivery_date();
		$zipcode = $address['zipcode'];
		$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['coupon'] - $order['bonus'] - $order['cumulative'];
		$data = [
			'partner_id' => self::API_UID,
			'key' => self::API_KEY,
			'usluga' => $order['paid'] ? '24' : '24КО',
			'order_number' => $order['id'],
			'sposob_dostavki' => self::$dtype[0],
			'date_dost' => $delivery_date,
			'delivery_time_from' => '11:00',
			'delivery_time_to' => '15:00',
			'region_iz' => 'МСК',
			'zip_v' => $zipcode,
			'region_v' => $order['region'],
			'raion_v' => '',
			'city_v' => $order['city'],
			'adres' => $address_str,
			'punkt_vivoz' => '',
			'cont_name' => $order['firstname'].' '.$order['lastname'],
			'cont_tel' => $order['phone'],
			'mesta' => 1,
			'code_1' => '',
			'code_2' => '',
			'code_3' => '',
			'ves_kg' => 1.50,
			'nal_plat' => $order_sum,
			'ocen_sum' => $order_sum
		];
		print_r($data);
		file_put_contents(FILES.'/dguru/orders/'.$order['id'].'-request.xml', json::encode($data));
		$url = self::API_URL . self::$actions['send_order'];
		$response = web::http_request($url, 'POST', $data);
		file_put_contents(FILES.'/dguru/orders/'.$order['id'].'-response.xml', $response);
		return $response;
	}
	
	static function update_orders() {
		$url = self::API_URL . self::$actions['order_status'];
		$data = [
			'partner_id' => self::API_UID,
			'key' => self::API_KEY,
			'order_number' => 6619
		];
		$response = web::http_request($url, 'POST', $data);
		print_r($response);
		exit;
	}
	
}

?>