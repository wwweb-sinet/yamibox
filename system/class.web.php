<?php

/* for web services */

class Web {

	static function send_sms($phoneNumber, $text, $time_value = 0) {
		$url = x::config('SMS_SERVICE_URL');
		date_default_timezone_set("UTC");
		$date = strlen($time_value) == 10
			? date('c', time() + $time_value)
			: time() + $time_value;
		if(is_numeric($time_value)) {
			$date = strlen($time_value) == 10 ? date('c', $time_value) : date('c', time() + $time_value);
		}
		else {
			$date = strlen($time_value) == 25 ? $time_value : date('c');
		}
		$url .= '?phone=' . $phoneNumber . '&text=' . rawurlencode($text) . '&scheduleTime=' . rawurlencode($date);
		$result = web::http_request($url);
		$result = date('Y-m-d H:i:s').';'.$phoneNumber.';'.$result;
		x::set('SMS_RESULT', $result);
		$ret = strpos($result, 'accepted') !== false;
		if($ret) $result .= " TEXT: ".$text;
		file_put_contents(FILES.'/sms.log', $result."\r\n", FILE_APPEND);
		return $ret;
	}

	static function http_request($url, $method = 'GET', $data = NULL, $options = NULL, $debug = false) {
		$curl = curl_init();
		$opts = array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_CONNECTTIMEOUT => 30,
			CURLOPT_URL => $url,
			CURLINFO_HEADER_OUT => TRUE,
			CURLOPT_SSL_VERIFYPEER => FALSE
		);
		if(is_array($options)) {
			$opts += $options;
		}
		if($method == 'POST') {
			$opts[CURLOPT_POST] = TRUE;
			if((is_array($data) && !empty($data)) || is_string($data)) {
				$opts[CURLOPT_POSTFIELDS] = $data;
			}
		}
		elseif($method == 'GET' && $data) {
			$querystring = http_build_query($data);
			$opts[CURLOPT_URL] .= '?'.$querystring;
		}
		curl_setopt_array($curl, $opts);
		$response = curl_exec($curl);
		if($debug) print_r(curl_getinfo($curl));
		if(x::config('DEBUG')) $GLOBALS['CURL_INFO'] = curl_getinfo($curl);
		$error = curl_error($curl);
		curl_close($curl);
		return $error ? $error : $response;
	}
	
	// russian post api request
	static function rp_delivery_info($tracking_id = null) {
		if(!$tracking_id) $tracking_id = trim(REQUEST('tracking_id'));
		if(!$tracking_id) return false;
		$api_url = 'http://russianpostcalc.ru/api_v1.php';
		$api_pwd = 'dbrecmrf693856';
		$data['apikey'] = 'ae593cfb2a1e03683f4b9e996cd1e1c9';
		$data['method'] = 'parcel';
		$data['rpo'] = $tracking_id;
		$data2 = $data;
		$data2[] = $api_pwd;
		$data['hash'] = md5(implode("|", $data2));
		return self::http_request($api_url, 'POST', $data);
	}
	
	static function rp_calc($to_index, $cost) {
		$api_url = 'http://russianpostcalc.ru/api_v1.php';
		$api_pwd = 'dbrecmrf693856';
		$data['apikey'] = 'ae593cfb2a1e03683f4b9e996cd1e1c9';
		$data['method'] = 'calc';
		$data['from_index'] = '101000';
		$data['to_index'] = $to_index;
		$data['ob_cennost_rub'] = $cost;
		$data['weight'] = '1.5';
		$data2 = $data;
		$data2[] = $api_pwd;
		$data['hash'] = md5(implode("|", $data2));
		return self::http_request($api_url, 'POST', $data);
	}

}

?>