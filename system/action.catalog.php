<?php

// redirect from deprecated links
if(isset($args[3])) {
	if($args[3] == 'discounts') return redirect('/catalog?filter=41', 301);
}

$document_title = 'Каталог товаров Yamibox';

$sort_types = json::get('sort-types');
$categories = shop::getCategories();
$filters = json::get('filters');
$brands = shop::get_brands();
$brandsSelected = GET('brand');

if(GET('filter')) {
	tpl::set('meta-robots-rule', 'NOINDEX');
	tpl::make('meta-robots');
}

$category = NULL;

// check for sub category
if($args[1]) { // category url
	foreach($categories as $cat) {
		if($cat['url'] == $args[1]) {
			$category = $cat;
			break;
		}
	}
}

$_SESSION['catalog_url'] = $_SERVER['REQUEST_URI'];

$sql = 'select count(*) from product p where status > 0 and stock > 0';

$SHOW_FILTERS = false;

if($category) {
	$SHOW_FILTERS = $category['use_filters'];
	x::data('menu-category', $category['id']);
	//if(x::config('ENABLE_COUNTERS')) tpl::make('rr-category-view');
	make_title($category['name'], 'catalog-title');
	tpl::set('document_meta_keywords', $category['meta_keywords'] ?: $category['name'].','.COMMON_KEYWORDS);
	tpl::set('document_meta_description', $category['meta_description'] ?: $category['name'].'. Корейская косметика в интернет-магазине Yamibox.ru');
	$cat_ids = shop::concat_categories($category['id']);
	$document_title = $category['name'].' - '.$document_title;
	$sql .= ' and id in (select product_id from product_category where category_id in ('.$cat_ids.'))';
	if($category['parent_id']) {
		$parent = $categories[$category['parent_id']];
		$breadcrumbs['/catalog'.$parent['url']] = $parent['name'];
		if($parent['parent_id']) {
			$gparent = $categories[$parent['parent_id']];
			$breadcrumbs['/catalog'.$gparent['url']] = $gparent['name'];
			x::data('menu-category-level1', $parent['parent_id']);
		}
		else {
			x::data('menu-category-level1', $category['parent_id']);
		}
	}
	else {
		x::data('menu-category-level1', $category['id']);
	}
	$breadcrumbs['/catalog'.$category['url']] = $category['name'];
	$byurlname = array_group($categories, 'url_name');
	tpl::set('category-seo', $category['seo']);
	tpl::set('category-id', $category['id']);
	tpl::set('category-name', $category['name']);
}
elseif($q = GET('q')) {
	make_title('Поиск в каталоге', 'catalog-title');
	$breadcrumbs['not required'] = 'Поиск';
}
else {
	make_title('Все товары', 'catalog-title');
	$breadcrumbs = [];
	tpl::set('category-id', 0);
}

shop::makeBreadcrumbs($breadcrumbs);

// набор колонок изменяется, если применен поиск
$CATALOG_SELECT = CATALOG_SELECT;

if($search_str = trim(GET('q'))) {
	$sort_types = [6 => ['text' => 'По релевантности', 'sql' => 'match_name desc, match_full desc']] + $sort_types;
	tpl::set('search-str', html2chars($search_str));
	$str = db::escape($search_str);
	$CATALOG_SELECT .= ', match(name) against('.$str.') as match_name, match(name, short_text, meta_keywords) against('.$str.') as match_full';
	$sql .= ' and ((match(name, short_text, meta_keywords) against (' . $str . ') > 1) or manufacturer like '. $str .')';
}

if(COOKIE('outofstock')) {
	tpl::set('outofstock-checked', ' checked');
}
else {
	$sql .= ' and stock > 1';
}

$filter_links = [];
$filtersSelected = [];

qs::remove('p');

if(($filter = GET('filter')) && preg_match_all('/\d{2,2}/', $filter, $match)) {
	sort($match[0]);
	$filtersSelected = $match[0];
	$filter_ids = $filter_spf = [];
	foreach($match[0] as $key => $filterId) {
		if(!isset($filters[$filterId])) {
			unset($filtersSelected[$key]);
			continue;
		}
		if($filterId < 30 || ($filterId > 60 && $filterId < 70)) { // skin type
			$filter_ids[] = $filterId;
		}
		elseif($filterId == 41) { // discounts
			x::data('discounts-selected', true);
			$sql .= ' and (price_old > price or exists(select 1 from product_option where product_id = p.id and price_old > price))';
		}
		elseif($filterId <= 40 || ($filterId >= 70 && $filterId <= 90)) { // brand
			continue;
		}
		elseif($filterId < 60) {
			if($filterId == 51) $filter_spf[] = 'spf < 15';
			if($filterId == 52) $filter_spf[] = 'spf >= 15 and spf < 30';
			if($filterId == 53) $filter_spf[] = 'spf >= 30 and spf < 50';
			if($filterId == 54) $filter_spf[] = 'spf >= 50';
		}
		else continue;
		$fs = $match[0];
		unset($fs[$key]);
		$filter_code = implode('', $fs);
		$filter_links[] = ['href' => $filter_code ? qs::set('filter', $filter_code) : qs::remove('filter'), 'text' => $filters[$filterId]];
		qs::reset();
	}
	if($filter_ids) $sql .= ' and id in (select distinct product_id from product_filters where filter_id in ('.implode(',', $filter_ids).'))';
	if($filter_spf) $sql .= ' and spf > 0 and ('. implode(' or ', $filter_spf) .')';
}

$filter_brand = [];
if($brandsSelected) {
	$brandsSelected = explode(',', $brandsSelected);
	array_walk($brandsSelected, 'trim');
	sort($brandsSelected);
	foreach($brandsSelected as $brand_url) {
		if(!$brand_url) continue;
		foreach($brands as $brand) {
			if($brand_url == $brand['url']) {
				$filter_brand[] = db::escape($brand['name']);
				break;
			}
		}
	}
}
else {
	$brandsSelected = [];
}
if($filter_brand) $sql .= ' and manufacturer in ('. implode(', ', $filter_brand) .')';

tpl::set('document-title', $document_title);

//printf("TOTAL PRODUCTS: %d", $total_products);

$orderBy = $sort_types[0]['sql'];

if($selected_sort_type = absint(REQUEST('sort'))) {
	if(isset($sort_types[$selected_sort_type])) {
		$orderBy = $sort_types[$selected_sort_type]['sql'];
	}
}

qs::remove('sort');
foreach($sort_types as $id => $type) {
	tpl::set('option-value', $id ? qs::set('sort', $id) : qs::remove('sort'));
	tpl::set('option-selected', $selected_sort_type == $id ? ' selected' : '');
	tpl::set('option-text', $type['text']);
	tpl::make('option', 'sort-option');
}
qs::reset();

$total_products = db::querySingle($sql);
$PAGE_LIMIT_CATALOG = PAGE_LIMIT_CATALOG;
if($total_products > PAGE_LIMIT_CATALOG && $total_products - PAGE_LIMIT_CATALOG <= PAGE_LIMIT_CATALOG / 2) $PAGE_LIMIT_CATALOG *= 2;
$offset = offset($PAGE_LIMIT_CATALOG);
$sql = str_replace('count(*)', $CATALOG_SELECT, $sql);
$sql .= sprintf(' order by (status < 1 or stock < 2), %s', $orderBy);
if(GET('p') !== 'all') {
	$sql .= sprintf(' limit %d, %d', $offset, $PAGE_LIMIT_CATALOG);
}

db::query($sql);

foreach(db::fetchAll() as $product) {
	shop::makeProduct($product, 'catalog-products');
}

if(!$total_products) {
	alert('Товары не найдены.', 'notice', 'catalog-error');
}

// applied filters
foreach($brandsSelected as $key => $brand_url) {
	if(!$brand_url) continue;
	foreach($brands as $brand) {
		if($brand['url'] != $brand_url) continue;
		$bs = $brandsSelected;
		unset($bs[$key]);
		qs::remove('p');
		$filter_code = implode(',', $bs);
		$filter_href = $filter_code ? qs::set('brand', $filter_code) : qs::remove('brand');
		tpl::set('filter-href', $filter_href);
		tpl::set('filter-text', $brand['name']);
		tpl::make('catalog-filter');
		qs::reset();
	}
}
foreach($filter_links as $link) {
	tpl::set('filter-href', $link['href']);
	tpl::set('filter-text', $link['text']);
	tpl::make('catalog-filter');
}
if(tpl::get('catalog-filter')) {
	qs::remove('brand');
	qs::remove('p');
	tpl::set('filter-clear-href', qs::remove('filter'));
	tpl::make('catalog-filters', 'catalog-left-column');
}
qs::reset();

// categories menu
shop::makeLeftMenu($category, 'catalog-left-column');

// filters menu
qs::remove('p');
$filter_code = $filtersSelected ? implode('', $filtersSelected) : '';
foreach($filters as $filterId => $text) {
	if(!$SHOW_FILTERS && ($filterId < 30 || ($filterId > 60 && $filterId < 70))) continue;
	if($filterId < 20) $key = 'skin';
	elseif($filterId < 30) $key = 'age';
	elseif($filterId <= 40 || ($filterId >= 70 && $filterId <= 90)) continue;
	elseif($filterId < 50) $key = 'special';
	elseif($filterId < 60) $key = 'spf';
	elseif($filterId < 70) $key = 'condition';
	else continue;
	if(in_array($filterId, $filtersSelected)) continue;
	$fs = $filtersSelected;
	$fs[] = $filterId;
	sort($fs);
	$filter_code = implode('', $fs);
	tpl::set('filter-text', $text);
	tpl::set('filter-href', qs::set('filter', $filter_code));
	tpl::make('filter-link', $key.'-filter');
}
qs::reset();

qs::remove('p');
foreach($brands as $brand) {
	if(in_array($brand['url'], $brandsSelected)) continue;
	$bs = $brandsSelected;
	$bs[] = $brand['url'];
	sort($bs);
	$filter_code = implode(',', $bs);
	tpl::set('filter-text', $brand['name']);
	tpl::set('filter-href', qs::set('brand', $filter_code));
	tpl::make('filter-link', 'brand-filter');
}
if(tpl::get('skin-filter')) tpl::make('filters-menu-skin');
if(tpl::get('age-filter')) tpl::make('filters-menu-age');
if(tpl::get('brand-filter')) tpl::make('filters-menu-brand');
if(tpl::get('spf-filter')) tpl::make('filters-menu-spf');
if(tpl::get('condition-filter')) tpl::make('filters-menu-condition');
tpl::make('catalog-filters-menu', 'catalog-left-column');
qs::reset();

if(x::config('ENABLE_COUNTERS')) {
	tpl::make('retail-rocket-category');
}

shop::paginator($total_products, $PAGE_LIMIT_CATALOG, true);
tpl::make('catalog', 'main');

?>