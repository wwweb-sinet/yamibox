<?php

class ppapi {
	
	const IKN = '9990148912';
	
	static $api_url = 'http://e-solution.pickpoint.ru/api/';
	static $session_id = NULL;
	
	static function login() {
		if(self::$session_id) return self::$session_id;
		$data = json_encode([
			'Login' => 'yamibox',
			'Password' => 'dbrecmrf693856'
		]);
		$url = self::$api_url.'login';
		$options = [CURLOPT_HTTPHEADER => ['Content-Type: application/json']];
		$response = web::http_request($url, 'POST', $data, $options);
		// var_dump($response);
		$data = json::decode($response);
		self::$session_id = $data['SessionId'];
		return self::$session_id;
	}
	
	static function logout() {
		if(!self::$session_id) return true;
		$url = self::$api_url.'logout';
		$data['SessionId'] = self::$session_id;
		$options = [CURLOPT_HTTPHEADER => array('Content-Type: application/json')];
		$response = web::http_request($url, 'POST', $data, $options);
		$data = json::decode($response);
		$result = (bool)$data['Success'];
		if($result) self::$session_id = NULL;
		return $result;
	}
	
	static function get_points() {
		$url = self::$api_url.'postamatlist';
		$response = web::http_request($url);
		file_put_contents(FILES.'/points/pickpoint.json', $response);
		return $response;
	}
	
	static function ajax() {
		$action = trim(REQUEST('action'));
		$result = 'null';
		if($action == 'point_info') {
			if($number = trim(REQUEST('number'))) {
				$point = self::point_info($number, false);
				if(is_array($point)) {
					$cart_sum = cart::$sum_with_discounts;
					if($cart_sum >= 2000 && $point['region'] == 'Московская обл.') $point['delivery_price'] = 0;
					if($cart_sum >= 3000 && $point['region'] != 'Московская обл.') $point['delivery_price'] = 0;
					$result = json::encode($point);
				}
			}
			else $result = 'empty number';
		}
		die($result);
	}
	
	static function point_info($number, $encode = true) {
		db::query('select * from points_pp where number = ?', $number);
		if(!$info = db::fetchArray()) return 'Point not found';
		return $encode ? json::encode($info) : $info;
	}
	
	static function create_order($pp_order) {
		$url = self::$api_url.'createsending';
		$data = json::encode($pp_order);
		$options = [CURLOPT_HTTPHEADER => ['Content-Type: application/json']];
		$response = web::http_request($url, 'POST', $data, $options);
		return $response;
	}
	
	static function send_order($order) {
		$session_id = ppapi::login();
		$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
		$edtn = md5($order['id'].$order_sum);
		$postage_type = $order['paid'] ? '10001' : '10003';
		$postamat = json::decode($order['delivery_data']);
		$address = json::decode($order['address']);
		$address_str = str::address2str($address);
		if(strpos($order['city'], ',')) {
			list($city, $region) = explode(',', $order['city'], 2);
		}
		else {
			$city = $order['city'];
			$region = $order['region'] ?: '';
		}
		$pp_order = [
			'SessionId' => $session_id,
			'Sendings' => [
				[
					'EDTN' => $edtn,
					'IKN' => ppapi::IKN,
					'Invoice' => [
						'SenderCode' => $order['id'],
						'BarCode' => '',
						'GCBarCode' => '',
						'Description' => 'Заказ '.$order['id'].'. Интернет-магазин Yamibox.ru',
						'RecipientName' => $order['firstname'].' '.$order['lastname'],
						'PostamatNumber' => @$postamat['number'] ?: 0,
						'MobilePhone' => $order['phone'],
						'Email' => $order['email'],
						'PostageType' => $postage_type,
						'GettingType' => '101',
						'PayType' => 1,
						'Sum' => $order['paid'] ? 0 : $order_sum,
						'InsuareValue' => 0,
						'Width' => 15,
						'Height' => 10,
						'Depth' => 10,
						'ClientReturnAddress' => [
							'CityName' => $city,
							'RegionName' => $region,
							'Address' => $address_str,
							'FIO' => $order['firstname'].' '.$order['lastname'],
							'PostCode' => @$address['zipcode'] ?: '',
							'Organisation' => 'ИП Андреев П.Р.',
							'PhoneNumber' =>  $order['phone'],
							'Comment' => $order['comment']
						],
						'UnclaimedReturnAddress' => [
							'CityName' => 'Москва',
							'RegionName' => 'Московская область',
							'Address' => 'Большая Татарская, 35с5',
							'FIO' => 'Иванова Дина',
							'PostCode' => '115093',
							'Organisation' => 'ИП Андреев П.Р.',
							'PhoneNumber' => '79688061135',
							'Comment' => 'домофон 36'
						],
						'SubEncloses' => [
							[
								'Line' => '',
								'ProductCode' => '',
								'GoodsCode' => '',
								'Name' => 'Косметика и парфюимерия',
								'Price' => $order_sum
							]
						]
					]
				]
			]
		];
		$response = ppapi::create_order($pp_order);
		$response_data = json::decode($response);
		if(!empty($response_data['CreatedSendings'])) {
			$invoice_number = $response_data['CreatedSendings'][0]['InvoiceNumber'];
			db::query('update orders set tracking_id = ? where id = ?', $invoice_number, $order['id']);
		}
		$file = FILES.'/pp_orders/'.$order['id'].'.txt';
		file_put_contents($file, $response);
		return $response_data;
	}
	
	// routine action
	static function import_points() {
		$plist = ppapi::get_points();
		$data = json::decode($plist);
		if($data) {
			db::query('truncate table points_pp');
		}
		$email_notice = true;
		$num = 0;
		$sql = '';
		foreach($data as $postamat) {
			$params = $p = [];
			$postamat['delivery_price'] = 270;
			if(in_array($postamat['CitiId'], [893,992])) {
				$postamat['delivery_price'] = 210;
			}
			foreach($postamat as $column => $value) {
				$column = strtolower($column);
				$p[$column] = $value;
				$params[] = $column.' = :'.$column;
			}
			if(!$sql) {
				$sql = 'replace into points_pp set ';
				$sql .= implode(', ', $params);
				db::prepare($sql);
			}
			db::set($p);
			db::execute();
			if(!db::count()) {
				if(x::config('DEBUG')) {
					var_dump($p);
					var_dump($params);
					var_dump($sql);
					abort(db::error());
				}
				elseif($email_notice) {
					shop::sendEmail('wwweb.sinet@gmail.com', 'Ошибка импорта точек PickPoint', db::error());
					$email_notice = false;
				}
			}
			else {
				$num++;
			}
		}
		db::query('update points_pp pp set delivery_price = ifnull((select distinct pp_pvz from delivery where pp_id = pp.citiid), 300)');
		// update days
		db::query('update points_pp pp set days = (select max(pp_dt) from delivery where pp_id = pp.citiid)');
		return ['import_points' => $num];
	}
	
	static function tracksendings($id_list = []) {
		
	}
	
	static function update_orders() {
		ob_start();
		$session_id = self::login();
		$have_data = false;
		$url = self::$api_url . 'getInvoicesChangeState';
		$data['SessionId'] = $session_id;
		$data['DateFrom'] = '01.09.2014';
		$data['DateTo'] = date('d.m.Y');
		$data = json::encode($data);
		$options = [CURLOPT_HTTPHEADER => ['Content-Type: application/json']];
		if($response = web::http_request($url, 'POST', $data, $options)) {
			file_put_contents(FILES.'/cache/orders_pickpoint.json', $response);
			$have_data = strlen($response) > 100;
			if(!$have_data) {
				log_write('ppapi::update_orders ОШИБКА: '.$response);
				return ['ppapi::update_orders' => $response];
			}
		}
		else {
			log_write('ppapi::update_orders ОШИБКА: данные не получены');
			return ['ppapi::update_orders' => 'ОШИБКА: данные не получены'];
		}
		$num = 0;
		if($have_data) {
			$orders = json::decode($response);
			$orders = self::filter_orders($orders);
			$delivered = $intransit = $pickpoint = [];
			$os = [
				101 => 3, 102 => 3, 103 => 3, 104 => 3, 105 => 3, 106 => 3, 107 => 3, 108 => 3,
				109 => 8, 110 => 8,
				111 => 4,
				112 => 9, 114 => 9, 115 => 9, 116 => 9, 117 => 9,
				113 => 10
			];
			// присвоение заказам иденификатора СД
			db::prepare('update orders set dservice = 5, tracking_id = ? where id = ?');
			foreach($orders as $order) {
				$tracking_id = $order['InvoiceNumber'] ?: NULL;
				$order_id = absint($order['SenderInvoiceNumber']);
				db::set([$tracking_id, $order_id]);
				db::execute();
			}
			// обновление статусов заказов
			db::prepare('update orders set status = ?, dtime = ? where id = ? and status in(3,8,9)');
			foreach($orders as $order) {
				if(!array_key_exists($order['State'], $os)) continue;
				$order_id = intval($order['SenderInvoiceNumber']);
				$status = $os[$order['State']];
				$dtime = in_array($status, [8,4,10]) ? $order['ChangeDT'] : NULL;
				if($dtime) {
					$ddate = DateTime::createFromFormat('d.m.y H:i', $dtime);
					$dtime = $ddate->format('Y-m-d H:i:s');
				}
				if($status != 4) $dtime = NULL;
				db::set([$status, $dtime, $order_id]);
				db::execute();
				if(!db::count()) continue;
				if($status == 4) {
					$delivered[] = $order_id;
					$num ++;
				}
				if($status == 3) {
					$intransit[] = $order_id;
				}
				if($status == 8) {
					$pickpoint[] = $order_id;
				}
			}
			if(!x::config('DEBUG')) {
				// email-уведомление о полученном заказе
				if($delivered) {
					delivered_email($delivered);
					order::status_sms($delivered, 'order-delivered');
					// activate coupon for next order
					activate_coupons($delivered);
				}
				// email-уведомление о передаче в СД
				if($intransit) {
					order::status_sms($intransit, 'intransit-pp');
					intransit_email($intransit);
				}
				// email-уведомление о доставке в ПВЗ
				if($pickpoint) {
					pickpoint_email($pickpoint);
				}
			}
			if($num) log_write('PickPoint: Обновлено заказов '.$num);
			else log_write('PickPoint: Доставленных заказов нет');
			tpl::append('main', '<pre>'.ob_get_clean().'</pre>');
			return ['ppapi::update_orders' => 'Доставлено заказов: '.$num];
		}
		// если данные не получены
		return ['ppapi::update_orders' => 'Данные не получены'];
	}
	
	static function filter_orders($orders) {
		$filtered = [];
		foreach($orders as $order) {
			$order_id = $order['SenderInvoiceNumber'];
			if(strlen($order_id) > 4) continue;
			if(isset($filtered[$order_id])) {
				$order_set = $filtered[$order_id];
				$ddate1 = DateTime::createFromFormat('d.m.y H:i', $order_set['ChangeDT']);
				$ddate2 = DateTime::createFromFormat('d.m.y H:i', $order['ChangeDT']);
				if($ddate2->getTimestamp() > $ddate1->getTimestamp()) {
					$filtered[$order_id] = $order;
				}
			}
			else {
				$filtered[$order_id] = $order;
			}
		}
		return $filtered;
	}
	
}

?>