<?php

set_time_limit(0);

$RR = FALSE;

if(isset($args[1]) && $args[1] == 'office') {
	$filename = 'yml-office.xml';
	$sql = 'select *, (select max(category_id) from product_category where product_id = p.id) as cid, (select name from product_groups where rel = p.rel) as rel_name from product p where status > 0 and quantity > 0 and stock = 2 and name not like \'%Подписка%\' and id != 540 order by id';
}
else {
	if(isset($args[1]) && $args[1] == 'rr') {
		$RR = TRUE;
		$filename = 'yml-rr.xml';
	}
	else {
		$filename = 'yml.xml';
	}
	$sql = 'select *, (select max(category_id) from product_category where product_id = p.id) as cid, (select name from product_groups where rel = p.rel) as rel_name from product p where status > 0 and name not like \'%Подписка%\' and id != 540 order by id';
}

if(!function_exists('make_yml_offer')) {
	function make_yml_offer($product, $images) {
		if($product['stock'] > 2) $product['quantity'] = intval($product['stock_quantity']);
		if(preg_match('#(\[.*?\]\s*)#', $product['name'], $match)) {
			$product['name'] = str_replace($match[1], '', $product['name']);
		}
		array_walk($product, function(&$val) {
			$val = html2chars($val);
		});
		tpl::push($product);
		$available = $product['stock'] > 1;
		tpl::set('available', $available ? 'true' : 'false');
		if($product['weight']) tpl::make('yml-offer-weight');
		if($product['volume']) tpl::make('yml-offer-volume');
		if($product['spf']) tpl::make('yml-offer-spf');
		if(isset($product['group_id'])) {
			tpl::make('yml-offer-group');
		}
		$i = 0;
		foreach($images as $image) {
			tpl::set('picture', $image);
			tpl::make('yml-offer-picture');
			$i++; if($i == 10) break;
		}
		if($product['rel_name']) {
			tpl::make('yml-offer-rel');
		}
		if($product['price_old']) {
			tpl::make('yml-offer-oldprice');
		}
		if(!preg_match('#[а-я]#ui', $product['option_name']) && preg_match('#\d+#', $product['option_name'])) {
			tpl::make('yml-offer-color');
		}
		tpl::set('sex', $product['cid'] == 6 ? 'Мужской' : 'Женский');
		tpl::make('yml-offer');
		tpl::clear('yml-offer-picture', 'yml-offer-volume', 'yml-offer-weight', 'yml-offer-spf', 'yml-offer-group', 'yml-offer-rel', 'yml-offer-oldprice', 'yml-offer-color');
	}
}

if(!function_exists('makeCategories')) {
	function makeCategories($categories) {
		foreach($categories as $cat) {
			tpl::set('category-id', $cat['id']);
			tpl::set('category-name', $cat['name']);
			if($cat['parent_id']) {
				tpl::set('category-parent', $cat['parent_id']);
				tpl::make('yml-category-p');
			}
			tpl::make('yml-category');
			tpl::clear('yml-category-p');
		}
	}
}

tpl::load('yml');
tpl::set('yml-date', date('Y-m-d H:i'));
$categories = shop::getCategories();
makeCategories($categories);
$DEFAULT_CATEGORY_MARKET = 'Все товары / Одежда, обувь и аксессуары / Красота';
$market_categories = json::get('yandex_categories');
// $json = json::encode($market_categories);
// file_put_contents(JSON.'/yandex_categories.json', $json);
db::query('select * from product_option order by (quantity = 0), price');
$options = db::fetchAll();
$options = array_group($options, 'product_id');
$images = get_files(FILES.'/product/', '#.*\.jpg$#');
sort($images);
db::query($sql);
$num_offers = 0;
$offers_instock = 0;
while($product = db::fetchArray()) {
	if(!$cid = absint($product['cid'])) continue;
	if(!isset($categories[$cid])) continue;
	$market_category = @$market_categories[$cid] ?: $DEFAULT_CATEGORY_MARKET;
	$product['market_category'] = $market_category;
	if(isset($options[$product['id']])) {
		$product_options = $options[$product['id']];
		$orig_id = $product['id'];
		$orig_name = $product['name'];
		$url_name = $product['url_name'];
		foreach($product_options as $option) {
			if(!$option['price']) continue;
			$img_dir = $RR ? '/options/thumbs/' : '/options/';
			$img_path = $img_dir.$option['option_id'].'.jpg';
			if(!file_exists(FILES.$img_path) || !$option['status'] || $option['stock'] <= 1) continue;
			$imglist = [x::config('SITE_URL').'/files'.$img_path];
			$product['id'] = $orig_id.'00'.$option['id'];
			$product['spf'] = $option['spf'];
			$product['price'] = $option['price'];
			$product['price_old'] = $option['price_old'];
			$product['quantity'] = $option['quantity'];
			$product['weight'] = $option['weight'];
			$product['volume'] = $option['volume'];
			$product['name'] = $orig_name.' - '. ($option['name'] ?: 'M'.substr($option['barcode'], -4, 4));
			$product['text'] = preg_replace('#<.+?>#msi', '', $product['text']);
			$product['text'] = preg_replace('#&\wdash;#si', '-', $product['text']);
			$product['text'] = preg_replace('#&[a-z]+?;#si', ' ', $product['text']);
			$product['url_name'] = $url_name . '?option_id=' . $option['id'];
			$product['group_id'] = $orig_id.'00';
			$product['url_name'] = $url_name;
			$product['option_name'] = $option['name'];
			make_yml_offer($product, $imglist);
			$num_offers += 1;
			if($option['stock'] > 1) $offers_instock += 1;
		}
	}
	else {
		if(!$product['price']) continue;
		$imglist = [];
		foreach($images as $image) {
			$imgname = finfo($image, 'filename');
			if(!preg_match('#^('.$product['id'].')?(\-\d+)$#', $imgname)) continue;
			$img_dir = $RR ? '/files/product/thumbs/' : '/files/product/';
			$imglist[] = x::config('SITE_URL').$img_dir.$imgname.'.jpg';
		}
		$product['text'] = preg_replace('#<.+?>#msi', '', $product['text']);
		$product['text'] = preg_replace('#&\wdash;#si', '-', $product['text']);
		$product['text'] = preg_replace('#&[a-z]+?;#si', ' ', $product['text']);
		$product['option_name'] = '';
		make_yml_offer($product, $imglist);
		$num_offers += 1;
		if($product['stock'] > 1) $offers_instock += 1;
	}
}

$yml = tpl::make('yml');
$yml_file = FILES.'/'.$filename;
file_put_contents($yml_file, $yml);
// write log
$log_file = FILES.'/yml.log';
$log_line = sprintf("%s %s %s\n", date("Y-m-d H:i:s"), $filename, $offers_instock.'|'.$num_offers);
file_put_contents($log_file, $log_line, FILE_APPEND);

if(!defined('ROUTINE')) { // Вызов по псевдо-ссылке, отдаем контент
	header('Content-Type: text/xml');
	echo $yml;
	exit;
}

?>