<?php

class product {
	
	const DEFAULT_IMAGE = '/files/no_image.jpg';
	
	static function getImage($product) {
		$id = is_numeric($product) ? $product : $product['id'];
		$filename = '/files/product/thumbs/'.$id.'-1.jpg';
		if(file_exists(ROOT.$filename)) return $filename;
		$filename = '/files/product/'.$id.'-1.jpg';
		if(file_exists(ROOT.$filename)) return $filename;
		return static::DEFAULT_IMAGE;
	}
	
	static function updateImage() {
		db::prepare('update product set image = 1 where image = 0 and id = :id');
		$dir = dir(FILES.'/product/');
		while($file = $dir->read()) {
			$filename = finfo($file, 'filename');
			if(!preg_match('#\d+\-\d+#', $filename)) continue;
			list($id, $n) = explode('-', $filename);
			db::set('id', $id);
			db::execute();
		}
		$dir->close();
		db::prepare('update product_option set image = 1 where image = 0 and option_id = :option_id');
		$dir = dir(FILES.'/variations/');
		while($file = $dir->read()) {
			$filename = finfo($file, 'filename');
			if(!preg_match('#\d+\-\d+#', $filename)) continue;
			db::set('option_id', $filename);
			db::execute();
		}
		$dir->close();
	}
	
	static function getReviewData($product_id, $limit = 0) {
		$sql = 'select * from product_review where status = 1 and product_id = ? order by ctime desc';
		$limit = absint($limit);
		if($limit > 0) $sql .= ' limit 0, '.$limit;
		db::query($sql, $product_id);
		$reviews = db::fetchAll();
		$count = db::count();
		$avg = $recommends = $recommend_rate = 0;
		$stats = [5=>0, 4=>0, 3=>0, 2=>0, 1=>0];
		foreach($reviews as $review) {
			$avg += $review['rating'];
			$recommends += $review['recommend'];
			$stats[$review['rating']]++;
		}
		if($avg) $avg = round($avg / $count, 1);
		if($recommends) $recommend_rate = round($recommends/$count*100);
		return array('count'=>$count, 'avg'=>$avg, 'recommends'=>$recommends, 'recommend_rate'=>$recommend_rate, 'reviews'=>$reviews, 'stats'=>$stats);
	}
	
	static function getOptions($pid) {
		db::query('select * from product_option where product_id = ? order by stock = 1, price asc, name asc', $pid);
		$options = [];
		foreach(db::fetchAll() as $option) {
			if(!$option['name']) $option['name'] = 'option-'.$option['id'];
			$options[$option['id']] = $option;
		}
		return $options;
	}
	
	static function getRelatives($id) {
		db::query('select '.CATALOG_SELECT.' from product p where barcode != \'0000000000000\' and rel > 0 and rel = (select rel from product where id = ?) and status > 0 and stock > 1 order by price desc', $id);
		return db::fetchAll();
	}
	
	static function get_recommended($id) {
		$key = 'product-recommended-'.$id;
		if(!$recommended = cache::get($key)) {
			db::query('select '.CATALOG_SELECT.' from product p where status > 0 and stock > 1 and id != :product_id and id in(select product_id from product_category where category_id in(select category_id from product_category where product_id = :product_id)) order by rand() limit 10', ['product_id' => $id]);
			$recommended = db::fetchAll();
			cache::set($key, $recommended, 86400);
		}
		return $recommended;
	}
	
	static function get_viewed($cookie, $product_id = 0) {
		$sql = 'select '.CATALOG_SELECT.', (select utime from product_view where product_id = p.id and cookie = :cookie) as utime from product p where status > 0 and stock > 1 and id in (select distinct product_id from product_view where cookie = :cookie)';
		$params = ['cookie' => $cookie];
		if($product_id) {
			$sql .= ' and id != :product_id';
			$params['product_id'] = $product_id;
		}
		$sql .= ' order by utime desc limit 5';
		db::query($sql, $params);
		if(db::count() < 5) return false;
		$products = db::fetchAll();
		return $products;
	}
	
	static function getVideo($productId) {
		$video = [];
		db::query('select video_url from product_video where product_id = ?', $productId);
		while($row = db::fetchArray()) {
			$video[] = $row['video_url'];
		}
		return $video;
	}
	
	static function getStockId($product_id) {
		$manufacturer = db::querySingle('select manufacturer from product where id = '.$product_id);
		foreach(shop::get_brands() as $brand) {
			if($brand['name'] == $manufacturer) return $brand['stock'];
		}
		return 2;
	}
	
	static function import_csv($file, $manufacturer) {
		$f = fopen($file, 'r');
		
		// Array ( [0] => id [1] => option_id [2] => barcode [3] => artikul [4] => option_name [5] => name [6] => short_text [7] => spf [8] => volume [9] => price [10] => categories [11] => price_buyin )
		
		function _set_categories($line) {
			$product_id = intval($line[0]);
			$categories = explode(',', $line[10]);
			foreach($categories as $category_id) {
				$category_id = intval($category_id);
				db::query('insert ignore into product_category set product_id = ?, category_id = ?', $product_id, $category_id);
			}
		}
		
		function _import_product($line) {
			$url_name = str::str2url($line[6].' '.$line[5]);
			$product_id = intval($line[0]);
			$text = db::querySingle('select text from product where id = '.$product_id);
			$data = [
				'id' => $product_id,
				'barcode' => $line[1] ? '0000000000000' : ($line[2] ?: NULL),
				'name' => $line[5],
				'url_name' => $url_name,
				'short_text' => $line[6],
				'page_title' => $line[6].' '.$line[5],
				'volume' => correct_float($line[8]) ?: NULL,
				'spf' => $line[7] ?: NULL,
				'price' => intval($line[9]),
				'price_buyin' => $line[1] ? 0 : floatval(@$line[11]),
				'artikul' => $line[1] ? NULL : ($line[3] ?: NULL),
				'manufacturer' => $line[12],
				'text' => $text ?: NULL
			];
			db::query('replace into product set id = :id, barcode = :barcode, name = :name, url_name = :url_name, short_text = :short_text, page_title = :page_title, volume = :volume, spf = :spf, price = :price, price_buyin = :price_buyin, artikul = :artikul, manufacturer = :manufacturer, status = 0, text = :text', $data);
		}
		
		function _import_option($line) {
			$data = [
				'option_id' => $line[1],
				'barcode' => $line[2] ?: NULL,
				'name' => $line[4],
				'product_id' => intval($line[0]),
				'price' => intval($line[9]),
				'price_buyin' => floatval(@$line[11]),
				'volume' => correct_float($line[8]) ?: NULL,
				'spf' => $line[7] ?: NULL,
				'artikul' => $line[3] ?: NULL
			];
			db::query('replace into product_option set option_id = :option_id, barcode = :barcode, name = :name, status = 1, product_id = :product_id, price = :price, price_buyin = :price_buyin, volume = :volume, spf = :spf, artikul = :artikul', $data);
		}
		
		$product_id = 0;
		$add = $skip = 0;
		
		while($line = fgetcsv($f, 9999, '|')) {
			array_walk($line, 'trim');
			if(count($line) < 11 || !correct_float($line[9])) {
				$skip++;
				continue;
			}
			$add++;
			$line[12] = $manufacturer;
			$id = intval($line[0]);
			if($id && $line[1]) {
				$product_id = $id;
				_import_product($line);
				_import_option($line);
				_set_categories($line);
			}
			elseif($line[1]) {
				$line[0] = $product_id;
				_import_option($line);
			}
			elseif($id) {
				_import_product($line);
				_set_categories($line);
			}
		}
		fclose($f);
		shop::update_stock_from_import();
		return ['add' => $add, 'skip' => $skip];
	}
	
	static function update_from_csv($file) {
		$f = fopen($file, 'r');
		$result = ['products' => 0, 'options' => 0];
		while($line = fgetcsv($f, 9999, "\t")) {
			$id = trim($line[0]) ?: NULL;
			$name = trim($line[1]) ?: NULL;
			$option_name = trim($line[2]) ?: NULL;
			$barcode = trim($line[3]) ?: NULL;
			$artikul = trim($line[4]) ?: NULL;
			$price_buyin = correct_float($line[5]);
			$price = absint($line[6]);
			$price_old = absint($line[7]) ?: NULL;
			if(!$id || !preg_match('#(\d+)?\-?(\d+)?#', $id, $match)) continue;
			if(isset($match[2])) {
				$sql = 'update product_option set name = :name, barcode = :barcode, artikul = :artikul, price = :price, price_old = :price_old, price_buyin = :price_buyin where option_id = :option_id';
				$data = [
					'name' => $option_name,
					'barcode' => $barcode,
					'artikul' => $artikul,
					'price' => $price,
					'price_old' => $price_old,
					'price_buyin' => $price_buyin,
					'option_id' => $id
				];
				db::query($sql, $data);
				if(db::count()) $result['options']++;
			}
			else {
				$sql = 'update product set name = :name, barcode = :barcode, artikul = :artikul, price = :price, price_old = :price_old, price_buyin = :price_buyin where id = :id';
				$data = [
					'name' => $name,
					'barcode' => $barcode,
					'artikul' => $artikul,
					'price' => $price,
					'price_old' => $price_old,
					'price_buyin' => $price_buyin,
					'id' => $id
				];
				db::query($sql, $data);
				if(db::count()) $result['products']++;
			}
		}
		fclose($f);
		return $result;
	}
	
	static function update_filters_from_csv($file) {
		$f = fopen($file, 'r');
		$result = ['products' => 0, 'options' => 0];
		$filters = [
			3 => 11,
			4 => 12,
			5 => 13,
			6 => 15,
			7 => 61,
			8 => 62,
			9 => 63,
			10 => 64,
			11 => 21,
			12 => 22,
			13 => 23,
			14 => 24
		];
		fgetcsv($f); // skip headers
		while($line = fgetcsv($f, 9999, "\t")) {
			$oid = strpos($line[0], '-') ? $line[0] : 0;
			preg_match('#^(\d+)#', $line[0], $match);
			$pid = $match[1];
			db::query('delete from product_filters where product_id = ? and option_id = ifnull((select id from product_option where option_id = ?),0)', $pid, $oid);
			$changes = false;
			$sql = $oid ? 'insert into product_filters set product_id = :pid, option_id = ifnull((select id from product_option where option_id = :oid),0), filter_id = :fid' : 'insert into product_filters set product_id = :pid, option_id = :oid, filter_id = :fid';
			db::prepare($sql);
			foreach($line as $key => $value) {
				if($key < 3 || $key > 14) continue;
				if(!trim($value)) continue;
				$fid = $filters[$key];
				db::set(['pid'=>$pid, 'oid'=>$oid, 'fid'=>$fid]);
				db::execute();
				$changes = true;
			}
			if($changes) {
				if($oid > 0) $result['options']++;
				else $result['products']++;
			}
		}
		fclose($f);
		return $result;
	}
	
	static function update_invent($inputFileName) {
		new phpexcel();
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		
		$products = [];
		for($row = 1; $row <= $highestRow; ++$row) {
			$barcode = $objWorksheet->getCellByColumnAndRow(3, $row)->getFormattedValue();
			if(!preg_match('/\d{13}/', $barcode)) continue;
			$quantity = $objWorksheet->getCellByColumnAndRow(6, $row)->getFormattedValue();
			$quantity = intval($quantity);
			if(isset($products[$barcode])) {
				if($quantity > $products[$barcode]) $products[$barcode] = $quantity;
			}
			else {
				$products[$barcode] = $quantity;
			}
		}
		db::query('update product set quantity = 0 where id != 540');
		db::query('update product_option set quantity = 0');
		db::prepare('update product set quantity = :quantity where barcode = :barcode');
		$p = 0;
		foreach($products as $barcode => $quantity) {
			db::set([
				'barcode' => $barcode,
				'quantity' => $quantity
			]);
			db::execute();
			$p += db::count();
		}
		db::prepare('update product_option set quantity = :quantity where barcode = :barcode');
		$po = 0;
		foreach($products as $barcode => $quantity) {
			db::set([
				'barcode' => $barcode,
				'quantity' => $quantity
			]);
			db::execute();
			$po += db::count();
		}
		return ['products' => $p, 'options' => $po];
	}
	
	static function update_quantity() {
		db::query('update product p set quantity = (select sum(quantity) from product_option where product_id = p.id) where (select count(*) from product_option where product_id = p.id) > 0');
		$result = ['product.quantity' => db::count()];
		db::query('update product p set reserve = (select sum(reserve) from product_option where product_id = p.id) where (select count(*) from product_option where product_id = p.id) > 0');
		$result['product.reserve'] = db::count();
		db::query('update product p set stock_quantity = if(barcode like \'000%\', NULL, (select max(quantity) from product_import where barcode = p.barcode))');
		$result['product.stock_quantity'] = db::count();
		db::query('update product_option po set stock_quantity = (select max(quantity) from product_import where barcode = po.barcode)');
		$result['product_option.stock_quantity'] = db::count();
		
		return ['updateProductQuantity' => $result];
	}
	
	static function resize_photo() {
		if(!is_dir(FILES.'/product/thumbs')) mkdir(FILES.'/product/thumbs');
		$files = get_files(FILES.'/product/', '/\.jpg$/i');
		ob_start();
		$img = new SimpleImage();
		foreach($files as $file) {
			$dest = str_replace('/product/', '/product/thumbs/', $file);
			if(file_exists($dest)) continue;
			$img->load($file)->resize(200, 200)->save($dest);
			print($file."\n");
		}
		if(!is_dir(FILES.'/options/thumbs')) mkdir(FILES.'/options/thumbs');
		$files = get_files(FILES.'/options/', '/\.jpg$/i');
		foreach($files as $file) {
			$dest = str_replace('/options/', '/options/thumbs/', $file);
			if(file_exists($dest)) continue;
			$img->load($file)->resize(200, 200)->save($dest);
			print($file."\n");
		}
		$out = ob_get_clean();
		if(AJAX) die($out ?: 'Нет новых изображений');
		return $out;
	}
	
	static function update_view() {
		if(!AJAX) die('AJAX ONLY');
		if(!$product_id = absint(REQUEST('product_id'))) die('ID REQUIRED');
		ob_start();
		db::query('select views from product_view where cookie = ? and product_id = ? and option_id = 0', account::$cookie, $product_id);
		if($views = db::fetchSingle()) {
			db::query('update product_view set views = ? where cookie = ? and product_id = ? and option_id = 0', $views+1, account::$cookie, $product_id);
			echo "update: ".$views+1;
		}
		else {
			db::query('insert ignore into product_view set cookie = ?, product_id = ?, option_id = 0, user_id = ?, views = 1', account::$cookie, $product_id, account::$info['id']);
			echo "insert ok";
		}
		$response = ob_get_clean();
		die($response);
	}
	
	static function import_supplier_data() {
		$stock_list = shop::get_stock_list();
		$files = get_files(FILES.'/import/');
		db::query('truncate table product_import');
		db::prepare('insert ignore into product_import set barcode = :barcode, artikul = :artikul, name = :name, stock = :stock, quantity = :quantity, price_usd = :price_usd, filename = :filename');
		$result = [];
		foreach($files as $file) {
			if(!$f = @fopen($file, 'r')) {
				continue;
			}
			$filename = finfo($file, 'filename');
			$filename = strtolower($filename);
			if($filename == 'holika-andreev (txt)' && $stock_list[3]['status']) {
				$result['holika'] = 0;
				while($line = fgetcsv($f, 9999, "\t", '"', '\\')) {
					$cols = count($line);
					if($cols < 9) continue;
					if(!preg_match('#(\d{13,13})#', $line[2], $match)) continue;
					$barcode = $match[1];
					$artikul = trim($line[0]);
					$name = trim($line[1]);
					$quantity = preg_replace('/[^0-9]/s', '', $line[5]) ?: 0;
					$quantity = $quantity / 1000;
					$quantity += (preg_replace('/[^0-9]/s', '', $line[7]) ?: 0) / 1000;
					$price = preg_replace('#[^\d\.,]#s', '', $line[3]);
					$price_usd = correct_float($price);
					$stock = 3;
					$data = [
						'barcode' => $barcode,
						'artikul' => $artikul,
						'name' => $name,
						'stock' => $stock,
						'quantity' => $quantity,
						'price_usd' => $price_usd,
						'filename' => $filename
					];
					db::set($data);
					if(!db::execute()) {
						print_r($line);
						exit(db::error());
					}
					$error = db::error(true);
					if(intval($error[0])) {
						return $error[2];
					}
					$result['holika'] += db::count();
				}
			}
			elseif($filename == 'novacia' && $stock_list[4]['status']) {
				$maxq = [];
				$result['novacia'] = 0;
				while($line = fgetcsv($f, 9999, "\t")) {
					$cols = count($line);
					if($cols < 3) continue;
					if(!preg_match('#(\d{13,13})#', $line[0], $match)) continue;
					$barcode = $match[1];
					$artikul = NULL;
					$name = trim($line[1]);
					$q_index = 2;
					$quantity = trim($line[$q_index]);
					$quantity = explode(',', $quantity)[0];
					$quantity = preg_replace('/[^0-9]/s', '', $quantity);
					$quantity = intval($quantity);
					$price_usd = preg_replace('#([^0-9])#', '', $line[3]);
					$stock = 4;
					$data = [
						'barcode' => $barcode,
						'artikul' => $artikul,
						'name' => $name,
						'stock' => $stock,
						'quantity' => $quantity,
						'price_usd' => $price_usd,
						'filename' => $filename
					];
					db::set($data);
					db::execute();
					if(!isset($maxq[$barcode]) || $quantity > $maxq[$barcode]) {
						$maxq[$barcode] = $quantity;
					}
					$error = db::error(true);
					if(intval($error[0])) {
						return $error[2];
					}
					$result['novacia'] += db::count();
				}
			}
			elseif($filename == 'missha') {
				// файл есть, но никогда не обновляется, поэтому наличие определяется по штрих-коду
				// если шк содержит 0 в начале (выставляется вручную), значит его нет на складе
				// обработка в shop::update_stock_from_import()
			}
			elseif(in_array($filename, ['royalskin', 'sharashara', 'skinhouse', 'ahc']) && $stock_list[6]['status']) {
				$result['skin79_others'] = 0;
				while($line = fgetcsv($f, 9999, "\t")) {
					$barcode = $line[0];
					$quantity = intval($line[1]);
					if(!$barcode) continue;
					$data = [
						'barcode' => $barcode,
						'name' => NULL,
						'stock' => 6,
						'quantity' => $quantity,
						'price_usd' => 0,
						'filename' => $filename
					];
					db::set($data);
					db::execute();
					$error = db::error(true);
					if(intval($error[0])) {
						return $error[2];
					}
					$result['skin79_others'] += db::count();
				}
			}
			elseif($filename == 'yuliakim' && $stock_list[7]['status']) {
				$result['yuliakim'] = 0;
				while($line = fgetcsv($f, 9999, "\t")) {
					if(!$line || count($line) < 5) continue;
					if(!preg_match('#(\d{13,13})#', $line[2], $match)) continue;
					$barcode = $match[1];
					$artikul = trim($line[0]);
					$name = trim($line[1]);
					$price = preg_replace('#([^0-9])#', '', $line[3]);
					$quantity = intval($line[4]);
					if(!$barcode) continue;
					$data = [
						'artikul' => $artikul,
						'barcode' => $barcode,
						'name' => $name,
						'stock' => 7,
						'quantity' => $quantity,
						'price_usd' => $price,
						'filename' => $filename
					];
					db::set($data);
					db::execute();
					$error = db::error(true);
					if(intval($error[0])) {
						return $error[2];
					}
					$result['yuliakim'] += db::count();
				}
			}
		}
		if($stock_list[5]['status']) $result += import::missha();
		if($stock_list[6]['status']) $result += import::skin79_txt();
		if($stock_list[8]['status']) $result += import::lanixm();
		if(isset($maxq)) {
			db::prepare('update product_import set quantity = ? where barcode = ?');
			foreach($maxq as $barcode => $quantity) {
				db::set([$quantity, $barcode]);
				db::execute();
			}
		}
		return ['import_supplier_data' => $result];
	}
	
	static function update_price_buyin_from_import() {
		db::query('update product p set price_buyin = ifnull((select max(price_usd) from product_import where barcode = p.barcode and stock in(2,3,4,5,6,7,8)), price_buyin) where barcode not like ?', '000%');
		$result['update_price.products'] = db::count();
		db::query('update product_option po set price_buyin = ifnull((select max(price_usd) from product_import where barcode = po.barcode and stock in(2,3,4,5,6,7,8)), price_buyin)');
		$result['update_price.options'] = db::count();
		db::query('update product set price_buyin = NULL where price_buyin = 0');
		db::query('update product_option set price_buyin = NULL where price_buyin = 0');
		return $result;
	}
	
	static function update_artikul_from_import() {
		db::query('update product p set artikul = ifnull((select max(artikul) from product_import where barcode = p.barcode), artikul) where barcode not like ?', '000%');
		$result['update_artikul.products'] = db::count();
		db::query('update product_option po set artikul = ifnull((select max(artikul) from product_import where barcode = po.barcode), artikul)');
		$result['update_artikul.options'] = db::count();
		return $result;
	}
	
	// обновление кол-ва товаров после отправления заявки на склад
	static function updateStock($stock_id, $items) {
		$products = $options = [];
		foreach($items as $item) {
			if($item['option_id']) {
				$oid = $item['option_id'];
				$options[$oid] = $item['quantity'];
			}
			else {
				$pid = $item['id'];
				$products[$pid] = $item['quantity'];
			}
		}
		db::prepare('update product set quantity = if(quantity - ? < 0, 0, quantity - ?) where id = ?');
		foreach($products as $productId => $quantity) {
			db::set([$quantity, $quantity, $productId]);
			db::execute();
		}
		db::prepare('update product_option set quantity = if(quantity - ? < 0, 0, quantity - ?) where id = ?');
		foreach($options as $optionId => $quantity) {
			db::set([$quantity, $quantity, $optionId]);
			db::execute();
		}
		shop::update_stock_from_import();
	}
	
	static function export_for_1c() {
		db::query('select '.CATALOG_SELECT.' from product p where status = 1 and barcode is not null order by id');
		$products = db::fetchAll();
		db::query('select * from product_option po where barcode is not null order by product_id, option_id');
		$options = db::fetchAll();
		$options = array_group($options, 'product_id');
		$brands = shop::get_brands();
		$file = FILES.'/1c/goods.csv';
		$num = 0;
		$columns = ['id','option_id','barcode','name','short_text','price','price_buyin','manufacturer','weight','stock','stock_quantity', 'quantity'];
		$f = fopen($file, 'w+');
		fputcsv($f, $columns, "\t");
		foreach($products as $product) {
			$stock_id = 1;
			foreach($brands as $id => $brand) {
				if($brand['name'] == $product['manufacturer']) {
					$stock_id = $brand['stock'];
					break;
				}
			}
			if($po = @$options[$product['id']]) {
				foreach($po as $option) {
					$line = [
						$product['id'],
						$option['id'],
						$option['barcode'],
						$product['name'].' - '.$option['name'],
						$product['short_text'],
						$option['price'],
						$option['price_buyin'],
						$product['manufacturer'],
						$option['weight'],
						$stock_id,
						$option['stock_quantity'],
						$option['quantity']
					];
					fputcsv($f, $line, "\t");
					$num++;
				}
			}
			else {
				if($product['barcode'] == '0000000000000') continue;
				$line = [
					$product['id'],
					'', // option_id
					$product['barcode'],
					$product['name'],
					$product['short_text'],
					$product['price'],
					$product['price_buyin'],
					$product['manufacturer'],
					$product['weight'],
					$stock_id,
					$product['stock_quantity'],
					$product['quantity']
				];
				fputcsv($f, $line, "\t");
				$num++;
			}
		}
		fclose($f);
		// stock list
		$file = FILES.'/1c/stock.csv';
		$f = fopen($file, 'w+');
		fputcsv($f, ['stock_id', 'stock_name'], "\t");
		foreach(shop::get_stock_list() as $stock_id => $stock) {
			$line = [$stock_id, $stock['name']];
			fputcsv($f, $line, "\t");
		}
		fclose($f);
		return ['product::export_for_1c' => $num];
	}
	
	static function export_for_invent() {
		db::query('select id, name, barcode, price, price_buyin from product p where status = 1 order by id');
		$products = db::fetchAll();
		db::query('select product_id, name, barcode, price, price_buyin from product_option po where status = 1');
		$options = db::fetchAll();
		$options = array_group($options, 'product_id');
		$brands = shop::get_brands();
		$num = 0;
		$file = FILES.'/temp/moysklad-goods.csv';
		$f = fopen($file, 'w+');
		$columns = ['Наименование','Артикул','Цена продажи','Закупочная цена','Штрихкод EAN13'];
		array_walk($columns, function(&$val) {
			$val = mb_convert_encoding($val, 'CP1251', 'UTF-8');
		});
		fputcsv($f, $columns, ';');
		foreach($products as $product) {
			if($po = @$options[$product['id']]) {
				foreach($po as $option) {
					$name = str_replace(',', '.', $product['name']) .' - '. str_replace(',', '.', $option['name']);
					$line = [
						$name,
						$option['barcode'],
						$option['price'],
						$option['price_buyin'],
						$option['barcode']
					];
					array_walk($line, function(&$val) {
						$val = mb_convert_encoding($val, 'CP1251', 'UTF-8');
					});
					fputcsv($f, $line, ';');
					$num++;
				}
			}
			else {
				if(str::startsWith($product['barcode'], '000')) continue;
				$line = [
					str_replace(',', '.', $product['name']),
					$product['barcode'],
					$product['price'],
					$product['price_buyin'],
					$product['barcode']
				];
				array_walk($line, function(&$val) {
						$val = mb_convert_encoding($val, 'CP1251', 'UTF-8');
					});
				fputcsv($f, $line, ';');
				$num++;
			}
		}
		fclose($f);
	}
	
	static function define_margintop() {
		// select categories level 2
		$lvl2_str = db::querySingle('select group_concat(id) from categories where status = 1 and parent_id in(select id from categories where parent_id = 0)');
		$lvl2 = explode(',', $lvl2_str);
		// put products into table
		db::query('truncate table product_margintop');
		foreach($lvl2 as $cid) {
			$cat_ids = shop::concat_categories($cid);
			db::query('select id, (price - price_buyin) as margin from product p where status = 1 and stock > 1 and not exists(select 1 from product_option po where product_id = p.id) and id in (select product_id from product_category where category_id in ('.$cat_ids.'))');
			$products = db::fetchAll();
			db::query('select id, product_id, (price - price_buyin) as margin from product_option po where status = 1 and stock > 1 and product_id in (select product_id from product_category where category_id in ('.$cat_ids.'))');
			$options = db::fetchAll();
			db::prepare('insert into product_margintop set category_id = :cid, product_id = :pid, option_id = :oid, margin = :margin');
			foreach($products as $product) {
				db::set([
					'cid' => $cid,
					'pid' => $product['id'],
					'oid' => 0,
					'margin' => $product['margin']
				]);
				db::execute();
			}
			foreach($options as $option) {
				db::set([
					'cid' => $cid,
					'pid' => $option['product_id'],
					'oid' => $option['id'],
					'margin' => $option['margin']
				]);
				db::execute();
			}
		}
		// select limited by 10 for each category
		$cat_str = db::querySingle('select group_concat(distinct category_id) from product_margintop');
		$cat_ids = explode(',', $cat_str);
		$sql = [];
		$sql_tpl = '(select * from product_margintop where category_id = %s order by margin desc limit 10)';
		foreach($cat_ids as $cid) {
			$sql[] = sprintf($sql_tpl, $cid);
		}
		$sql = implode(' UNION ALL ', $sql);
		db::query($sql);
		$rows = db::fetchAll();
		// clear table
		db::query('truncate table product_margintop');
		// and put back the result
		db::prepare('insert into product_margintop set category_id = :category_id, product_id = :product_id, option_id = :option_id, margin = :margin');
		foreach($rows as $row) {
			db::set($row);
			db::execute();
		}
		return ['product::define_margintop' => db::querySingle('select count(*) from product_margintop')];
	}
	
	static function get_margintop() {
		$sql = 'select * from (select * from product_margintop order by rand()) as shuffled group by category_id';
		db::query($sql);
		$rows = db::fetchAll();
		$margintop = $pids = $products = [];
		foreach($rows as $row) {
			$margintop[$row['category_id']] = $row['product_id'];
			if(!in_array($row['product_id'], $pids)) $pids[] = $row['product_id'];
		}
		db::query('select '.CATALOG_SELECT.' from product p');
		while($product = db::fetchArray()) {
			$products[$product['id']] = $product;
		}
		array_walk($margintop, function(&$value) use($products) {
			$value = $products[$value];
		});
		return $margintop;
	}
	
	static function update_product_sum() {
		$sql = 'select cart_items from orders where user_id != 1 and status in(0,2,3,4,8) and ctime >= ?';
		$ctime = date('Y-m-d H:i:s', strtotime('today - 3 months'));
		db::query($sql, $ctime);
		$items = [];
		while($order = db::fetchArray()) {
			$cart_items = json::decode($order['cart_items']);
			if(!$cart_items) continue;
			foreach($cart_items as $item) {
				$key = $item['id'];
				if($item['option_id']) $key .= '-'.$item['option_id'];
				if(isset($items[$key])) {
					$items[$key]['summ'] += $item['price'] * $item['quantity'];
				}
				else {
					$items[$key] = [
						'pid' => $item['id'],
						'oid' => intval($item['option_id']),
						'summ' => $item['price'] * $item['quantity']
					];
				}
			}
		}
		db::prepare('replace into product_sum set pid = :pid, oid = :oid, summ = :summ');
		foreach($items as $item) {
			db::set($item);
			db::execute();
		}
		$count = db::querySingle('select count(*) from product_sum');
		return ['product::update_product_sum' => $count];
	}
	
	static function update_product_sum_top() {
		$p_count = db::querySingle('select count(*) from product p where stock > 1 and status > 0 and not exists(select 1 from product_option where product_id = p.id)');
		$o_count = db::querySingle('select count(*) from product_option where stock > 1 and status > 0');
		$total = $p_count + $o_count;
		$limit = 300;
		db::query('select ps.* from product p left join product_sum ps on p.id = ps.pid where stock > 1 and summ > 0 order by summ desc limit ?', $limit);
		$items = db::fetchAll();
		// all new items are available, others must be not
		db::query('update product_sum_top set available = 0');
		db::prepare('replace into product_sum_top set pid = :pid, oid = :oid, summ = :summ, available = 1');
		foreach($items as $item) {
			db::set($item);
			db::execute();
		}
		$count = db::querySingle('select count(*) from product_sum_top');
		return ['product::update_product_sum_top' => $count];
	}
	
	// автомат. розничные цены
	static function auto_price() {
		function _new_price($price_buyin, $rate) {
			$new_price = $price_buyin * $rate;
			$precision = $new_price >= 1000 ? -2 : -1;
			$new_price = round($new_price, $precision);
			$new_price = $new_price - ($new_price >= 1050 ? 10 : 1);
			return $new_price;
		}
		$stocks = shop::get_stock_list();
		$skip_cid = promo::get_meta()['category_id'];
		db::query('select id, price, price_old, price_buyin, (select stock from brands where name like p.manufacturer) as stock_id from product p where id != 540 and not exists(select 1 from product_option where product_id = p.id) and id not in(select distinct product_id from product_category where category_id = ?)', $skip_cid);
		$products = db::fetchAll();
		$products = array_group($products, 'stock_id');
		db::query('select id, price, price_old, price_buyin, (select stock from brands where name = (select manufacturer from product where id = po.product_id)) as stock_id from product_option po where product_id not in(select distinct product_id from product_category where category_id = ?)', $skip_cid);
		$options = db::fetchAll();
		$options = array_group($options, 'stock_id');
		db::prepare('update product set price = :price, price_old = NULL where id = :id');
		$n = 0;
		foreach($stocks as $id => $stock) {
			if(!$stock['price_rate']) continue;
			if(!isset($products[$id])) continue;
			foreach($products[$id] as $product) {
				if(!$product['price_buyin']) continue;
				$new_price = _new_price($product['price_buyin'], $stock['price_rate']);
				db::set([
					'price' => $new_price,
					'id' => $product['id']
				]);
				db::execute();
				$n += db::count();
			}
		}
		db::prepare('update product_option set price = :price, price_old = NULL where id = :id');
		foreach($stocks as $id => $stock) {
			if(!$stock['price_rate']) continue;
			if(!isset($options[$id])) continue;
			foreach($options[$id] as $option) {
				if(!$option['price_buyin']) continue;
				$new_price = _new_price($option['price_buyin'], $stock['price_rate']);
				db::set([
					'price' => $new_price,
					'id' => $option['id']
				]);
				db::execute();
				$n += db::count();
			}
		}
		// update price and price_old for products with options
		db::query('update product p set price = ifnull((select min(price) from product_option where product_id = p.id and status = 1), price) where exists(select 1 from product_option where product_id = p.id)');
		db::query('update product p set price_old = (select min(price_old) from product_option where product_id = p.id and status = 1) where exists(select 1 from product_option where product_id = p.id)');
		// instock discount
		$instock_discount_check = config::get('instock_discount_check');
		$instock_discount_pc = config::get('instock_discount_pc');
		if(intval($instock_discount_check)) {
			promo::instock_discount_apply($instock_discount_pc);
		}
		return ['product::auto_price' => $n];
	}
	
}

?>