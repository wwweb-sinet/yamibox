<?php

class QS {
	
	static $params = array();
	static $args = array();
	protected static $_args = [];
	protected static $_params = [];
	
	static function parse($url = NULL) {
		self::$params = parse_url($url ? $url : SERVER('REQUEST_URI'));
		self::$_params = self::$params;
		if(isset(self::$params['query'])) {
			parse_str(self::$params['query'], self::$args);
			self::$_args = self::$args;
		}
	}
	
	static function get($arg) {
		return isset(self::$args[$arg]) ? self::$args[$arg] : '';
	}
	
	static function set($arg, $val) {
		self::$args[$arg] = $val;
		return self::build();
	}
	
	static function remove($arg) {
		if(isset(self::$args[$arg])) unset(self::$args[$arg]);
		return self::build();
	}
	
	static function build($include_path = TRUE) {
		$qs = empty(self::$args) ? '' : '?' . http_build_query(self::$args);
		return $include_path ? self::$params['path'].$qs : $qs;
	}
	
	static function reset() {
		self::$args = self::$_args;
		self::$params = self::$_params;
	}
}

qs::parse();

?>