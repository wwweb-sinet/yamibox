<?php

class delivery {
	
	static function get_pr($zipcode, $region, $sum = 0) {
		$deliveries = [];
		$sum = $sum ?: cart::$sum_with_discounts;
		$tarif_main = $tarif_1class = 0;
		$response = web::rp_calc($zipcode, $sum);
		file_put_contents(FILES.'/rp_calc.json', $response);
		$dostavka = json::decode($response);
		if(isset($dostavka['calc'])) {
			foreach($dostavka['calc'] as $delivery) {
				$tarif = ceilby($delivery['cost'], 10);
				if($delivery['type'] == 'rp_1class') {
					$tarif_1class = $tarif;
				}
				if($delivery['type'] == 'rp_main') {
					$tarif_main = $tarif;
				}
			}
		}
		if($tarif_main) {
			$deliveries[] = [
				'dservice' => 8,
				'dtype' => 3,
				'name' => 'Почта России, посылка',
				'price' => $tarif_main + 100,
				'price_real' => $tarif_main,
				'days' => '7-14'
			];
		}
		if($tarif_1class) {
			$deliveries[] = [
				'dservice' => 8,
				'dtype' => 3,
				'name' => 'Почта России, отправление 1 класса',
				'price' => $tarif_1class + 100,
				'price_real' => $tarif_1class,
				'days' => '3-10'
			];
		}
		$central = str::is_central('', $region);
		$required_sum = $central ? 3000 : 4000;
		$free = 100000;
		foreach($deliveries as &$delivery) {
			$limit = ceilby($delivery['price'] * 10, 100);
			// apply free shipping
			if($sum >= $required_sum && $sum >= $limit) $delivery['price'] = 0;
			if($limit < $free) $free = $limit;
		}
		if($free < $required_sum) $free = $required_sum;
		$data = ['list' => $deliveries, 'free' => $free, 'zipcode' => $zipcode];
		return $data;
	}
	
	static function json($path, $type) {
		// if(!AJAX) die('AJAX ONLY');
		$city = trim(REQUEST('city')) ?: 'Москва';
		$region = trim(REQUEST('region'));
		$sum = absint(REQUEST('sum'));
		$zipcode = trim(REQUEST('zipcode'));
		$_SESSION['zipcode'] = $zipcode;
		switch($type) {
			case 'pr':
				$data = str::is_moscow($city) ? ['list'=>[]] : self::get_pr($zipcode, $region, $sum);
				break;
			case 'sl_points':
				$data = shoplogistics::get_pickups($city, $region, $sum);
				break;
			case 'sl_courier':
				$data = shoplogistics::get_courier($city, $region, $sum);
				break;
			case 'locations':
				$data = shoplogistics::get_city_list($region);
				break;
			case 'metro':
				$data = shoplogistics::get_metros($city);
				break;
		}
		die(json::encode($data));
	}
	
}

?>