<?php

defined('JSON') or define('JSON', dirname(dirname(__FILE__)).'/json');
// customized JSON ( width default options )
class JSON {
	
	protected static $data = array();
	
	static function encode($data, $options = NULL) {
		if(!$options) $options =  JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE;
		return json_encode($data, $options);
	}
	
	static function decode($data, $assoc = true, $depth = 16) {
		return json_decode($data, $assoc, $depth);
	}
	
	static function get($filename) {
		if(!isset(json::$data[$filename])) json::load($filename);
		return json::$data[$filename];
	}
	
	static function load($filename) {
		if(!self::$data) {
			self::$data = cache::get('JSON');
		}
		if(isset(self::$data[$filename])) {
			return self::$data[$filename];
		}
		if(file_exists($filename)) $file = $filename;
		else $file = JSON.DS.$filename.'.json';
		if(!file_exists($file))
			return abort('JSON :: Unable to load JSON file: ' . $file);
		$filename = finfo($file, 'filename');
		$contents = file_get_contents($file);
		json::$data[$filename] = json::decode($contents);
		cache::set('JSON', json::$data);
		return json::$data[$filename];
	}
	
	static function put($file, $data) {
		return file_put_contents($file, json::encode($data));
	}
	
	static function error() {
		switch (json_last_error()) {
			case JSON_ERROR_NONE:
				return 'No errors';
			case JSON_ERROR_DEPTH:
				return 'Maximum stack depth exceeded';
			case JSON_ERROR_STATE_MISMATCH:
				return 'Underflow or the modes mismatch';
			case JSON_ERROR_CTRL_CHAR:
				return 'Unexpected control character found';
			case JSON_ERROR_SYNTAX:
				return 'Syntax error, malformed JSON';
			case JSON_ERROR_UTF8:
				return 'Malformed UTF-8 characters, possibly incorrectly encoded';
			default:
				return 'Unknown error';
		}
	}
	
}

?>