<?php

tpl::load('intro');

tpl::set('meta-robots-rule', 'INDEX,FOLLOW');
tpl::make('meta-robots');

tpl::make('intro-features-1', 'intro-promo');

// SLIDER

$slider = slider::get();
if($autodiscount_check = config::get('autodiscount_check')) {
	$meta = promo::get_meta();
	if($meta['category_id']) {
		$href = sprintf("/catalog%s?utm_source=promo&utm_medium=index&utm_campaign=%s", $meta['category_url'], $meta['date']);
		$slide = [
			'filename' => $meta['date'].'.jpg',
			'href' => $href,
			'label' => 'Скидка 15%',
			'alt' => 'Скидка 15%',
			'sort' => 0
		];
		array_splice($slider, 0, 0, [$slide]);
	}
}
foreach($slider as $slide) {
	tpl::push($slide);
	tpl::make('intro-slide');
}
tpl::make('intro-slider', 'intro-promo');

tpl::make('intro-features-2', 'intro-promo');

$brands = shop::get_brands();
$filters = json::get('filters');

// brand logo
if(!$brands_count = cache::get('brands_count')) {
	db::query('select manufacturer, count(manufacturer) as count from product where status = 1 and stock > 1 and manufacturer is not null group by manufacturer order by count desc');
	while($brand = db::fetchArray()) {
		$brands_count[$brand['manufacturer']] = $brand['count'];
	}
	cache::set('brands_count', $brands_count);
}
$i = 0;
foreach($brands_count as $name => $count) {
	foreach($brands as $brand) {
		if(!$brand['logo'] || $brand['name'] != $name) continue;
		foreach($filters as $filterId => $text) {
			if($text == $brand['name']) break;
		}
		tpl::set('brand-name', $brand['name']);
		tpl::set('brand-href', '/catalog?brand='.$brand['url']);
		tpl::set('brand-logo-src', '/files/promo/logo/thumbs/'.$brand['logo']);
		tpl::make('intro-brand');
		$i++; if($i == 7) {
			tpl::make('intro-brands', 'intro-promo');
			break 2;
		}
	}
}

// CATALOG

if(!$new_arrivals = cache::get('new_arrivals')) {
	db::query('select '.CATALOG_SELECT.' from product p where status = 1 and (stock > 1) order by (status < 1 or stock < 2), utime desc limit 20');
	$new_arrivals = db::fetchAll();
	cache::set('new_arrivals', $new_arrivals, 1800);
}
$i = 0;
foreach($new_arrivals as $product) {
	shop::makeProduct($product, 'new-arrivals');
	$i++;
	if($i == 10) {
		tpl::make('new-arrivals-slide');
		tpl::clear('new-arrivals');
	}
}
tpl::make('new-arrivals-slide');

if(!$bestsellers = cache::get('bestsellers')) {
	db::query('select '.CATALOG_SELECT.' from product p where status = 1 and stock > 1 and (select count(*) from product_review where product_id = p.id) > 10 order by rand() limit 20');
	$bestsellers = db::fetchAll();
	cache::set('bestsellers', $bestsellers, 1800);
}
$i = 0;
foreach($bestsellers as $product) {
	shop::makeProduct($product, 'bestsellers');
	$i++;
	if($i == 10) {
		tpl::make('bestsellers-slide');
		tpl::clear('bestsellers');
	}
}
tpl::make('bestsellers-slide');

if(!$promoted = cache::get('promoted')) {
	$sql = 'select '.CATALOG_SELECT.' from product p where status = 1 and stock > 1';
	$sql .= ' and id in (select product_id from product_category where category_id = 62)';
	$sql .= ' order by rand() limit 20';
	db::query($sql);
	$promoted = db::fetchAll();
	cache::set('promoted', $promoted, 1800);
}
$i = 0;
foreach($promoted as $product) {
	shop::makeProduct($product, 'promoted');
	$i++;
	if($i == 10) {
		tpl::make('promoted-slide');
		tpl::clear('promoted');
	}
}
tpl::make('promoted-slide');

if(!$discounts = cache::get('discounts')) {
	db::query('select '.CATALOG_SELECT.' from product p where status = 1 and (stock > 1) and (price_old > price or exists(select 1 from product_option where product_id = p.id and price_old > price)) order by rand() limit 20');
	$discounts = db::fetchAll();
	cache::set('discounts', $discounts, 1800);
}
$i = 0;
foreach($discounts as $product) {
	shop::makeProduct($product, 'discounts');
	$i++;
	if($i == 10) {
		tpl::make('discounts-slide');
		tpl::clear('discounts');
	}
}
tpl::make('discounts-slide');

tpl::make('intro-catalog', 'main');

$random_brand = array_random($brands);
// Баннера пока нет, перетасовываем
while(!$random_brand['banner']) {
	$random_brand = array_random($brands);
}
foreach($filters as $filterId => $text) {
	if($text == $random_brand['name']) break;
}
tpl::set('brand-name', $random_brand['name']);
tpl::set('brand-banner', $random_brand['banner']);
tpl::set('brand-href', '/catalog?filter='.$filterId);
tpl::make('banner-brand', 'main');

// BLOG

db::query('select * from blog order by id desc limit 3');

foreach(db::fetchAll() as $row) {
	tpl::push($row);
	$preview = $row['preview'] ?: $row['content'];
	$preview = strip_tags($preview);
	if(strlen($preview) > 120) {
		$preview = substr($preview, 0, strpos($preview,' ',120));
		$last_char = substr($preview, -1);
		if(!in_array($last_char, ['.','!','?',';'])) $preview .= '...';
		tpl::set('preview', $preview);
	}
	if(!$row['image']) tpl::set('image', 'blog.jpg');
	tpl::make('intro-post');
}

tpl::make('intro-posts', 'main');
tpl::make('intro-seo', 'main');
tpl::make('rr-widget-intro', 'main');

?>