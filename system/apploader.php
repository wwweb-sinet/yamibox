<?php

require(__DIR__.'/class.x.php');
require(__DIR__.'/common.php');

$useragent = SERVER('HTTP_USER_AGENT');

define('MOBILE', (bool)preg_match('#^m\.yamibox\.(ru|dev)$#', $_SERVER['HTTP_HOST']));

require_once(MOBILE ? XDIR.'/mobile.controller.php' : XDIR.'/app.controller.php');

?>