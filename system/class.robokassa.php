<?php

class robokassa {
	
	static function import_excerpt() {
		$dir = FILES.'/robokassa';
		$n = 0;
		db::prepare('insert ignore into robokassa_ex set year = :year, month = :month, order_id = :order_id, sum = :sum, filename = :filename');
		$files = get_files($dir, '/^Yamibox.*\.csv$/i');
		foreach($files as $file) {
			$data['filename'] = basename($file);
			preg_match('/(\d{4})\.(\d{1,2})/', $data['filename'], $match);
			$data['year'] = $match[1];
			$data['month'] = intval($match[2]);
			$f = fopen($file, 'r');
			while($line = fgetcsv($f, 9999, ';')) {
				if(!$data['order_id'] = intval($line[0])) continue;
				if(!$data['sum'] = correct_float($line[3])) continue;
				db::set($data);
				db::execute();
				if(db::count()) $n++;
			}
			fclose($f);
		}
		$files = get_files($dir, '/^Spisok.*\.csv$/i');
		foreach($files as $file) {
			$data['filename'] = basename($file);
			preg_match('/(\d{1,2})\.(\d{4})/', $data['filename'], $match);
			$data['year'] = $match[2];
			$data['month'] = intval($match[1]);
			$f = fopen($file, 'r');
			while($line = fgetcsv($f, 9999, ';')) {
				if(!$data['order_id'] = intval($line[0])) continue;
				if(!$data['sum'] = correct_float($line[5])) continue;
				db::set($data);
				db::execute();
				if(db::count()) $n++;
			}
			fclose($f);
		}
		return ['robokassa::import_excerpt' => $n];
	}
	
}

?>