<?php

class Account {
	
	const COMMON = 10;
	const PNL = 30;
	const EMPLOYEE = 50;
	const ADMIN = 100;
	
	static $auth = false;
	static $admin = false;
	static $pnl = false;
	static $employee = false;
	static $cookie = NULL;
	static $info = array('id'=>0,'status'=>0); // user row from DB
	
	static function authenticate($email, $password, $force = false) {
		$error = false;
		db::query('select * from user where email = ?', $email);
		if(!$user = db::fetchArray()) {
			$error = 'user-not-exists';
		}
		elseif(!$user['password']) {
			tpl::set('empty-password', true);
			return false;
		}
		elseif(!password_verify($password, $user['password'])) {
			$error = 'user-password';
		}
		elseif(!$user['status']) {
			$error = 'user-status';
			self::sendActivationEmail($email, $user['cookie'], 'email-registration');
		}
		if($error && !$force) {
			$errors = json::get('errors');
			alert($errors[$error], 'error', 'auth-error');
			return false;
		}
		$old_cookie = COOKIE('cookie');
		self::$admin = $user['status'] == self::ADMIN;
		self::$employee = $user['status'] == self::EMPLOYEE;
		self::$pnl = $user['status'] == self::PNL;
		self::$auth = true;
		self::$cookie = $user['cookie'];
		self::$info = $user;
		addCookie('cookie', self::$cookie);
		self::restoreCart($old_cookie, self::$cookie);
		return true;
	}
	
	static function restoreCart($old_cookie, $new_cookie) {
		db::query('select * from userdata where cookie = ?', $old_cookie);
		$userdata = db::fetchArray();
		if(!@$userdata['cart']) return;
		db::query('delete from userdata where cookie in (?, ?)', $old_cookie, $new_cookie);
		db::query('replace into userdata set cookie = ?, user_id = ?, cart = ?', $new_cookie, self::$info['id'], $userdata['cart']);
	}
	
	static function logout() {
		if(self::$auth) {
			cache::clear('cart-value-'.self::$cookie);
			cache::clear('cart-'.self::$cookie);
			delCookie('cookie');
			if($cookie = COOKIE('admin')) {
				delCookie('admin');
				addCookie('cookie', $cookie);
			}
		}
		return redirect('/');
	}
	
	static function register($email, $password, $confirm = NULL) {
		$error = false;
		$email = strtolower(trim($email));
		$pl = strlen($password);
		if(!$email || !$email = filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$error = 'email';
		}
		$cookie = sha1($email . password_hash($password, PASSWORD_DEFAULT));
		db::query('select * from user where email = ?', $email);
		$user = db::fetchArray();
		if(@$user['password']) {
			$error = 'email-registered';
		}
		/* elseif($pl < 5 || $pl > 20) {
			$error = 'password-length';
		} */
		elseif($confirm && $password !== $confirm) {
			$error = 'password-confirm';
		}
		elseif(!self::sendActivationEmail($email, $cookie, 'email-registration')) {
			$error = 'sendmail-error';
		}
		elseif($user['cookie'] && !$user['password']) {
			if(!db::query('update user set cookie = :cookie, password = :password where id = :id', array(
				'cookie' => $cookie, 'password' => password_hash($password, PASSWORD_DEFAULT), 'id' => $user['id']
			)))
			$error = 'db-error';
		}
		elseif(!db::query('insert into user (email, cookie, password, bonus) values (:email, :cookie, :password, 40)',
				array('email'=>$email, 'password'=>password_hash($password, PASSWORD_DEFAULT), 'cookie'=>$cookie)
			)) {
			$error = 'db-error';
		}
		if($error) {
			$errors = json::get('errors');
			tpl::set('error', $errors[$error]);
			return false;
		}
		return db::lastInsertId();
	}
	
	static function sendActivationEmail($email, $cookie, $tpl) {
		$subjects = array(
			'email-restore'=>'Восстановление пароля. Yamibox.ru',
			'email-registration'=>'Подтверждение регистрации. Yamibox.ru'
		);
		tpl::load('email');
		tpl::set('user-email', $email);
		tpl::set('user-key', $cookie);
		$message = tpl::make($tpl);
		return shop::sendEmail($email, $subjects[$tpl], $message);
	}
	
	static function sendRestorationEmail($email) {
		$error = false;
		$email = strtolower(trim($email));
		if(!$email || !$email = filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$error = 'email';
		}
		elseif(!$user = self::fetch($email)) {
			$error = 'user-not-exists';
		}
		elseif(!self::sendActivationEmail($email, $user['cookie'], 'email-restore')) {
			$error = 'sendmail-error';
		}
		if($error) {
			$errors = json::get('errors');
			tpl::set('error', $errors[$error]);
			return false;
		}
		return true;
	}
	
	static function fetch($email) {
		db::query('select * from user where email = ?', $email);
		return db::count() ? db::fetchArray() : false;
	}
	
	static function registration_confirm() {
		$errors = json::get('errors');
		$error = false;
		if(!$email = GET('email')) {
			$error = 'email-required';
		}
		elseif(!$key = GET('key')) {
			$error = 'activation-key-error';
		}
		elseif(strlen($key) !== 40) {
			$error = 'activation-key-error';
		}
		elseif(!$email = filter_input(INPUT_GET, 'email', FILTER_VALIDATE_EMAIL)) {
			$error = 'email';
		}
		elseif(!self::activate($email, $key)) {
			$error = 'activation-error';
		}
		else {
			self::authenticate($email, self::$info['password'], true);
		}
		if($error) {
			$errors = json::get('errors');
			return alert($errors[$error], '');
		}
		tpl::make('registration-success', 'main');
	}
	
	static function restoration_confirm() {
		$errors = json::get('errors');
		$error = false;
		if(!$email = GET('email')) {
			$error = 'email-required';
		}
		elseif(!$key = GET('key')) {
			$error = 'activation-key-error';
		}
		elseif(strlen($key) !== 40) {
			$error = 'activation-key-error';
		}
		elseif(!$email = filter_var(strtolower(GET('email')), FILTER_VALIDATE_EMAIL)) {
			$error = 'email';
		}
		elseif(!$user = self::fetch($email)) {
			$error = 'user-not-exists';
		}
		elseif($key !== $user['cookie']) {
			$error = 'activation-key-error';
		}
		elseif(!$pwd = self::resetPassword($email)) {
			$error = 'unknown';
		}
		else {
			self::authenticate($email, $pwd, true);
			tpl::set('user-new-password', $pwd);
		}
		if($error) {
			$errors = json::get('errors');
			return alert($errors[$error], '');
		}
		tpl::make('restoration-success', 'main');
	}
	
	static function resetPassword($email) {
		$pwd = str::generatePassword();
		$password_hash = password_hash($pwd, PASSWORD_DEFAULT);
		$key = sha1($email . $password_hash);
		db::query('update user set password = :pwd, cookie = :key where email = :email', array(
			'pwd'=>$password_hash, 'key'=>$key, 'email'=>$email
		));
		if(!db::count()) return false;
		cache::clear('user-'.$key);
		return $pwd;
	}
	
	static function activate($email, $cookie) {
		if(!self::$info = self::fetch($email)) {
			return false;
		}
		if(!self::$info['cookie'] || $cookie !== self::$info['cookie']) {
			return false;
		}
		$status = self::$info['status'] ?: 10;
		db::query('update user set status = ? where email = ?', $status, $email);
		return db::count();
	}
	
	static function changePassword($pwd_old, $pwd_new, $pwd_confirm = NULL) {
		if(!self::$auth) {
			return redirect('/login');
		}
		$password_hash = password_hash($pwd_new, PASSWORD_DEFAULT);
		$cookie = sha1(self::$info['email'] . $password_hash);
		$error = false;
		$pwd_cur = self::$info['password'];
		if(password_verify($pwd_old, $pwd_cur)) {
			if($pwd_confirm) {
				$ln = strlen($pwd_new);
				/* if($ln > 20 || $ln < 5) {
					$error = 'password-length';
				} */
				if($pwd_confirm !== $pwd_new) {
					$error = 'password-confirm';
				}
			}
		}
		else {
			$error = 'user-password';
		}
		if($error) {
			$errors = json::get('errors');
			alert($errors[$error], 'error', 'error');
			return false;
		}
		else  {
			db::query('update user set password = :pwd, cookie = :cookie where email = :email', array(
				'pwd'=>$password_hash, 'cookie'=>$cookie, 'email'=>self::$info['email']
			));
			addCookie('cookie', $cookie);
		}
		return true;
	}
	
	static function get($key = NULL) {
		return empty(self::$info) ? NULL : ($key ? self::$info[$key] : self::$info);
	}
	
	static function init() {
		$cookie = trim(COOKIE('cookie'));
		if(strlen($cookie) == 40) {
			self::$cookie = $cookie;
			if($user = self::getData($cookie)) {
				self::$auth = $user['status'] > 0;
				self::$admin = $user['status'] == self::ADMIN;
				self::$pnl = $user['status'] == self::PNL;
				self::$employee = $user['status'] == self::EMPLOYEE;
				self::$info = $user;
				if(($ip = SERVER('REMOTE_ADDR')) && self::$auth) {
					if($ip !== $user['last_ip']) {
						db::query('update user set last_ip = :ip where id = :id', array(
							'ip' => $ip, 'id' => $user['id']
						));
						$user['last_ip'] = $ip;
						cache::set('user-'.$user['cookie'], $user);
					}
				}
				self::updateLoginTime();
			}
		}
		else {
			self::$cookie = sha1(session_id());
		}
		addCookie('cookie', self::$cookie);
	}
	
	static function getData($cookie) {
		if(!$user = cache::get('user-'.$cookie)) {
			db::query('select * from user where cookie = ?', $cookie);
			if(!$user = db::fetchArray()) {
				$user = self::$info;
			}
			cache::set('user-'.$cookie, $user);
		}
		return $user;
	}
	
	static function update($data) {
		if(!array_key_exists('id', $data)) return false;
		foreach($data as $key => $value) {
			if($key == 'id') continue;
			$params[] = $key.' = :'.$key;
		}
		$params = implode(', ', $params);
		$sql = 'update user set '.$params.' where id = :id';
		db::query($sql, $data);
		cache::clear(['user-'.self::$cookie]);
	}
	
	static function reset() {
		$bonus_spend = SESSION('bonus-spend');
		unset($_SESSION['bonus-spend'], $_SESSION['coupon'], $_SESSION['coupon-discount']);
		if(self::$auth) {
			$bonus = self::$info['bonus'] - $bonus_spend;
			if($bonus < 0) $bonus = 0;
			self::update(array(
				'id' => self::$info['id'],
				'bonus' => $bonus
			));
		}
	}
	
	static function updateLoginTime() {
		if(!$ltime = SESSION('ltime')) $ltime = $_SESSION['ltime'] = $_SERVER['REQUEST_TIME'];
		if($_SERVER['REQUEST_TIME'] - $ltime > rand(10,30)) {
			db::query('update user set ltime = CURRENT_TIMESTAMP where id = ?', self::$info['id']);
			$_SESSION['ltime'] = $_SERVER['REQUEST_TIME'];
		}
	}
	
	static function addReview($productId) {
		$error = false;
		$recommend = absint(REQUEST('recommend')) ? 1 : 0;
		if(!$author = html2chars(trim(REQUEST('author')))) {
			$error = 'Введите ваше имя';
		}
		elseif(!$content = trim(REQUEST('content'))) {
			$error = 'Введите текст отзыва';
		}
		elseif(!$rating = absint(REQUEST('rating'))) {
			$error = 'Выберите оценку товара (от 1 до 5)';
		}
		elseif($rating > 5) {
			$rating = 5;
		}
		if($error) {
			$response['status'] = 'error';
			$response['msg'] = $error;
		}
		else {
			$response['status'] = 'ok';
			$response['msg'] = 'Ваш отзыв принят и будет опубликован после модерации.';
			$data = array(
				'product_id' => $productId,
				'user_id' => self::$info['id'],
				'rating' => $rating,
				'author' => $author,
				'content' => $content,
				'status' => self::$admin ? 1 : 0,
				'recommend' => $recommend
			);
			db::query('insert into product_review (product_id, user_id, rating, recommend, author, content, status) values(:product_id, :user_id, :rating, :recommend, :author, :content, :status)', $data);
			$data['email'] = self::$info['email'];
			$data['review_id'] = db::lastInsertId();
			//if(!self::$admin) shop::sendReviewEmail($data);
		}
		return json::encode($response);
	}
	
	static function getCumulative($userId = 0) {
		if(account::$admin && account::$employee) return 0;
		if(!$userId) $userId = self::$info['id'];
		db::query('select sum(total_sum) - sum(bonus) from orders where user_id = ? and status = 4 and paid = 1', $userId);
		if(!$orders_total = db::fetchSingle()) return 0;
		$cumulative = json::get('cumulative');
		usort($cumulative, function($a, $b) {
			return $a['sum'] > $b['sum'] ? 1 : 0;
		});
		$discount = 0;
		foreach($cumulative as $cum) {
			if($orders_total >= $cum['sum']) $discount = $cum['discount'];
		}
		return $discount;
	}

	static function show() {
		self::pageLoad();
		tpl::set('document-title', 'Личные данные');
		tpl::push(self::$info);
		if($address = self::$info['address']) {
			tpl::push(json::decode($address));
		}
		if(SESSION('email_confirmed')) {
			unset($_SESSION['email_confirmed']);
			tpl::make('gtm-account-confirmed');
		}
		if(REQUEST_METHOD == 'POST') {
			$account['id'] = self::$info['id'];
			if($password = POST('password')) {
				if(strlen($password) < 3) alert('Пароль слишком короткий! Хотя бы 3 символа!', 'error', 'right-column-content');
				$confirm = POST('password_confirm');
				if($confirm == $password) $account['password'] = password_hash($password, PASSWORD_DEFAULT);
				else alert('Неправильное подтверждение пароля', 'error', 'right-column-content');
			}
			array_walk($_POST, function(&$val) {
				$val = html2chars(substr(trim($val), 0, 64));
			});
			$account['firstname'] = POST('firstname');
			$account['lastname'] = POST('lastname');
			$account['phone'] = POST('phone');
			$account['bdate'] = preg_match('#(\d{4,4}-\d{2,2}-\d{2,2})#', POST('bdate'), $match) ? $match[1] : NULL;
			$account['address'] = json::encode([
				'region' => POST('region'),
				'city' => POST('city'),
				'zipcode' => POST('zipcode'),
				'street' => POST('street'),
				'building' => POST('building'),
				'building-add' => POST('building-add'),
				'apt' => POST('apt')
			]);
			self::update($account);
			tpl::push($_POST);
			alert('Сохранено', 'success', 'right-column-content');
		}
		tpl::set('cumulative', self::getCumulative());
		tpl::make('account-info', 'right-column-content');
		tpl::make('right-column');
		tpl::make('account-page', 'main');
	}

	static function activation_page() {
		make_title('Активация учетной записи');
		$result = self::activate(REQUEST('email'), REQUEST('key'));
		if($result === false) {
			return alert('Неправильный ключ активации', 'error', 'main');
		}
		if($result == 0) {
			addCookie('cookie', self::$info['cookie']);
			return alert('Ваша учетная запись уже активирована.');
		}
		addCookie('cookie', self::$info['cookie']);
		$_SESSION['email_confirmed'] = 1;
		return redirect('/account');
	}

	static function registration_page() {
		make_title('Регистрация');
		die_stupid_bot();
		if(self::$auth) {
			return alert('Чтобы зарегистрировать новую учетную запись, необходимо <a href="/account/logout">выйти из системы</a>.', '');
		}
		if(REQUEST_METHOD == 'POST') {
			tpl::set('email', POST('email'));
			if(self::register(POST('email'), POST('password'))) {
				return tpl::make('reg-success', 'main');
			}
			elseif($error = tpl::get('error')) {
				alert($error, 'error', 'reg-error');
			}
		}
		tpl::make('form-user-reg', 'main');
	}

	static function login() {
		if(self::$auth)
			return redirect('/account');
		make_title('Вход в личный кабинет');
		die_stupid_bot();
		if(!SESSION('return_url')) $_SESSION['return_url'] = SESSION('referer');
		if(REQUEST_METHOD == 'POST') {
			$email = strtolower(trim(POST('email')));
			tpl::set('email', html2chars($email));
			if(self::authenticate($email, POST('password'))) {
				$return_url = SESSION('return_url');
				$_SESSION['return_url'] = '';
				return redirect($return_url);
			}
			elseif(tpl::get('empty-password')) {
				tpl::set('email', $email);
				self::sendRestorationEmail($email);
				tpl::make('restore-transfer-success', 'main');
			}
			elseif($error = tpl::get('error')) {
				alert($error, 'error', 'error');
			}
		}
		tpl::make('form-user-login', 'main');
	}

	static function restore() {
		if(isset($_GET['email']) && isset($_GET['key'])) {
			return self::restoration_confirm();
		}
		if(self::$auth)
			return redirect('/account');
		die_stupid_bot();
		make_title('Восстановление пароля');
		if(REQUEST_METHOD == 'POST') {
			tpl::set('email', POST('email'));
			if(self::sendRestorationEmail(POST('email'))) {
				return tpl::make('restore-success', 'main');
			}
		}
		tpl::make('form-user-restore', 'main');
	}

	static function orders() {
		self::pageLoad();
		tpl::set('document-title', 'История заказов');
		$status = json::get('order-status');
		$user_id = ($id = absint(GET('user_id'))) && (account::$admin || account::$employee) ? $id : self::$info['id'];
		db::query('select * from orders where user_id = ? order by id desc', $user_id);
		if(db::count()) {
			$total = 0;
			foreach(db::fetchAll() as $order) {
				tpl::push($order);
				$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
				tpl::set('order-sum', $order_sum);
				tpl::set('cdate', ts2date('d.m.Y', $order['ctime']));
				if($order['paid']) {
					tpl::make('order-paid');
				}
				else {
					tpl::make('order-not-paid');
					if($order['ptype'] > 3) {
						$login = x::config('ROBOX_LOGIN');
						$password = x::config('ROBOX_PASSWORD_1');
						$inv_desc = "Оплата заказа №".$order['id']." в интернет-магазине Yamibox.ru";
						$out_sum = number_format($order_sum, 2, '.', '');
						$crc = md5(sprintf('%s:%s:%s:%s', $login, $out_sum, $order['id'], $password));
						tpl::set('robox-login', $login);
						tpl::set('robox-description', $inv_desc);
						tpl::set('robox-sum', $out_sum);
						tpl::set('robox-crc', $crc);
						tpl::set('invId', $order['id']);
						tpl::make('robox-payment-link');
					}
				}
				tpl::set('status-text', $status[$order['status']]);
				tpl::make('account-order');
				tpl::clear('order-paid','order-not-paid', 'robox-payment-link');
				if($order['status'] == 4) $total += $order_sum;
			}
			tpl::set('total', $total);
			tpl::make('account-orders', 'right-column-content');
		}
		else {
			tpl::make('account-orders-empty', 'right-column-content');
		}
		tpl::make('right-column');
		tpl::make('account-page', 'main');
	}

	static function feedback() {
		self::pageLoad();
		if(REQUEST_METHOD == 'POST') {
			if(!$lastFeedback = SESSION('lastFeedback')) $lastFeedback = $_SESSION['lastFeedback'] = $_SERVER['REQUEST_TIME'] - 200;
			$timeLeft = 120 - ($_SERVER['REQUEST_TIME'] - $lastFeedback);
			$text = html2chars(trim(POST('feedback')));
			if(!$text) {
				return alert('Нет текста сообщения.', 'warning');
			}
			tpl::set('feedback-text', $text);
			if($timeLeft <= 0) {
				db::query('insert into feedback(user_id, text) values(?, ?)', self::$info['id'], $text);
				$_SESSION['lastFeedback'] = $_SERVER['REQUEST_TIME'];
				alert('Сообщение отправлено, мы ответим как только сможем.', 'success', 'right-column-content');
				shop::sendFeedbackEmail($text);
				tpl::clear('feedback-text');
			}
			else {
				alert('Извините, установлено ограничение. Нужно подождать еще '. $timeLeft.' с.', 'waring', 'right-column-content');
			}
		}
		tpl::set('document-title', 'Связаться с нами');
		tpl::make('account-feedback', 'right-column-content');
		tpl::make('right-column');
		tpl::make('account-page', 'main');
	}

	static function reviews() {
		self::pageLoad();
		db::query('select *, (select name from product where id = pr.product_id) as product_name, (select url_name from product where id = pr.product_id) as product_url_name from product_review pr where user_id = ? order by id desc', self::$info['id']);
		if(db::count()) {
			foreach(db::fetchAll() as $review) {
				tpl::push($review);
				tpl::set('cdate', ts2date('d.m.Y H:i', $review['ctime']));
				tpl::set('product-img', product::getImage($review['product_id']));
				tpl::set('product-href', '/product/'.$review['product_url_name']);
				tpl::set('rating-value', $review['rating']);
				tpl::clear('rating');
				tpl::make('rating');
				tpl::make('account-review');
			}
			tpl::make('account-reviews', 'right-column-content');
		}
		else {
			tpl::make('account-reviews-empty', 'right-column-content');
		}
		tpl::set('document-title', 'Мои отзывы');
		tpl::make('right-column');
		tpl::make('account-page', 'main');
	}

	static function yamibonus() {
		self::pageLoad();
		tpl::set('bonus', self::$info['bonus']);
		tpl::set('cumulative', self::getCumulative());
		tpl::set('document-title', 'Мои ямибонусы');
		tpl::make('account-yamibonus', 'right-column-content');
		tpl::make('right-column');
		tpl::make('account-page', 'main');
	}
	
	static function pageLoad() {
		if(!self::$auth) redirect('/account/login');
		shop::makeBreadcrumbs();
		tpl::make('account-menu', 'left-column-content');
		tpl::make('left-column');
	}
	
	static function wishlistAdd($path, $productId, $optionId = 0) {
		if(!$productId = intval($productId)) die('Отсутствует идентификатор товара!');
		$optionId = intval($optionId);
		$in_stock = isset($_GET['in_stock']) ? (absint(GET('in_stock')) > 0 ? 1 : 0) : 1;
		if(account::$auth) {
			db::query('insert ignore into wishlist(user_id, product_id, option_id, in_stock) values(?, ?, ?, ?)', account::$info['id'], $productId, $optionId, $in_stock);
			$out = tpl::make('add2wishlist-success');
		}
		else {
			$out = tpl::make('add2wishlist-auth');
		}
		die($out);
	}
	
	static function wishlist() {
		self::pageLoad();
		db::query('select *, (select name from product_option where id = w.option_id) as name, (select price from product_option where id = w.option_id) as price, (select price_old from product_option where id = w.option_id) as price_old, if(option_id = 0, (select stock from product where id = w.product_id), (select stock from product_option where id = w.option_id)) as stock from wishlist w where user_id = ?', intval(account::$info['id']));
		if($wishlist = db::fetchAll()) {
			db::query('select * from product where status > -1 and id in(select product_id from wishlist where user_id = ?) order by name', account::$info['id']);
			$products = [];
			while($product = db::fetchArray()) {
				$products[$product['id']] = $product;
			}
			foreach($wishlist as $item) {
				if(!isset($products[$item['product_id']])) continue;
				$product = $products[$item['product_id']];
				if($item['option_id'] > 0) {
					$product['option-name'] = ' - '.$item['name'];
					$product['price'] = $item['price'];
					$product['price_old'] = $item['price_old'];
					tpl::set('option-id', '/'.$item['option_id']);
					tpl::set('product-option-id', '?option_id='.$item['option_id']);
				}
				else {
					tpl::set('option-id', '');
					tpl::set('product-option-id', '');
				}
				tpl::push($product);
				tpl::set('product-img', product::getImage($product));
				if($product['price_old'] && $product['price_old'] > $product['price']) {
					tpl::make('price-old');
				}
				if($item['stock'] > 1 && $product['status'] > 0) {
					tpl::make('icon-confirm', 'delivery-icon');
					tpl::set('stock-text', 'Есть на складе');
					if($item['option_id'] > 0) {
						tpl::set('add2cart-href', '/product/'.$product['url_name']);
						tpl::set('add2cart-class', '');
					}
					else {
						tpl::set('add2cart-href', '/add2cart/'.$product['id']);
						tpl::set('add2cart-class', ' colorbox');
					}
					tpl::make('product-add2cart');
				}
				else {
					tpl::make('icon-cancel', 'delivery-icon');
					tpl::set('stock-text', 'Нет в наличии');
				}
				tpl::make('account-wishlist-item');
				tpl::clear('price-old', 'delivery-icon', 'product-add2cart');
			}
			tpl::make('account-wishlist-items');
		}
		else {
			tpl::make('account-wishlist-empty');
		}
		tpl::set('document-title', 'Мой Wishlist');
		tpl::make('account-wishlist', 'right-column-content');
		tpl::make('right-column');
		tpl::make('account-page', 'main');
	}
	
	static function wishlistRemove($path, $productId, $optionId = 0) {
		db::query('delete from wishlist where user_id = ? and product_id = ? and option_id = ?', account::$info['id'], $productId, $optionId);
		if(AJAX) die('ok');
		return redirect('/account/wishlist');
	}
	
	static function coupons() {
		self::pageLoad();
		tpl::set('document-title', 'Мои купоны');
		db::query('select * from coupon where user_id = ?', account::$info['id']);
		$coupons = db::fetchAll();
		foreach($coupons as $coupon) {
			tpl::set('code', $coupon['code']);
			tpl::set('cdate', date('Y-m-d', strtotime($coupon['ctime'])));
			$ctime = strtotime($coupon['ctime']);
			tpl::set('edate', date('Y-m-d', strtotime('+30 days', $ctime)));
			$postfix = $coupon['pc'] ? '%' : '&nbsp;руб.';
			tpl::set('discount-text', $coupon['amount'].$postfix);
			tpl::make('account-coupon');
		}
		if($coupons) tpl::make('account-coupons', 'right-column-content');
		else tpl::make('account-coupons-empty', 'right-column-content');
		tpl::make('right-column');
		tpl::make('account-page', 'main');
	}
	
	static function update_last_order_time() {
		if(!account::$auth) {
			if(!$email = REQUEST('email')) return false;
			$email = strtolower(trim($email));
			db::query('update user set last_order_time = CURRENT_TIMESTAMP where email = ?', $email);
		}
		else {
			db::query('update user set last_order_time = CURRENT_TIMESTAMP where id = ?', account::$info['id']);
		}
		return db::count() > 0;
	}
	
}

account::init();
tpl::load('account');

?>