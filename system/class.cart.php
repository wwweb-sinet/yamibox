<?php

class Cart {
	
	static $products = array();
	static $options = array();
	static $gifts = ['small'=>[],'big'=>[]];
	static $sum = 0;
	static $sum_with_discounts = 0;
	
	static function notice($msg, $type = 'error') {
		tpl::set('cart-msg', $msg);
		if($type == 'success' && x::config('ENABLE_COUNTERS')) {
			tpl::make('yandex-goal-add2cart');
		}
		tpl::make('cart-'.$type);
		$html = tpl::make('cart-notice', 'main');
		if(AJAX) exit($html);
	}
	
	static function count() {
		return array_sum(self::$products) + array_sum(self::$options);
	}
	
	static function addItem($path, $productId = 0, $optionId = 0) {
		$quantity = absint(REQUEST('quantity'));
		if(!$quantity) $quantity = 1;
		if(!$optionId) $optionId = absint(REQUEST('oid'));
		if($optionId) {
			db::query('select * from product_option where id = ?', $optionId);
			if(!$item = db::fetchArray()) {
				return self::notice('Не найден указанный тип товара.');
			}
			if($item['status'] < 1 || $item['stock'] < 2) {
				return self::notice('Выбранный тип товара недоступен.');
			}
			self::addOption($optionId, $quantity);
		}
		else {
			if(!$productId = absint($productId)) {
				return self::notice('Не указан иднтификатор товара.');
			}
			db::query('select '.CATALOG_SELECT.' from product p where id = ?', $productId);
			if(!$item = db::fetchArray()) {
				return self::notice('Товар не найден.');
			}
			if(!$item['status'] || $item['stock'] < 2) {
				return self::notice('Выбранный товар недоступен.');
			}
			if($item['option_count'] > 0) {
				return self::notice('Необходимо выбрать один из вариантов товара.');
			}
			self::addProduct($productId, $quantity);
		}
		cart::save();
		if(AJAX) {
			$cart_count = cart::count();
			$word = str::numberof($cart_count, 'товар', ['','а','ов']);
			$cart_text = sprintf("%d %s<br>%d&nbsp;р.", $cart_count, $word, cart::$sum_with_discounts);
			$widget_text = sprintf("%d %s на сумму %d р.", $cart_count, $word, cart::$sum_with_discounts);
			$cart = [
				'sum' => cart::$sum_with_discounts,
				'count' => $cart_count,
				'cart_text' => $cart_text,
				'widget_text' => $widget_text
			];
			die(json::encode($cart));
		}
		return redirect(SESSION('referer'));
	}
	
	static function addRel() {
		$ids = REQUEST('ids');
		foreach($ids as $productId) {
			self::addProduct($productId);
		}
		self::save();
		if(AJAX) {
			$cart_count = cart::count();
			$word = str::numberof($cart_count, 'товар', ['','а','ов']);
			$cart_text = sprintf("%d %s<br>%d&nbsp;р.", $cart_count, $word, cart::$sum_with_discounts);
			$widget_text = sprintf("%d %s на сумму %d р.", $cart_count, $word, cart::$sum_with_discounts);
			$cart = [
				'sum' => cart::$sum_with_discounts,
				'count' => $cart_count,
				'cart_text' => $cart_text,
				'widget_text' => $widget_text
			];
			die(json::encode($cart));
		}
		return redirect(SESSION('referer'));
	}
	
	static function addOption($optionId, $qty = 1) {
		if(self::hasOption($optionId)) {
			cart::$options[$optionId] += $qty;
		}
		else {
			cart::$options[$optionId] = $qty;
		}
		$price = db::querySingle('select price from product_option where id = '.$optionId);
		self::$sum += $price * $qty;
		self::$sum_with_discounts += $price * $qty;
	}
	
	static function addProduct($productId, $qty = 1) {
		if(cart::hasProduct($productId)) {
			cart::$products[$productId] += $qty;
		}
		else {
			cart::$products[$productId] = $qty;
		}
		$price = db::querySingle('select price from product where id = '.$productId);
		self::$sum += $price * $qty;
		self::$sum_with_discounts += $price * $qty;
	}
	
	static function addGift() {
		if(self::$sum_with_discounts < 1000) return false;
		$n = floor(self::$sum_with_discounts / 1000);
		$m = floor(self::$sum_with_discounts / 5000);
		for($i = 0; $i < $n; $i++) {
			if(self::$gifts['small']) {
				self::$gifts['small']['quantity']++;
			}
			else {
				self::$gifts['small'] = [
					'price' => 0,
					'name' => 'Подарок',
					'short_text' => 'Пробники',
					'img' => '0-1.jpg',
					'quantity' => 1
				];
			}
		}
		// !!! ВРЕМЕННО УБИРАЕМ ПОДАРКИ ОТ 5000 Р
		/* for($i = 0; $i < $m; $i++) {
			if(self::$gifts['big']) {
				self::$gifts['big']['quantity']++;
			}
			else {
				self::$gifts['big'] = [
					'price' => 0,
					'name' => 'Подарок за каждые 5000 рублей',
					'short_text' => 'Универсальный алоэ-гель',
					'img' => '518-1.jpg',
					'quantity' => 1
				];
			}
		} */
		// !!! ВРЕМЕННО УБИРАЕМ ПОДАРКИ ОТ 3000 Р
		// self::$gifts['big'] = [];
		// return self::$gifts;
	}
	
	static function save() {
		$cart = cart::$products || cart::$options ? self::getJSON() : NULL;
		db::query('insert ignore into userdata (cookie, user_id) VALUES (?, ?)', account::$cookie, account::$info['id']);
		db::query('update userdata set cart = :cart where cookie = :cookie',
			array('cookie'=>account::$cookie, 'cart'=>$cart)
		);
		cache::clear('cart-value-'.account::$cookie);
		cache::clear('cart-'.account::$cookie);
		unset($_SESSION['bonus-spend'], $_SESSION['coupon'], $_SESSION['coupon-discount']);
	}
	
	static function getJSON() {
		$products = !empty(cart::$products) ? cart::$products : NULL;
		$options = !empty(cart::$options) ? cart::$options : NULL;
		$gifts = !empty(cart::$gifts) ? cart::$gifts : NULL;
		$json_data = array(
			'products' => $products,
			'options' => $options
		);
		return json::encode($json_data);
	}
	
	static function removeItem($path, $productId, $optionId = 0) {
		if($optionId) {
			if(cart::removeOption($optionId)) cart::save();
		}
		else {
			if(cart::removeProduct($productId)) cart::save();
		}
		if(AJAX) {
			cart::init();
			$cart_count = cart::count();
			$word = str::numberof($cart_count, 'товар', ['','а','ов']);
			$cart_text = sprintf("%d %s<br>%d&nbsp;р.", $cart_count, $word, cart::$sum_with_discounts);
			$widget_text = sprintf("%d %s на сумму %d р.", $cart_count, $word, cart::$sum_with_discounts);
			$cart = [
				'sum' => cart::$sum_with_discounts,
				'count' => $cart_count,
				'cart_text' => $cart_text,
				'widget_text' => $widget_text
			];
			die(json::encode($cart));
		}
		redirect('/cart');
	}
	
	static function removeProduct($productId) {
		if(cart::hasProduct($productId)) {
			unset(cart::$products[$productId]);
			return true;
		}
		return false;
	}
	
	static function removeOption($optionId) {
		if(cart::hasOption($optionId)) {
			unset(cart::$options[$optionId]);
			return true;
		}
		return false;
	}
	
	static function clear() {
		cart::$products = array();
		cart::$options = array();
	}
	
	static function init() {
		if($cart = cart::getData(account::$cookie)) {
			if($cart['cart']) {
				cart::$sum = 0;
				$items = json::decode($cart['cart']);
				$removes = false;
				if($items['products']) {
					$idList = implode(', ', array_keys($items['products']));
					db::query('select id, price, quantity, stock from product p where status > 0 and stock > 1 and id in ('.$idList.')');
					foreach(db::fetchAll() as $product) {
						// if($product['stock'] > 2 && $product['quantity'] < 5) $product['quantity'] = 5;
						$qty = $items['products'][$product['id']];
						// if($qty > $product['quantity']) $qty = $product['quantity'];
						cart::$products[$product['id']] = $qty;
						cart::$sum += $product['price'] * $qty;
					}
					$removes = (db::count() !== count($items['products']));
				}
				if($items['options']) {
					$idList = implode(', ', array_keys($items['options']));
					db::query('select id, price, quantity, stock from product_option po where status > 0 and stock > 1 and id in ('.$idList.')');
					foreach(db::fetchAll() as $option) {
						// if($option['stock'] > 2 && $option['quantity'] < 5) $option['quantity'] = 5;
						$qty = $items['options'][$option['id']];
						// if($qty > $option['quantity']) $qty = $option['quantity'];
						cart::$options[$option['id']] = $qty;
						cart::$sum += $option['price'] * $qty;
					}
					if(!$removes) $removes = (db::count() !== count($items['options']));
				}
				if($removes) cart::save();
			}
			$bonus_discount = intval(SESSION('bonus-spend'));
			$cumulative = 0;
			$cum_discount = 0;
			if(account::$auth) {
				$cumulative = account::getCumulative();
				$cum_discount = round((cart::$sum / 100) * $cumulative);
			}
			$coupon_discount = intval(SESSION('coupon-discount'));
			cart::$sum_with_discounts = cart::$sum - $bonus_discount - $cum_discount - $coupon_discount;
			// Подарки пробники временно отключаем
			// self::addGift(); // только после расчета суммы с учетом скидок
		}
		tpl::load('cart');
	}
	
	static function getData($cookie, $force = false) {
		if(!($cartval = cache::get('cart-value-'.$cookie)) || $force) {
			db::query('select * from userdata where cookie = ?', $cookie);
			$cartval = db::fetchArray();
			cache::set('cart-value-'.$cookie, $cartval);
		}
		return $cartval;
	}
	
	static function getProducts($group_by = NULL) {
		if(!cart::$products) return array();
		$idList = implode(', ', array_keys(cart::$products));
		db::query('select '.CATALOG_SELECT.' from product p where id in ('.$idList.')');
		$products = db::fetchAll();
		return $group_by ? array_group($products, $group_by) : $products;
	}
	
	static function getOptions($group_by = NULL) {
		if(!cart::$options) return array();
		$idList = implode(', ', array_keys(cart::$options));
		db::query('select *, (select max_bonus from product where id = po.product_id) as max_bonus from product_option po where id in ('.$idList.')');
		$options = db::fetchAll();
		return $group_by ? array_group($options, $group_by) : $options;
	}
	
	static function getOrderJSON() {
		$order_items = array();
		
		foreach(cart::getProducts() as $product) {
			$order_items[] = [
				'id' => $product['id'],
				'name' => $product['name'],
				'short_text' => $product['short_text'],
				'barcode' => $product['barcode'],
				'price' => $product['price'],
				'price_old' => $product['price_old'],
				'price_buyin' => $product['price_buyin'],
				'quantity' => cart::$products[$product['id']],
				'manufacturer' => $product['manufacturer'],
				'url_name' => $product['url_name'],
				'weight' => $product['weight'],
				'volume' => $product['volume'],
				'option_name' => NULL,
				'option_id' => NULL,
				'stock' => $product['stock']
			];
		}
		
		if($options = cart::getOptions('id')) {
			$products = shop::getOptionProducts(array_keys($options));
			foreach($options as $id => $opts) {
				foreach($opts as $option) {
					$order_items[] = [
						'id' => $option['product_id'],
						'name' => $products[$option['product_id']]['name'],
						'short_text' => $products[$option['product_id']]['short_text'],
						'barcode' => $option['barcode'],
						'price' => $option['price'],
						'price_old' => $option['price_old'],
						'price_buyin' => $option['price_buyin'],
						'quantity' => cart::$options[$option['id']],
						'manufacturer' => $products[$option['product_id']]['manufacturer'],
						'url_name' => $products[$option['product_id']]['url_name'],
						'weight' => $option['weight'],
						'volume' => $option['volume'],
						'option_name' => $option['name'],
						'option_id' => $option['id'],
						'stock' => $option['stock']
					];
				}
			}
		}
		
		return json::encode($order_items);
	}
	
	static function getMaxBonus() {
		$max_bonus = 0;
		foreach(cart::getProducts() as $product) {
			$max_bonus += round($product['price'] / 100 * $product['max_bonus']);
		}
		foreach(cart::getOptions() as $option) {
			$max_bonus += round($option['price'] / 100 * $option['max_bonus']);
		}
		return $max_bonus;
	}
	
	static function hasProduct($productId) {
		return array_key_exists($productId, cart::$products);
	}
	
	static function hasOption($optionId) {
		return array_key_exists($optionId, cart::$options);
	}
	
	// вычет товаров после оформления заказа
	static function updateStock() {
		foreach(cart::$products as $product_id => $quantity) {
			$qty = db::querySingle('select quantity from product where id = '.$product_id);
			if($qty > 0) {
				$qty -= $quantity;
				$qty_new = $qty > 0 ? $qty : 0;
				db::query('update product set quantity = ? where id = ?', $qty_new, $product_id);
				if($qty < 0) {
					waybill::add_item($product_id, 0, abs($qty));
				}
			}
		}
		foreach(self::getOptions() as $option) {
			$product_id = $option['product_id'];
			$option_id = $option['id'];
			$qty = db::querySingle('select quantity from product_option where id = '.$option_id);
			if($qty > 0) {
				$qty -= self::$options[$option_id];
				$qty_new = $qty > 0 ? $qty : 0;
				db::query('update product_option set quantity = ? where id = ?', $qty_new, $option_id);
				if($qty < 0) {
					waybill::add_item($product_id, $option_id, abs($qty));
				}
			}
		}
		shop::update_stock_from_import();
	}
	
	static function widget() {
		if(!AJAX) die('AJAX ONLY');
		$cart_count = cart::count();
		tpl::set('cart-sum', cart::$sum_with_discounts);
		tpl::set('cart-count', $cart_count);
		if($cart_count) {
			$cart_text = sprintf("%d %s на сумму %d р.", $cart_count, str::numberof(cart::count(), 'товар', ['','а','ов']), cart::$sum_with_discounts);
			$items = json::decode(self::getOrderJSON());
			foreach($items as $item) {
				tpl::set('name', $item['name']);
				tpl::set('manufacturer', $item['manufacturer']);
				tpl::set('url_name', $item['url_name']);
				tpl::set('short_text', $item['short_text']);
				tpl::set('price', $item['price'] * $item['quantity']);
				tpl::set('product-href', '/product/'.$item['url_name']);
				tpl::set('product-img', product::getImage($item));
				tpl::set('pid', $item['id']);
				tpl::set('oid', intval($item['option_id']));
				tpl::set('qty', $item['quantity']);
				if($item['option_name']) {
					tpl::set('option-name', $item['option_name']);
					tpl::make('option-label');
				}
				tpl::make('cart-widget-item');
				tpl::clear('option-label');
			}
		}
		else {
			$cart_text = 'Корзина пуста';
		}
		tpl::set('cart-text', $cart_text);
		$html = tpl::make('cart-widget');
		die($html);
	}
	
}

cart::init();

?>