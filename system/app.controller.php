<?php

// значение по умолчанию для первого визита
if(!isset($_SESSION['referer'])) $_SESSION['referer'] = 'http://'.$_SERVER['HTTP_HOST'];

// --- ROUTES ---

x::route('/', 'action.intro.php');
x::route('/(new_style|var3)', function() { return redirect('/', 301); });
x::route('/sitemap.xml', 'action.sitemap.php');
x::route('/(yml?-?(office|rr)?.xml)', 'shop::get_yml');
x::route('/test/(\w+)', 'test::run');
x::route('/catalog((/([\w\-_]+)+)*)', 'action.catalog.php');
x::route('/product/([^/]+)', 'action.product.php');
x::route('/cart', 'action.cart.php');
x::route('/cart/order', 'action.order.php');
x::route('/cart/order2', 'action.order2.php');
x::route('/cart/widget', 'cart::widget');
x::route('/ddelivery_ajax', 'ddelivery::load');
x::route('/add2cart/(\d+)/?(\d+)?', 'cart::addItem');
x::route('/addrel', 'cart::addRel');
x::route('/remove/(\d+)/?(\d+)?', 'cart::removeItem');
x::route('/wishlist-add/(\d+)/?(\d+)?', 'account::wishlistAdd');
x::route('/wishlist-remove/(\d+)/?(\d+)?', 'account::wishlistRemove');
x::route('/order-info/(\w{32,32}|\d+)', 'order::show');
x::route('/yamiblog/?([\w\-_]+)?', 'blog::posts');
x::route('/yamiblog/add_record', 'blog::addRecord');
x::route('/yamiblog/add_comment', 'blog::addComment');
x::route('/page/([\w\-_]+)', 'blog::showPage');
x::route('/robox_payment/?(fail|success)?', 'shop::paymentResult');
x::route('/product_view', 'product::update_view');
x::route('/brands', 'blog::brands_page');
x::route('/podbirator', 'action.podbirator.php');
x::route('/ajax/(call_me|subscribe)', 'shop::ajax');

x::route('/account', 'account::show');
x::route('/account/login', 'account::login');
x::route('/account/orders', 'account::orders');
x::route('/account/logout', 'account::logout');
x::route('/account/restore', 'account::restore');
x::route('/account/reviews', 'account::reviews');
x::route('/account/feedback', 'account::feedback');
x::route('/account/wishlist', 'account::wishlist');
x::route('/account/coupons', 'account::coupons');
x::route('/account/yamibonus', 'account::yamibonus');
x::route('/account/new', 'account::registration_page');
x::route('/account/activate', 'account::activation_page');

x::route('/admin', function(){return redirect(account::$pnl ? '/admin/pnl' : '/admin/orders');});
x::route('/admin/drop-cache', 'admin::dropCache');
x::route('/admin/posts/?(\d+|new)?', 'admin::posts');
x::route('/admin/orders/?(\d+)?', 'admin::orders');
x::route('/admin/orders/print', 'admin::print_orders');
x::route('/admin/orders/send', 'admin::send_orders');
x::route('/admin/suppliers', 'admin::suppliers');
x::route('/admin/order-delivery/(\d+)', 'admin::order_delivery');
x::route('/admin/coupons/?(\d+)?', 'admin::coupons');
x::route('/admin/clients/?(\d+)?', 'admin::clients');
x::route('/admin/reviews/?(\d+)?', 'admin::reviews');
x::route('/admin/quittance/(\d+|all)', 'admin::quittance');
x::route('/admin/products/?(\d+|new)?', 'admin::products');
x::route('/admin/products/import', 'admin::products_import');
x::route('/admin/products/export', 'admin::products_export');
x::route('/admin/products/sticker', 'admin::products_sticker');
x::route('/admin/product_groups', 'admin::product_groups');
x::route('/admin/supplier_products', 'admin::supplier_products');
x::route('/admin/categories/?(\d+)?', 'admin::categories');
x::route('/admin/product-option/(\d+)', 'admin::productOption');
x::route('/admin/product-categories/(\d+)', 'admin::productCategories');
x::route('/admin/import_product_options', 'admin::import_product_options');
x::route('/admin/analytics', 'admin::analytics');
x::route('/admin/orders_statistics', 'admin::ordersStat');
x::route('/admin/orders_statistics2', 'admin::ordersStat2');
x::route('/admin/orders_statistics/graph', 'admin::orders_stat_graph');
x::route('/admin/graph_json', 'admin::graph_json');
x::route('/admin/attachOrder/(\d+)', 'admin::attachOrder');
x::route('/admin/wishlist', 'admin::wishlist');
x::route('/admin/sms_editor', 'admin::sms_editor');
x::route('/admin/edit_free_shipping', 'admin::edit_free_shipping');
x::route('/admin/waybill/?(\w+)?', 'waybill::admin_view');
x::route('/admin/add2waybill/(\d+)/?(\d+)?', 'waybill::addItem');
x::route('/admin/mysqldump', 'admin::mysqldump');
x::route('/admin/slider/?(\w+)?', 'admin::slider');
x::route('/admin/product_image', 'admin::product_image');
x::route('/admin/cashbox/?(\w+)?', 'cashbox::action');
x::route('/admin/monitoring', 'monitoring::attachment_list');
x::route('/admin/resize-photo', 'product::resize_photo');
x::route('/admin/barcode/(\d{13,13})\.png', 'barcode::image');
x::route('/admin/collecting', 'admin::collecting');
x::route('/admin/brands', 'admin::brands');
x::route('/admin/pnl', 'admin::pnl');
x::route('/admin/sverka', 'admin::sl_sverka');
x::route('/admin/bills', 'admin::bills');
x::route('/str2url', 'str::str2url_ajax');

x::route('/routine/(\w+)', 'action.routine.php');

x::route('/delivery/list/(points|courier|pr|sl_points|sl_courier|locations|metro)', 'delivery::json');

x::route('/ppapi', 'ppapi::ajax');
x::route('/waybill/ajax', 'waybill::ajax');

x::route('/admin/download_example/([\w\-]+\.csv)', 'admin::download_example_csv');

// --- RUN IT! ---

if(substr($_SERVER['REQUEST_URI'],0,6) == '/admin') {
	x::run('admin::layout');
}
elseif(defined('NEW_STYLE')) {
	x::run('shop::layout2');
}
elseif(defined('NEW_STYLE2')) {
	x::run('shop::layout3');
}
else {
	x::run('shop::layout');
}

// откуда пришел клиент (для следующего запроса)
$_SESSION['referer'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

session_write_close();

if(x::config('DEBUG')) {
	test::globals();
}

?>