<?php

class pnl {
	
	static $m_names = [
		1=>'Январь', 2=>'Февраль', 3=>'Март',
		4=>'Апрель', 5=>'Май', 6=>'Июнь',
		7=>'Июль', 8=>'Август', 9=>'Сентябрь',
		10=>'Октябрь', 11=>'Ноябрь', 12=>'Декабрь'
	];
	
	static $accounts = [
		'fee' => ['70601 810 2028 6121 0204', '47423 810 3028 6000 1758', '70601 810 6028 6121 0202'],
		'suppliers' => ['40702 810 8000 0000 6051', '40817 810 0382 5621 5121', '40702 810 0027 2000 1120', '40702 810 8503 4003 8418', '40702 810 5027 2000 0074', '40802 810 5023 0000 0831', '40702 810 5023 7000 0097'],
		'tax' => ['40101 810 1000 0001 0002']
	];
	
	static function main() {
		$action = REQUEST('action');
		if($action) {
			$response = 'no action';
			if($action == 'import_excerpt') {
				$response = self::import_excerpt();
			}
			if($action == 'update_pnl_data') {
				$response = self::update_pnl_data();
			}
			if($action == 'update-visitors') {
				$response = self::update_visitors();
			}
			if($action == 'update-company_name') {
				$response = self::update_company_name();
			}
			exit($response);
		}
		self::view();
	}
	
	// Разбор и импорт выписки по счету АльфаБанка
	static function import_excerpt() {
		if(!isset($_FILES['excerpt'])) {
			return 'Нет файла для загрузки';
		}
		if(!$file = uploadFile($_FILES['excerpt'], FILES.'/excerpt/')) {
			return 'Ошибка загрузки файла. Попробуйте еще раз.';
		}
		try {
			include 'PHPExcel/IOFactory.php';
			$inputFileType = PHPExcel_IOFactory::identify($file);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($file);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		function check_account($account) {
			$account = preg_replace('/\D/', '', $account);
			return (strlen($account) == 20);
		}
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();
		db::prepare('insert ignore into admin_excerpt_alfa values(?,?,?,?,?,?,?,?,?,?,?,?,?)');
		// echo "<pre>\n";
		$i = 0;
		for ($row = 1; $row <= $highestRow; $row++) {
			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE)[0];
			if(!check_account($rowData[7])) {
				// print_r($rowData);
				continue;
			}
			$data = array_slice($rowData, 0, 13);
			$date = DateTime::createFromFormat('d.m.Y', $data[0]);
			$data[0] = $date->format('Y-m-d');
			$data[2] = correct_float($data[2]);
			$data[3] = correct_float($data[3]);
			db::set($data);
			db::execute();
			$i += db::count();
		}
		// echo "\n</pre>";
		return $i ? strval($i) : 'Файл обработан, все строки пропущены';
	}
	
	static function get_excerpt_debit($year = NULL) {
		$data['year'] = $year ?: date('Y');
		db::query('select op_date, month(op_date) as month, company_name, account, sum(debit) as sum, (select name from admin_pnl_replace where account = e.account) as name_replace from admin_excerpt_alfa e where debit > 0 and year(op_date) = :year group by account, month order by if(name_replace is not null, name_replace, company_name)', $data);
		return db::fetchAll();
	}
	
	static function get_excerpt_credit($year = NULL) {
		$data['year'] = $year ?: date('Y');
		db::query('select op_date, month(op_date) as month, company_name, account, sum(credit) as sum, (select name from admin_pnl_replace where account = e.account) as name_replace from admin_excerpt_alfa e where credit > 0 and year(op_date) = :year group by account, month order by if(name_replace is not null, name_replace, company_name)', $data);
		return db::fetchAll();
	}
	
	static function get_orders_by_status($status, $year = NULL) {
		$data['year'] = $year ?: date('Y');
		$sql = 'select month(ctime) as month, (sum(total_sum) + sum(delivery_price) - sum(bonus) - sum(coupon) - sum(cumulative)) as sum, sum(delivery_price) as delivery_price, round(sum(delivery_price_real)) as delivery_price_real, count(*) as num from orders where year(ctime) = :year';
		if(is_array($status)) {
			$sql .= ' and status in('.implode(',',$status).')';
		}
		elseif($status) {
			$sql .= ' and status = :status';
			$data['status'] = $status;
		}
		$sql .= ' group by month order by month';
		db::query($sql, $data);
		$result = [];
		while($row = db::fetchArray()) {
			$result[$row['month']] = $row;
		}
		return $result;
	}
	
	static function get_orders_buyin_by_status($status, $year = NULL) {
		$data['year'] = $year ?: date('Y');
		$sql = 'select month(ctime) as month, cart_items from orders where year(ctime) = :year';
		if(is_array($status)) {
			$sql .= ' and status in('.implode(',',$status).')';
		}
		elseif($status) {
			$sql .= ' and status = :status';
			$data['status'] = $status;
		}
		$sql .= ' order by month';
		db::query($sql, $data);
		$order_items = db::fetchAll();
		$order_items = array_group($order_items, 'month');
		$result = [];
		foreach($order_items as $month => $orders) {
			$sum = 0;
			foreach($orders as $order) {
				$items = json::decode($order['cart_items']);
				foreach($items as $item) {
					$sum += (@$item['price_buyin'] ?: 0) * $item['quantity'];
				}
				$result[$month] = $sum;
			}
		}
		return $result;
	}
	
	static function get_sum_rk_fee($year = NULL) {
		$data['year'] = $year ?: date('Y');
		db::query('select month, sum(sum) as sum from robokassa_ex where year = :year group by month', $data);
		$result = [];
		while($row = db::fetchArray()) {
			$result[$row['month']] = $row['sum'];
		}
		return $result;
	}
	
	static function get_pnl_years() {
		if(!$pnl_years = cache::get('pnl-years')) {
			$pnl_years = [];
			db::query('select distinct year(ctime) as y from orders order by y');
			while($year = db::fetchSingle()) $pnl_years[] = $year;
			cache::set('pnl-years', $pnl_years, 86400);
		}
		return $pnl_years;
	}
	
	static function update_pnl_data() {
		$years = self::get_pnl_years();
		$months = range(1,12);
		// подготовка записей в таблице, в дальнейшем строки будут только обновляться
		db::prepare('insert ignore into admin_pnl set y = :y, m = :m');
		foreach($years as $year) {
			foreach($months as $month) {
				$data = ['y' => $year, 'm' => $month];
				db::set($data);
				db::execute();
			}
		}
		foreach($years as $year) {
			$excerpt_debit = self::get_excerpt_debit($year);
			$excerpt_credit = self::get_excerpt_credit($year);
			$orders_all = self::get_orders_by_status(null, $year);
			$orders_confirmed = self::get_orders_by_status(2,$year);
			$orders_shipping = self::get_orders_by_status([3,4,8], $year);
			$orders_return = self::get_orders_by_status([9,10], $year);
			$orders_cancel = self::get_orders_by_status(5, $year);
			$orders_status_3 = self::get_orders_by_status(3, $year);
			$orders_status_4 = self::get_orders_by_status(4, $year);
			$orders_status_8 = self::get_orders_by_status(8, $year);
			$orders_status_9 = self::get_orders_by_status(9, $year);
			$orders_status_10 = self::get_orders_by_status(10, $year);
			$orders_delivery = self::get_orders_by_status([3,4,8,9,10], $year);
			$sum_rk_fee = self::get_sum_rk_fee($year);
			$sum_orders_buyin = self::get_orders_buyin_by_status([3,4,8,9,10], $year);
			$sum_orders_buyin_status_3 = self::get_orders_buyin_by_status(3, $year);
			$sum_orders_buyin_status_4 = self::get_orders_buyin_by_status(4, $year);
			$sum_orders_buyin_status_8 = self::get_orders_buyin_by_status(8, $year);
			$sum_orders_buyin_status_9 = self::get_orders_buyin_by_status(9, $year);
			$sum_orders_buyin_status_10 = self::get_orders_buyin_by_status(10, $year);
			foreach($months as $m) {
				$income_fact = $sum_buyin = $sum_buyin_diff = $expense_total = $sum_gross_profit = 0;
				foreach($excerpt_credit as $credit) {
					if($m == $credit['month']) $income_fact += $credit['sum'];
				}
				foreach($excerpt_debit as $debit) {
					if($m == $debit['month']) $expense_total += $debit['sum'];
				}
				$data = [
					'buyers' => intval(@$orders_all[$m]['num']),
					'orders' => intval(@$orders_all[$m]['num']),
					'orders_confirmed' => intval(@$orders_confirmed[$m]['num']),
					'orders_shipping' => intval(@$orders_shipping[$m]['num']),
					'orders_return' => intval(@$orders_return[$m]['num']),
					'orders_cancel' => intval(@$orders_cancel[$m]['num']),
					'orders_status_3' => intval(@$orders_status_3[$m]['num']),
					'orders_status_4' => intval(@$orders_status_4[$m]['num']),
					'orders_status_8' => intval(@$orders_status_8[$m]['num']),
					'orders_status_9' => intval(@$orders_status_9[$m]['num']),
					'orders_status_10' => intval(@$orders_status_10[$m]['num']),
					'sum_orders' => @$orders_all[$m]['sum'] ?: 0,
					'sum_orders_confirmed' => @$orders_confirmed[$m]['sum'] ?: 0,
					'sum_orders_shipping' => @$orders_shipping[$m]['sum'] ?: 0,
					'sum_orders_return' => @$orders_return[$m]['sum'] ?: 0,
					'sum_orders_cancel' => @$orders_cancel[$m]['sum'] ?: 0,
					'sum_orders_status_3' => @$orders_status_3[$m]['sum'] ?: 0,
					'sum_orders_status_4' => @$orders_status_4[$m]['sum'] ?: 0,
					'sum_orders_status_8' => @$orders_status_8[$m]['sum'] ?: 0,
					'sum_orders_status_9' => @$orders_status_9[$m]['sum'] ?: 0,
					'sum_orders_status_10' => @$orders_status_10[$m]['sum'] ?: 0,
					'sum_orders_buyin' => @$sum_orders_buyin[$m] ?: 0,
					'sum_orders_buyin_status_3' => @$sum_orders_buyin_status_3[$m] ?: 0,
					'sum_orders_buyin_status_4' => @$sum_orders_buyin_status_4[$m] ?: 0,
					'sum_orders_buyin_status_8' => @$sum_orders_buyin_status_8[$m] ?: 0,
					'sum_orders_buyin_status_9' => @$sum_orders_buyin_status_9[$m] ?: 0,
					'sum_orders_buyin_status_10' => @$sum_orders_buyin_status_10[$m] ?: 0,
					'sum_delivery_price' => @$orders_delivery[$m]['delivery_price'] ?: 0,
					'sum_delivery_price_real' => @$orders_delivery[$m]['delivery_price_real'] ?: 0,
					'income_total' => @$orders_shipping[$m]['sum'] ?: 0,
					'income_fact' => $income_fact,
					'sum_buyin' => $sum_buyin,
					'sum_buyin_diff' => $sum_buyin_diff,
					'sum_rk_fee' => intval(@$sum_rk_fee[$m]),
					'expense_total' => $expense_total
				];
				$data['sum_gross_profit'] = $data['sum_orders_shipping'] + $data['sum_orders_buyin_status_10'] - $data['sum_orders_buyin'] - $data['sum_rk_fee'] - $data['sum_delivery_price_real'] - $data['sum_orders_return'];
				$values_set = [];
				foreach($data as $column => $value) {
					$values_set[] = $column.' = :'.$column;
				}
				$values_set = implode(', ', $values_set);
				$data['year'] = $year;
				$data['month'] = $m;
				$sql = 'update admin_pnl set '.$values_set.' where y = :year and m = :month';
				db::query($sql, $data);
			}
		}
		self::update_pnl_internal();
		return 'ok';
	}
	
	// values calculated in-table
	static function update_pnl_internal() {
		db::query('update admin_pnl pnl set sum_buyin = ifnull((select sum(price_buyin) from wb_log where year(rtime) = pnl.y and month(rtime) = pnl.m), 0)');
		// db::query('update admin_pnl set expense_total = (expense_total + sum_rk_fee + sum_delivery_price_real)');
		db::query('update admin_pnl set conversion = ifnull(round(buyers / visitors * 100, 2), 0), orders_return_pc = ifnull(round(orders_return / orders * 100, 2), 0), orders_success_pc = ifnull(round(orders_shipping / orders * 100, 2), 0), sum_buyin_pc = ifnull(round(sum_buyin / income_total * 100, 2), 0), order_sum_avg = ifnull(round(sum_orders / orders), 0), balance_expected = income_total - expense_total, balance_fact = income_fact - expense_total');
	}
	
	static function update_visitors() {
		$value = absint(POST('value'));
		$year = absint(POST('year'));
		$month = absint(POST('month'));
		$years = self::get_pnl_years();
		if(!in_array($year, $years)) return 'Неверно задан год';
		if($month < 1 || $month > 12) return 'Неверно задан месяц';
		db::query('update admin_pnl set visitors = ? where y = ? and m = ?', $value, $year, $month);
		$result = db::count();
		self::update_pnl_internal();
		return $result ? 'ok' : 'Not changed';
	}
	
	static function update_company_name() {
		$account = trim(POST('account'));
		$name = trim(POST('value'));
		if(!$account) return 'Ошибка! Не задан номер счета.';
		if($name) {
			db::query('replace into admin_pnl_replace set account = ?, name = ?', $account, $name);
		}
		else {
			db::query('delete from admin_pnl_replace where account = ?', $account);
		}
		return db::count() ? 'ok' : 'Not changed';
	}
	
	static function view() {
		make_title('PnL Yamibox');
		tpl::load('admin-pnl');
		// year selector options
		$year = GET('year') ?: '2015';
		$years = self::get_pnl_years();
		$years = array_reverse($years);
		foreach($years as $y) {
			tpl::set('option-value', $y);
			tpl::set('option-text', $y);
			tpl::set('option-selected', $y == $year ? ' selected' : '');
			tpl::make('option', 'year-option');
		}
		function _money_format(&$val) {
			if(preg_match('/^(\-?\d+)$/', $val)) $val = number_format($val, 0, '.', ' ');
			elseif(is_float($val)) $val = number_format($val, 2, '.', ' ');
		}
		// table output
		$excerpt_debit = self::get_excerpt_debit($year);
		$excerpt_credit = self::get_excerpt_credit($year);
		$excerpt_debit = array_group($excerpt_debit, 'account');
		$excerpt_credit = array_group($excerpt_credit, 'account');
		foreach($excerpt_credit as $account => $op_list) {
			tpl::set('account', $account);
			tpl::set('company_name', $op_list[0]['name_replace'] ?: $op_list[0]['company_name']);
			$year_credit = 0;
			for($m = 1; $m <= 12; $m++) {
				tpl::set('credit', '-');
				foreach($op_list as $credit) {
					if($credit['month'] == $m) {
						$credit_sum = round($credit['sum']);
						$year_credit += $credit_sum;
						_money_format($credit_sum);
						tpl::set('credit', $credit_sum);
						break;
					}
				}
				tpl::make('col-credit');
			}
			tpl::set('year_credit', $year_credit);
			tpl::make('row-credit');
			tpl::clear('col-credit');
		}
		$supplier_debit_sum = [];
		foreach($excerpt_debit as $account => $op_list) {
			$is_tax = in_array($account, self::$accounts['tax']);
			$is_fee = in_array($account, self::$accounts['fee']);
			$is_supplier = in_array($account, self::$accounts['suppliers']);
			tpl::set('account', $account);
			tpl::set('company_name', $op_list[0]['name_replace'] ?: $op_list[0]['company_name']);
			$year_debit = 0;
			for($m = 1; $m <= 12; $m++) {
				tpl::set('debit', '-');
				foreach($op_list as $debit) {
					if($debit['month'] == $m) {
						$debit_sum = round($debit['sum']);
						$year_debit += $debit_sum;
						_money_format($debit_sum);
						tpl::set('debit', $debit_sum);
						if($is_supplier) {
							if(!isset($supplier_debit_sum[$m])) $supplier_debit_sum[$m] = 0;
							$supplier_debit_sum[$m] += $debit['sum'];
						}
						break;
					}
				}
				tpl::set('year_debit', $year_debit);
				if($is_tax) tpl::make('col-debit-tax');
				elseif($is_fee) tpl::make('col-debit-fee');
				elseif($is_supplier) tpl::make('col-debit-supplier');
				else tpl::make('col-debit');
			}
			if($is_tax) tpl::make('row-debit-tax');
			elseif($is_fee) tpl::make('row-debit-fee');
			elseif($is_supplier) tpl::make('row-debit-supplier');
			else tpl::make('row-debit');
			tpl::clear('col-debit', 'col-debit-tax', 'col-debit-fee', 'col-debit-supplier');
		}
		db::query('select * from admin_pnl where y = ?', $year);
		$pnl_rows = db::fetchAll();
		$first = reset($pnl_rows);
		$totals = [];
		foreach($pnl_rows as $data) {
			if(isset($supplier_debit_sum[$data['m']])) {
				$s_debit = $supplier_debit_sum[$data['m']];
				$sum_buyin_diff = $s_debit ? $data['sum_buyin'] - $s_debit : 0;
			}
			else {
				$sum_buyin_diff = 0;
			}
			$sum_buyin_diff = round($sum_buyin_diff);
			// year totals
			foreach($data as $key => $value) {
				if(in_array($key, ['y','m','is_final','utime'])) continue;
				if(!isset($totals[$key])) $totals[$key] = 0;
				$totals[$key] += $value;
			}
			if(!isset($totals['sum_buyin_diff'])) $totals['sum_buyin_diff'] = 0;
			$totals['sum_buyin_diff'] += $sum_buyin_diff;
			// month
			_money_format($sum_buyin_diff);
			tpl::set('sum_buyin_diff', $sum_buyin_diff);
			tpl::make('col-sum_buyin_diff');
			array_walk($data, '_money_format');
			tpl::push($data);
			tpl::set('data-year', $year);
			tpl::set('data-month', $data['m']);
			tpl::set('month-name', self::$m_names[$data['m']]);
			tpl::make('col-month-name');
			tpl::make('col-visitors');
			tpl::make('col-buyers');
			tpl::make('col-conversion');
			tpl::make('col-orders');
			tpl::make('col-orders_confirmed');
			tpl::make('col-orders_shipping');
			tpl::make('col-orders_return');
			tpl::make('col-orders_cancel');
			tpl::make('col-orders_return_pc');
			tpl::make('col-orders_status_3');
			tpl::make('col-orders_status_4');
			tpl::make('col-orders_status_8');
			tpl::make('col-orders_status_9');
			tpl::make('col-orders_status_10');
			tpl::make('col-sum_orders');
			tpl::make('col-sum_orders_confirmed');
			tpl::make('col-sum_orders_shipping');
			tpl::make('col-sum_orders_return');
			tpl::make('col-sum_orders_cancel');
			tpl::make('col-sum_orders_status_3');
			tpl::make('col-sum_orders_status_4');
			tpl::make('col-sum_orders_status_8');
			tpl::make('col-sum_orders_status_9');
			tpl::make('col-sum_orders_status_10');
			tpl::make('col-sum_orders_buyin');
			tpl::make('col-sum_orders_buyin_status_3');
			tpl::make('col-sum_orders_buyin_status_4');
			tpl::make('col-sum_orders_buyin_status_8');
			tpl::make('col-sum_orders_buyin_status_9');
			tpl::make('col-sum_orders_buyin_status_10');
			tpl::make('col-sum_delivery_price');
			tpl::make('col-sum_delivery_price_real');
			tpl::make('col-orders_success_pc');
			tpl::make('col-order_sum_avg');
			tpl::make('col-income_total');
			tpl::make('col-income_fact');
			tpl::make('col-sum_buyin');
			tpl::make('col-sum_buyin_pc');
			tpl::make('col-sum_rk_fee');
			tpl::make('col-expence_total');
			tpl::make('col-balance_expected');
			tpl::make('col-balance_fact');
			tpl::make('col-sum_gross_profit');
		}
		// year totals
		$active_months = ($year == date('Y')) ? date('n') : 12;
		foreach($totals as $key => $value) {
			$new_key = 'year_'.$key;
			if(in_array($key, ['visitors','conversion','orders_return_pc','orders_success_pc','order_sum_avg','sum_buyin_pc'])) {
				$totals[$new_key] = round($value/$active_months, 2);
			}
			else {
				$totals[$new_key] = $value;
			}
			unset($totals[$key]);
		}
		array_walk($totals, '_money_format');
		tpl::push($totals);
		tpl::set('year', $year);
		if(account::$admin || account::$employee) {
tpl::load('admin-pnl');
			tpl::make('admin-pnl-restricted-1');
			tpl::make('admin-pnl-restricted-2');
		}
		tpl::make('admin-pnl', 'main');
	}
	
}

?>