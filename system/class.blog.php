<?php

class Blog {
	
	static function posts($path, $post_url = NULL) {
		if($post_url) return self::post($post_url);
		make_title('Ямиблог');
		tpl::set('document_meta_keywords', 'Блог, ямиблог, yamiblog');
		tpl::set('document_meta_description', 'Новостной блог интернет-магазина корейской косметики');
		shop::makeBreadcrumbs();
		$sql = 'select count(*) from blog';
		$total_posts = db::querySingle($sql);
		$sql = 'select * from blog order by id desc limit :offset, :limit';
		db::query($sql, array(
			'offset' => offset(PAGE_LIMIT_BLOG),
			'limit' => PAGE_LIMIT_BLOG
		));
		while($blog = db::fetchArray()) {
			if(!$blog['image']) $blog['image'] = 'blog.jpg';
			tpl::push($blog);
			tpl::make('blog-item');
		}
		shop::paginator($total_posts, PAGE_LIMIT_BLOG);
		if(tpl::get('blog-item')) tpl::make('blog-list', 'main');
	}
	
	static function post($post_url) {
		db::query('select * from blog where title_url = ?', $post_url);
		if(!$record = db::fetchArray()) {
			return x::do_action('404.php');
		}
		tpl::set('document_meta_keywords', $record['meta_keywords']);
		tpl::set('document_meta_description', $record['meta_description']);
		shop::makeBreadcrumbs(['/yamiblog/'.$post_url => $record['title']]);
		tpl::push($record);
		if($record['image']) tpl::make('blog-item-image');
		if($post_url == 'dostavka') {
			/* $region = COOKIE('region_to') ?: 'Москва';
			$city = COOKIE('city_to') ?: 'Москва';
			$region_match = false;
			foreach(shoplogistics::get_regions() as $oblast) {
				tpl::set('option-value', $oblast['name']);
				tpl::set('option-text', $oblast['name']);
				tpl::set('option-selected', $oblast['name'] == $region ? ' selected' : '');
				tpl::make('option', 'region-option');
				if($oblast['name'] == $region) $region_match = true;
			}
			if($region_match) {
				$cities = shoplogistics::get_city_list($region);
				foreach($cities as $c) {
					tpl::set('option-value', $c['name']);
					tpl::set('option-text', $c['name']);
					tpl::set('option-selected', $c['name'] == $city ? ' selected' : '');
					tpl::make('option', 'city-option');
				}
			}
			tpl::make('delivery-map', 'main'); */
		}
		tpl::make('blog-item-detailed', 'main');
	}
	
	static function addRecord() {
		
	}
	
	static function addComment() {
		
	}
	
	static function showPage($path, $title_url) {
		if(AJAX && in_array($title_url, ['oplata', 'dostavka'])) {
			$html = tpl::make('page-'.$title_url);
			exit($html);
		}
		return self::post($title_url);
	}
	
	static function brands_page() {
		shop::makeBreadcrumbs(['/brands' => 'Бренды']);
		$brands = shop::get_brands('order by name');
		$filters = json::get('filters');
		tpl::load('intro');
		$names = [];
		foreach($brands as $brand) {
			$names[] = $brand['name'];
			if(!$brand['logo']) continue;
			foreach($filters as $filterId => $text) {
				if($text == $brand['name']) break;
			}
			tpl::set('brand-name', $brand['name']);
			tpl::set('brand-href', '/catalog?filter='.$filterId);
			tpl::set('brand-logo-src', '/files/promo/logo/'.$brand['logo']);
			tpl::make('intro-brand');
		}
		tpl::set('document_meta_keywords', implode(',', $names));
		tpl::make('brands-page', 'main');
	}
	
}

tpl::load('shop');
tpl::load('blog');

?>