<?php

tpl::load('podbirator');

if(REQUEST_METHOD == 'POST') {
	$filters[] = absint(POST('stype'));
	$filters[] = absint(POST('sstate'));
	$bdate = POST('bdate');
	$bdate = new DateTime($bdate);
	$now = new DateTime();
	$age = $bdate->diff($now)->format('%y');
	if($age > 45) $age_filter = 24;
	elseif($age > 35) $age_filter = 23;
	elseif($age > 25) $age_filter = 22;
	$filters = implode(',', $filters);
	$labels_morning = [44=>'Очищение',51=>'Тонер',53=>'Эссенция',52=>'Эмульсия',37=>'Крем для лица',38=>'Крем для глаз',1=>'ББ-крем'];
	$labels_evening = [72=>'Демакияж',44=>'Очищение',51=>'Тонер',53=>'Эссенция',52=>'Эмульсия',95=>'Ночной крем для лица',38=>'Ночной крем для глаз'];
	$sql = 'select '.CATALOG_SELECT.', (select max(category_id) from product_category where product_id = p.id) as cid from product p where stock > 1 and status = 1 and id in(select distinct product_id from product_filters where filter_id in('.$filters.')) and id in(select product_id from product_category where category_id in(44, 51, 53, 52, 37, 38, 30, 1)) order by rand() limit 7';
	db::query($sql);
	$products = db::fetchAll();
	foreach($products as $product) {
		tpl::push($product);
		tpl::set('img-src', product::getImage($product));
		tpl::make('podbirator-product', 'morning');
	}
	$html = tpl::make('podbirator-result');
	die($html);
}
shop::makeBreadcrumbs(['/podbirator' => 'Подбиратор']);
if(account::$auth) {
	tpl::set('bdate', account::$info['bdate']);
}
tpl::make('podbirator', 'main');

?>