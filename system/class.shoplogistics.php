<?php

function modify_range($range) {
	preg_match('/(\d+)?\-?(\d+)?/', $range, $match);
	$min = $match[1] + 2;
	$max = isset($match[2]) ? $match[2] + 2 : 0;
	return $max ? $min.'-'.$max : $min;
}

class shoplogistics {

	const API_URL = 'http://client-shop-logistics.ru/index.php?route=deliveries/api';
	const API_KEY = '9cbc7cb73ac8c61402c442589437f79e';

	static function request($xml) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, self::API_URL);
		curl_setopt($curl, CURLOPT_POST, TRUE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl, CURLOPT_POSTFIELDS, 'xml='.urlencode(base64_encode($xml)));
		$response = curl_exec($curl);
		curl_close($curl);
		return $response;
	}
	
	static function get_deliveries() {
		$xml = '<?xml version="1.0"?>
		<request>
		<function>get_deliveries</function>
		<api_id>'.self::API_KEY.'</api_id>
		<delivery_date></delivery_date>
		<date_added/>
		<status/>
		<order_id></order_id>
		<code></code>
		<start_date_added>2015-01-01</start_date_added>
		<end_date_added>2015-01-26</end_date_added>
		</request>';
		$response = self::request($xml);
		return str::xmltoarray($response);
	}

	static function get_tarifs() {
		// all - все, pickup - только самовывоз, courier - только курьерская
		$xml = '<?xml version="1.0"?>
		<request>
		<function>get_all_tarifs</function>
		<api_id>'.self::API_KEY.'</api_id>
		<from_city_code>405065</from_city_code>
		<weight>1.5</weight>
		<delivery_type>all</delivery_type>
		<num>300</num>
		<max_price>0</max_price>
		</request>';
		$response = self::request($xml);
		$data = str::xmltoarray($response);
		return $data['tarifs']['tarif'];
	}

	static function get_dictionary($type) {
		/*
		city - список городов доставки,
		metro - список метро,
		pickup - список пунктов самовывоза
		filials - филиалы,
		status - список статусов,
		partners - список партнеров
		oblast - список областей
		district - список районов
		*/
		$xml = '<?xml version="1.0"?>
		<request>
		<function>get_dictionary</function>
		<api_id>'.self::API_KEY.'</api_id>
		<dictionary_type>'.$type.'</dictionary_type>
		</request>';
		$response = self::request($xml);
		return str::xmltoarray($response);
	}
	
	static function get_order_status() {
		$xml = '<?xml version="1.0"?>
		<request>
		<function>get_order_status_array</function>
		<api_id>'.self::API_KEY.'</api_id>
		<deliveries><!-- order-code -->
		<code>{#tracking_id}</code><!-- order-code -->
		</deliveries>
		</request>';
		tpl::load_string($xml, 'sl-order-status-request');
		db::query('select tracking_id from orders where tracking_id is not null and status not in(4,5,6,7,10) and id >= 4120');
		if(!db::count()) return false;
		while($order = db::fetchArray()) {
			tpl::set('tracking_id', $order['tracking_id']);
			tpl::make('order-code');
		}
		$xml = tpl::make('sl-order-status-request');
		$response = self::request($xml);
		file_put_contents(FILES.'/shoplogistics/orders.xml', $response);
		$data = str::xmltoarray($response)['deliveries']['delivery'];
		return $data;
	}
	
	static function get_post_deliveries_array() {
		$xml = '<?xml version="1.0"?>
		<request>
		<function>get_post_deliveries_array</function>
		<api_id>'.self::API_KEY.'</api_id>
		<deliveries>
		<!-- order-code-sl-pr --><code>{#tracking_id}</code><!-- order-code-sl-pr -->
		</deliveries>
		</request>';
		tpl::load_string($xml, 'sl-pr-status-request');
		db::query('select tracking_id from orders where length(tracking_id) = 18 and dtype = 3 and status not in(4,5,6,7,10) and id >= 4120');
		if(!db::count()) return false;
		while($order = db::fetchArray()) {
			tpl::set('tracking_id', $order['tracking_id']);
			tpl::make('order-code-sl-pr');
		}
		$xml = tpl::make('sl-pr-status-request');
		$response = self::request($xml);
		$response = preg_replace('/<service_comment>.*?<\/service_comment>/ui', '', $response);
		file_put_contents(FILES.'/shoplogistics/orders-pr.xml', $response);
		$data = str::xmltoarray($response)['deliveries']['delivery'];
		return $data;
	}
	
	static function get_metros($city) {
		$key = 'metro-'.md5($city);
		if(!$metros = cache::get($key)) {
			db::query('select * from sl_metros where city_name = ? order by name', $city);
			$metros = db::fetchAll();
			cache::set($key, $metros, 900); // 15 minutes cache
		}
		return $metros;
	}
	
	static function get_city_list($region) {
		db::query('select * from sl_cities where oblast_code = (select code from sl_regions where name = ?) order by name', $region);
		return db::fetchAll();
	}
	
	static function get_regions() {
		if(!$regions = cache::get('sl-regions')) {
			$regions = [];
			db::query('select * from sl_regions order by sort, name');
			while($region = db::fetchArray()) {
				$regions[$region['code']] = $region;
			}
			cache::set('sl-regions', $regions);
		}
		return $regions;
	}
	
	static function get_pickups($city, $region, $sum = 0) {
		$points = [];
		$dservice = json::get('dservice');
		$sl_place_type = json::get('sl_place_type');
		$city = mb_strtolower($city, 'utf-8');
		$moscow = str::is_moscow($city);
		// ShopLogistics
		db::query('select *, (select price from sl_tarifs where pickup_place_code = pu.code_id) as delivery_price from sl_pickups pu where city_code_id in(select code_id from sl_cities where name like :city) order by delivery_partner, name', ['city' => $city]);
		$pickups = db::fetchAll();
		foreach($pickups as $pickup) {
			$dkey = $pickup['delivery_partner'] ? $sl_place_type[$pickup['delivery_partner']] : 19;
			$delivery_price = ceilby($pickup['delivery_price'], 5);
			$point = [
				'code' => $pickup['code_id'],
				'delivery_name' => $dservice[$dkey]['name'],
				'dservice' => $dkey,
				'dtype' => 5,
				'name' => $pickup['name'],
				'cityname' => $pickup['city_name'],
				'address' => $pickup['address'],
				'phone' => $pickup['phone'],
				'metro' => $pickup['metro'],
				'latitude' => $pickup['latitude'],
				'longitude' => $pickup['longitude'],
				'delivery_price' => $delivery_price,
				'delivery_price_real' => $pickup['delivery_price'],
				'days' => modify_range($pickup['srok_dostavki']),
				'worktime' => $pickup['worktime']
			];
			$points[] = $point;
		}
		// PickPoint
		/* $city_f = preg_replace('#\,\s*(москва|санкт-петербург)#ui', '', $city);
		db::query("select id as code, 'PickPoint' as delivery_name, 5 as dservice, 4 as dtype, name, citiname as cityname, address, null as phone, metro, latitude, longitude, delivery_price, delivery_price as delivery_price_real, days, null as worktime from points_pp where match(citiname) against(:city in boolean mode)", ['city'=>$city_f]);
		while($point = db::fetchArray()) {
			$points[] = $point;
		} */
		$sum = $sum ?: cart::$sum_with_discounts;
		$free = 100000;
		$central = str::is_central($city, $region);
		$required_sum = $central ? 3000 : 4000;
		foreach($points as &$point) {
			$limit = ceilby($point['delivery_price'] * 10, 100);
			// apply free shipping
			if($sum >= $required_sum && $sum >= $limit) $point['delivery_price'] = 0;
			if($limit < $free && $point['dservice'] != 10) $free = $limit;
		}
		if($free < $required_sum) $free = $required_sum;
		return ['list' => $points, 'free' => $free, 'central' => $central];
	}
	
	static function get_courier($city, $region, $sum = 0) {
		$deliveries = [];
		$sum = $sum ?: cart::$sum_with_discounts;
		$dservice = json::get('dservice');
		$sl_place_type = json::get('sl_place_type');
		$moscow = str::is_moscow($city);
		db::query('select *, (select name from sl_cities where code_id = t.to_city_code) as cityname from sl_tarifs t where tarifs_type = 1 and to_city_code = (select code_id from sl_cities where name = :city) order by delivery_partner', ['city' => $city]);
		while($delivery = db::fetchArray()) {
			$dkey = $delivery['delivery_partner'] ? $sl_place_type[$delivery['delivery_partner']] : 19;
			$delivery_price = ceilby($delivery['price'], 5);
			$deliveries[] = [
				'city' => $delivery['cityname'],
				'dservice' => $dkey,
				'dtype' => 5,
				'name' => $dservice[$dkey]['name'],
				'price' => $delivery_price,
				'price_real' => $delivery['price'],
				'days' => modify_range($delivery['srok_dostavki'])
			];
		}
		$free = 100000;
		$central = str::is_central($city, $region);
		$required_sum = $central ? 3000 : 4000;
		foreach($deliveries as &$delivery) {
			$delivery['price'] += 100;
			$limit = ceilby($delivery['price'] * 10, 100);
			// apply free shipping
			if($sum >= $required_sum && $sum >= $limit) $delivery['price'] = 0;
			if($limit < $free) $free = $limit;
		}
		if($free < $required_sum) $free = $required_sum;
		return ['list' => $deliveries, 'free' => $free];
	}
	
	static function import_tarifs() {
		$tarifs = self::get_tarifs();
		if(!$tarifs) {
			log_write('Не удалось получить тарифы ShopLogistics!');
			return false;
		}
		// save response
		$file = FILES.'/shoplogistics/tarifs.json';
		file_put_contents($file, json::encode($tarifs));
		// $tarifs = json::decode(file_get_contents($file));
		// put data into db
		db::query('truncate table sl_tarifs');
		$sql = 'insert into sl_tarifs set ';
		foreach(reset($tarifs) as $column => $value) {
			$pairs[] = $column . ' = :' . $column;
		}
		$sql .= implode(', ', $pairs);
		db::prepare($sql);
		$num = 0;
		foreach($tarifs as $tarif) {
			array_walk($tarif, function(&$val, $key) {
				if(is_array($val)) $val = NULL;
				if($key == 'comission_percent') {
					$val = floatval(str_replace(',', '.', $val));
				}
			});
			db::set($tarif);
			db::execute();
			$num++;
		}
		return ['shoplogistics::import_tarifs' => $num];
	}
	
	static function import_pickups() {
		$data = self::get_dictionary('pickup');
		$pickups = $data['pickups']['pickup'];
		if(!$pickups) {
			log_write('Не удалось получить список ПВЗ ShopLogistics!');
			return false;
		}
		$file = FILES.'/shoplogistics/pickups.json';
		file_put_contents($file, json::encode($pickups));
		db::query('truncate table sl_pickups');
		$sql = 'insert into sl_pickups set ';
		$columns = reset($pickups);
		$columns = array_slice($columns, 0, 17);
		foreach($columns as $column => $value) {
			$pairs[] = $column . ' = :' . $column;
		}
		$sql .= implode(', ', $pairs);
		db::prepare($sql);
		$num = 0;
		foreach($pickups as $pickup) {
			$pickup = array_slice($pickup, 0, 17);
			array_walk($pickup, function(&$val, $key) {
				if(is_array($val)) $val = NULL;
			});
			db::set($pickup);
			db::execute();
			$num++;
		}
		return ['shoplogistics::import_pickups' => $num];
	}
	
	static function import_metros() {
		$file = FILES.'/shoplogistics/metros.json';
		$result = shoplogistics::get_dictionary('metro');
		$metros = $result['metros']['metro'];
		file_put_contents($file, json::encode($metros));
		if(!$metros) {
			log_write('Ошибка импорта насленных пунктов из ShopLogistics!');
			return false;
		}
		db::query('truncate table sl_metros');
		db::prepare('insert into sl_metros set city_name = :city_name, city_code_id = :city_code_id, name = :name, code_id = :code_id');
		foreach($metros as $metro) {
			if($metro['name'] == 'test') continue;
			db::set($metro);
			db::execute();
		}
		return ['shoplogistics::import_metros' => count($metros)];
	}
	
	static function import_cities() {
		$file = FILES.'/shoplogistics/cities.json';
		if(TRUE) {
			$result = shoplogistics::get_dictionary('city');
			$cities = $result['cities']['city'];
			file_put_contents($file, json::encode($cities));
		}
		else {
			$json = file_get_contents($file);
			$cities = json::decode($json);
		}
		if(!$cities) {
			log_write('Ошибка импорта насленных пунктов из ShopLogistics!');
			return false;
		}
		db::query('truncate table sl_cities');
		db::prepare('insert into sl_cities set name = :name, code_id = :code_id, is_courier = :is_courier, is_filial = :is_filial, oblast_code = :oblast_code, district_code = :district_code, kladr_code = :kladr_code');
		foreach($cities as $city) {
			$city['district_code'] = NULL;
			db::set($city);
			db::execute();
		}
		return ['shoplogistics::import_cities' => count($cities)];
	}
	
	static function import_regions() {
		$file = FILES.'/shoplogistics/regions.json';
		if(TRUE) {
			$result = shoplogistics::get_dictionary('oblast');
			$regions = $result['oblast_list']['oblast'];
			file_put_contents($file, json::encode($regions));
		}
		else {
			$json = file_get_contents($file);
			$regions = json::decode($json);
		}
		if(!$regions) {
			log_write('Ошибка импорта регионов из ShopLogistics!');
			return false;
		}
		db::prepare('replace into sl_regions set name = :name, code = :code, sort = :sort');
		foreach($regions as $region) {
			if(!$region['name']) continue;
			$region['code'] = intval($region['code']);
			$region['sort'] = 3;
			if($region['code'] == 92) $region['sort'] = 0;
			if($region['code'] == 1) $region['sort'] = 1;
			if($region['code'] == 62) $region['sort'] = 2;
			db::set($region);
			db::execute();
		}
		db::query('select * from sl_regions r where not exists(select 1 from sl_cities where oblast_code = r.code)');
		foreach(db::fetchAll() as $region) {
			db::query('delete from sl_regions where code = ?', $region['code']);
		}
		cache::clear('sl-regions');
		return ['shoplogistics::import_regions' => count($regions)];
	}
	
	static function send_order($order) {
		if(is_numeric($order)) {
			$order = db::querySingle('select * from orders where id = '.$order, true);
		}
		if(!$order) return 'Заказ не найден';
		if($order['status'] != 2) return 'Заказ '.$order['id'].' не согласован';
		if($order['cstatus'] != 3) return 'Заказ '.$order['id'].' не собран';
		$template = $order['dtype'] == 3 ? 'sl-create-order-pr' : 'sl-create-order';
		tpl::load($template);
		$products = json::decode($order['cart_items']);
		$address = json::decode($order['address']);
		$delivery_data = $order['delivery_data'] ? json::decode($order['delivery_data']) : false;
		$tomorrow = strtotime('tomorrow');
		$delivery_date = date('Y-m-d', $tomorrow);
		$delivery_wd = date('w', $tomorrow);
		tpl::set('delivery-date', $delivery_date);
		tpl::set('time_from', $order['time_from'] ? $order['time_from'].':00' : '10:00');
		tpl::set('time_to', $order['time_to'] ? $order['time_to'].':00' : '19:00');
		tpl::set('order-date', date('Y-m-d'));
		tpl::set('city', $order['city']);
		tpl::set('region', $order['region']);
		tpl::set('email', $order['email']);
		tpl::set('order-id', $order['id']);
		tpl::set('address', str::address2str($address));
		tpl::set('zipcode', @$address['zipcode']);
		tpl::set('firstname', $order['firstname']);
		tpl::set('lastname', $order['lastname']);
		tpl::set('phone', $order['phone']);
		if($order['dtype'] == 3) {
			$pr_type = preg_match('#посылка#ui', $order['delivery_info']) ? 'разное' : '1 класс';
			tpl::set('pr_type', $pr_type);
		}
		$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
		tpl::set('price', $order['paid'] ? 0 : $order_sum);
		tpl::set('order-sum', ($order['dtype'] == 3 ? $order_sum : $order_sum - $order['delivery_price']));
		tpl::set('comment', $order['comment']);
		tpl::set('metro', @$address['metro'] ?: @$delivery_data['metro']);
		if($delivery_data) {
			tpl::set('pickup-place', @$delivery_data['name']);
			tpl::set('delivery_partner', @$delivery_data['delivery_partner']);
		}
		else {
			tpl::set('pickup-place', '');
			tpl::set('delivery_partner', '');
			tpl::set('metro', '');
		}
		foreach($products as $product) {
			// tpl::set('barcode', $product['barcode']);
			// tpl::make('sl-order-product-barcode');
			tpl::set('barcode', $product['barcode']);
			tpl::set('product-id', $product['id']);
			tpl::set('product-name', html2chars($product['name']));
			if($product['option_id']) {
				tpl::append('product-id', '-'.$product['option_id']);
				tpl::append('product-name', ' '.html2chars($product['option_name']));
			}
			tpl::set('product-quantity', $product['quantity']);
			tpl::set('product-price', $product['price']);
			tpl::make('sl-order-product');
		}
		$xml = tpl::make($template);
		tpl::clear('sl-order-product');
		file_put_contents(FILES.'/shoplogistics/orders/'.$order['id'].'-request.xml', $xml);
		$response = self::request($xml);
		file_put_contents(FILES.'/shoplogistics/orders/'.$order['id'].'-response.xml', $response);
		if(preg_match('#<code>(\d+)</code>#', $response, $match)) {
			$code = $match[1];
			db::query('update orders set tracking_id = ? where id = ?', $code, $order['id']);
			return $code;
		}
		$response = str::xmltoarray($response);
		return $response;
	}
	
	static function update_orders() {
		ob_start();
		$orders_sl = self::get_order_status();
		$sl_status = [];
		foreach(json::get('sl-order-status') as $s) {
			$sl_status[$s['name']] = $s['status'];
		}
		$num = 0;
		if($orders_sl) {
			$delivered = $intransit = $pickpoint = $confirmed = [];
			// обновление статусов заказов
			db::prepare('update orders set status = ?, dtime = ? where id = ? and status in(2,3,8,9) and cstatus = 3');
			foreach($orders_sl as $order) {
				if(!array_key_exists($order['status'], $sl_status)) continue;
				$order_id = intval($order['order_id']);
				$status = $sl_status[$order['status']];
				$dtime = in_array($status, [4,10]) ? date('Y-m-d H:i:s') : NULL;
				db::set([$status, $dtime, $order_id]);
				db::execute();
				if(!db::count()) continue;
				if($status == 2) {
					$confirmed[] = $order_id;
				}
				if($status == 4) {
					$delivered[] = $order_id;
					$num++;
				}
				if($status == 3) {
					$intransit[] = $order_id;
				}
				if($status == 8) {
					$pickpoint[] = $order_id;
				}
			}
			if($confirmed) {
				db::prepare('update orders set stime = if(stime is null, CURRENT_TIMESTAMP, stime) where id = :order_id');
				foreach($confirmed as $order_id) {
					db::set('order_id', $order_id);
					db::execute();
				}
			}
			// email-уведомление о полученном заказе
			if($delivered) {
				delivered_email($delivered);
				order::status_sms($delivered, 'order-delivered');
				// activate coupon for next order
				activate_coupons($delivered);
			}
			// email-уведомление о передаче в СД
			if($intransit) {
				intransit_email($intransit);
				order::status_sms($intransit, 'intransit-sl');
			}
			// email-уведомление о доставке в ПВЗ
			if($pickpoint) {
				pickpoint_email($pickpoint);
			}
			if($num) log_write('ShopLogistics: Обновлено заказов '.$num);
			else log_write('ShopLogistics: Доставленных заказов нет');
			tpl::append('main', '<pre>'.ob_get_clean().'</pre>');
			return ['shoplogistics::update_orders' => ['Согласовано' => $confirmed, 'Доставлено' => $delivered, 'В пути' => $intransit, 'Прибыл в ПВЗ' => $pickpoint]];
		}
		// если данные не получены
		return ['shoplogistics::update_orders' => 'Данные не получены'];
	}
	
	static function update_orders_pr() {
		ob_start();
		$orders_sl = self::get_post_deliveries_array();
		self::update_pr_tracking_id($orders_sl);
		$sl_status = [];
		foreach(json::get('sl-order-status') as $s) {
			$sl_status[$s['name']] = $s['status'];
		}
		$num = 0;
		if($orders_sl) {
			$delivered = $intransit = $pickpoint = $confirmed = [];
			// обновление статусов заказов
			db::prepare('update orders set status = ?, dtime = ? where id = ? and status in(2,3,8,9) and cstatus = 3');
			foreach($orders_sl as $order) {
				if(!array_key_exists($order['status'], $sl_status)) continue;
				$order_id = intval($order['order_id']);
				$status = self::get_pr_status($order);
				$dtime = in_array($status, [4,10]) ? date('Y-m-d H:i:s') : NULL;
				db::set([$status, $dtime, $order_id]);
				db::execute();
				if(!db::count()) continue;
				if($status == 2) {
					$confirmed[] = $order_id;
				}
				if($status == 4) {
					$delivered[] = $order_id;
					$num++;
				}
				if($status == 3) {
					$intransit[] = $order_id;
				}
				if($status == 8) {
					$pickpoint[] = $order_id;
				}
			}
			if($confirmed) {
				db::prepare('update orders set stime = if(stime is null, CURRENT_TIMESTAMP, stime) where id = :order_id');
				foreach($confirmed as $order_id) {
					db::set('order_id', $order_id);
					db::execute();
				}
			}
			// email-уведомление о полученном заказе
			if($delivered) {
				delivered_email($delivered);
				order::status_sms($delivered, 'order-delivered');
				// activate coupon for next order
				activate_coupons($delivered);
			}
			// email-уведомление о передаче в СД
			if($intransit) {
				intransit_email($intransit);
				order::status_sms($intransit, 'intransit-sl');
			}
			// email-уведомление о доставке в ПВЗ
			if($pickpoint) {
				pickpoint_email($pickpoint);
			}
			if($num) log_write('ShopLogistics: Обновлено заказов '.$num);
			else log_write('ShopLogistics: Доставленных заказов нет');
			tpl::append('main', '<pre>'.ob_get_clean().'</pre>');
			return ['shoplogistics::update_orders_pr' => ['Согласовано' => $confirmed, 'Доставлено' => $delivered, 'В пути' => $intransit, 'Прибыл в ПВЗ' => $pickpoint]];
		}
		// если данные не получены
		return ['shoplogistics::update_orders_pr' => 'Данные не получены'];
	}
	
	static function get_pr_status($order) {
		$os = [
			'Прибыло в место вручения' => 8,
			'Вручение адресату' => 4,
			'Вручение отправителю' => 10
		];
		if($order['delivery_history']) {
			if(is_array($order['delivery_history']['record'])) {
				$last_record = end($order['delivery_history']['record']);
				if(isset($os[$last_record['oper_attr']])) return $os[$last_record['oper_attr']];
			}
		}
		return 3;
	}
	
	static function update_pr_tracking_id($orders_sl = []) {
		if(!$orders_sl) $orders_sl = self::get_post_deliveries_array();
		$num = 0;
		if(!$orders_sl) return $num;
		db::prepare('update orders set pr_id = ? where id = ?');
		foreach($orders_sl as $order) {
			if(!$order['post_id']) continue;
			db::set([$order['post_id'], $order['order_id']]);
			db::execute();
			$num++;
		}
		return $num;
	}
	
	static function put_products() {
		$tpl = '<?xml version="1.0"?>
		<request>
		<function>client_add_update_products</function>
		<api_id>'.self::API_KEY.'</api_id>
		<products>
		<!-- sl-put-product -->
		<product>
		<name>{#name}</name>
		<articul>{#barcode}</articul>
		<barcode>{#barcode}</barcode>
		</product>
		<!-- sl-put-product -->
		</products>
		</request>';
		tpl::load_string($tpl, 'sl-put-products');
		db::query('select id, name, artikul, barcode from product where status = 1');
		$products = db::fetchAll();
		db::query('select * from product_option where status = 1');
		$options = db::fetchAll();
		$options = array_group($options, 'product_id');
		$num = 0;
		foreach($products as $product) {
			if(preg_match('#(\[.*?\]\s*)#', $product['name'], $match)) {
				$product['name'] = str_replace($match[1], '', $product['name']);
			}
			$product['name'] = html2chars($product['name']);
			if(isset($options[$product['id']])) {
				$product_options = $options[$product['id']];
				foreach($product_options as $option) {
					$option['name'] = html2chars($option['name']);
					tpl::set('name', $product['name'].' - '.$option['name']);
					tpl::set('barcode', $option['barcode']);
					tpl::make('sl-put-product');
					$num++;
				}
			}
			else {
				tpl::push($product);
				tpl::make('sl-put-product');
				$num++;
			}
		}
		$xml = tpl::make('sl-put-products');
		file_put_contents(FILES.'/shoplogistics/products-export.xml', $xml);
		// echo $xml;
		// exit;
		$response = self::request($xml);
		file_put_contents(FILES.'/shoplogistics/products-export-response.xml', $response);
		return $num;
	}
	
	static function create_act($stock_id, $products) {
		$stock_list = shop::get_stock_list();
		// act data
		$act['zabor_place_code'] = $stock_list[$stock_id]['sl_id'];
		$act['express'] = 0;
		$act['importation_date'] = date('Y-m-d', strtotime('tomorrow'));
		$act['provider'] = $stock_list[$stock_id]['name'];
		$act['products'] = json::encode($products);
		// save to DB
		db::query('insert into sl_act set zabor_place_code = :zabor_place_code, express = :express, importation_date = :importation_date, provider = :provider, products = :products', $act);
		$act['id'] = db::lastInsertId();
		return true;
	}
	
	static function send_act($act_id) {
		// create xml for request
		$tpl = '<?xml version="1.0"?>
		<request>
		<function>client_add_update_products_act</function>
		<api_id>'.self::API_KEY.'</api_id>
		<products_acts>
			<products_act>
			<number>{#id}</number>
			<zabor_place_code>{#zabor_place_code}</zabor_place_code>
			<express>{#express}</express>
			<importation_date>{#importation_date}</importation_date> 
			<provider>{#provider}</provider> 
			<!-- sl-act-product -->
			<products>
				<articul>{#barcode}</articul>
				<name>{#name}</name>
				<price>{#price}</price>
				<quantity>{#quantity}</quantity>
				<barcode>{#barcode}</barcode>
			</products>
			<!-- sl-act-product -->
			</products_act>
		</products_acts>
		</request>';
		if(!tpl::get('sl-put-act')) tpl::load_string($tpl, 'sl-put-act');
		db::query('select * from sl_act where id = ?', $act_id);
		$act = db::fetchArray();
		$products = json::decode($act['products']);
		foreach($products as $product) {
			if($product['option_name']) {
				$product['name'] .= ' - '.$product['option_name'];
			}
			$product['name'] = html2chars($product['name']);
			tpl::push($product);
			tpl::make('sl-act-product');
		}
		tpl::push($act);
		tpl::set('provider', 'Склад '.$act['provider']);
		$xml = tpl::make('sl-put-act');
		tpl::clear('sl-act-product');
		// request and log
		$date = date('Ymd');
		file_put_contents(FILES.'/shoplogistics/act/'.$date.'_request.xml', $xml);
		if(x::config('DEBUG')) {
			return count($products);
		}
		$response = self::request($xml);
		file_put_contents(FILES.'/shoplogistics/act/'.$date.'_response.xml', $response);
		return str::xmltoarray($response);
	}
	
	static function tracking_widget($tracking_id, $dtype) {
		$key = 'sl-tracking-'.$tracking_id;
		if(!$html = cache::get($key)) {
			if($dtype == 3) {
				tpl::load('shop');
				$data = file_get_contents(FILES.'/shoplogistics/orders-pr.xml');
				$data = str::xmltoarray($data)['deliveries']['delivery'];
				$records = null;
				foreach($data as $delivery) {
					if($delivery['code'] == $tracking_id) {
						$records = $delivery['delivery_history']['record'];
						break;
					}
				}
				if(!$records) return false;
				foreach($records as $record) {
					tpl::set('date', $record['oper_date']);
					tpl::set('location', $record['operation_address']);
					tpl::set('status', $record['oper_attr']);
					tpl::make('pr-tracking-record');
				}
				$html = tpl::make('pr-tracking');
				cache::set($key, $html, 600);
			}
			else {
				$url = 'http://shop-logistics.ru/personal/order_check.php?ID='.$tracking_id;
				$response = web::http_request($url);
				$response = mb_convert_encoding($response, 'utf-8', 'windows-1251');
				if(!preg_match('#<div class="content_part">(.+?)</div>#msi', $response, $match)) return false;
				$html = str_replace('<h1>Проверка статуса отправления</h1>', '', $match[1]);
				$html = preg_replace('/<script.*?>.*?<\/script>/', '', $html);
				$html = trim($html);
				cache::set($key, $html, 600);
			}
		}
		return $html;
	}

}

?>