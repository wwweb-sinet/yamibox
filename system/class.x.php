<?php

error_reporting(-1); // show all errors

define('DS', '/');
define('XDIR', dirname(__FILE__));
define('AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest');
define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);
define('REQUEST_POST', $_SERVER['REQUEST_METHOD'] == 'POST');

class X {

	private static $routes = array();
	private static $config = array();
	public static $data = array();

	static function data($key = NULL, $value = NULL, $append = false) {
		if(is_null($key))
			return x::$data;
		if(is_array($key)) {
			foreach($key as $k=>$v) x::data($k, $v);
			return x::$data;
		}
		if(is_null($value))
			return isset(x::$data[$key]) ? x::$data[$key] : false;
		if($append) {
			if(!isset(x::$data[$key]))
				x::data($key, $value);
			else x::$data[$key] .= $value;
			return x::$data[$key];
		}
		x::$data[$key] = $value;
	}
	
	static function get($key) {
		return isset(self::$data[$key]) ? self::$data[$key] : '';
	}
	
	static function set($key, $value) {
		self::$data[$key] = $value;
	}
	
	static function append($key, $value) {
		if(isset(self::$data[$key])) self::$data[$key] .= $value;
		else self::$data[$key] = $value;
	}
	
	static function push($array) {
		foreach($array as $key => $value) {
			self::set($key, $value);
		}
	}

	static function clear() {
		$args = func_get_args();
		if(empty($args))
			x::$data = array();
		else {
			foreach($args as $key)
				unset(x::$data[$key]);
		}
	}

	static function config($key = NULL, $value = NULL) {
		if(is_null($key))
			return x::$config;
		if(is_array($key)) {
			foreach($array as $k=>$v) x::config($k, $v);
		}
		if(is_null($value))
			return isset(x::$config[$key]) ? x::$config[$key] : NULL;
		x::$config[$key] = $value;
	}

	/**
	*	ROUTER
	*	'GET/' and 'POST/' patterns are allowed
	*/
	static function route($pattern, $action) {
		// if(!preg_match('/(GET|POST|REQUEST)(\/.*)/', $pattern, $match))
			// return abort('X :: Syntax error in route pattern: '.$pattern);
		x::$routes[$pattern] = $action;
	}

	static function run($callback = NULL) {

		$CALL = false;
		$PATH = qs::$params['path'];
		x::data('PATH', $PATH);
		$ALTPATH = substr($PATH, -1) == '/' ? substr($PATH, 0, strlen($PATH) - 1) : $PATH.'/';

		foreach(x::$routes as $pattern => $action) {
			if(preg_match('#^'.$pattern.'$#u', $PATH, $match)) {
				x::do_action($action, $match);
				$CALL = TRUE;
				break;
			}
			elseif(preg_match('#^'.$pattern.'$#ui', $ALTPATH, $match)) {
				return redirect($ALTPATH, 301);
			}
		}

		//default action
		if(!$CALL) {
			x::do_action('404.php');
		}

		if(is_callable($callback)) {
			x::do_action($callback);
		}
	}

	static function do_action($action, $args = array()) {
		// action is file to include
		if(is_string($action) && preg_match('/\.php$/', $action)) {
			$file = XDIR.DS.$action;
			if(!file_exists($file) || !is_readable($file))
				return abort('X :: file not found: ' . $file);
			include($file);
		}
		// action is callable
		else {
			if(!is_callable($action))
				return abort('X :: Unable to trigger action: ' . $action);
			if(empty($args)) {
				$path_parts = explode('/', qs::$params['path']);
				$args = (array) end($path_parts);
			}
			call_user_func_array($action, $args);
		}
	}

	static function autoloader($class) {
		$class = strtolower($class);
		$filename = XDIR.DS.'class.'.$class.'.php';
		if(!file_exists($filename)) $filename = str_replace('class.', '', $filename);
		if(!file_exists($filename)) abort('unable to load class "'.$class.'"');
		require($filename);
	}
}
spl_autoload_register('x::autoloader');

// GLOBAL FUNCTIONS

function status($statusCode = 200) {
	header('HTTP/1.0: '.$statusCode, true);
}

function fileval($file) {
	$file = realpath($file);
	if(!$file)
		return abort('File not found: ' . $file);
	ob_start();
	include($file);
	return ob_get_clean();
}

function absint($var) {
	return abs(intval($var));
}

function finfo($file, $key=false) {
	$finfo = pathinfo($file);
	return $key ? $finfo[$key] : $finfo;
}

function fileext($filename) {
	if(!file_exists($filename)) {
		$f = explode('.', $filename);
		return end($f);
	}
	return finfo($filename, 'extension');
}

function redirect($url = null, $status = 302) {
	if(!$url)
		$url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
	header('HTTP/1.0 '.$status, true);
	header('Location: '.$url, true);
	exit;
}

function array_random(&$array) {
	$key = array_rand($array);
	return $key===false ? false : $array[$key];
}

function array_group(array $array, $key, $index = NULL) {
	$result = array();
	foreach($array as $k=>$v) {
		if(is_null($index)) $result[$v[$key]][] = $v;
		else $result[$v[$key]][$v[$index]] = $v;
	}
	return $result;
}

function html2chars($str) {
	$replacement = get_html_translation_table(HTML_SPECIALCHARS);
	return strtr($str, $replacement);
}

function chars2html($str) {
	$replacement = array_flip(get_html_translation_table(HTML_SPECIALCHARS));
	return strtr($str, $replacement);
}

// debug_print_backtrace()
function abort($errormsg=NULL) {
	ob_start();
	if($errormsg) echo '<h3>'.$errormsg."</h3>\n";
	echo "<pre>\n";
	debug_print_backtrace();
	echo "</pre>\n";
	header('Content-Type:text/html;charset=utf-8', true);
	if(x::config('DEBUG'))
		die(ob_get_clean());
	else
		print_r(ob_get_clean());
}

function get_files($dir='./', $pattern='/\w+/', $skip=0, $limit=0) {
	// correct directory path
	if(is_file($dir)) $dir = dirname($dir);
	$dir = strtr($dir, '\\', '/');
	if(substr($dir, -1)!=='/') $dir .= '/';
	if(!$d = opendir($dir))
		abort('unable to open dir: '.$dir);
	$result = array();
	while(false!==$file=readdir($d)) {
		if(!is_file($dir.$file)) continue;
		if($skip>0) {
			$skip--; continue;
		}
		if(preg_match($pattern, $file)) {
			$result[] = $dir.$file;
			$limit--; if($limit==0) break;
		}
	}
	closedir($d);
	return $result;
}

function getClosest($search, $arr) {
	$closest = null;
	foreach($arr as $item) {
		if($closest == null || abs($search - $closest) > abs($item - $search)) {
			$closest = $item;
		}
	}
	return $closest;
}

function uploadFile($file, $dest_dir, $filename = NULL) {
	$file['name'] = mb_convert_encoding($file['name'], 'CP1251', 'UTF-8');
	$dest = $dest_dir . ($filename ? $filename : $file['name']);
	return (move_uploaded_file($file['tmp_name'], $dest)) ? $dest : false;
}

function ts2date($format, $timeStr) {
	return date($format, strtotime($timeStr));
}

function GET($key) {
	return isset($_GET[$key]) ? $_GET[$key] : '';
}
function POST($key) {
	return isset($_POST[$key]) ? $_POST[$key] : '';
}
function REQUEST($key) {
	return isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
}
function COOKIE($key) {
	return isset($_COOKIE[$key]) ? $_COOKIE[$key] : '';
}
function SESSION($key) {
	return isset($_SESSION[$key]) ? $_SESSION[$key] : '';
}
function SERVER($key) {
	return isset($_SERVER[$key]) ? $_SERVER[$key] : '';
}

?>