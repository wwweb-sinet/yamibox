<?php

class MA {
	
	static function orders($path, $orderId = NULL) {
		if(!is_null($orderId)) return self::order(absint($orderId));
		tpl::set('page-title', 'Заказы');
		$status = json::get('order-status');
		$cstatus = json::get('order-cstatus');
		$selected_status = absint(GET('status'));
		$selected_cstatus = absint(GET('cstatus'));
		// status filter select
		tpl::set('option-value', qs::remove('status'));
		tpl::set('option-text', 'Все');
		tpl::set('option-selected', '');
		tpl::make('option', 'option-status');
		foreach($status as $id => $text) {
			tpl::set('option-value', qs::set('status', $id));
			tpl::set('option-text', $text);
			tpl::set('option-selected', isset($_GET['status']) && $id == $selected_status ? ' selected' : '');
			tpl::make('option', 'option-status');
		}
		qs::reset();
		qs::remove('p');
		// collection status select
		foreach($cstatus as $id => $text) {
			tpl::set('option-value', qs::set('cstatus', $id));
			tpl::set('option-text', $text);
			tpl::set('option-selected', isset($_GET['cstatus']) && $id == $selected_cstatus ? ' selected' : '');
			tpl::make('option', 'option-cstatus');
		}
		qs::reset();
		$sql = 'select count(*) from orders where true';
		// filter by user_id
		if($user_id = absint(GET('user_id'))) {
			$sql .= ' and user_id = '.$user_id;
		}
		// filter by status
		if(isset($_GET['status'])) {
			$sql .= ' and status = '.$selected_status;
		}
		if(isset($_GET['cstatus'])) {
			$sql .= ' and cstatus = '.$selected_cstatus;
		}
		$numOrders = db::querySingle($sql);
		$sql = str_replace('count(*)', '*', $sql) . ' order by id desc limit ?, ?';
		$limit = 40;
		db::query($sql, offset($limit), $limit);
		while($order = db::fetchArray()) {
			tpl::push($order);
			tpl::set('status-text', @$status[$order['status']] ?: 'ОШИБКА');
			tpl::set('cstatus-text', @$cstatus[$order['cstatus']] ?: 'ОШИБКА');
			tpl::set('cdate', ts2date('d.m.Y', $order['ctime']));
			tpl::set('order-sum', $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon']);
			tpl::make('mobile-admin-order');
		}
		// autoload next page on scroll
		if(AJAX) die(tpl::get('mobile-admin-order'));
		tpl::make('mobile-admin-orders', 'main');
	}
	
	static function order($orderId) {
		if($action = REQUEST('action')) {
			if($action == 'delete-item') return admin::deleteOrderItem($orderId);
			if($action == 'change-quantity') return order::update_products($orderId);
			if($action == 'addfromcart') return admin::addProductsFromCartToOrder($orderId);
			if($action == 'create-ms') return admin::createMSOrder($orderId);
		}
		elseif(REQUEST_METHOD == 'POST') {
			return order::update($orderId);
		}
		tpl::set('page-title', '<a href="/admin/orders">Заказы</a>&nbsp;&rarr;&nbsp;'.$orderId);
		// get order from db
		db::query('select * from orders where id = ?', $orderId);
		if(!$order = db::fetchArray()) {
			return alert('Такого заказа нет', 'warning', 'main');
		}
		tpl::push($order);
		if($order['comment']) tpl::walk('comment', 'html2chars');
		tpl::set('order-id', $order['id']);
		$status = json::get('order-status');
		$cstatus = json::get('order-cstatus');
		$dtypes = json::get('dtype');
		$ptypes = json::get('ptype');
		foreach($ptypes as $id => $ptype) {
			tpl::set('option-value', $id);
			tpl::set('option-text', $ptype['text']);
			tpl::set('option-selected', $id == $order['ptype'] ? ' selected' : '');
			tpl::make('option', 'option-ptype');
		}
		foreach($dtypes as $id => $dtype) {
			tpl::set('option-value', $id);
			tpl::set('option-text', $dtype['text']);
			tpl::set('option-selected', $id == $order['dtype'] ? ' selected' : '');
			tpl::make('option', 'option-dtype');
		}
	
		foreach($status as $id => $text) {
			tpl::set('option-value', $id);
			tpl::set('option-text', $text);
			tpl::set('option-selected', $id == $order['status'] ? ' selected' : '');
			tpl::make('option', 'option-status');
		}
		foreach($cstatus as $value => $text) {
			tpl::set('option-value', $value);
			tpl::set('option-text', $text);
			tpl::set('option-selected', $value == $order['cstatus'] ? ' selected' : '');
			tpl::make('option', 'option-cstatus');
		}
		// order details here
		$products = json::decode($order['cart_items']);
		$i = 1;
		foreach($products as $product) {
			tpl::push($product);
			tpl::set('stock-sign', $product['stock'] > 2 ? 'С' : '&nbsp;');
			tpl::set('product-sum', $product['price'] * $product['quantity']);
			tpl::set('option-name', $product['option_id'] ? ' - '.$product['option_name'] : '');
			tpl::set('n', $i); $i++;
			tpl::make('mobile-admin-order-product');
		}
		if($order['user_id']) {
			db::query('select ltime, bdate from user where id = ?', $order['user_id']);
			tpl::push(db::fetchArray());
		}
		if($order['address']) {
			$address = json::decode($order['address']);
			if($address) {
				tpl::push($address);
			}
		}
		tpl::set('cart-sum-with-discounts', $order['total_sum'] - $order['bonus'] - $order['cumulative'] - $order['coupon']);
		tpl::set('order-total', $order['delivery_price'] + $order['total_sum'] - $order['bonus'] - $order['cumulative'] - $order['coupon']);
		
		tpl::make('mobile-admin-order-details', 'main');
	}
	
}

if(!account::$admin) die('ACCESS DENIED');

tpl::load('mobile-admin');

?>