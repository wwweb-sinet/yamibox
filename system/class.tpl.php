<?php

defined('XDIR') or die('X Class Required.');

defined('XDIR_TEMPLATES') or define('XDIR_TEMPLATES', dirname(XDIR).DS.'templates');

class TPL {
	
	const EXTENSION = '.html';
	
	private static $templates = array();
	
	private static $blocks = array();
	
	private static $make = array();
	
	private static $tmp = array();
	
	static function set($key, $val) {
		x::set($key, $val);
	}
	
	static function get($key) {
		return x::get($key);
	}
	
	static function append($key, $val) {
		return x::append($key, $val);
	}
	
	static function push($array) {
		if(!is_array($array)) return false;
		return x::push($array);
	}
	
	static function clear() {
		call_user_func_array('x::clear', func_get_args());
	}
	
	static function reset() {
		call_user_func_array('x::clear', self::$make);
	}
	
	// not safe
	static function walk($var, $callback) {
		if(!is_callable($callback))
			return abort('Unable to trigger callback "'.$callback.'"');
		return x::data($var, call_user_func($callback, x::data($var)));
	}
	
	/**
	*	Load template file to tpl::$templates
	*	Loads included files recursively
	*/
	static function load($tpl, $appendTo = NULL) {
		$tpl = strtolower($tpl);
		$file = realpath(XDIR_TEMPLATES . DS . $tpl . TPL::EXTENSION);
		if(in_array($file, tpl::$templates))
			return true;
		if(!$file || !is_readable($file))
			return abort('Unable to load template: '.$tpl);
		$content = file_get_contents($file);
		if(!$appendTo) $appendTo = $tpl;
		tpl::get_blocks($content, $appendTo);
		tpl::$templates[] = $file;
		return true;
	}
	
	static function load_string($content, $name) {
		tpl::get_blocks($content, $name);
		tpl::$templates[] = $name;
	}
	
	/**
	*	Make $block and append result to tpl::$blocks[$block] or tpl::$blocks[$appendTo]
	*/
	static function make($block, $appendTo = NULL) {
		if(!isset(tpl::$blocks[$block])) {
			// try load template file if exists
			if(!tpl::load($block))
				return abort('Block or template not found: '.$block);
		}
		$content = tpl::$blocks[$block];
		tpl::_make($content);
		if($appendTo) $block = $appendTo;
		if(!in_array($block, self::$make)) self::$make[] = $block;
		tpl::append($block, $content);
		return $content;
	}
	
	/**
	*	Make block $block and append result to $parent.
	*	When $count reached, make $parent block.
	*/
	static function imake($block, $parent, $count) {
		if($count < 1)
			return abort('Tpl::imake() $count < 1');
		if(!isset(tpl::$tmp[$block]))
			tpl::$tmp[$block] = $count;
		tpl::$tmp[$block]--;
		tpl::make($block);
		if(tpl::$tmp[$block] == 0) {
			tpl::make($parent);
			tpl::clear($block);
			tpl::$tmp[$block] = $count;
		}
		return tpl::$tmp[$block];
	}
	
	/**
	*	Service method, matches and replaces variables
	*/
	protected static function _make(&$content) {
		if(preg_match_all('/{#([A-Za-z0-9_-]+)}/s', $content, $vars))
		{
			foreach($vars[1] as $key=>$varname) {
				$value = x::data($varname);
				$content = str_replace($vars[0][$key], $value, $content);
			}
		}
		// $content = trim($content)."\n";
	}
	
	private static function get_blocks(&$content, $appendTo) {
		if(!$appendTo)
			abort('TPL :: Block name required');
		tpl::$blocks[$appendTo] = $content;
		if(preg_match_all('/<!--\s*([A-Za-z0-9_-]+)\s*-->(.*)<!--\s*\1\s*-->/ms', $content, $blocks)) {
			foreach($blocks[1] as $key=>$block) {
				if(isset(tpl::$blocks[$block])) {
					return abort('TPL :: duplicate entry for block ' . $block);
				}
				tpl::$blocks[$block] = $blocks[2][$key];
				// convert childs to variables
				tpl::$blocks[$appendTo] = str_replace($blocks[0][$key], '{#'.$block.'}', tpl::$blocks[$appendTo]);
				// recursion
				tpl::get_blocks($blocks[2][$key], $block);
			}
		}
		if(preg_match_all('/<!--\s*@([A-Za-z0-9_-]+)\s*-->/s', $content, $includes)) {
			foreach($includes[1] as $key=>$include) {
				// recursion
				tpl::load($include);
				// replace tag with file contents
				tpl::$blocks[$appendTo] = str_replace($includes[0][$key], tpl::$blocks[$include], tpl::$blocks[$appendTo]);
				// includes are not re-usable
				unset(tpl::$blocks[$include]);
			}
		}
	}
	
}

?>