<?php

class Slider {
	
	static function get() {
		$slider = json::decode(file_get_contents(FILES.'/slider.json'));
		usort($slider, function($a, $b) {
			return $a['sort'] > $b['sort'] ? 1 : 0;
		});
		return $slider;
	}
	
	static function save($slides) {
		file_put_contents(FILES.'/slider.json', json::encode($slides));
	}
	
	static function update() {
		$slides = array();
		array_walk($_POST, function(&$val, $key) {
			if(is_string($val)) $val = trim($val);
		});
		foreach($_POST['filename'] as $key => $filename) {
			$slides[] = array(
				'filename' => basename($filename),
				'href' => $_POST['href'][$key],
				'label' => $_POST['label'][$key],
				'alt' => $_POST['alt'][$key],
				'sort' => $_POST['sort'][$key]
			);
		}
		self::save($slides);
		die('ok');
	}
	
	static function remove() {
		$filename = trim(REQUEST('filename'));
		$slides = self::get();
		foreach($slides as $key => $slide) {
			if($slide['filename'] == $filename || !$slide['filename']) {
				unset($slides[$key]);
				break;
			}
		}
		self::save($slides);
		die('ok');
	}
	
	static function add() {
		if(!$img = $_FILES['img'])
			return redirect('/admin/slider');
		$img_src = uploadFile($img, FILES.'/promo/780x310/');
		$slides = self::get();
		$slides[] = array(
			'filename' => basename($img_src),
			'href' => trim(POST('href')),
			'label' => trim(POST('label')),
			'alt' => trim(POST('alt')) ?: '*',
			'sort' => trim(POST('sort'))
		);
		self::save($slides);
		die('ok');
	}
	
}

?>