<?php

class bill {
	
	static function main() {
		$action = REQUEST('action');
		if($action) {
			$response = 'no action';
			if($action == 'bill-insert') {
				$response = self::insert_bill();
			}
			if($action == 'update-defer-days') {
				$response = self::update_defer_days();
			}
			exit($response);
		}
		self::view();
	}
	
	static function insert_bill() {
		if(!$sum = correct_float(POST('sum'))) return 'Сумма не может быть 0';
		if(!$sid = absint(POST('sid'))) return 'NO SUPPLIER ID';
		db::query('insert into bills set supplier_id = ?, sum = ?', $sid, $sum);
		return db::count() ? 'ok' : db::error();
	}
	
	static function update_defer_days() {
		if(!$sid = absint(POST('sid'))) return 'NO SUPPLIER ID';
		$defer_days = absint(POST('defer_days'));
		db::query('update product_stock set defer_days = ? where id = ?', $defer_days, $sid);
		return db::count() ? 'ok' : db::error();
	}
	
	static function get_excerpt() {
		db::query('select op_date, (select stock from product_stock_accounts where account = ex.account) as supplier_id, sum(debit) as debit, account from admin_excerpt_alfa ex where debit > 0 and account in(select account from product_stock_accounts) group by account, op_date order by supplier_id asc, op_date desc');
		$excerpt = db::fetchAll();
		return $excerpt;
	}
	
	static function view() {
		tpl::load('admin-bills');
		make_title('Счета');
		$stocks = shop::get_stock_list(false);
		db::query('select supplier_id, ctime, sum(sum) as sum, sum(debit) as debit from (
(select b.*, NULL as debit from bills b)
union
(select (select stock from product_stock_accounts where account = ex.account) as supplier_id, op_date as ctime, NULL as sum, sum(debit) as debit from admin_excerpt_alfa ex where debit > 0 and account in(select account from product_stock_accounts) group by account, op_date)
) as t
group by supplier_id, ctime
order by supplier_id, ctime desc');
		$bills = db::fetchAll();
		$bills = array_group($bills, 'supplier_id');
		$dt_now = new DateTime();
		$max_rows = 0;
		foreach($bills as $supplier_id => $bill_rows) {
			$n = count($bill_rows);
			if($n > $max_rows) $max_rows = $n;
		}
		$sum_debt_total = 0;
		foreach($stocks as $supplier) {
			if($supplier['id'] < 3) continue;
			$supplier_debit = isset($debit[$supplier['id']]) ? $debit[$supplier['id']] : [];
			$sum_bill = $sum_defer = $sum_debit = $sum_debt = 0;
			tpl::set('supplier_id', $supplier['id']);
			tpl::set('supplier_name', $supplier['name']);
			tpl::set('sum_bill_start', $supplier['sum_bill_start']);
			tpl::set('defer_days', $supplier['defer_days']);
			$num_rows = $max_rows;
			$bill_rows = @$bills[$supplier['id']] ?: [];
			foreach($bill_rows as $bill) {
				$dt_bill = DateTime::createFromFormat('Y-m-d', $bill['ctime']);
				$bill_date = $dt_bill->format('d.m.Y');
				$op_date = $dt_bill->format('Y-m-d');
				$interval = $dt_bill->diff($dt_now);
				$days_diff = $interval->format('%a');
				if($days_diff <= $supplier['defer_days']) {
					tpl::set('bill_class', 'deferred');
					$sum_defer += $bill['sum'];
				}
				else {
					tpl::set('bill_class', '');
				}
				tpl::set('bill_sum', $bill['sum']);
				tpl::set('bill_date', $bill_date);
				tpl::set('debit', $bill['debit'] ?: '');
				tpl::make('supplier-bill');
				$sum_bill += $bill['sum'];
				$sum_debit += $bill['debit'];
				$num_rows--;
			}
			tpl::set('sum_defer', $sum_defer);
			while($num_rows > 0) {
				tpl::set('bill_sum', '');
				tpl::set('bill_date', '');
				tpl::set('debit', '');
				tpl::make('supplier-bill');
				$num_rows--;
			}
			$sum_debt = $supplier['sum_bill_start'] + $sum_bill - $sum_debit - $sum_defer;
			$sum_debt = round($sum_debt, 2);
			$sum_debt_total += $sum_debt;
			tpl::set('sum_debt', $sum_debt);
			tpl::set('sum_bill', $sum_bill);
			tpl::set('sum_debit', $sum_debit);
			tpl::make('admin-bills-supplier');
			tpl::clear('supplier-bill');
		}
		tpl::set('sum_debt_total', $sum_debt_total);
		tpl::make('admin-bills', 'main');
	}
	
	static function import() {
		$file = FILES.'/temp/scheta.csv';
		$f = fopen($file, 'r');
		$skip = 12;
		db::prepare('insert into bills set supplier_id = :supplier_id, ctime = :ctime, sum = :sum');
		while($line = fgetcsv($f, 9999, '|')) {
			if($skip) {
				$skip--; continue;
			}
			$bills[3] = array_slice($line, 6, 2);
			$bills[4] = array_slice($line, 0, 2);
			$bills[5] = array_slice($line, 12, 2);
			$bills[6] = array_slice($line, 3, 2);
			$bills[7] = array_slice($line, 9, 2);
			$bills[8] = array_slice($line, 15, 2);
			foreach($bills as $supplier_id => $bill) {
				if(!$bill[0]) continue;
				$sum = correct_float($bill[1]);
				$dt = DateTime::createFromFormat('m/d/Y', $bill[0]);
				$ctime = $dt->format('Y-m-d 12:00:00');
				$data = [
					'supplier_id' => $supplier_id,
					'sum' => $sum,
					'ctime' => $ctime
				];
				db::set($data);
				db::execute();
			}
		}
	}
	
}

?>