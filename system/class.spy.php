<?php

class spy {
	
	static function fetch_pages($source) {
		$result = call_user_func('spy::'.$source.'_pages');
		return $result;
	}
	
	static function asiacream_pages() {
		$result = [];
		$urls_file = FILES.'/asiacream/urls.txt';
		$urls = file($urls_file, FILE_IGNORE_NEW_LINES);
		foreach($urls as $line) {
			list($brand, $url) = explode('|', $line);
			$filename = $brand . '.html';
			$html = file_get_contents($url);
			$filesize = strlen($html);
			file_put_contents(FILES.'/asiacream/'.$filename, $html);
			$result[] = ['file' => $filename, 'size' => $filesize];
		}
		return $result;
	}
	
	static function bbcream_pages() {
		$brands = [
			16 => 'tony-moly',
			35 => 'holika-holika',
			52 => 'lioele',
			43 => 'mizon',
			45 => 'secret-key',
			9 => 'skin79',
			26 => 'the-face-shop',
			51 => 'the-saem',
			15 => 'baviphat',
			49 => 'enprani',
			6 => 'etude-house'
		];
		$url = 'http://bbcream.ru/index.php?route=module/filterpro/getproducts';
		$data = [
			'instock' => 'on',
			'manufacturer_id' => 16,
			'page' => 1,
			'sort' => 'p.sort_order',
			'order' => 'ASC',
			'limit' => 1000,
			'route' => 'product/manufacturer/info',
			'min_price' => 25,
			'max_price' => 10000,
			'getPriceLimits' => 'true'
		];
		$result = [];
		$options = [CURLOPT_HTTPHEADER => ['Content-Type:Content-Type:application/x-www-form-urlencoded']];
		foreach($brands as $brand_id => $filename) {
			$data['manufacturer_id'] = $brand_id;
			$response = web::http_request($url, 'POST', $data);
			$response = json::decode($response);
			$html = $response['result_html'];
			file_put_contents(FILES.'/bbcream/'.$filename.'.html', $html);
			$result[] = $filename.': OK';
		}
		return $result;
	}
	
	static function lunifera_pages() {
		$result = [];
		$urls_file = FILES.'/lunifera/urls.txt';
		$urls = file($urls_file, FILE_IGNORE_NEW_LINES);
		foreach($urls as $url) {
			$qs = '?limit=60';
			$filename = basename($url);
			$html = file_get_contents($url.$qs);
			$filesize = strlen($html);
			file_put_contents(FILES.'/lunifera/'.$filename, $html);
			$result[] = ['file' => $filename, 'size' => $filesize];
			// additional pages
			if(preg_match('#<div class="pages">(.*?)</div>#ms', $html, $match)) {
				$paginator_html = $match[1];
				preg_match_all('#<a href=.*?>(\d+)</a>#', $paginator_html, $match);
				foreach($match[1] as $page) {
					if($page < 2) continue;
					$page_url = $url . $qs . '&p=' . $page;
					$filename = basename($url);
					$filename = str_replace('.', '-'.$page.'.', $filename);
					$html = file_get_contents($page_url);
					$filesize = strlen($html);
					file_put_contents(FILES.'/lunifera/'.$filename, $html);
					$result[] = ['file' => $filename, 'size' => $filesize];
				}
			}
		}
		return $result;
	}
	
	static function parse_content($source, $product_html) {
		$product = [
			'source' => $source,
			'price_old' => NULL
		];
		if($source == 'asiacream') {
			preg_match('#><a.+?>(.+)</a>#', $product_html, $match);
			$product['name'] = trim($match[1]);
			preg_match('#id=\"productPrice(\d+)\"#', $product_html, $match);
			$product['source_id'] = trim($match[1]);
			$price_old = preg_match('#class=\"PricepriceWithoutTax\".*?>(\d+)\s*руб\s*</span>#', $product_html, $match);
			$product['price_old'] = $price_old ? trim($match[1]) : NULL;
			preg_match('#span\s+class=\"PricesalesPrice\".*?>(\d+)\s*руб\s*</span>#', $product_html, $match);
			$product['price'] = $match[1];
		}
		elseif($source == 'bbcream') {
			preg_match('#^<a.*?>(.+?)<#ms', $product_html, $match);
			$product['name'] = trim(preg_replace('#\s+#', ' ', $match[1]));
			preg_match('#<div class="price">(.*?)</div>#ms', $product_html, $match);
			if(preg_match('#span#ms', $match[1])) {
				preg_match('#<span class="price\-old">(.+?)</span>#ms', $match[1], $price);
				$product['price_old'] = intval(preg_replace('#[^0-9]#ms', '', $price[1]));
				preg_match('#<span class="price\-new">(.+?)</span>#ms', $match[1], $price);
				$product['price'] = intval(preg_replace('#[^0-9]#ms', '', $price[1]));
			}
			else {
				$product['price'] = intval(preg_replace('#[^0-9]#ms', '', $match[1]));
			}
			preg_match('#addToCart\(\'(\d+)\'\)#', $product_html, $match);
			$product['source_id'] = $match[1];
		}
		elseif($source == 'lunifera') {
			preg_match('#span class="product-name"><a.*?>(.+?)<#', $product_html, $match);
			$product['name'] = trim($match[1]);
			//preg_match('#([\w\-\&]+)#', $product['name'], $match);
			$product['name'] = preg_replace('#[а-я]#ui', '', $product['name']);
			$product['name'] = preg_replace('#^([^\w]+)#', '', $product['name']);
			$product['name'] = str_replace('&amp;', '&', $product['name']);
			preg_match('#id="product\-price\-(\d+)"#', $product_html, $match);
			$product['source_id'] = trim($match[1]);
			preg_match_all('#span class=\"price.*?>(.*?)</span>#msi', $product_html, $match);
			$product_prices = [];
			foreach($match[1] as $price_str) {
				$price = preg_replace('#[^0-9]#', '', $price_str);
				if($price = intval($price)) $product_prices[] = $price;
			}
			$count_prices = count($product_prices);
			if($count_prices == 1) {
				$product['price'] = $product_prices[0];
				$product['price_old'] = NULL;
			}
			elseif($count_prices == 2) {
				$product['price'] = min($product_prices);
				$product['price_old'] = max($product_prices);
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
		return $product;
	}
	
	static function import_products($source, $html) {
		$html_products = self::get_products($source, $html);
		if(!$html_products) return ['spy::import_products' => 'товары не найдены: '.$source];
		// print_r($matches);
		$num = 0;
		db::prepare('replace into product_monitoring set source = :source, name = :name, source_id = :source_id, price = :price, price_old = :price_old');
		foreach($html_products as $product_html) {
			if(!$product = self::parse_content($source, $product_html)) {
				continue;
			}
			db::query('select * from product_monitoring where source = ? and source_id = ?', $product['source'], $product['source_id']);
			if(($local_product = db::fetchArray()) && $local_product['local_id']) {
				if(self::compare($local_product, $product)) continue;
				$product['local_id'] = $local_product['local_id'];
				$product['name'] = $local_product['name'];
				$sql = 'replace into product_monitoring set source = :source, name = :name, source_id = :source_id, price = :price, price_old = :price_old, local_id = :local_id';
			}
			elseif(!@$local_product['local_id']) {
				$sql = 'replace into product_monitoring set source = :source, name = :name, source_id = :source_id, price = :price, price_old = :price_old, local_id = best_fulltext_match(:name)';
				if(@$local_product['name']) {
					$product['name'] = $local_product['name'];
				}
			}
			else {
				continue;
			}
			db::query($sql, $product);
			$num += db::count();
		}
		return ['spy::import_products' => $source.': '.$num];
	}
	
	static function compare($local_product, $product) {
		if($product['source'] != $local_product['source']) return false;
		// if($product['name'] != $local_product['name']) return false;
		if($product['price'] != $local_product['price']) return false;
		if($product['price_old'] != $local_product['price_old']) return false;
		if($product['source_id'] != $local_product['source_id']) return false;
		// if(!isset($product['local_id'])) return false;
		// if($product['local_id'] != $local_product['local_id']) return false;
		return true;
	}
	
	static function get_products($source, $html) {
		$products = false;
		if($source == 'asiacream') {
			preg_match_all('#catProductTitle(.+?)cat\-cart#ms', $html, $match);
			$products = isset($match[1]) ? $match[1] : false;
		}
		elseif($source == 'bbcream') {
			preg_match_all('#class="name">(.*?)class="wishlist#ms', $html, $match);
			$products = isset($match[1]) ? $match[1] : false;
		}
		elseif($source == 'lunifera') {
			preg_match_all('#li class="item(.*?)div class="actions"#ms', $html, $match);
			$products = isset($match[1]) ? $match[1] : false;
		}
		return $products;
	}
	
	static function deploy($source) {
		$fetch_pages = REQUEST('fetch_pages') == 'false' ? false : true;
		if($fetch_pages && !self::fetch_pages($source)) {
			return ['spy::deploy' => 'страницы не загружены: '. $source];
		}
		$result = [];
		$files = get_files(FILES.'/'.$source.'/', '#.+\.html$#');
		foreach($files as $file) {
			$html = file_get_contents($file);
			$result[] = self::import_products($source, $html);
		}
		self::write_csv($source);
		return $result;
	}
	
	static function write_csv($source) {
		$csv_file = FILES.'/'.$source.'/'.$source.'.csv';
		$f = fopen($csv_file, 'w');
		db::query('select name, price, price_old from product_monitoring where source = ? order by name asc', $source);
		while($product = db::fetchArray()) {
			fputcsv($f, $product, "\t", '"');
		}
		fclose($f);
	}
	
}

?>