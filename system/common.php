<?php

defined('ROOT') or die('Access denied');
defined('XDIR') or die('X Class required');

if(!headers_sent()) {
	header('Content-Type:text/html;charset=utf-8');
	header('X-UA-Compatible: IE=Edge,chrome=IE9');
}

// load config
$SERVER_NAME = preg_replace('#^m\.#', '', SERVER('SERVER_NAME'));
require_once(XDIR.DS.'config.'.$SERVER_NAME.'.php');

define('FILES', ROOT.DS.'files');
define('JSON', ROOT.DS.'json');
define('CACHE', FILES.DS.'cache');

define('PAGE_LIMIT_CATALOG', 24);
define('PAGE_LIMIT_BLOG', 8);

define('BLOG_BLOG', 0);
define('BLOG_PROMO', 1);
define('BLOG_ARTICLE', 2);
define('BLOG_STATIC_PAGE', 3);

define('COMMON_KEYWORDS', 'корейская косметика,интернет-магазин,цена,купить,ямибокс,yamibox');

define('CATALOG_SELECT', '*, (select ifnull(round(avg(rating),1),0) from product_review where status = 1 and product_id = p.id) as rating_avg, (select count(*) from product_review where status = 1 and product_id = p.id) as review_count, (select count(*) from product_option where status = 1 and product_id = p.id) as option_count, (select ifnull(sum(quantity),0) from product_option where status = 1 and product_id = p.id) as option_quantity, (select min(price) from product_option where product_id = p.id) as min_price, (select min(price_old) from product_option where product_id = p.id) as min_price_old, (select max(spf) from product_option where product_id = p.id) as max_spf');

define('CURRENT_PAGE', (($p = absint(GET('p'))) > 1 || GET('p') == 'all') ? $p : 1);

ini_set('date.timezone', 'Europe/Moscow');
ini_set('session.gc_maxlifetime', '24000');
setlocale(LC_TIME, 'ru_RU.UTF-8', 'rus');

session_start();

x::data('site-url', x::config('SITE_URL'));
x::data('sid', session_id());
x::data('referer', SESSION('referer'));
x::data('request-uri', $_SERVER['REQUEST_URI']);

if(substr($_SERVER['REQUEST_URI'],0,10) == '/new_style') {
	define('NEW_STYLE', TRUE);
}
if(substr($_SERVER['REQUEST_URI'],0,5) == '/var3') {
	define('NEW_STYLE2', TRUE);
}
$layout_template = defined('NEW_STYLE') ? 'shop-layout2' : 'shop-layout';
if(defined('NEW_STYLE2')) $layout_template = 'shop-layout3';
tpl::load($layout_template);
tpl::load('common');
tpl::make('loadgif');
tpl::set('document-title', x::config('DOCUMENT_TITLE'));

function check_sid($sid=NULL) {
	if($sid==NULL && isset($_REQUEST['sid']) && !empty($_REQUEST['sid']))
		$sid = $_REQUEST['sid'];
	return empty($sid) ? false : $sid == session_id();
}

function check_captcha($code) {
	$code = trim($code);
	$ret = (!empty($code) && strtolower($code)==strtolower(SESSION('captcha')));
	$_SESSION['captcha'] = NULL;
	return $ret;
}

function alert($msg, $class='notice', $appendTo = 'main') {
	x::data('alert-msg', $msg);
	x::data('alert-class', $class);
	return tpl::make('alert', $appendTo);
	
}

function offset($limit = 20) {
	return ($page = absint(GET('p'))) > 1 ? ($page - 1) * $limit : 0;
}

function make_title($title_text, $appendTo = 'main') {
	tpl::set('document-title', $title_text);
	tpl::make('page-title', $appendTo);
}

function correct_float($val) {
	$val = str_replace(',', '.', $val);
	$val = preg_replace('/([^\d\.])/s', '', $val);
	return floatval($val);
}

function moneyFormat($float, $decimals = 0) {
	$float = ceil($float);
	return number_format($float, $decimals, '.', ' ');
}

function discountPrice($price, $discount) {
	return ceil($price * (100 - $discount) / 100);
}

function addCookie($key, $value) {
	setcookie($key, $value, time() + x::config('COOKIE_LIFETIME'), '/', x::config('COOKIE_DOMAIN'));
}

function delCookie($key) {
	setcookie($key, NULL, time() - x::config('COOKIE_LIFETIME') * 2, '/', x::config('COOKIE_DOMAIN'));
}

function log_write($str) {
	$date = date('Y-m-d H:i:s');
	file_put_contents(FILES.'/x.log', $date.' '.$str."\r\n", FILE_APPEND);
}

function die_stupid_bot() {
	if(REQUEST('leaveempty') || REQUEST('empty') || REQUEST('robot') || REQUEST('bot')) {
		status('401 Access Denied');
		die('DIE STUPID BOT! >:D');
	}
}
function float_rand($min, $max, $round = 1){
	$randomfloat = $min + mt_rand() / mt_getrandmax() * ($max - $min);
	if($round>0) {
		$randomfloat = round($randomfloat,$round);
	}
	return $randomfloat;
}
function add_pc($number, $pc) {
	return $number * (1 + $pc / 100);
}

if(!function_exists('password_hash')) {
	function password_hash($password) {
		return sha1($password);
	}
}

function br2nl($str) {
	$str = preg_replace('#<br?[\s/]*>#i', "\r\n", $str);
	return $str;
}

function ceilby($num, $by) {
	return ceil($num / $by) * $by;
}

?>