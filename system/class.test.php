<?php

set_time_limit(180);

class Test {
	
	static function run($path, $method) {
		if(!account::$admin && REQUEST('key') !== '5b2f22faa2ccb1c76c512eab6c12a1328d824de2') die('ACCESS DENIED');
		if(!method_exists('Test', $method)) {
			die('NO SUCH METHOD');
		}
		call_user_func('Test::'.$method);
	}
	
	static function cache_clear() {
		if($key = GET('key')) {
			cache::clear($key);
		}
		else {
			cache::flush();
		}
	}
	
	static function globals() {
		ob_start();
		echo '<pre style="background:white">';
		print_r($GLOBALS);
		printf("Memory: %d\r\n", memory_get_usage());
		print_r(cache::stats());
		print_r(db::$queries);
		db::query('show profiles');
		print_r(db::fetchAll());
		echo '</pre>';
		$out = ob_get_clean();
		if(qs::$params['path'] == '/test/globals') tpl::append('main', $out);
		else echo $out;
	}
	
	static function phpinfo() {
		phpinfo();
		exit;
	}
	
	static function importClients() {
		$csv = FILES.'/clients.csv';
		$f = fopen($csv, 'r');
		fgets($f); // skip first line (column names)
		db::prepare('insert ignore into user(firstname, lastname, phone, email) values(:firstname, :lastname, :phone, :email)');
		while($data = fgetcsv($f, 9999, "\t", '"')) {
			db::set('firstname', $data[0]);
			db::set('lastname', $data[1]);
			db::set('phone', $data[2]);
			db::set('email', $data[3]);
			db::execute();
		}
		db::query('update user set cookie = sha1(concat(id,email)) where password = \'\'');
	}
	
	static function archiveOrders() {
		$file = 'C:/Users/wwweb/Desktop/orders.csv';
		$f = fopen($file, 'r');
		fgets($f);
		$orders = [];
		while($line = fgetcsv($f, 9999, "\t", '"','\\')) {
			if(!$id = $line[0]) continue;
			$orders[] = $line;
		}
		fclose($f);
		$orders = array_group($orders, 0);
		foreach($orders as $id => $array) {
			$sum = 0;
			foreach($array as $key => $order) {
				$sum += $order[17];
			}
			$orders[$id] = $order;
			$orders[$id][17] = $sum;
		}
		$f = fopen('C:/Users/wwweb/Desktop/orders2.csv', 'w');
		foreach($orders as $order) {
			fputcsv($f, $order);
		}
		fclose($f);
	}
	
	static function archiveOrders2() {
		$file = 'C:/Users/wwweb/Desktop/orders2.csv';
		$f = fopen($file, 'r');
		$orders = [];
		while($line = fgetcsv($f)) {
			$orders[] = $line;
		}
		fclose($f);
		foreach($orders as $key => $order) {
			$id = 0;
			if($email = strtolower(trim($order[7]))) {
				$id = db::querySingle('select id from user where email = \''.$email.'\'');
			}
			$orders[$key][20] = $id;
		}
		$f = fopen('C:/Users/wwweb/Desktop/orders3.csv', 'w');
		foreach($orders as $order) {
			fputcsv($f, $order);
		}
		fclose($f);
	}
	
	static function importOrders() {
		$file = FILES.'/yamibox.csv';
		db::prepare('insert ignore into orders values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
		$f = fopen($file, 'r');
		fgets($f);
		while($line = fgetcsv($f, 9999, '|', '"', '"')) {
			//print_r($line);
			db::set($line);
			db::execute();
		}
		fclose($f);
	}
	
	static function reviewCount() {
		$file = FILES.'/popular.html';
		$html = file_get_contents($file);
		preg_match_all('#<a href="(.*?)" target="_blank">(.*?)</a>#msi', $html, $match);
		db::prepare('select '.CATALOG_SELECT.' from product p where url_name = :url_name');
		foreach($match[1] as $i => $href) {
			db::set('url_name', basename($href));
			db::execute();
			$product = db::fetchArray(true);
			$c = $product['review_count'];
			$postfix = $match[2][$i] . ' - ' . $c . ' otzyvov';
			$a = sprintf('<p><a href="%s" target="_blank">%s</a></p>'."\r\n", $href, $postfix);
			print_r($a);
		}
		exit;
	}
	
	static function coupon() {
		$coupon = shop::createCoupon(true, true, [
			'pc'=>1,
			'amount'=>5,
			'order_id'=>0
		]);
		tpl::append('main', $coupon['code']);
	}
	
	static function filters() {
		$filters = json::get('filters');
		$skinFilters = [$filters[21], $filters[22], $filters[23]];
		db::query('select id, filters from product where filters is null or filters not like \'% лет%\'');
		$products = db::fetchAll();
		foreach($products as $product) {
			$productFilters = $product['filters'] ? explode('|', $product['filters']) : [];
			foreach($skinFilters as $sf) {
				if(!in_array($sf, $productFilters)) $productFilters[] = $sf;
			}
			sort($productFilters);
			$value = implode('|', $productFilters);
			db::query('update product set filters = ? where id = ?', $value, $product['id']);
		}
	}
	
	static function update_cities() {
		db::query('select * from orders where delivery_info like ?', '%самовывоз%');
		$orders = db::fetchAll();
		foreach($orders as $order) {
			$address_json = $order['address'];
			$address = json::decode($address_json);
			if($address && $address['city']) {
				db::query('update orders set city = ? where id = ?', $address['city'], $order['id']);
			}
		}
	}
	
	static function supplier() {
		$files = get_files(FILES.'/import/');
		print_r($files);
	}
	
	static function holika() {
		$file = FILES.'/import/holika-unclear.csv';
		$f = fopen($file, 'r');
		$o = fopen(FILES.'/import/holika.csv', 'w+');
		while($line = fgetcsv($f, 9999)) {
			if(!preg_match('#\d{13,13}#', $line[0], $match)) continue;
			$barcode = $match[0];
			fputcsv($o, [$barcode, $line[1], $line[2]]);
		}
	}
	
	static function dberror() {
		db::query('select count(*) from product_import');
		$error = db::error(true);
		var_dump($error);
		var_dump((bool) intval($error[0]));
		exit;
	}
	
	static function lower() {
		$str = 'МосКВА';
		die(mb_strtolower($str, 'utf-8'));
	}
	
	static function clear_cache_key() {
		if(!$key = GET('key')) return;
		cache::clear($key);
	}
	
	static function update_stock_id() {
		db::query('select id, cart_items from orders');
		$orders = db::fetchAll();
		db::prepare('update orders set cart_items = ? where id = ?');
		function _update($id, $data) {
			db::set([$data, $id]);
			db::execute();
		}
		foreach($orders as $order) {
			$items = json::decode($order['cart_items']);
			foreach($items as &$item) {
				if(!isset($item['stock']) || $item['stock'] == 3) $item['stock'] = 2;
				elseif($item['stock'] == 2) $item['stock'] = 3;
			}
			$data = json::encode($items);
			_update($order['id'], $data);
		}
	}
	
	static function import_product_csv() {
		$filename = REQUEST('file') ?: 'tonymoly.txt';
		$file = FILES.'/'.$filename;
		$f = fopen($file, 'r');
		fgets($f); // skip headers line
		db::prepare('insert ignore into product set id = :id, manufacturer = :manufacturer, name = :name, url_name = :url_name, price = :price, short_text = :short_text, barcode = :barcode');
		while($line = fgetcsv($f, 9999, "\t")) {
			array_walk($line, 'trim');
			$data['id'] = intval($line[0]);
			$data['manufacturer'] = $line[1];
			$data['name'] = $line[2];
			$data['price'] = $line[3];
			$data['short_text'] = $line[4];
			$data['barcode'] = $line[5];
			$data['url_name'] = str::str2url($line[4].' '.$line[2]);
			db::set($data);
			if(!db::execute()) die(db::error());
		}
	}
	
	static function import_product_artikul() {
		$files = get_files(FILES.'/temp/', '/^sup_.+\.txt$/i');
		foreach($files as $file) {
			$f = fopen($file, 'r');
			fgets($f); // skip first line
			while($line = fgetcsv($f, 9999, "\t")) {
				$barcode = trim($line[0]);
				$artikul = trim($line[1]);
				$price = correct_float(trim($line[2]));
				db::query('update product set artikul = ?, price_buyin = ? where barcode = ?', $artikul, $price, $barcode);
				if(!db::count()) db::query('update product_option set artikul = ?, price_buyin = ? where barcode = ?', $artikul, $price, $barcode);
			}
			fclose($f);
		}
	}
	
	static function import_products() {
		$file = FILES.'/temp/HH140820.csv';
		product::import_csv($file);
		echo "<pre>\r\n";
		echo "PRODUCTS:\r\n\r\n";
		db::query('select * from product order by id desc limit 0, ?', $limit_products);
		print_r(db::fetchAll());
		echo "OPTIONS:\r\n\r\n";
		db::query('select * from product_option order by id desc limit 0, ?', $limit_options);
		print_r(db::fetchAll());
		echo "\r\n</pre>";
		
		exit;
	}
	
	static function update_art_by_sku() {
		$file = FILES.'/temp/PRICE220814.csv';
		$f = fopen($file, 'r');
		db::prepare('update product set artikul = :art where barcode = :sku');
		while($line = fgetcsv($f, 9999, '|')) {
			$sku = trim($line[1]);
			if(!$line || count($line) < 2 || !$sku) continue;
			$art = trim($line[2]) ?: NULL;
			db::set('sku', $sku);
			db::set('art', $art);
			db::execute();
			if(db::count()) print_r($line);
		}
		rewind($f);
		db::prepare('update product_option set artikul = :art where barcode = :sku');
		while($line = fgetcsv($f, 9999, '|')) {
			$sku = trim($line[1]);
			if(!$line || count($line) < 2 || !$sku) continue;
			$art = trim($line[2]);
			db::set('sku', $sku);
			db::set('art', $art);
			db::execute();
			if(db::count()) print_r($line);
		}
		fclose($f);
	}
	
	static function update_price_buyin() {
		$file = FILES.'/temp/PRICE220814.csv';
		$f = fopen($file, 'r');
		while($line = fgetcsv($f, 9999, "|")) {
			$data['barcode'] = trim(@$line[1]);
			$data['price_buyin'] = correct_float(@$line[7]);
			if(!$data['barcode'] || !$data['price_buyin']) continue;
			db::query('update product set price_buyin = :price_buyin where barcode = :barcode', $data);
			if(db::count()) print_r($line);
			db::query('update product_option set price_buyin = :price_buyin where barcode = :barcode', $data);
			if(db::count()) print_r($line);
		}
		fclose($f);
	}
	
	static function update_url_name() {
		db::query('select id, name, short_text from product');
		$products = db::fetchAll();
		db::prepare('update product set url_name = ? where id = ?');
		foreach($products as $product) {
			$url_name = str::str2url($product['short_text'].' '.$product['name']);
			db::set([$url_name, $product['id']]);
			db::execute();
		}
	}
	
	static function cities_list() {
		$file = FILES.'/temp/deliv_cities.txt';
		$cities = [];
		$f = fopen($file, 'r');
		while($line = fgets($f)) {
			$line = trim($line);
			if(preg_match('/^\w{2,}$/ui', $line)) {
				$cities[str::str2url($line)] = ucfirst($line);
			}
		}
		fclose($f);
		file_put_contents(JSON.'/delivery-cities.json', json::encode($cities));
	}
	
	static function cities_list2() {
		$cities = json::get('delivery-cities');
		$result = [];
		foreach($cities as $letter => $list) {
			foreach($list as $cityname) {
				$url = str::str2url($cityname);
				$result[$letter][$url] = $cityname;
			}
		}
		print_r($result);
		file_put_contents(JSON.'/delivery-cities.json', json::encode($result));
		exit;
	}
	
	static function update_rpo() {
		db::query('select id, delivery_info from orders where delivery_info regexp \'[0-9]{10,}\'');
		$orders = db::fetchAll();
		foreach($orders as $order) {
			preg_match('/(\d{10,})/', $order['delivery_info'], $match);
			db::query('update orders set tracking_id = ? where id = ?', $match[1], $order['id']);
		}
	}
	
	static function rpo() {
		$response = json::decode(web::rp_delivery_info('11509378062536'));
		$log_json = $response['log_json'];
		unset($response['log_json']);
		print_r($response);
		print_r(json::decode($log_json));
		exit;
	}
	
	static function mongo_export() {
		$products = [];
		db::query('select * from product where status > -1 order by id asc');
		while($product = db::fetchArray()) {
			if($product['barcode']) $product['barcode'] = (string)$product['barcode'];
			$products[$product['id']] = $product;
		}
		db::query('select * from product_option where status > -1 order by id asc');
		while($option = db::fetchArray()) {
			if($option['barcode']) $option['barcode'] = (string)$option['barcode'];
			$products[$option['product_id']]['options'][$option['id']] = $option;
		}
		foreach($products as &$product) {
			if(!isset($product['options'])) $product['options'] = NULL;
			$product = json::encode($product, JSON_UNESCAPED_UNICODE);
		}
		$data = implode(",\n", $products);
		file_put_contents(FILES.'/mongo-export.json', $data);
	}
	
	static function mongo_query() {
		// connect
		$m = new MongoClient();
		// select a database
		$db = $m->yamibox;
		// select a collection (analogous to a relational database's table)
		$collection = $db->products;
		// add a record
		//$document = array( "title" => "Calvin and Hobbes", "author" => "Bill Watterson" );
		//$collection->insert($document);
		// add another record, with a different "shape"
		//$document = array( "title" => "XKCD", "online" => true );
		//$collection->insert($document);
		// find everything in the collection
		$cursor = $collection->find();
		$cursor->limit(20);
		// iterate through the results
		foreach($cursor as $document) {
			print_r($document);
		}
		exit;
	}
	
	static function update_product_image() {
		echo "<pre>";
		product::updateImage();
		echo "</pre>";
		exit;
	}
	
	static function update_reviews() {
		$file = FILES.'/from4023.txt';
		$id = 4023;
		$f = fopen($file, 'r');
		db::prepare('update product_review set content = ? where id = ?');
		while($content = fgets($f)) {
			if(!$content = trim($content)) continue;
			db::set([$content, $id]);
			db::execute();
			$id++;
		}
		exit($id);
	}
	
	static function ppapi_login() {
		$login = ppapi::login();
		var_dump($login);
	}
	
	static function ppapi_order() {
		$session_id = ppapi::login();
		var_dump($session_id);
		$order_id = 3011;
		$order_sum = 2035;
		$edtn = md5($order_id.$order_sum);
		$pp_order = [
			'SessionId' => $session_id,
			'Sendings' => [
				[
					'EDTN' => $edtn,
					'IKN' => 9990003041,
					'Invoice' => [
						'SenderCode' => $order_id,
						'BarCode' => '',
						'GCBarCode' => '',
						'Description' => 'TEST',
						'RecipientName' => 'Client Name',
						'PostamatNumber' => '7802-005',
						'MobilePhone' => '79688061135',
						'Email' => 'info@yamibox.ru',
						'PostageType' => '10001',
						'GettingType' => '101',
						'PayType' => 1,
						'Sum' => $order_sum,
						'InsuareValue' => 0,
						'Width' => 15,
						'Height' => 10,
						'Depth' => 10,
						'ClientReturnAddress' => [
							'CityName' => 'Москва',
							'RegionName' => 'Московская область',
							'Address' => 'Большая Татарская, 35с5',
							'FIO' => 'Виктория Андреева',
							'PostCode' => '115093',
							'Organisation' => 'ИП Андреев П.Р.',
							'PhoneNumber' =>  '79688061135',
							'Comment' => 'comment text'
						],
						'UnclaimedReturnAddress' => [
							'CityName' => 'Москва',
							'RegionName' => 'Московская область',
							'Address' => 'Большая Татарская, 35с5',
							'FIO' => 'Виктория Андреева',
							'PostCode' => '115093',
							'Organisation' => 'ИП Андреев П.Р.',
							'PhoneNumber' => '79688061135',
							'Comment' => 'comment text'
						],
						'SubEncloses' => [
							[
								'Line' => '',
								'ProductCode' => '',
								'GoodsCode' => '',
								'Name' => 'Косметика и парфюимерия',
								'Price' => $order_sum
							]
						]
					]
				]
			]
		];
		$response = ppapi::create_order($pp_order);
		var_dump($response);
	}
	
	static function axiomus() {
		$data = [
			'data' => '<?xml version="1.0" standalone="yes"?>
			<singleorder>
			<mode>get_regions</mode>
			<auth ukey="567135892ed25f51b7de6517abb238df" />
			</singleorder>'
		];
		$api_url = 'http://axiomus.ru/hydra/api_xml.php';
		$response = web::http_request($api_url, 'POST', $data);
		file_put_contents(FILES.'/axiomus-response.xml', $response);
	}
	
	static function get_points() {
		boxberry::get_points();
		qiwipost::get_points();
		ppapi::get_points();
		axiomus::get_points();
	}
	
	static function pp_points() {
		$csv = file_get_contents(FILES.'/points/pickpoint.json');
		ppapi::import_points($csv);
	}
	
	static function bb_points() {
		$json = file_get_contents(FILES.'/points/boxberry.json');
		boxberry::import_points($json);
	}
	
	static function qp_points() {
		$csv = file_get_contents(FILES.'/points/qiwipost.csv');
		qiwipost::import_points($csv);
	}
	
	static function sd_points() {
		$xml = file_get_contents(FILES.'/points/sdek.xml');
		sdek::import_points($xml);
	}
	
	static function brands_report() {
		db::query('select cart_items from orders');
		$brands = [];
		while($order = db::fetchArray()) {
			if(!$order['cart_items']) continue;
			$cart_items = json::decode($order['cart_items']);
			foreach($cart_items as $item) {
				if(!@$item['manufacturer'] || !@$item['price'] || !@$item['quantity']) continue;
				if(!isset($brands[$item['manufacturer']])) $brands[$item['manufacturer']] = 0;
				$brands[$item['manufacturer']] += $item['price'] * $item['quantity'];
			}
		}
		echo "<pre>\n";
		foreach($brands as $brand => $sum) {
			printf("%s	%d\n", $brand, $sum);
		}
		echo "</pre>";
		exit;
	}
	
	static function holika_report() {
		db::query('select cart_items from orders');
		$products = [];
		while($order = db::fetchArray()) {
			if(!$order['cart_items']) continue;
			$cart_items = json::decode($order['cart_items']);
			foreach($cart_items as $item) {
				if(!@$item['manufacturer'] || !@$item['price'] || !@$item['quantity']) continue;
				if($item['manufacturer'] != 'Holika Holika') continue;
				$name = $item['name'];
				if($item['option_name']) $name .= ' - ' . $item['option_name'];
				if(!isset($products[$name])) $products[$name] = ['quantity' => 0, 'sum' => 0];
				$products[$name]['quantity'] += $item['quantity'];
				$products[$name]['sum'] += $item['price'] * $item['quantity'];
			}
		}
		ksort($products);
		echo "<pre>\n";
		foreach($products as $name => $data) {
			printf("%s	%d	%d\n", $name, $data['quantity'], $data['sum']);
		}
		echo "</pre>";
		exit;
	}
	
	static function import_delivery_data() {
		$file = FILES.'/sd-cities.csv';
		$f = fopen($file, 'r');
		fgets($f);
		db::query('truncate table delivery');
		db::prepare('replace into delivery values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
		while($line = fgetcsv($f, 9999, '|')) {
			if(count($line) < 17) continue;
			if(!$line[0] || !$line[1]) continue;
			array_walk($line, function(&$val) {
				$val = trim($val);
				if(!$val) $val = NULL;
			});
			array_splice($line, 17, 10);
			db::set($line);
			if(!db::execute()) {
				print_r($line);
			}
		}
		exit;
	}
	
	static function delivery() {
		print_r(delivery::get_city_points('Москва'));
		exit;
	}
	
	static function pp_orders() {
		print_r(ppapi::update_orders());
		exit;
	}
	
	static function sdek_orders() {
		print_r(sdek::update_orders());
		exit;
	}
	
	static function db_update() {
		db::prepare('update orders set dservice = 5, ctime = ? where id = ?');
		db::set(['2014-11-05 03:59:47', 3632]);
		db::execute();
		var_dump(db::count());
		exit;
	}
	
	static function asiacream() {
		asiacream::fetch_pages();
		exit;
	}
	
	static function asiacream_parser() {
		$result = [];
		$files = get_files(FILES.'/asiacream/', '#.+\.html$#');
		foreach($files as $file) {
			$html = file_get_contents($file);
			$result[] = asiacream::get_products($html);
		}
		return $result;
	}
	
	static function lunifera() {
		lunifera::fetch_pages();
		exit;
	}
	
	static function lunifera_parser() {
		$result = [];
		$files = get_files(FILES.'/lunifera/', '#.+\.html$#');
		foreach($files as $file) {
			$html = file_get_contents($file);
			$result[] = lunifera::get_products($html);
		}
		print_r($result);
		return $result;
	}
	
	static function bbcream() {
		bbcream::fetch_pages();
		exit;
	}
	
	static function bbcream_parser() {
		$result = [];
		$files = get_files(FILES.'/bbcream/', '#.+\.html$#');
		foreach($files as $file) {
			$html = file_get_contents($file);
			$result[] = bbcream::get_products($html);
		}
		print_r($result);
		return $result;
	}
	
	static function rp_calc() {
		print_r(json::decode(web::rp_calc('115093', '677000', '2650')));
		exit;
	}
	
	static function fake_review_ctime() {
		db::query('select id from product_review where id <= 2148 order by id desc');
		$reviews = db::fetchAll();
		$date = DateTime::createFromFormat('Y-m-d', '2014-02-10');
		db::prepare('update product_review set ctime = :ctime where id = :id');
		$i = 0;
		foreach($reviews as $review) {
			if($i > rand(35, 40)) {
				$i = 0;
				$date->modify('-1 day');
			}
			$ctime = $date->format('Y-m-d H:i:s');
			$data['id'] = $review['id'];
			$data['ctime'] = $ctime;
			db::set($data);
			db::execute();
			$i++;
		}
	}
	
	static function checkout() {
		var_dump(checkout::update_orders());
		exit;
	}
	
	static function sl_partners() {
		$result = shoplogistics::get_dictionary('partners');
		print_r($result);
		$file = FILES.'/shoplogistics/partners.json';
		file_put_contents($file, json::encode($result['partners']['partner']));
		exit;
	}
	
	static function sl_tarifs() {
		$result = shoplogistics::get_tarifs();
		print_r($result);
		$file = FILES.'/shoplogistics/tarifs.json';
		file_put_contents($file, json::encode($result['tarifs']['tarif']));
		exit;
	}
	
	static function sl_cities() {
		$file = FILES.'/shoplogistics/cities.json';
		if(TRUE) {
			$result = shoplogistics::get_dictionary('city');
			$cities = $result['cities']['city'];
			file_put_contents($file, json::encode($cities));
		}
		else {
			$json = file_get_contents($file);
			$cities = json::decode($json);
		}
		db::prepare('insert into sl_cities set name = :name, code_id = :code_id, is_courier = :is_courier, is_filial = :is_filial, oblast_code = :oblast_code, district_code = :district_code');
		foreach($cities as $city) {
			$city['district_code'] = NULL;
			db::set($city);
			db::execute();
		}
	}
	
	static function sl_regions() {
		shoplogistics::import_regions();
	}
	
	static function sl_tarifs_import() {
		shoplogistics::import_tarifs();
	}
	
	static function sl_pickups_import() {
		shoplogistics::import_pickups();
	}
	
	static function slc() {
		db::query('select * from sl_cities');
		while($city = db::fetchArray()) {
			if(preg_match_all('#[a-z]+#i', $city['name'], $match)) {
				printf($city['name'].' '.$city['code_id']."\r\n");
				print_r($match);
			}
		}
		exit;
	}
	
	static function sl_order() {
		db::query('select * from orders order by id desc limit 1');
		$orders = db::fetchAll();
		foreach($orders as $order) {
			$response = shoplogistics::send_order($order);
			print_r($response);
		}
		exit;
	}
	
	static function sl_order_status() {
		$response = shoplogistics::get_order_status();
		print_r($response);
		exit;
	}
	
	static function sl_order_update() {
		print_r(shoplogistics::update_orders_pr());
		exit;
	}
	
	static function order_status_recovery() {
		db::prepare('update orders set status = ? where id = ?');
		$file = FILES.'/temp/order-status-recovery.csv';
		$f = fopen($file, 'r');
		while($line = fgetcsv($f, 9999, '|')) {
			db::set([$line[1],$line[0]]);
			db::execute();
		}
		exit;
	}
	
	static function update_order_items() {
		db::query('select id, cart_items from orders where id >= 4921');
		$orders = db::fetchAll();
		$brands = json::get('brands');
		$stocks = [];
		foreach($brands as $brand) {
			$stocks[$brand['name']] = $brand['stock'];
		}
		db::prepare('update orders set cart_items = ? where id = ?');
		foreach($orders as $order) {
			$items = json::decode($order['cart_items']);
			foreach($items as &$item) {
				$item['stock'] = $stocks[$item['manufacturer']];
			}
			$items = json::encode($items);
			db::set([$items, $order['id']]);
			db::execute();
		}
	}
	
	static function barcode() {
		$code = GET('code') ?: "2015010605082";
		barcode39::image($code, 'code128', 40);
	}
	
	static function email() {
		$from = GET('from') ?: 'info@yamibox.ru';
		shop::sendEmail('info@yamibox.ru', 'TEST', 'TEST', ['from'=>$from]);
		exit;
	}
	
	static function yandex_text() {
		$text = db::querySingle('select text from product where length(text) >= 500 order by rand() limit 1');
		$response = yandex::post_text($text);
		var_dump($response);
		exit;
	}
	
	static function users_mango() {
		db::query("select concat(firstname, ' ', lastname) as name, phone, email, bdate, address, ifnull((select sum(total_sum) + sum(delivery_price) - sum(bonus) - sum(coupon) - sum(cumulative) from orders where user_id = u.id and status = 4), 0) as order_sum from user u where phone is not null and phone != '' order by id desc");
		$users = db::fetchAll();
		$f = fopen(FILES.'/temp/users_mango2.csv', 'w+');
		foreach($users as $user) {
			$user['name'] = trim($user['name']);
			if($user['address']) {
				$address = json::decode($user['address']);
				$user['address'] = str::address2str($address);
			}
			if($user['bdate']) {
				$bdate = DateTime::createFromFormat('Y-m-d', $user['bdate']);
				$user['bdate'] = $bdate->format('d.m.Y');
			}
			$phone = str::filter_phone_number($user['phone']);
			$user['phone'] = $phone ?: NULL;
			$user['order_sum'] = 'Сумма доставленных '.$user['order_sum'];
			fputcsv($f, $user, ';');
		}
		fclose($f);
	}
	
	static function sl_deliveries() {
		$dcache = FILES.'/temp/sl_deliveries.json';
		if(!file_exists($dcache)) {
			$deliveries = shoplogistics::get_deliveries();
			file_put_contents($dcache, json::encode($deliveries, JSON_UNESCAPED_UNICODE));
		}
		else {
			$deliveries = json::decode(file_get_contents($dcache));
		}
		// print_r($deliveries);
		db::prepare('update orders set tracking_id = ? where id = ?');
		foreach($deliveries['deliveries']['delivery'] as $delivery) {
			db::set([$delivery['code'], $delivery['order_id']]);
			db::execute();
		}
		exit;
	}
	
	static function status_sms() {
		$sms = order::status_sms([5246], 'intransit-pr');
		var_dump($sms);
		exit;
	}
	
	static function postcalc() {
		$request['v'] = 2000;
		$request['t'] = '629757';
		$response = postcalc::request($request);
		print_r(json::decode($response));
		exit;
	}
	
	static function price_buyin() {
		db::query('select id, cart_items from orders');
		$orders = db::fetchAll();
		$products = $options = [];
		db::query('select id, price_buyin from product');
		while($product = db::fetchArray()) {
			$products[$product['id']] = $product['price_buyin'];
		}
		db::query('select id, price_buyin from product_option');
		while($option = db::fetchArray()) {
			$options[$option['id']] = $option['price_buyin'];
		}
		db::prepare('update orders set cart_items = :cart_items where id = :id');
		foreach($orders as $order) {
			$cart_items = json::decode($order['cart_items']);
			foreach($cart_items as &$item) {
				if($item['option_id'] > 0) {
					$item['price_buyin'] = @$options[$item['option_id']] ?: NULL;
				}
				else {
					$item['price_buyin'] = @$products[$item['id']] ?: NULL;
				}
			}
			$order['cart_items'] = json::encode($cart_items);
			db::set($order);
			db::execute();
		}
		exit;
	}
	
	static function mail_attachment() {
		$file = FILES.'/ms.zip';
		shop::sendEmail(x::config('SHOP_EMAIL'), 'attachment', 'check the file', ['attachment' => $file]);
	}
	
	static function merge_orders() {
		admin::merge_orders(5971, 5999);
	}
	
	static function sl_put_products() {
		$response = shoplogistics::put_products();
		// $response = str::xmltoarray($response);
		print($response);
		exit;
	}
	
	static function sl_transfer_products() {
		db::query('select * from product where status = 1 and quantity > 0');
		$products = db::fetchAll();
		db::query('select * from product_option where status = 1 and quantity > 0');
		$options = db::fetchAll();
		$options = array_group($options, 'product_id');
		$items = [];
		foreach($products as $product) {
			$po = isset($options[$product['id']]) ? $options[$product['id']] : [];
			if($po) {
				foreach($po as $o) {
					$items[] = [
						'id' => $o['product_id'],
						'name' => $product['name'],
						'barcode' => $o['barcode'],
						'price' => $o['price'],
						'price_old' => $o['price_old'],
						'quantity' => $o['quantity'],
						'manufacturer' => $product['manufacturer'],
						'url_name' => $product['url_name'],
						'weight' => $o['weight'],
						'volume' => $o['volume'],
						'option_name' => $o['name'],
						'option_id' => $o['id'],
						'option_uid' => $o['option_id'],
						'stock' => $o['stock']
					];
				}
			}
			else {
				$items[] = [
					'id' => $product['id'],
					'name' => $product['name'],
					'barcode' => $product['barcode'],
					'price' => $product['price'],
					'price_old' => $product['price_old'],
					'quantity' => $product['quantity'],
					'manufacturer' => $product['manufacturer'],
					'url_name' => $product['url_name'],
					'weight' => $product['weight'],
					'volume' => $product['volume'],
					'option_name' => NULL,
					'option_id' => NULL,
					'option_uid' => NULL,
					'stock' => $product['stock']
				];
			}
		}
		shoplogistics::create_act(2, $items);
	}
	
	static function sl_tracking_widget() {
		$tracking_id = db::querySingle('select tracking_id from orders where tracking_id is not null order by id desc limit 1');
		$html = shoplogistics::tracking_widget($tracking_id);
		print_r($html);
		exit;
	}
	
	static function metro() {
		$metro = shoplogistics::import_metros();
		print_r($metro);
		exit;
	}
	
	static function dguru_date() {
		$result = dguru::get_delivery_date();
		print_r($result);
		exit;
	}
	
	static function dguru_order() {
		$result = dguru::send_order(6620);
		print_r($result);
		exit;
	}
	
	static function export1c() {
		order::export_for_1c();
		exit;
	}
	
	static function orders_for_rr() {
		db::query('select *, date(ctime) as cdate from orders where status in (4,8)');
		$orders = db::fetchAll();
		$file = FILES.'/temp/orders_for_rr.csv';
		$f = fopen($file, 'w+');
		foreach($orders as $order) {
			$cart_items = json::decode($order['cart_items']);
			$user_id = $order['user_id'] ?: '';
			foreach($cart_items as $item) {
				$item_id = $item['option_id'] ? $item['id'].'00'.$item['option_id'] : $item['id'];
				$line = [];
				$line[] = $order['id'];
				$line[] = $order['cdate'];
				$line[] = $item_id;
				$line[] = $user_id;
				fputcsv($f, $line, ';');
			}
		}
		fclose($f);
		exit;
	}
	
	static function skin79() {
		import::skin79_txt();
		exit;
	}
	
	static function create_account() {
		$email = 'help@yamibox.ru';
		$password = 'letmedothat4u';
		$password_hash = password_hash($password, PASSWORD_DEFAULT);
		$cookie = sha1($email . $password_hash);
		db::query('insert ignore into user set email = ?, cookie = ?, password = ?, status = 50', $email, $cookie, $password_hash);
		var_dump(db::count());
		exit;
	}
	
	static function dguru_orders() {
		dguru::update_orders();
	}
	
	static function update_pr_tracking_id() {
		$result = shoplogistics::update_pr_tracking_id();
		print_r($result);
		exit;
	}
	
	static function parikmag() {
		$products = parikmag::get_products();
		$result = parikmag::save_products($products);
		var_dump($result);
		exit;
	}
	
	static function parikmag_import() {
		parikmag::import();
		exit;
	}
	
	static function parikmag_images() {
		parikmag::fetch_images();
		exit;
	}
	
	static function margintop() {
		product::get_margintop();
		exit;
	}
	
	static function top100() {
		// x::do_action('action.yml-top.php');
		product::update_product_sum_top();
		exit;
	}
	
	static function top100yml() {
		x::do_action('action.yml-top.php');
		exit;
	}
	
	static function banner() {
		promo::main();
		exit;
	}
	
	static function import_missha() {
		$result = import::missha();
		print_r($result);
		exit;
	}
	
	static function auto_price() {
		$result = product::auto_price();
		print_r($result);
		exit;
	}
	
	static function correct_text() {
		db::query("select id, text from product where text not like '<p>%' and text not like '%описание товара%' and text not like '%<table%'");
		$products = db::fetchAll();
		db::prepare('update product set text = :text where id = :id');
		foreach($products as $product) {
			$text = $product['text'];
			$text = explode('<br />', $text);
			foreach($text as &$p) {
				$p = '<p>'.trim($p).'</p>';
			}
			$text = implode("\n", $text);
			$text = str_replace('<p></p>', '', $text);
			$product['text'] = $text;
			db::set($product);
			db::execute();
		}
		exit;
	}
	
	static function update_stock() {
		$result = shop::update_stock_from_import();
		print_r($result);
		exit;
	}
	
	static function robokassa() {
		$result = robokassa::import_excerpt();
		print_r($result);
		exit;
	}
	
	static function config() {
		$object = new stdClass();
		$object->parameter = date('Y-m-d H:i:s');
		config::set('test_key', 'string value');
		config::set('object', $object);
		config::set('test_key2', ['array key' => 'array value', 'boolean' => true, 'integer' => 12345, 'object' => $object]);
		$config = config::get();
		print_r($config);
		exit;
	}
	
	static function products_healing() {
		$f = fopen(FILES.'/temp/healing_products.csv', 'w+');
		db::query('select id, name, url_name from product where status >= 0 and text like ? or text like ?', '%лече%', '%лечи%');
		while($product = db::fetchArray()) {
			$product['url_name'] = 'http://yamibox.ru/product/'.$product['url_name'];
			fputcsv($f, $product, "\t");
		}
		fclose($f);
	}
	
	static function generate_banner() {
		$cid = promo::get_meta()['category_id'];
		promo::generate_banner($cid);
		exit;
	}
	
	static function import_bills() {
		bill::import();
		exit;
	}
	
	static function user_orders() {
		$file = FILES.'/temp/user_orders.csv';
		$f = fopen($file, 'w+');
		fputcsv($f, ['Имя','Фамилия','Email','Телефон','Сумма доставленных','Последний заказ','Заказов доставлено','Средний чек','Последний визит'], ";");
		db::query('select firstname, lastname, email, phone, sum(if(status = 4, (total_sum + delivery_price - cumulative - bonus - coupon), 0)) as sum_orders_delivered, (select max(last_order_time) from user where id = o.user_id) as last_order_time, sum(if(status = 4, 1, 0)) as orders_delivered, round(sum(if(status = 4, (total_sum + delivery_price - cumulative - bonus - coupon), 0))/sum(if(status = 4, 1, 0)), 2) as order_avg, (select max(ltime) from user where id = o.user_id) as ltime from orders o where length(email) > 3 group by email order by sum_orders_delivered desc');
		while($row = db::fetchArray()) {
			fputcsv($f, $row, ";");
		}
		fclose($f);
		header('Content-Type:text/csv');
		header('Content-Disposition: attachment; filename="'.basename($file).'"');
		readfile($file);
		exit;
	}
	
	static function getresponse() {
		$gr = new getresponse('0f55d00c50a41af76057cecf8713bfec', true);
		$firstname = account::$info['firstname'];
		$lastname = account::$info['lastname'];
		$email = account::$info['email'];
		// $email = 'cottonhead@mail.ru';
		$customs = [
			'subscriber_email' => $email,
			'subscriber_first_name' => $firstname,
			'subscriber_last_name' => $lastname
		];
		$response = $gr->addContact('VHvl2', $firstname, $email, 'standard', 0, $customs);
		print_r($response);
		exit;
	}
	
	static function apply_auto_discount() {
		$meta = promo::get_meta();
		print_r($meta);
		$cid = absint(GET('cid')) ?: $meta['category_id'];
		$discount_pc = absint(GET('pc')) ?: 15;
		$result = promo::discount_apply($cid, $discount_pc);
		var_dump($result);
		exit;
	}
	
	static function gr_client() {
		$gr = getresponse::get_instance();
		$client = $gr->getContactsByEmail('noemail@example.com');
		print_r($client);
		exit;
	}
	
	static function move_contact_to_ordered() {
		$email_list = [];
		db::query('select distinct email from orders where email is not null and email != \'\' and status != 5 and id = 1');
		while($email = db::fetchSingle()) $email_list[] = $email;
		foreach($email_list as $email) {
			$result = getresponse::move_contact_to_ordered($email);
			if($result) {
				print_r($result);
			}
		}
		exit;
	}
	
	static function gr_add_contacts() {
		$email_list = [];
		ob_end_clean();
		db::query('select distinct email from user u where email is not null and email != \'\' and not exists(select 1 from orders where user_id = u.id or email = u.email)');
		while($email = db::fetchSingle()) $email_list[] = $email;
		foreach($email_list as $email) {
			$result = getresponse::add_contact($email);
			if($result) {
				print_r($result);
			}
		}
		exit;
	}
	
	static function import_lanixm() {
		$result = import::lanixm();
		print_r($result);
		exit;
	}
	
	static function sl_pr() {
		$result = shoplogistics::get_post_deliveries_array();
		print_r($result);
		exit;
	}
	
}

?>