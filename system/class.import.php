<?php

class Import {
	
	static function skin79() {
		$file_url = 'http://koreancosmeticsopt.ru/price/Price%20(XLSX).xlsx';
		$inputFileName = FILES.'/import/Skin79.xlsx';
		if(!file_put_contents($inputFileName, fopen($file_url, 'r'))) {
			self::set_skin79_status(false);
		}
		else {
			self::set_skin79_status(true);
		}
		new phpexcel();
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		
		db::prepare('replace into product_import set barcode = :barcode, artikul = :artikul, name = :name, stock = :stock, quantity = :quantity, price_usd = :price_usd, filename = :filename');
		$i = 0;
		for($row = 1; $row <= $highestRow; ++$row) {
			$row_data = [];
			for($col = 0; $col <= $highestColumnIndex; ++$col) {
				$value = $objWorksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
				$row_data[] = $value == '#NULL!' ? 0 : $value;
			}
			// print_r($row_data);
			$barcode = $row_data[5]; if(!preg_match('/\d{13}/', $barcode)) continue;
			$artikul = $row_data[4];
			$name = $row_data[3];
			$quantity = intval($row_data[9]);
			$price = floatval($row_data[8]);
			db::set([
				'barcode' => $barcode,
				'artikul' => $artikul,
				'name' => $name,
				'stock' => 6,
				'quantity' => $quantity,
				'price_usd' => $price,
				'filename' => 'Skin79.xlsx'
			]);
			db::execute();
			$i++;
		}
		return ['import::skin79' => $i];
	}
	
	static function skin79_txt() {
		$file_url = 'http://koreancosmeticsopt.ru/price/Price%20(TXT).txt';
		$inputFileName = FILES.'/import/Skin79.txt';
		if(!file_put_contents($inputFileName, fopen($file_url, 'r'))) {
			return self::skin79();
		}
		db::prepare('replace into product_import set barcode = :barcode, artikul = :artikul, name = :name, stock = :stock, quantity = :quantity, price_usd = :price_usd, filename = :filename');
		$i = 0;
		$f = fopen($inputFileName, 'r');
		while($line = fgetcsv($f, 9999, "\t")) {
			// print_r($line);
			$barcode = $line[5]; if(!preg_match('/\d{13}/', $barcode)) continue;
			$artikul = $line[4];
			$name = $line[3];
			$quantity = intval(correct_float($line[9]));
			$price = correct_float($line[7]);
			db::set([
				'barcode' => $barcode,
				'artikul' => $artikul,
				'name' => $name,
				'stock' => 6,
				'quantity' => $quantity,
				'price_usd' => $price,
				'filename' => 'Skin79.txt'
			]);
			db::execute();
			$i++;
		}
		return ['import::skin79' => $i];
	}
	
	static function get_skin79_status() {
		return !file_exists(FILES.'/temp/skin79.fail');
	}
	
	static function set_skin79_status($success) {
		if($success) {
			if(self::get_skin79_status()) unlink(FILES.'/temp/skin79.fail');
		}
		else {
			file_put_contents(FILES.'/temp/skin79.fail', '');
		}
	}
	
	static function missha() {
		$inputFileName = FILES.'/import/missha.xlsx';
		new phpexcel();
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		
		db::prepare('replace into product_import set barcode = :barcode, artikul = :artikul, name = :name, stock = :stock, quantity = :quantity, price_usd = :price_usd, filename = :filename');
		$i = 0;
		for($row = 1; $row <= $highestRow; ++$row) {
			$row_data = [];
			for($col = 0; $col <= $highestColumnIndex; ++$col) {
				$value = $objWorksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
				$row_data[] = $value == '#NULL!' ? 0 : $value;
			}
			$barcode = $row_data[0]; if(!preg_match('/\d{13}/', $barcode)) continue;
			$name = $row_data[1];
			$quantity = intval($row_data[4]);
			$price = floatval($row_data[2]);
			db::set([
				'barcode' => $barcode,
				'artikul' => NULL,
				'name' => $name,
				'stock' => 5,
				'quantity' => $quantity,
				'price_usd' => $price,
				'filename' => 'missha.xlsx'
			]);
			db::execute();
			$i += db::count();
		}
		return ['import::missha' => $i];
	}
	
	static function upload_lanixm() {
		if(!isset($_FILES['lanixm'])) die('NO FILE!');
		if(!$file = uploadFile($_FILES['lanixm'], FILES.'/import/', 'lanixm.xls')) die('ERROR UPLOADING FILE!');
		self::lanixm();
		die('ok');
	}
	
	static function lanixm() {
		$inputFileName = FILES.'/import/lanixm.xls';
		new phpexcel();
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		db::query('delete from product_import where stock = 8');
		db::prepare('replace into product_import set barcode = :barcode, artikul = :artikul, name = :name, stock = :stock, quantity = :quantity, price_usd = :price_usd, filename = :filename');
		$i = 0;
		for($row = 1; $row <= $highestRow; ++$row) {
			$row_data = [];
			for($col = 0; $col <= $highestColumnIndex; ++$col) {
				$value = $objWorksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
				$row_data[] = $value == '#NULL!' ? 0 : $value;
			}
			if(!preg_match('/(\d{13})/', $row_data[1], $match)) continue;
			$barcode = $match[1];
			$name = $row_data[3];
			$quantity = 50;
			if(strpos(mb_strtolower($row_data[8], 'utf-8'), 'нет') !== false) $quantity = 0;
			$price = correct_float($row_data[6]);
			db::set([
				'barcode' => $barcode,
				'artikul' => NULL,
				'name' => $name,
				'stock' => 8,
				'quantity' => $quantity,
				'price_usd' => $price,
				'filename' => 'lanixm.xls'
			]);
			db::execute();
			$i += db::count();
		}
		return ['import::lanixm' => $i];
	}
	
}

set_time_limit(0);
?>