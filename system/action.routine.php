<?php

if(!account::$admin && REQUEST('key') !== '5b2f22faa2ccb1c76c512eab6c12a1328d824de2') {
	status('403 Forbidden');
	die('ACCESS DENIED');
}

set_time_limit(0);
ignore_user_abort(true);

define('ROUTINE', TRUE);

function updateProductQuantity() {
	return product::update_quantity();
}

// чтобы выводить в ЛК правильный статус наличия
// пока не используется
function updateWishlist() {
	db::query('update wishlist set is_stock = 0 where option_id = 0 and product_id in(select id from product where status >= 0 and stock > 1)');
	db::query('update wishlist set is_stock = 0 where option_id > 0 and option_id in(select id from product_option where status >= 0 and stock > 1)');
}

function updateExpireDate() {
	db::query('update product set expires = NULL where expires = \'\'');
	db::query('update product p set expires = (select min(expires) from product_option where product_id = p.id) where (select count(*) from product_option where product_id = p.id) > 0');
	$c = db::count();
	return ['updateExpireDate' => 'Обновлено товаров: '.$c];
}

function delete_product_view() {
	db::query('delete from product_view where utime < (now() - interval 3 month)');
	return [__FUNCTION__ => db::count()];
}

function optimize_tables() {
	db::query('show tables');
	$tables = db::fetchAll();
	$result = [];
	foreach($tables as $table) {
		$table_name = reset($table);
		db::query('optimize table `'.$table_name.'`');
		$result[] = db::fetchArray();
	}
	return [__FUNCTION__ => $result];
}

function sendWishlistNotice() {
	db::query('select id from product where id in(select product_id from wishlist where option_id = 0 and in_stock = 0) and stock > 1');
	$products = db::fetchAll();
	db::query('select id from product_option where id in(select option_id from wishlist where option_id > 0 and in_stock = 0) and stock > 1');
	$options = db::fetchAll();
	if(!$products && !$options) return;
	db::query('select * from user where id in (select user_id from wishlist w where in_stock = 0 and if(option_id > 0, (select stock from product_option where id = w.option_id), (select stock from product where id = w.product_id)) > 1)');
	$users = db::fetchAll();
	shop::sendWishlistNoticeEmail($users);
	// обновляем wishlist, чтобы не отсылать повторное уведомление
	if($products) {
		db::prepare('update wishlist set in_stock = 1 where product_id = ?');
		foreach($products as $product) {
			db::set(1, $product['id']);
			db::execute();
		}
	}
	if($options) {
		db::prepare('update wishlist set in_stock = 1 where option_id = ?');
		foreach($options as $option) {
			db::set(1, $option['id']);
			db::execute();
		}
	}
}

function activate_coupons($delivered) {
	foreach($delivered as $orderId) {
		// add bonus to user
		db::query('update user set bonus = bonus + (select ROUND((total_sum - bonus) / 100) from orders where id = ?) where id = (select user_id from orders where id = ?)', $orderId, $orderId);
		// activate coupon for next order
		db::query('update coupon set status = 1, ctime = CURRENT_TIMESTAMP where order_id = ?', $orderId);
		log_write('Заказ '.$orderId.': активирован купон, начислены бонусы');
	}
}

function delivered_sms($delivered) {
	db::query('select *, (select code from coupon where order_id = o.id) as coupon_code from orders o where id in(' . implode(',', $delivered) . ')');
	$sms_text = shop::get_sms_text('order-delivery');
	foreach(db::fetchAll() as $order) {
		if(!$order['coupon_code'] || !$order['phone']) continue;
		if(!$phone = str::filter_phone_number($order['phone'])) continue;
		$message = sprintf($sms_text, $order['coupon_code']);
		web::send_sms($phone, $message);
	}
}

function delivered_email($delivered = NULL) {
	if(!$delivered) die('EMPTY ID LIST');
	db::query('select * from orders where id in ('.implode(',', $delivered).')');
	foreach(db::fetchAll() as $order) {
		shop::sendDeliveredEmail($order);
	}
}

function intransit_email($intransit = NULL) {
	if(!$intransit) die('EMPTY ID LIST');
	db::query('select * from orders where id in ('.implode(',', $intransit).')');
	foreach(db::fetchAll() as $order) {
		shop::sendIntransitEmail($order);
	}
}

function pickpoint_email($pickpoint = NULL) {
	if(!$pickpoint) die('EMPTY ID LIST');
	db::query('select * from orders where id in ('.implode(',', $pickpoint).')');
	foreach(db::fetchAll() as $order) {
		shop::sendPickpointEmail($order);
	}
}

function order_review_email() {
	$sql = 'select * from orders where status = 4 and length(email) > 3 and review_request = 0 and unix_timestamp() - (86400*5) > unix_timestamp(dtime)';
	if(x::config('DEBUG')) {
		$sql = 'select * from orders where user_id = 1 order by id desc limit 1';
	}
	db::query($sql);
	$orders = db::fetchAll();
	$a = count($orders);
	$idList = [];
	foreach($orders as $order) {
		if(!shop::sendOrderReviewEmail($order)) continue;
		$idList[] = $order['id'];
	}
	if($idList) {
		db::query('update orders set review_request = 1 where id in('.implode(',', $idList).')');
	}
	return ['order_review_email.select' => $a, 'order_review_email.email_sent' => db::count()];
}

function update_orders_pr() {
	return shoplogistics::update_orders_pr();
}

function update_orders_pickpoint() {
	return ppapi::update_orders();
}

function update_orders_sdek() {
	return sdek::update_orders();
}

function update_orders_checkout() {
	return checkout::update_orders();
}

function update_orders_sl() {
	return shoplogistics::update_orders();
}

function happyBirthday() {
	db::query('select * from user where phone is not null and bdate is not null and if(bdate_last is null, true, bdate_last < YEAR(CURRENT_TIMESTAMP)) and YEAR(CURRENT_TIMESTAMP) - YEAR(bdate) > 12 and ((dayofyear(bdate) - dayofyear(current_date) between 0 and 15) or (dayofyear(bdate) + 365 - dayofyear(current_date) between 0 and 15))');
	//db::query('select * from user where id = 1');
	$result = [];
	$sms_template = json::get('sms')['happy-birthday'];
	$users = db::fetchAll();
	foreach($users as $user) {
		$phone = str::filter_phone_number($user['phone']);
		if($phone) {
			$coupon = [
				'pc' => 1,
				'amount' => 10,
				'order_id' => 0,
				'user_id' => $user['id'],
				'status' => 1
			];
			$coupon = shop::createCoupon(true, true, $coupon);
			$message = str_replace('COUPON', $coupon['code'], $sms_template);
			if(web::send_sms($phone, $message)) {
				db::query('update user set bdate_last = YEAR(CURRENT_TIMESTAMP) where id = ?', $user['id']);
			}
			$sms_result = x::get('SMS_RESULT');
			$result[] = $sms_result;
		}
	}
	return ['happyBirthday' => $result];
}

function import_supplier_data() {
	return product::import_supplier_data();
}

function import_points() {
	$result = [];
	$result[] = ppapi::import_points();
	$result[] = boxberry::import_points();
	$result[] = qiwipost::import_points();
	$result[] = sdek::import_points();
	return $result;
}

function import_asiacream() {
	return spy::deploy('asiacream');
}

function import_bbcream() {
	return spy::deploy('bbcream');
}

function import_lunifera() {
	return spy::deploy('lunifera');
}

function import_sl_data() {
	$result = [];
	$result[] = shoplogistics::import_tarifs();
	$result[] = shoplogistics::import_pickups();
	$result[] = shoplogistics::import_metros();
	$result[] = shoplogistics::import_cities();
	$result[] = shoplogistics::import_regions();
	return $result;
}

function delete_old_coupons() {
	db::query('update coupon set status = 0 where (user_id > 0 or order_id > 0) and ctime < now() - interval 1 month');
	$count = db::count();
	db::query('update coupon set status = 0 where quantity = 0');
	$count += db::count();
	return ['delete_old_coupons' => $count];
}

function send_waybills() {
	$dw = date('w');
	// disable for saturday and sunday
	if(in_array($dw, [0,6])) return ['waybill' => 'WEEKEND! WAITING FOR MONDAY'];
	$stock_list = shop::get_stock_list(false);
	foreach($stock_list as $stock) {
		if(!$stock['status'] || $stock['id'] < 3) continue;
		waybill::request($stock['url_name'], true);
		waybill::clear($stock['id']);
	}
	return ['waybill' => 'DONE!'];
}

function export_for_1c() {
	$result[] = product::export_for_1c();
	$result[] = order::export_for_1c();
	return $result;
}

function import_skin79() {
	return import::skin79_txt();
}

function cancel_old_orders() {
	$result = order::cancel_old_orders();
	return $result;
}

function parikmag_import() {
	return parikmag::import();
}

function margintop() {
	return product::define_margintop();
}

function update_product_sum() {
	return product::update_product_sum();
}
function update_product_sum_top() {
	return product::update_product_sum_top();
}
function yml_top100() {
	x::do_action('action.yml-top.php');
}
function generate_yml() {
	// занимает много времени
	x::do_action('action.yml.php');
	// yml-файл для Retail Rocket
	// x::do_action('action.yml.php', [1=>'rr']);
}

function auto_discount() {
	sleep(2);
	$meta = promo::get_meta();
	$date = DateTime::createFromFormat('Ymd', $meta['date']);
	$result['auto_discount'] = promo::main();
	return $result;
}

function auto_price() {
	$result = product::auto_price();
	return $result;
}

function update_products() {
	$result = product::update_price_buyin_from_import();
	$result += product::update_artikul_from_import();
	$result += product::update_quantity();
	return $result;
}

function call_every_5min() {
	$result = shop::update_stock_from_import();
	return $result;
}

function call_every_10min() {
	$result = [];
	$result[] = updateProductQuantity();
	$result[] = updateExpireDate();
	// обновление статусов заказов shoplogistics
	$result[] = update_orders_sl();
	$result[] = update_orders_pr();
	// export new orders for 1C
	$result[] = export_for_1c();
	return $result;
}

function call_every_30min() {
	
}

function call_every_hour() {
	update_products();
	sendWishlistNotice();
	delete_old_coupons();
	promo::discount_reset_not_instock();
	update_orders_pickpoint();
	update_orders_sdek();
	generate_yml();
}

function call_every_6_hours() {
	$result[] = margintop();
	// обновление таблицы сумм продаж
	$result[] = update_product_sum();
	// обновление списка топ-10% (топ100)
	$result[] = update_product_sum_top();
	// popular100.xml
	$result[] = yml_top100();
	return $result;
}

// runs at 20:00(MOSCOW)
function call_every_day_2000() {
	// импорт постаматов PickPoint
	$result[] = import_points();
	//
	// $result[] = happyBirthday();
	//
	$result[] = import_asiacream();
	$result[] = import_lunifera();
	$result[] = import_bbcream();
	$result[] = import_sl_data();
	return $result;
}

// runs at 11:00(MOSCOW)
function call_every_day_1100() {
	$result[] = delete_product_view();
	$result[] = optimize_tables();
	return $result;
}

function call_every_day_1000() {
	$result = [];
	$result[] = send_waybills();
	return $result;
}

function call_every_day_900() {
	$result = import_supplier_data();
	return $result;
}

function call_every_day_800() {
	
}

function call_every_day_0000() {
	$result[] =	cancel_old_orders();
	$result[] = auto_price();
	$result[] = auto_discount();
	return $result;
}

if(!function_exists($args[1])) {
	die('NO SUCH FUNCION');
}

$result = call_user_func($args[1]);
if($result) {
	ob_start();
	echo "<pre>\r\n";
	print_r($result);
	echo "\r\n</pre>";
	$result = ob_get_clean();
}
exit($result ?: 'FUNCTION EXECUTED. NO DATA RETURNED.');

?>