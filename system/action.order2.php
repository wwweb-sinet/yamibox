<?php

define('ONECLICK', strtolower(trim(REQUEST('form'))) == 'oneclick');

if(!cart::$sum) {
	return redirect('/cart');
}

die_stupid_bot();
$captcha = strtolower(REQUEST('captcha'));

make_title('Оформление заказа');

shop::makeBreadcrumbs();

tpl::load('order-form2');

tpl::push(account::$info);
if(account::$auth && account::$info['address']) {
	tpl::push(json::decode(account::$info['address']));
}
if($session_zipcode = SESSION('zipcode')) {
	tpl::set('zipcode', $session_zipcode);
}

$region = COOKIE('region_to') ?: 'Москва';
$city = COOKIE('city_to') ?: 'Москва';
tpl::set('city', $city);
tpl::set('region', $region);

$bonus_discount = intval(SESSION('bonus-spend'));
$cumulative = 0;
$cum_discount = 0;
if(account::$auth && !account::$admin) {
	$cumulative = account::getCumulative();
	$cum_discount = round((cart::$sum / 100) * $cumulative);
}

$coupon = SESSION('coupon');
$coupon_discount = intval(SESSION('coupon-discount'));

$dtype = absint(REQUEST('dtype'));
$ptype = absint(REQUEST('ptype'));

$dservice = absint(REQUEST('dservice'));

$delivery_info = trim(REQUEST('delivery_info'));
$delivery_price = absint(REQUEST('delivery_price'));
$delivery_price_real = absint(REQUEST('delivery_price_real'));

$pickuppoint = trim(REQUEST('pickuppoint'));

$region_match = false;
foreach(shoplogistics::get_regions() as $oblast) {
	tpl::set('option-value', $oblast['name']);
	tpl::set('option-text', $oblast['name']);
	tpl::set('option-selected', $oblast['name'] == $region ? ' selected' : '');
	tpl::make('option', 'region-option');
	if($oblast['name'] == $region) $region_match = true;
}
if(!$region_match && $region != 'Москва') $region = 'Москва';
$cities = shoplogistics::get_city_list($region);
foreach($cities as $c) {
	tpl::set('option-value', $c['name']);
	tpl::set('option-text', $c['name']);
	tpl::set('option-selected', $c['name'] == $city ? ' selected' : '');
	tpl::make('option', 'city-option');
}

foreach(json::get('ptype') as $value => $ptype_array) {
	if($value == 3) { // разрешена только оплата при получении
		tpl::push($ptype_array);
		tpl::set('value', $value);
		// tpl::set('ptype-checked', $value == $ptype ? ' checked' : '');
		tpl::set('ptype-checked', ' checked');
		tpl::make('ptype-option');
	}
}

// CART ITEMS

function makeProduct($product, $option = NULL) {
	if($option) {
		$product_count = cart::$options[$option['id']];
		$product['barcode'] = $option['barcode'];
		$product['price'] = $option['price'];
		$product['price_old'] = $option['price_old'];
		$product['stock'] = $option['stock'];
		tpl::set('option-name', $option['name']);
		tpl::make('order-form-cart-item-option-name');
	}
	else {
		$product_count = cart::$products[$product['id']];
	}
	tpl::push($product);
	tpl::set('product-img', product::getImage($product));
	tpl::set('product-href', '/product/'.$product['url_name']);
	tpl::set('product-count', $product_count);
	tpl::set('product-sum', $product_count * $product['price']);
	tpl::make('order-form-cart-item');
	tpl::clear('order-form-cart-item-option-name');
}

$products = cart::getProducts();
foreach($products as $product) {
	makeProduct($product);
}

if($options = cart::getOptions('id')) {
	$products = shop::getOptionProducts(array_keys($options));
	foreach($options as $id => $opts) {
		foreach($opts as $option) {
			makeProduct($products[$option['product_id']], $option);
		}
	}
}

foreach(cart::$gifts as $gift) {
	if(!$gift) continue;
	tpl::push($gift);
	tpl::make('order-form-cart-gift');
}

tpl::set('cart-count', cart::count());
tpl::set('cart-sum', cart::$sum);
tpl::set('discount-sum', $bonus_discount);
tpl::set('cumulative', $cumulative);
tpl::set('cumulative-discount', $cum_discount);
tpl::set('coupon-discount', $coupon_discount);
tpl::set('total-sum', cart::$sum - $bonus_discount - $cum_discount - $coupon_discount);
tpl::set('bonus-spend', $bonus_discount);

if(REQUEST_METHOD == 'POST') {
	array_walk($_POST, function(&$val, $key) {
		$val = $key == 'comment' ? trim($val) : html2chars(trim($val));
	});
	tpl::push($_POST);
	$firstname = POST('firstname');
	$lastname = POST('lastname');
	$email = POST('email');
	$phone = POST('phone');
	$region = intval(POST('manual_input')) ? POST('region_m') : POST('region');
	$city = intval(POST('manual_input')) ? POST('city_m') : POST('city');
	$comment = POST('comment');
	
	$address = ['city' => $city];
	
	$create_account = isset($_POST['create_account']);
	// $create_account = true;
	
	$error = false;
	if(ONECLICK && (!$firstname || !$phone || !$captcha)) {
		$error = 'form-fill';
	}
	elseif(ONECLICK && $captcha != strtolower(SESSION('captcha'))) {
		$error = 'captcha';
	}
	elseif(!ONECLICK && (!$firstname || !$phone || !$city)) {
		$error = 'form-fill';
	}
	// elseif(!ONECLICK && (!$email = filter_var($email, FILTER_VALIDATE_EMAIL))) {
		// $error = 'email';
	// }
	elseif(!ONECLICK && !$dtype) {
		$error = 'dtype';
	}
	elseif(strlen($phone) < 3) {
		$error = 'phone';
	}
	// Курьеры (наши, SL или ПР), адрес обязателен
	elseif(($dtype == 5 && !$pickuppoint) || in_array($dtype, [1,3])) {
		$zipcode = POST('zipcode');
		$metro = POST('metro');
		$street = POST('street');
		$building = POST('building');
		$building_add = POST('building-add');
		$apt = POST('apt');
		if($delivery_price === '' || $delivery_info === '') {
			$error = 'delivery-type-required';
		}
		elseif(!$city || $street == '' || $building == '') {
			$error = 'address-required';
		}
		else {
			$address = array(
				'region' => $region,
				'city' => $city,
				'zipcode' => $zipcode,
				'metro' => $metro,
				'street' => $street,
				'building' => $building,
				'building-add' => $building_add,
				'apt' => $apt
			);
		}
	}
	
	// try create account
	if(!account::$auth && $create_account) {
		$password = str::generatePassword(6);
		if($accountId = account::register($email, $password)) {
			$cookie = sha1($email . password_hash($password, PASSWORD_DEFAULT));
			$data = [
				'id' => $accountId,
				'cookie' => $cookie,
				'firstname' => $firstname,
				'lastname' => $lastname,
				'phone' => $phone,
				'city' => $city,
				'address' => json::encode($address),
				'bonus' => 40
			];
			account::update($data);
			account::$auth = true;
			account::$info = $data;
			account::$cookie = $cookie;
			addCookie('cookie', account::$cookie);
		}
	}
	
	if(ONECLICK) {
		if(account::$auth) {
			$lastname = account::$info['lastname'];
			$address = json::decode(account::$info['address']);
		}
		$city = tpl::get('city');
	}
	
	if($error) {
		$errors = json::get('errors');
		$response = [
			'status' => 'error',
			'message' => $errors[$error]
		];
		if(AJAX) die(json::encode($response));
		alert($errors[$error], 'error', 'main');
	}
	else {
		$data = array(
			'id' => NULL,
			'firstname' => $firstname,
			'lastname' => $lastname,
			'email' => $email,
			'phone' => $phone,
			'region' => $region,
			'city' => $city,
			'address' => json::encode($address),
			'dtype' => $dtype,
			'ptype' => $ptype,
			'total_sum' => cart::$sum,
			'bonus' => $bonus_discount,
			'cumulative' => $cum_discount,
			'coupon' => $coupon_discount,
			'cart_items' => cart::getOrderJSON(),
			'user_id' => account::$info['id'],
			'cookie' => account::$cookie,
			'uid' => md5(uniqid()),
			'comment' => $comment,
			'delivery_info' => $delivery_info ?: NULL,
			'delivery_price' => $delivery_price,
			'delivery_price_real' => $delivery_price_real,
			'dservice' => $dservice
		);
		if($pickuppoint) {
			// ПВЗ PickPoint
			if($dservice == 5) {
				db::query('select * from points_pp where id = ?', $pickuppoint);
				if($delivery_data = db::fetchArray()) {
					$data['delivery_data'] = json::encode($delivery_data);
				}
			}
			else {
				db::query('select * from sl_pickups where code_id = ?', $pickuppoint);
				if($delivery_data = db::fetchArray()) {
					$data['delivery_data'] = json::encode($delivery_data);
				}
			}
		}
		if($order_id = order::save($data)) {
			// if(in_array($data['dtype'],[1,2]) && $data['dservice'] != 10 && $data['dservice'] != 5) {
				// shoplogistics::send_order($order_id);
			// }
			$data['id'] = $order_id;
			$data['cumulative-percent'] = $cumulative;
			$data['address'] = str::address2str($address);
			// create coupon for next order
			$coupon_auto = json::get('coupon-auto');
			$coupon_amount = 0;
			foreach($coupon_auto as $ca) {
				if(cart::$sum >= $ca['sum']) $coupon_amount = $ca['discount'];
			}
			$c = cart::$sum < 1000 ? false : shop::createCoupon(true, false, [
				'pc' => 1,
				'amount' => $coupon_amount,
				'order_id' => $order_id,
				'user_id' => account::$info['id']
			]);
			$data['coupon-code'] = $c ? $c['code'] : '';
			if(ONECLICK) shop::sendOneclickOrderEmail($data);
			else shop::sendOrderEmail($data);
			// сбрасываем из сессии и вычитаем у клиента использованные бонусы
			account::reset(); // IMPORTANT! Must be called BEFORE cart::save()!
			cart::clear();
			cart::save();
			// update used coupon
			if($coupon) {
				db::query('update coupon set quantity = quantity - 1, used = used + 1, discount_sum = discount_sum + ? where code = ?', $coupon_discount, $coupon);
			}
			$response = [
				'status' => 'ok',
				'order_uid' => $data['uid'],
				'redirect' => '/order-info/'.$data['uid']
			];
			if(AJAX) die(json::encode($response));
			return redirect($response['redirect']);
		}
		else {
			$response = [
				'status' => 'ok',
				'message' => 'Извините, что-то пошло не так.'
			];
			if(AJAX) die(json::encode($response));
			return alert('Извините, что-то пошло не так.', 'error', 'main');
		}
	}
}
tpl::set('orderform-script-mtime', filemtime(ROOT.'/ui/js/orderform.js'));
tpl::make('oneclick-form');
tpl::make('order-form-delivery');
tpl::make('order-form2', 'main');

?>