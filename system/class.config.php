<?php

class config {
	
	const CACHE_KEY = 'yamibox_config';
	const CACHE_LIFETIME = 10e4;
	
	static function set($key, $value) {
		$data['key'] = $key;
		if(is_array($value) || is_object($value) || $value instanceof Traversable) {
			$data['value'] = json::encode($value);
			$data['json'] = 1;
		}
		else {
			$data['value'] = $value;
			$data['json'] = 0;
		}
		db::query('replace into yamibox_config set `key` = :key, value = :value, json = :json', $data);
		self::reload();
		return $value;
	}
	
	static function get($key = NULL) {
		if(!$config = cache::get(self::CACHE_KEY)) {
			$config = self::reload();
		}
		if(!$key) return $config;
		if(!isset($config[$key])) {
			return NULL;
		}
		return $config[$key];
	}
	
	static function reload() {
		$config = [];
		db::query('select * from yamibox_config order by `key`');
		while($row = db::fetchArray()) {
			if($row['json']) $value = json::decode($row['value']);
			else $value = $row['value'];
			$config[$row['key']] = $value;
		}
		cache::set(self::CACHE_KEY, $config, self::CACHE_LIFETIME);
		return $config;
	}
	
}

?>