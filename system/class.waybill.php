<?php

class Waybill {
	
	static $stocks = ['holika' => 3, 'novaciya' => 4, 'missha' => 5, 'skin79' => 6, 'faceshop' => 7, 'lanixm' => 8];
	
	static function get_items($stock, $ctime) {
		$items = $items_tmp = $_brands = [];
		$stock = absint($stock);
		$brands = shop::get_brands();
		foreach($brands as $brand) {
			$_brands[$brand['name']] = $brand['stock'];
		}
		db::query('select id, cart_items from orders where status = 2 and (ctime >= ? or stime >= ?)', $ctime, $ctime);
		$orders = db::fetchAll();
		foreach($orders as $order) {
			$cart_items = json::decode($order['cart_items']);
			// filter items by stock
			foreach($cart_items as $item) {
				$item['order_id'] = $order['id'];
				$item['orders'] = [$order['id'] => $item['quantity']];
				$item_stock = $_brands[$item['manufacturer']];
				if($item_stock != $stock) continue;
				$items_tmp[] = $item;
			}
		}
		foreach(self::get_additional_items($stock) as $item) {
			$item['order_id'] = NULL;
			$item['orders'] = [];
			$items_tmp[] = $item;
		}
		// get current quantity in office
		$quantity = [];
		db::query('select id, quantity from product p where quantity > 0 and not exists(select 1 from product_option where product_id = p.id)');
		while($p = db::fetchArray()) {
			$quantity['products'][$p['id']] = $p['quantity'];
		}
		db::query('select id, quantity from product_option where quantity > 0');
		while($po = db::fetchArray()) {
			$quantity['options'][$po['id']] = $po['quantity'];
		}
		// group by unique key and sum up quantity
		foreach($items_tmp as $item) {
			$item['option_id'] = intval($item['option_id']);
			$key = $item['id'].'-'.$item['option_id'];
			if(isset($items[$key])) {
				$items[$key]['quantity'] += $item['quantity'];
				if($item['order_id'] && !isset($items[$key]['orders'][$item['order_id']])) {
					$items[$key]['orders'][$item['order_id']] = $item['quantity'];
				}
			}
			else {
				$items[$key] = $item;
			}
		}
		// subtract office items
		foreach($items as $key => $item) {
			if($item['option_id'] && isset($quantity['options'][$item['option_id']])) {
				$q = $quantity['options'][$item['option_id']];
				$items[$key]['quantity'] -= $q;
				if($items[$key]['quantity'] <= 0) unset($items[$key]);
			}
			elseif(!$item['option_id'] && isset($quantity['products'][$item['id']])) {
				$q = $quantity['products'][$item['id']];
				$items[$key]['quantity'] -= $q;
				if($items[$key]['quantity'] <= 0) unset($items[$key]);
			}
		}
		// sort by name
		usort($items, function($a, $b) {
			return $a['name'] > $b['name'] ? 1 : 0;
		});
		self::update_from_db($items);
		return $items;
	}
	
	static function get_additional_items($stock_id) {
		$items = $wb_items = $products = $options = [];
		// waybill data
		db::query('select * from waybill where stock_id = ?', $stock_id);
		while($wb_item = db::fetchArray()) {
			$index = $stock_id.$wb_item['product_id'].intval($wb_item['option_id']);
			$wb_items[$index] = $wb_item;
		}
		// waybill products data
		db::query('select * from product where id in (select distinct product_id from waybill where stock_id = ?)', $stock_id);
		while($product = db::fetchArray()) {
			$products[$product['id']] = $product;
		}
		// waybill options data
		db::query('select * from product_option where id in (select distinct option_id from waybill where option_id > 0 and stock_id = ?)', $stock_id);
		while($option = db::fetchArray()) {
			$options[$option['id']] = $option;
		}
		foreach($options as $option) {
			$index = $stock_id.$option['product_id'].$option['id'];
			$items[] = [
				'id' => $option['product_id'],
				'name' => $products[$option['product_id']]['name'],
				'barcode' => $option['barcode'],
				'price' => $option['price'],
				'price_old' => $option['price_old'],
				'quantity' => $wb_items[$index]['quantity'],
				'manufacturer' => $products[$option['product_id']]['manufacturer'],
				'url_name' => $products[$option['product_id']]['url_name'],
				'weight' => $option['weight'],
				'volume' => $option['volume'],
				'option_name' => $option['name'],
				'option_id' => $option['id'],
				'stock' => $option['stock']
			];
		}
		foreach($products as $product) {
			$index = $stock_id.$product['id'].'0';
			if(!isset($wb_items[$index])) continue;
			$items[] = [
				'id' => $product['id'],
				'name' => $product['name'],
				'barcode' => $product['barcode'],
				'price' => $product['price'],
				'price_old' => $product['price_old'],
				'quantity' => $wb_items[$index]['quantity'],
				'manufacturer' => $product['manufacturer'],
				'url_name' => $product['url_name'],
				'weight' => $product['weight'],
				'volume' => $product['volume'],
				'option_name' => NULL,
				'option_id' => NULL,
				'stock' => $product['stock']
			];
		}
		return $items;
	}
	
	static function admin_view($path, $stock = NULL) {
		if($stock) return self::request($stock);
		// забрал товар со склада - зафиксируй время!
		if(REQUEST_METHOD == 'POST') {
			$stock = absint(POST('stock'));
			self::clear($stock);
			die('ok');
		}
		$stocks = shop::get_stock_list();
		$file = FILES.'/waybill-start.json';
		// время последнего сбора товаров со склада
		$datetime = json::decode(file_get_contents($file));
		make_title('Заказы со склада');
		function _make($items) {
			$n = $sum = 0;
			foreach($items as $item) {
				if($item['quantity'] <= 0) continue;
				$n++;
				$sum += $item['price_buyin'] * $item['quantity'];
				tpl::push($item);
				tpl::set('name', $item['manufacturer'].' '.$item['name']);
				foreach($item['orders'] as $order_id => $quantity) {
					tpl::set('order_id', $order_id);
					tpl::set('order_quantity', $quantity);
					tpl::make('waybill-product-order');
				}
				tpl::set('n', $n);
				tpl::set('product-sum', $item['price_buyin'] * $item['quantity']);
				if($item['option_name']) {
					tpl::set('option_name', ' - '.$item['option_name']);
				}
				tpl::make('waybill-product');
				tpl::clear('waybill-product-order');
			}
			return $sum;
		}
		foreach($stocks as $id => $stock) {
			if($id < 3) continue;
			tpl::set('stock-name', $stock['name']);
			tpl::set('stock-key', $stock['url_name']);
			$items = self::get_items($id, $datetime[$id]);
			$sum = _make($items);
			tpl::set('total-sum', $sum);
			tpl::set('waybill-start', $datetime[$id]);
			tpl::make('admin-waybill-list');
			tpl::clear('waybill-product');
			tpl::set('n', 0);
		}
		tpl::make('admin-waybill', 'main');
	}
	
	static function update_from_db(&$items) {
		foreach($items as &$item) {
			if($item['option_id']) {
				$option = db::querySingle('select artikul, price_buyin, (select short_text from product where id = po.product_id) as short_text from product_option po where id = '.$item['option_id'], true);
				$item['price_buyin'] = $option['price_buyin'];
				$item['artikul'] = $option['artikul'];
				$item['short_text'] = $option['short_text'];
			}
			else {
				$product = db::querySingle('select artikul, price_buyin, short_text from product where id = '.$item['id'], true);
				$item['price_buyin'] = $product['price_buyin'];
				$item['artikul'] = $product['artikul'];
				$item['short_text'] = $product['short_text'];
			}
			if(!$item['price_buyin']) $item['price_buyin'] = 0;
		}
	}
	
	static function request($stock_name, $send_request = false) {
		$stock_id = self::$stocks[$stock_name];
		if(!array_key_exists($stock_name, self::$stocks)) die('НЕТ ТАКОГО СКЛАДА');
		// время последнего сбора товаров со склада
		$file = FILES.'/waybill-start.json';
		$datetime = json::decode(file_get_contents($file))[$stock_id];
		make_title('Формирование заявки на закуп со склада');
		$items = self::get_items($stock_id, $datetime);
		$products = $items;
		$n = $sum = 0;
		foreach($products as $product) {
			if($product['quantity'] <= 0) continue;
			$n++;
			tpl::push($product);
			tpl::set('name', $product['manufacturer'] . ' ' . $product['name']);
			tpl::set('option-name', $product['option_name'] ? ' - '.$product['option_name'] : '');
			tpl::set('product-sum', $product['price_buyin'] * $product['quantity']);
			tpl::set('n', $n);
			tpl::make('admin-waybill-request-item');
			$sum += tpl::get('product-sum');
			if($send_request) {
				foreach($product['orders'] as $order_id => $quantity) {
					$log_data = [
						'stock_id' => $stock_id,
						'order_id' => $order_id,
						'product_id' => $product['id'],
						'option_id' => $product['option_id'],
						'quantity' => $quantity,
						'price_buyin' => $product['price_buyin']
					];
					db::query('insert into wb_log set stock_id = :stock_id, order_id = :order_id, product_id = :product_id, option_id = :option_id, quantity = :quantity, price_buyin = :price_buyin', $log_data);
				}
			}
		}
		tpl::set('total-sum', $sum);
		// Пробники
		$num_samples = $sum >= 500 ? ceil($sum / 500) : 0;
		if($num_samples > 0) {
			tpl::set('n', $n+1);
			tpl::set('name', '<strong>Любые недорогие пробники</strong>');
			tpl::set('option-name', '');
			tpl::set('barcode', '');
			tpl::set('artikul', '');
			tpl::set('short_text', 'пробники');
			tpl::set('price_buyin', '');
			tpl::set('quantity', $num_samples);
			tpl::set('product-sum', '');
			tpl::make('admin-waybill-request-item');
		}
		$stocks = shop::get_stock_list();
		tpl::set('stock-name', $stocks[$stock_id]['name']);
		tpl::set('date', $date = date('Y.m.d H:i'));
		if(!$send_request) {
			tpl::set('stock', $stock_id);
			tpl::make('admin-waybill-control');
		}
		$html = tpl::make('admin-waybill-request', 'main');
		tpl::clear('admin-waybill-request-item');
		if($send_request && $products) {
			product::updateStock($stock_id, $products);
			$email = [x::config('SHOP_EMAIL')];
			if(x::config('DEBUG')) {
				$email[] = 'wwweb.sinet@gmail.com';
			}
			else {
				if($stock_id == 3) {
					$email[] =  x::config('EMAIL_HOLIKA');
					$email[] = 'ahmed_nagoev@mail.ru';
				}
				elseif($stock_id == 4) {
					$email[] = x::config('EMAIL_NOVACIYA');
				}
				elseif($stock_id == 5) {
					$email[] = x::config('EMAIL_MISSHA');
					$email[] = 'cosline-msk@mail.ru';
				}
				elseif($stock_id == 6) {
					$email[] = x::config('EMAIL_SKIN79');
					$email[] = 'alina.kim2010@gmail.com';
					$email[] = 'vchepikova910@gmail.com';					
				}
				elseif($stock_id == 7) {
					$email[] = x::config('EMAIL_FACESHOP');
				}
				elseif($stock_id == 8) {
					$email[] = 'lamiso@lanixm.ru';
				}
			}
			$subject = 'Заявка от ИП Андреев П.Р. '.$date;
			$attachment = ($stock_id == 4) ? self::to_xls_novaciya($products, $stock_name) : self::to_xls($products, $stock_name);
			return shop::sendEmail($email, $subject, $html, ['layout' => 'email-layout-clear', 'from' => 'buh@yamibox.ru', 'attachment' => $attachment]);
		}
	}
	
	static function addItem($path, $product_id = 0, $option_id = 0) {
		$quantity = intval(REQUEST('quantity'));
		$result = self::add_item($product_id, $option_id, $quantity);
		die($result ? 'ok' : 'error');
	}
	
	static function add_item($product_id, $option_id, $quantity) {
		$product_id = intval($product_id);
		$option_id = intval($option_id);
		$stock_id = product::getStockId($product_id);
		if(!$stock_id || !$quantity) return false;
		db::query('select ifnull(quantity, 0) from waybill where product_id = ? and option_id = ? and stock_id = ?', $product_id, $option_id, $stock_id);
		$quantity += db::fetchSingle();
		db::query('replace into waybill set product_id = ?, option_id = ?, stock_id = ?, quantity = ?', $product_id, $option_id, $stock_id, $quantity);
		return true;
	}
	
	static function clear($stock) {
		$file = FILES.'/waybill-start.json';
		// время последнего сбора товаров со склада
		$datetime = json::decode(file_get_contents($file));
		$datetime_new = date('Y-m-d H:i:s');
		$datetime[$stock] = $datetime_new;
		file_put_contents($file, json::encode($datetime));
		db::query('delete from waybill where stock_id = ?', $stock);
		return true;
	}
	
	static function ajax() {
		$action = REQUEST('action');
		if($action == 'send-request') {
			$stock = intval(REQUEST('stock'));
			$stocks = array_flip(self::$stocks);
			$stock_name = $stocks[$stock];
			if(self::request($stock_name, true)) {
				if($clear = intval(REQUEST('clear'))) {
					self::clear($stock);
				}
				die('ok');
			}
		}
		die('error');
	}
	
	static function to_xls($products, $stock_name) {
		$data = [['№','Штрих-код','Артикул','Наименование','Краткое описание','Цена, руб','Количество','Сумма']];
		$n = $sum = 0;
		foreach($products as $product) {
			$n++;
			$name = $product['manufacturer'] .' '. $product['name'];
			if($product['option_name']) $name .= ' - ' . $product['option_name'];
			$product_sum = $product['price_buyin'] * $product['quantity'];
			$data[] = [
				$n, ''.$product['barcode'], $product['artikul'], $name, $product['short_text'], $product['price_buyin'], $product['quantity'], $product_sum
			];
			$sum += $product_sum;
		}
		$data[] = [
			'','','','','','','Итого',$sum
		];
		$date = date('Y.m.d H:i');
		$title = 'Заявка от ИП Андреев П.Р. '.$date;
		$excel = new phpexcel();
		$excel->getProperties()->setCreator("Yamibox.ru")
			->setLastModifiedBy("PHP5")
			->setTitle($title)
			->setSubject($title);
		$excel->setActiveSheetIndex(0);
		$excel->getActiveSheet()->getStyle('B')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CUSTOM1);
		$excel->getActiveSheet()->fromArray($data, NULL, 'A1');
		foreach(range('A', 'H') as $col) {
			$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
		}
		
		$filename = FILES.'/waybill/Andreev_PR_'.$stock_name.date('_Y_m_d_H_i').'.xls';
		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save($filename);
		return $filename;
	}
	
	static function to_xls_novaciya($products, $stock_name) {
		// Достаем все акртикулы
		$product_artikul = $option_artikul = [];
		db::query('select id, artikul from product where artikul is not null');
		while($product = db::fetchArray()) {
			$product_artikul[$product['id']] = $product['artikul'];
		}
		db::query('select id, artikul from product_option where artikul is not null');
		while($option = db::fetchArray()) {
			$option_artikul[$option['id']] = $option['artikul'];
		}
		//
		$date = date('Y.m.d H:i');
		new phpexcel();
		$inputFileName = FILES.'/novaciya-template.xls';
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		$objPHPExcel->setActiveSheetIndex(0);
		// $row = $objPHPExcel->getActiveSheet()->getHighestRow()+1;
		$row = 13;
		foreach($products as $product) {
			$name = $product['manufacturer'] .' '. $product['name'];
			if($product['option_name']) $name .= ' - ' . $product['option_name'];
			$artikul = $product['option_id']
						? @$option_artikul[$product['option_id']]
						: @$product_artikul[$product['id']];
			$product_sum = $product['price_buyin'] * $product['quantity'];
			$objPHPExcel->getActiveSheet()
				->setCellValue('A'.$row, '')
				->setCellValue('B'.$row, $product['barcode'])
				->setCellValue('C'.$row, '')
				->setCellValue('D'.$row, $artikul)
				->setCellValue('E'.$row, '')
				->setCellValue('F'.$row, $name)
				->setCellValue('G'.$row, '')
				->setCellValue('H'.$row, '')
				->setCellValue('I'.$row, '')
				->setCellValue('J'.$row, $product['quantity'])
				->setCellValue('K'.$row, $product['price_buyin'])
				->setCellValue('L'.$row, '')
				->setCellValue('M'.$row, '')
				->setCellValue('N'.$row, '')
				->setCellValue('O'.$row, $product_sum)
				->setCellValue('P'.$row, $product['short_text']);
			$row++;
		}
		$objPHPExcel->getActiveSheet()
			->setCellValue('O9', '=SUM(O13:O'.$row.')')
			->setCellValue('J9', '=SUM(J13:J'.$row.')');
		
		$filename = FILES.'/waybill/Andreev_PR_'.$stock_name.date('_Y_m_d_H_i').'.xls';
		// $filename = FILES.'/Andreev_PR_novaciya.xls';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $inputFileType);
		$objWriter->save($filename);
		return $filename;
	}
	
}

tpl::load('admin');

?>