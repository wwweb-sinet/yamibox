<?php

class kladr {
	
	static $api_url = 'http://kladr-api.ru/api.php';
	
	static function request($data) {
		return json::decode(web::http_request(self::$api_url, 'GET', $data));
	}
	
	static function search_object($str) {
		$data['oneString'] = 1;
		$data['withParent'] = 1;
		$data['limit'] = 5;
		$data['query'] = $str;
		return self::request($data);
	}
	
	static function get_zipcode($str) {
		$response = self::search_object($str);
		if(!isset($response['result'])) return false;
		foreach($response['result'] as $location) {
			if($zip = @$location['zip']) return $zip;
		}
		// check parents (regions)
		foreach($response['result'] as $location) {
			if(!isset($location['parents'])) continue;
			foreach($location['parents'] as $region) {
				if($zip = $region['zip']) return $zip;
			}
		}
		return false;
	}
	
}

?>