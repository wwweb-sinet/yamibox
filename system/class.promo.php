<?php

class Promo {
	
	static $category;
	
	static function main() {
		$meta = self::get_meta();
		$category = self::get_category();
		// reset previously set discounts
		self::discount_reset($meta['category_id']);
		// update && save meta
		$meta = [
			'category_id' => $category['id'],
			'category_url' => $category['url'],
			'date' => date('Ymd')
		];
		self::save_meta($meta);
		// reset any other discounts before applying new discounts
		self::discount_reset_all();
		// banner discount (random category)
		if($autodiscount_check = config::get('autodiscount_check')) {
			if(self::generate_banner($category['id'])) {
				self::discount_apply($category['id'], 15);
			}
		}
		$instock_discount_check = config::get('instock_discount_check');
		$instock_discount_pc = config::get('instock_discount_pc');
		if(intval($instock_discount_check)) {
			self::instock_discount_apply($instock_discount_pc);
		}
		return $meta;
	}
	
	static function generate_banner($category_id) {
		$date = date('Ymd');
		$date_start = date('d.m.y');
		//$date_end = date('d.m.y', strtotime('today + 2 days'));
		
		$date_text = sprintf('действует на сайте yamibox.ru только  %s до 23:59 (МСК)', $date_start);
		
		$category_text = self::get_category_text();
		$product_id = self::get_product_id($category_id);
		
		$templates = get_files(FILES.'/promo/', '/promo\-index[\d]\.png/i');
		$template = array_random($templates);
		$img_file = FILES.'/product/'.$product_id.'-1.jpg';
		$new_file = FILES.'/promo/1180x460/'.$date.'.jpg';
		$product_tmp = FILES.'/promo/product_image.jpg';
		$font = ROOT.'/ui/font/open_sans_regular.ttf';
		$color1 = '#ffffff';
		
		$img = new SimpleImage();
		$img->load($img_file)->resize(420,420)->save($product_tmp, 95, 'jpg');
		$img->create(1180, 460)
			->overlay($product_tmp, 'top left', 1, 53, 16)
			->overlay($template, 'left', 1)
			->text('Скидка 15% на все товары в категории '.$category_text, $font, 11, $color1, 'bottom right', -70, -52)
			->text($date_text, $font, 11, $color1, 'bottom right', -70, -36)
			->text('Суммируется скидка по купону, личная накопительная скидка и бонусы', $font, 11, $color1, 'bottom right', -70, -20)
			->save($new_file, 95, 'jpg');
		// create smaller version for old design (780x310)
		$small = FILES.'/promo/780x310/'.$date.'.jpg';
		$img->load($new_file)
			->resize(795, 310)
			->crop(7,0,780,310)
			->save($small);
		return true;
	}
	
	static function get_category_text() {
		$c = self::get_category();
		$rsaquo = ' › ';
		if($c['grandparent_name']) {
			return $c['grandparent_name'] . $rsaquo . $c['parent_name'] . $rsaquo . $c['name'];
		}
		return $c['parent_name'] . $rsaquo . $c['name'];
	}
	
	static function get_meta() {
		if(!$meta = config::get('autodiscount')) {
			$meta = json::decode(file_get_contents(FILES.'/promo/autodiscount.json'));
			if(!$meta) $meta = [
				'category_id' => 0,
				'category_url' => '#',
				'date' => date('Ymd')
			];
			config::set('autodiscount', $meta);
		}
		return $meta;
	}
	
	static function save_meta($meta) {
		return config::set('autodiscount', $meta);
	}
	
	static function get_category() {
		// current category
		if(self::$category) return self::$category;
		// get previously used category id to skip it
		$meta = self::get_meta();
		$prev_cid = $meta['category_id'];
		$categories = shop::getCategories();
		$categories_filtered = [];
		foreach($categories as $cid => $cat) {
			if($cid == $prev_cid) continue;
			$parent = isset($categories[$cat['parent_id']]) ? $categories[$cat['parent_id']] : ['parent_id'=>0];
			if($parent['parent_id'] && $cat['product_count'] >= 10) {
				$categories_filtered[] = $cid;
			}
		}
		$categories_filtered = implode(',', $categories_filtered);
		db::query('select *, (select name from categories where id = c.parent_id) as parent_name, (select name from categories where id = (select parent_id from categories where id = c.parent_id)) as grandparent_name from categories c where id in('.$categories_filtered.') and parent_id in(select id from categories where parent_id > 0) and (select count(distinct product_id) >= 10 from product_category where category_id = c.id) order by rand() limit 1');
		self::$category = db::fetchArray();
		// save category id to skip it later
		return self::$category;
	}
	
	static function get_product_id($category_id) {
		db::query('select product_id from product_category pc where category_id = ? and (select stock > 1 from product where id = pc.product_id) order by (select max(summ) from product_sum where pid = pc.product_id) desc limit 1', $category_id);
		if(!$pid = db::fetchSingle()) {
			db::query('select id from product where stock > 1 and id in(select product_id from product_category where category_id = ?) order by utime desc limit 1', $category_id);
			$pid = db::fetchSingle();
		}
		return $pid;
	}
	
	static function discount_apply($category_id, $discount_pc) {
		$koeff = (100 - $discount_pc) / 100;
		$n = 0;
		// update products
		db::query('update product p set price_old = price, price = ceiling(price * ?) where id in(select product_id from product_category where category_id = ?) and id != 540 and not exists(select 1 from product_option where product_id = p.id)', $koeff, $category_id);
		$n += db::count();
		// update options
		db::query('update product_option set price_old = price, price = ceiling(price * ?) where product_id in(select product_id from product_category where category_id = ?)', $koeff, $category_id);
		$n += db::count();
		return $n;
	}
	
	static function instock_discount_apply($discount_pc) {
		$koeff = (100 - $discount_pc) / 100;
		$autodiscount_check = config::get('autodiscount_check');
		$category_id = $autodiscount_check ? self::get_meta()['category_id'] : 0;
		// update products
		db::query('update product p set price_old = price, price = ceiling(price * ?) where quantity > 0 and id != 540 and id not in(select distinct product_id from product_category where category_id = ?) and not exists(select 1 from product_option where product_id = p.id)', $koeff, $category_id);
		// update options
		db::query('update product_option set price_old = price, price = ceiling(price * ?) where quantity > 0 and product_id not in(select distinct product_id from product_category where category_id = ?)', $koeff, $category_id);
	}
	
	static function discount_reset($category_id) {
		// update products
		db::query('update product p set price = price_old, price_old = NULL where price_old > 0 and id in(select product_id from product_category where category_id = ?) and not exists(select 1 from product_option where product_id = p.id)', $category_id);
		// update options
		db::query('update product_option set price = price_old, price_old = NULL where price_old > 0 and product_id in(select product_id from product_category where category_id = ?)', $category_id);
	}
	
	static function instock_discount_reset() {
		$autodiscount_check = config::get('autodiscount_check');
		$category_id = $autodiscount_check ? self::get_meta()['category_id'] : 0;
		// update products
		db::query('update product p set price = price_old, price_old = NULL where price_old > 0 and quantity > 0 and id not in(select distinct product_id from product_category where category_id = ?) and not exists(select 1 from product_option where product_id = p.id)', $category_id);
		// update options
		db::query('update product_option set price = price_old, price_old = NULL where price_old > 0 and quantity > 0 and product_id not in(select distinct product_id from product_category where category_id = ?)', $category_id);
	}
	
	// запускать каждый час, только после автоскидки
	static function discount_reset_not_instock() {
		$category_id = self::get_meta()['category_id'];
		// update products
		db::query('update product p set price = price_old, price_old = NULL where price_old > 0 and quantity = 0 and id not in(select distinct product_id from product_category where category_id = ?) and not exists(select 1 from product_option where product_id = p.id)', $category_id);
		// update options
		db::query('update product_option set price = price_old, price_old = NULL where price_old > 0 and quantity = 0 and product_id not in(select distinct product_id from product_category where category_id = ?)', $category_id);
	}
	
	static function discount_reset_all() {
		db::query('update product set price = price_old, price_old = NULL where price_old > 0');
		db::query('update product_option set price = price_old, price_old = NULL where price_old > 0');
	}
	
}

?>