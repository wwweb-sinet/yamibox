<?php

class Cache {
	
	private static $connection = NULL;
	
	static function init() {
		if(!cache::$connection instanceof Memcache) {
			$er = error_reporting();
			error_reporting(0);
			$config = x::config('MEMCACHE');
			cache::$connection = new Memcache;
			if(!cache::$connection->connect($config['host'], $config['port']))
				cache::$connection = NULL;
			error_reporting($er);
		}
	}
	
	static function get($key) {
		if(!cache::$connection instanceof Memcache) return false;
		return cache::$connection->get($key);
	}
	
	static function set($key, $value, $lifetime = 3600) {
		if(!cache::$connection instanceof Memcache) return false;
		return cache::$connection->set($key, $value, 0, $lifetime);
	}
	
	static function clear($key) {
		if(!cache::$connection instanceof Memcache) return false;
		if(is_array($key)) {
			foreach($key as $k) cache::clear($k);
		}
		return cache::$connection->delete($key);
	}
	
	static function flush() {
		if(!cache::$connection instanceof Memcache) return false;
		return cache::$connection->flush();
	}
	
	static function stats() {
		if(!cache::$connection instanceof Memcache) return false;
		return cache::$connection->getStats();
	}
	
}

cache::init();

?>