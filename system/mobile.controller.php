<?php

// значение по умолчанию для первого визита
if(!isset($_SESSION['referer'])) $_SESSION['referer'] = 'http://'.$_SERVER['HTTP_HOST'];

// --- ROUTES ---

x::route('/', 'mobile.intro.php');
x::route('/admin', function() { return redirect('/admin/orders'); });
x::route('/admin/orders/?(\d+)?', 'ma::orders');

// --- RUN IT! ---

tpl::set('desktop-version-link', str_replace('m.yamibox', 'yamibox', x::config('SITE_URL')).$_SERVER['REQUEST_URI']);

if(substr($_SERVER['REQUEST_URI'],0,6) == '/admin') {
	x::run(function() {
		echo tpl::make('mobile-admin-layout');
	});
}
else {
	x::run('m::layout');
}

// откуда пришел клиент (для следующего запроса)
$_SESSION['referer'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

if(x::config('DEBUG')) {
	test::globals();
}

?>