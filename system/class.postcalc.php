<?php

class postcalc {
	
	static $api_url = 'http://api.postcalc.ru/';
	
	static function request($request) {
		$request += [
			'f' => 'Москва',
			'w' => 1500,
			'o' => 'json',
			'cs' => 'UTF-8',
			'st' => 'yamibox.ru',
			'ml' => 'info@yamibox.ru',
			'r' => 1
		];
		$response = web::http_request(self::$api_url, 'GET', $request);
		if(substr($response,0,3) == "\x1f\x8b\x08" ) {
			$response = gzinflate(substr($response,10,-8));
		}
		return $response;
	}
	
	static function update_orders() {
		db::query('select * from orders where tracking_id > 0 and dtype = 3 and status not in (4,5,6,7,10)');
		$orders = db::fetchAll();
		$delivered = $pickpoint = [];
		$os = [
			'Прибыло в место вручения' => 8,
			'Вручение адресату' => 4,
			'Вручение отправителю' => 10
		];
		$num = 0;
		foreach($orders as $order) {
			$delivery = web::rp_delivery_info($order['tracking_id']);
			$delivery = json::decode($delivery);
			$status0 = @$delivery['status0'];
			$vozvrat = false;
			if(@$delivery['log_json']) {
				$log = json::decode($delivery['log_json']);
				foreach($log as $state) {
					if($state['status1'] == 'Возврат') {
						$vozvrat = true;
						break;
					}
				}
			}
			if(!$status0) continue;
			if(!isset($os[$status0])) continue;
			$status = $os[$status0];
			if($vozvrat && $status == 8) {
				$status = 9;
			}
			$dtime = in_array($status, [4,10]) ? date('Y-m-d H:i:s', strtotime($delivery['date'])) : NULL;
			db::query('update orders set status = ?, dtime = ? where id = ?', $status, $dtime, $order['id']);
			if(!db::count()) continue;
			$num++;
			if($status == 4) $delivered[] = $order['id'];
			if($status == 8) $pickpoint[] = $order['id'];
		}
		if(!x::config('DEBUG')) {
			// email-уведомление о полученном заказе
			if($delivered) {
				delivered_email($delivered);
				order::status_sms($delivered, 'order-delivered');
				activate_coupons($delivered);
			}
			// email-уведомление о доставке в ПВЗ
			if($pickpoint) {
				pickpoint_email($pickpoint);
				order::status_sms($pickpoint, 'pickup-pr');
			}
		}
		if($num) log_write('postcalc Обновлено заказов: '.$num);
		else log_write('postcalc Доставленных заказов нет');
		tpl::append('main', '<pre>'.ob_get_clean().'</pre>');
		return ['postcalc::update_orders' => 'Доставлено заказов: '.$num];
	}
	
}

?>