<?php

class yandex {
	
	static $api_url = 'https://webmaster.yandex.ru/api/v2/hosts/19707861';
	static $client_id = '3551992e20e0454d9e93f4cb1e05190d';
	static $client_secret = '4888071389d14ad094795b07b40d582b';
	static $code = '7066522';
	static $token = '9c472d7bacc94743b875e8607eaded03';
	
	static function get_code() {
		// later
	}
	
	static function get_token() {
		$data = array(
			'grant_type'=> 'authorization_code',
			'code'=> self::$code,
			'client_id'=> self::$client_id,
			'client_secret'=> self::$client_secret
		);
		$options = array('Content-type: application/x-www-form-urlencoded');
		$response = web::http_request('https://oauth.yandex.ru/token', 'POST', $data, $options);
		$response = json::decode($response);
		return @$response['access_token'] ?: false;
	}
	
	static function post_text($text) {
		$text = preg_replace('#<.+?>#msi', '', $text);
		$text = preg_replace('#&\wdash;#si', '-', $text);
		$text = preg_replace('#&[a-z]+?;#si', ' ', $text);
		if(strlen($text) < 500) return false;
		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<original-text>
		  <content>'.$text.'</content>
		</original-text>';
		$xml = urlencode($xml);
		$url = self::$api_url.'/original-texts/';
		$options = [
			CURLOPT_HTTPHEADER => ['Authorization: OAuth '.self::$token]
		];
		$response = web::http_request($url, 'POST', $xml, $options);
		return preg_match('#<original\-text><id>(\w+)</id>#', $response, $match) ? $match[1] : false;
	}
	
}

?>