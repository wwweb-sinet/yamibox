<?php

class checkout {
	
	const API_URL = 'http://platform.checkout.ru';
	const API_KEY = 'uf77fpoQXIZJzF2f75W4U1LD';
	
	static $status = [
		'CREATED' => 0,
		'CANCELLED_BEFORE_SHIPMENT' => 0,
		'FORMED' => 3,
		'IN_SENDING' => 3,
		'DELIVERED' => 4,
		'PARTIALLY_DELIVERED' => 0,
		'CANCELLED_AT_DELIVERY' => 9,
		'LOSS_DAMAGE' => 9,
		'CONFIRMED' => 10
	];
	
	static function login() {
		$response = web::http_request(self::API_URL.'/service/login/ticket/'.self::API_KEY);
		$response = json::decode($response);
		return $response['ticket'];
	}
	
	static function get_order_status($order_id) {
		$url = self::API_URL.'/service/order/platformstatushistory/'.$order_id;
		$response = web::http_request($url, 'GET', ['apiKey'=>self::API_KEY]);
		$order = json::decode($response);
		return $order;
	}
	
	static function get_orders($id_list) {
		$orders = [];
		foreach($id_list as $order_id) {
			if(!$order = self::get_order_status($order_id)) continue;
			$data = end($order['platformStatusHistory']);
			$ddate = DateTime::createFromFormat('d.m.Y H:i:s', $data['date']);
			$dtime = $ddate->format('Y-m-d H:i:s');
			$orders[] = [
				'id' => $order_id,
				'status' => self::$status[$data['status']],
				'dtime' => $dtime
			];
		}
		return $orders;
	}
	
	static function update_orders() {
		ob_start();
		$id_list = [];
		db::query('select id from orders where id >= 3999 order by id desc');
		while($order_id = db::fetchSingle()) {
			$id_list[] = $order_id;
		}
		$orders = checkout::get_orders($id_list);
		db::prepare('update orders set status = :status, dtime = :dtime where id = :id');
		$num = 0;
		$delivered = $intransit = $pickpoint = [];
		foreach($orders as $order) {
			if($order['status'] == 0) continue;
			if($order['status'] != 4) $order['dtime'] = NULL;
			db::set($order);
			db::execute();
			if(!db::count()) continue;
			// activate coupon for next order
			if($status == 4) {
				$delivered[] = $order['id'];
				$num++;
			}
			if($status == 3) {
				$intransit[] = $order['id'];
			}
			if($status == 8) {
				$pickpoint[] = $order['id'];
			}
		}
		if(!x::config('DEBUG')) {
			// email-уведомление о полученном заказе
			if($delivered) {
				delivered_email($delivered);
				delivered_sms($delivered);
				activate_coupons($delivered);
			}
			// email-уведомление о передаче в СД
			if($intransit) {
				intransit_email($intransit);
			}
			// email-уведомление о доставке в ПВЗ
			if($pickpoint) {
				pickpoint_email($pickpoint);
			}
		}
		if($num) log_write('Checkout: Обновлено заказов '.$num);
		else log_write('Checkout: Доставленных заказов нет');
		tpl::append('main', '<pre>'.ob_get_clean().'</pre>');
		return ['checkout::update_orders' => 'Доставлено заказов: '.$num];
	}
	
}

?>