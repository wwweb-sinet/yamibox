<?php

class cashbox extends admin {
	
	const DEBIT1 = 1;
	const DEBIT2 = 2;
	const CREDIT = 3;
	
	const DEBIT3 = 4;
	
	static function action($path, $action = 'view') {
		if($action && is_callable('cashbox::'.$action)) {
			return call_user_func('cashbox::'.$action);
		}
		else {
			die('NO SUCH METHOD');
		}
	}
	
	static function view() {
		make_title('Касса');
		// fetch data and make view
		$date_start = GET('date_start') ?: NULL;
		$date_end = GET('date_end') ?: NULL;
		if(!$date_start) $date_start = date('Y-m-d', strtotime('now - 6 days'));
		tpl::set('date_start', $date_start);
		if($date_end) {
			tpl::set('date_end', $date_end);
		}
		else {
			$date_end = date('Y-m-d');
		}
		if($data = self::get_data($date_start, $date_end)) {
			$totals = self::get_totals();
			// print_r($data);
			$inputs = [
				1 => ['width'=>75,'placeholder'=>'№ заказа'],
				2 => ['width'=>75,'placeholder'=>'№ заказа'],
				4 => ['width'=>150,'placeholder'=>'описание'],
				3 => ['width'=>150,'placeholder'=>'описание']
			];
			$today = date('Y-m-d');
			foreach($data as $date => $records) {
				if(!$date) $date = $today;
				tpl::set('date', $date);
				$sum_credit = $sum_debit1 = $sum_debit2 = $sum_debit3 = 0;
				foreach($records as $record) {
					tpl::push($record);
					switch($record['type']) {
						case self::DEBIT1:
							$sum_debit1 += $record['cash'];
							tpl::set('note', sprintf('<a href="/admin/orders/%1$s" target="_blank">%1$s</a>', $record['note']));
							break;
						case self::DEBIT2:
							$sum_debit2 += $record['cash'];
							tpl::set('note', sprintf('<a href="/admin/orders/%1$s" target="_blank">%1$s</a>', $record['note']));
							break;
						case self::DEBIT3:
							$sum_debit3 += $record['cash'];
							// tpl::set('note', sprintf('<a href="/admin/orders/%1$s" target="_blank">%1$s</a>', $record['note']));
							break;
						case self::CREDIT:
							$sum_credit += $record['cash'];
					}
					tpl::make('cashbox-record', 'cashbox-record-'.$record['type']);
				}
				$sum_last_day = @$totals[$date]['leftover'];
				$sum_day = $sum_last_day + $sum_debit1 + $sum_debit2 + $sum_debit3 - $sum_credit;
				tpl::set('sum-debit', $sum_debit1 + $sum_debit2 + $sum_debit3);
				tpl::set('sum-debit-1', $sum_debit1);
				tpl::set('sum-debit-2', $sum_debit2);
				tpl::set('sum-debit-3', $sum_debit3);
				tpl::set('sum-credit', $sum_credit);
				tpl::set('sum-day', $sum_day);
				tpl::set('sum-last-day', $sum_last_day);
				if($date == $today) {
					foreach($inputs as $type => $data) {
						tpl::set('type', $type);
						tpl::push($data);
						tpl::make('cashbox-record-input', 'cashbox-record-'.$type);
					}
				}
				tpl::make('cashbox-day');
				tpl::clear('cashbox-record-1', 'cashbox-record-2', 'cashbox-record-3', 'cashbox-record-4');
			}
		}
		tpl::make('cashbox', 'main');
	}
	
	static function get_data($date_start, $date_end) {
		$sql = 'select *, date(ctime) as cdate from cashbox where date(ctime) between ? and ? order by cdate desc, id asc';
		db::query($sql, [$date_start, $date_end]);
		$data = db::fetchAll();
		$data = array_group($data, 'cdate');
		$today = date('Y-m-d');
		if($date_end == $today && !isset($data[$today])) {
			array_splice($data, 0, 0, [$today=>[]]);
		}
		// print_r($data);
		return $data;
	}
	
	static function input() {
		$data = [
			'cash' => intval(REQUEST('cash')),
			'type' => intval(REQUEST('type')),
			'note' => trim(REQUEST('note')),
			'debit1' => 0,
			'debit2' => 0,
			'debit3' => 0,
			'credit' => 0
		];
		if(!in_array($data['type'], [1,2,3,4])) die('Неправильный тип операции');
		if($data['type'] == 3) {
			$data['credit'] = $data['cash'];
		}
		else {
			$key = 'debit'.($data['type'] == 4 ? 3 : $data['type']);
			$data[$key] = $data['cash'];
		}
		// все поля обязательны!
		if(!$data['cash'] || !$data['type'] || !$data['note']) {
			die('Все поля обязательны!');
		}
		if(in_array($data['type'], [1,2])) {
			db::query('select count(*) from cashbox where note = ?', $data['note']);
			if(db::fetchSingle() > 0) die('Нельзя! Заказ с таким номером уже записан!');
		}
		db::prepare('insert into cashbox set cash = :cash, type = :type, note = :note, debit1 = :debit1, debit2 = :debit2, debit3 = :debit3, credit = :credit');
		db::set($data);
		db::execute();
		die(db::lastInsertId() ? 'ok' : db::error());
	}
	
	static function order_sum() {
		if(!$order_id = absint(REQUEST('order_id'))) die('');
		db::query('select (total_sum - bonus - coupon - cumulative + delivery_price) as order_sum from orders where id = ?', $order_id);
		if(!db::count()) die('no');
		$sum = db::fetchSingle();
		die($sum);
	}
	
	static function get_totals() {
		db::query('select date(ctime) as cdate, sum(debit1) as debit1, sum(debit2) as debit2, sum(debit3) as debit3, sum(credit) as credit, (sum(debit1) + sum(debit2) + sum(debit3) - sum(credit)) as diff from cashbox group by date(ctime) order by cdate desc');
		while($row = db::fetchArray()) {
			$totals[$row['cdate']] = $row;
		}
		ksort($totals);
		$leftover = 0;
		foreach($totals as $date => $total) {
			$totals[$date]['leftover'] = $leftover;
			$leftover += $total['diff'];
		}
		$today = date('Y-m-d');
		if(!isset($totals[$today])) {
			$totals[$today] = ['cdate'=>$today,'debit1'=>0,'debit2'=>0,'debit3'=>0,'credit'=>0,'diff'=>0];
			ksort($totals);
		}
		$totals = array_reverse($totals);
		// print_r($totals);
		return $totals;
	}
	
}

tpl::load('cashbox');

?>