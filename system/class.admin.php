<?php

class Admin {
	
	static $employee_allowed_status = [0,2,5]; // статусы заказов, которые может переключать менеджер
	
	static function checkAuth() {
		$admin = (account::$auth && (account::$admin || account::$employee || account::$pnl));
		if(!$admin) return false;
		if(account::$employee) {
			// ограничение доступа для сторонних менеджеров (из ШЛ)
			if(!preg_match('#^/admin/(orders|supplier_products|quittance|attachOrder|order-delivery)#', qs::$params['path'])) return false;
		}
		if(account::$pnl) {
			// ограничение доступа аккаунта PnL
			if(!preg_match('#^/admin/pnl#', qs::$params['path'])) return false;
		}
		return true;
	}
	
	static function products($path, $id = NULL) {
		if($id) return self::product($id);
		if(REQUEST_METHOD == 'POST')
			return self::updateProducts();
		
		make_title('Товары');
		
		tpl::set('table-view', SESSION('table-view') ?: 1);
		tpl::set('instock_discount_pc', config::get('instock_discount_pc'));
		tpl::set('instock-discount-checked', config::get('instock_discount_check') ? ' checked' : '');
		tpl::set('autodiscount-checked', config::get('autodiscount_check') ? ' checked' : '');
		
		tpl::set('products-print-href', qs::set('print', 'true'));
		qs::reset();
		tpl::set('products-copypaste-href', qs::set('print', 'copypaste'));
		qs::reset();
		tpl::set('products-export-href', qs::set('export', 'csv'));
		qs::reset();
		tpl::set('products-invent-href', qs::set('export', 'invent'));
		qs::reset();
		
		$PRINT = GET('print');
		$EXPORT = GET('export');
		
		if($EXPORT == 'csv2') return self::products_export_filters();
		if($EXPORT == 'csv-1c') return self::products_export_1c();
		if($EXPORT == 'csv-invent') return self::products_export_invent();
		
		$status = array(-1 => 'DEL', 0 => 'OFF', 1 => 'ON');
		
		$limits = [40, 80, 100, 150, 200, 10000];
		$limit_selected = absint(GET('limit'));
		$params['limit'] = $limit_selected ?: 40;
		$params['offset'] = offset($params['limit']);
		
		$quantity_min = absint(GET('quantity_min'));
		$quantity_max = absint(GET('quantity_max'));
		$quantity_max_isset = isset($_GET['quantity_max']) && $_GET['quantity_max'] !== '';
		$reserve_min = absint(GET('reserve_min'));
		$reserve_max = absint(GET('reserve_max'));
		$reserve_max_isset = isset($_GET['reserve_max']) && $_GET['reserve_max'] !== '';
		$stock_min = absint(GET('stock_min'));
		$stock_max = absint(GET('stock_max'));
		$stock_max_isset = isset($_GET['stock_max']) && $_GET['stock_max'] !== '';
		$price_min = absint(GET('price_min'));
		$price_max = absint(GET('price_max'));
		$price_max_isset = isset($_GET['price_max']) && $_GET['price_max'] !== '';
		$price_buyin_min = absint(GET('price_buyin_min'));
		$price_buyin_max = absint(GET('price_buyin_max'));
		$price_buyin_max_isset = isset($_GET['price_buyin_max']) && $_GET['price_buyin_max'] !== '';
		$reviews_min = absint(GET('reviews_min'));
		$reviews_max = absint(GET('reviews_max'));
		$reviews_max_isset = isset($_GET['reviews_max']) && $_GET['reviews_max'] !== '';
		
		$update_time = SESSION('update_time') ?: 0;
		if(($quantity_min || $quantity_max || $reserve_min || $reserve_max || $stock_min || $stock_max) && time() -$update_time > 3000) {
			product::update_quantity();
			$_SESSION['update_time'] = time();
		}
		
		$orderBy = 'id desc';
		$select_sql = CATALOG_SELECT.', (select count(*) from product_option where product_id = p.id) as vcount, (select sum(quantity) from product_option where product_id = p.id) as options_quantity, (select ifnull(max(quantity), \'?\') from product_import where barcode = p.barcode) as supplier_quantity, (select group_concat(filter_id) from product_filters where product_id = p.id and option_id = 0) as filters';
		$sql = 'select count(*) from product p where true';
		if($status_filter = GET('status')) {
			$s = array_search($status_filter, $status);
			$sql .= ' and status = '.$s;
		}
		else {
			$sql .= ' and status >= 0';
		}
		if($quantity_max_isset) {
			if($quantity_min) {
				$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and quantity between :quantity_min and :quantity_max), quantity between :quantity_min and :quantity_max)';
				$params['quantity_min'] = $quantity_min;
				$params['quantity_max'] = $quantity_max;
			}
			else {
				$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and quantity <= :quantity_max), quantity <= :quantity_max)';
				$params['quantity_max'] = $quantity_max;
			}
		}
		elseif($quantity_min) {
			$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and quantity >= :quantity_min), quantity >= :quantity_min)';
			$params['quantity_min'] = $quantity_min;
		}
		if($stock_max_isset) {
			if($stock_min) {
				$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and stock_quantity between :stock_min and :stock_max), stock_quantity between :stock_min and :stock_max)';
				$params['stock_min'] = $stock_min;
				$params['stock_max'] = $stock_max;
			}
			else {
				$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and stock_quantity <= :stock_max), stock_quantity <= :stock_max)';
				$params['stock_max'] = $stock_max;
			}
		}
		elseif($stock_min) {
			$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and stock_quantity >= :stock_min), stock_quantity >= :stock_min)';
			$params['stock_min'] = $stock_min;
		}
		if($price_max_isset) {
			if($price_min) {
				$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and price between :price_min and :price_max), price between :price_min and :price_max)';
				$params['price_min'] = $price_min;
				$params['price_max'] = $price_max;
			}
			else {
				$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and price <= :price_max), price <= :price_max)';
				$params['price_max'] = $price_max;
			}
		}
		elseif($price_min) {
			$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and price >= :price_min), price >= :price_min)';
			$params['price_min'] = $price_min;
		}
		if($price_buyin_max_isset) {
			if($price_buyin_min) {
				$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and price_buyin between :price_buyin_min and :price_buyin_max), price_buyin between :price_buyin_min and :price_buyin_max)';
				$params['price_buyin_min'] = $price_buyin_min;
				$params['price_buyin_max'] = $price_buyin_max;
			}
			else {
				$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and price_buyin <= :price_buyin_max), price_buyin <= :price_buyin_max)';
				$params['price_buyin_max'] = $price_buyin_max;
			}
		}
		elseif($price_buyin_min) {
			$sql .= ' and if(barcode like \'000%\', exists(select 1 from product_option where product_id = p.id and price_buyin >= :price_buyin_min), price_buyin >= :price_buyin_min)';
			$params['price_buyin_min'] = $price_buyin_min;
		}
		if($reviews_max_isset) {
			if($reviews_min) {
				$sql .= ' and ((select count(id) from product_review where product_id = p.id) between :reviews_min and :reviews_max)';
				$params['reviews_min'] = $reviews_min;
				$params['reviews_max'] = $reviews_max;
			}
			else {
				$sql .= ' and (select count(id) from product_review where product_id = p.id) <= :reviews_max';
				$params['reviews_max'] = $reviews_max;
			}
		}
		elseif($reviews_min) {
			$sql .= ' and (select count(id) from product_review where product_id = p.id) >= :reviews_min';
			$params['reviews_min'] = $reviews_min;
		}
		tpl::push($params);
		$cid = absint(GET('cid'));
		tpl::set('cid', $cid);
		if($cid) {
			$cat_ids = shop::concat_categories($cid);
			$sql .= ' and id in (select product_id from product_category where category_id = '.$cid.')';
		}
		$search = trim(GET('search'));
		tpl::set('search', html2chars($search));
		if($search) {
			tpl::set('search-str', html2chars($search));
			$str = db::escape($search);
			$like = db::escape('%'.$search.'%');
			if(preg_match('#\d{13,14}#', $search, $match)) {
				$sql .= sprintf(' and (barcode = \'%1$s\' or id in(select product_id from product_option where barcode = \'%1$s\'))', $match[0]);
			}
			elseif(preg_match('#^(\d{1,4})?\-?(\d+)?$#', $search, $match)) {
				$product_id = $match[1];
				$sql .= ' and id = '.$product_id;
			}
			else $sql .= sprintf(' and ((name like %1$s or short_text like %1$s) or(artikul like %1$s) or id in(select product_id from product_option where artikul like %1$s))', $like);
		}
		if($notext = GET('notext')) {
			$sql .= " and (text is null or length(text) < 10 or text like :text)";
			$params['text'] = '%описание товара%';
		}
		if($zerobar = GET('zerobar')) {
			$sql .= ' and (stock_status = 0 or exists(select 1 from product_option where status = 1 and product_id = p.id and stock_status = 0))';
		}
		if($hastext = GET('hastext')) {
			$sql .= ' and text not like \'%Описание товара%\' and text is not null and text != \'\'';
		}
		if($image = GET('image')) {
			product::updateImage();
			$sql .= ' and (image = 1 or exists(select 1 from product_option where product_id = p.id and image = 1))';
		}
		if($noimage = GET('noimage')) {
			product::updateImage();
			$sql .= ' and (image = 0 or exists(select 1 from product_option where product_id = p.id and image = 0))';
		}
		if($reserve = GET('reserve')) {
			$sql .= ' and (reserve > 0 or exists(select 1 from product_option where status = 1 and product_id = p.id and reserve > 0))';
		}
		if($discount = GET('discount')) {
			$sql .= ' and (price_old > 0 or exists(select 1 from product_option where product_id = p.id and price_old > 0))';
		}
		if($noreview = GET('noreview')) {
			$sql .= ' and (select count(id) from product_review where product_id = p.id) = 0';
		}
		if($opts = GET('opts')) {
			$sql .= ' and exists(select 1 from product_option where product_id = p.id)';
		}
		if($noopts = GET('noopts')) {
			$sql .= ' and not exists(select 1 from product_option where product_id = p.id)';
		}
		tpl::set('notext-checked', $notext ? ' checked' : '');
		tpl::set('zerobar-checked', $zerobar ? ' checked' : '');
		tpl::set('hastext-checked', $hastext ? ' checked' : '');
		tpl::set('noimage-checked', $noimage ? ' checked' : '');
		tpl::set('image-checked', $image ? ' checked' : '');
		tpl::set('reserve-checked', $reserve ? ' checked' : '');
		tpl::set('discount-checked', $discount ? ' checked' : '');
		tpl::set('noreview-checked', $noreview ? ' checked' : '');
		tpl::set('opts-checked', $opts ? ' checked' : '');
		tpl::set('noopts-checked', $noopts ? ' checked' : '');
		$brands = shop::get_brands();
		$brand_filter = trim(GET('brand'));
		if($brand_filter) {
			$brand_name = NULL;
			foreach($brands as $brand) {
				if($brand['url'] == $brand_filter) {
					$brand_name = $brand['name'];
					break;
				}
			}
			if($brand_name) {
				$sql .= ' and manufacturer = '. db::escape($brand_name);
			}
		}
		$params_for_count = $params;
		unset($params_for_count['limit'], $params_for_count['offset']);
		db::query($sql, $params_for_count);
		$total_products = db::fetchSingle();
		if($PRINT || $EXPORT) {
			$orderBy = 'name asc';
		}
		$sql = str_replace('count(*)', $select_sql, $sql).' order by '.$orderBy.' limit :offset, :limit';
		if($PRINT || $EXPORT) {
			$params['offset'] = 0;
			$params['limit'] = 10000;
		}
		db::query($sql, $params);
		$products = db::fetchAll();
		
		if($PRINT == 'true') return self::products_print($products);
		if($PRINT == 'copypaste') return self::products_copypaste($products);
		if($EXPORT == 'csv') return self::products_export($products);
		if($EXPORT == 'invent') return self::products_export_invent($products);
		
		foreach($brands as $brand) {
			tpl::set('option-value', $brand['url']);
			tpl::set('option-text', $brand['name']);
			tpl::set('option-selected', $brand['url'] == $brand_filter ? ' selected' : '');
			tpl::make('option', 'admin-products-brand-filter');
		}
		
		foreach($status as $value => $text) {
			tpl::set('option-value', $text);
			tpl::set('option-text', $text);
			tpl::set('option-selected', $text === $status_filter ? ' selected' : '');
			tpl::make('option', 'admin-products-status-filter');
		}
		
		foreach($limits as $limit) {
			tpl::set('option-value', $limit);
			tpl::set('option-text', $limit);
			tpl::set('option-selected', $limit_selected === $limit ? ' selected' : '');
			tpl::make('option', 'admin-products-limit');
		}
		
		$brand_stock = [];
		foreach($brands as $brand) {
			$brand_stock[$brand['name']] = $brand['stock'];
		}
		
		$filters = json::get('filters');
		$age_filters = [21 => 'до 25', 22 => 'пос 25', 23 => 'пос 35', 24 => 'пос 45'];
		
		foreach($products as $product) {
			if(!$product['quantity']) $product['quantity'] = 0;
			tpl::push($product);
			$image = product::getImage($product);
			tpl::set('product-img', $image);
			if($product['vcount'] > 1) {
				tpl::set('quantity', $product['options_quantity'] ?: 0);
				tpl::set('supplier_quantity', '');
				tpl::set('admin-product-size', '****');
				tpl::make('admin-product-vlink');
				tpl::make('quantity-text');
				tpl::make('admin-product-prices-empty');
			}
			else {
				if(!is_numeric($product['stock_quantity'])) tpl::set('stock_quantity', '?');
				tpl::make('quantity-input');
				tpl::make('reserve-input');
				tpl::set('stock-status-checked', $product['stock_status'] ? ' checked' : '');
				tpl::make('admin-product-stock-status');
				tpl::set('barcode', $product['barcode']);
				tpl::make('admin-product-barcode');
				tpl::make('admin-product-size');
				tpl::make('admin-product-prices');
				$stock_id = @$brand_stock[$product['manufacturer']];
				if($stock_id > 2) {
					tpl::make('admin-product-add2waybill');
				}
				$product_filters = $product['filters'] ? explode(',', $product['filters']) : [];
				tpl::set('checkbox-name', 'filter');
				tpl::set('checkbox-class', 'product-filter');
				tpl::set('checkbox-disabled', ' data-pid="'.$product['id'].'" data-oid="0"');
				foreach($filters as $filter => $text) {
					if($filter > 15 && ($filter < 61 || $filter >= 70)) continue;
					tpl::set('checkbox-checked', in_array($filter, $product_filters) ? ' checked' : '');
					$text = mb_substr($text, 0, 3, 'utf-8');
					tpl::set('checkbox-text', $text);
					tpl::set('checkbox-value', $filter);
					tpl::make('checkbox', 'admin-product-filters');
				}
				foreach($filters as $filter => $text) {
					if($filter > 24 || $filter < 21) continue;
					tpl::set('checkbox-checked', in_array($filter, $product_filters) ? ' checked' : '');
					$text = $age_filters[$filter];
					tpl::set('checkbox-text', $text);
					tpl::set('checkbox-value', $filter);
					tpl::make('checkbox', 'admin-product-filters');
				}
			}
			foreach($status as $value => $text) {
				tpl::set('status-value', $value);
				tpl::set('status-text', $text);
				tpl::set('status-selected', $product['status'] == $value ? ' selected' : '');
				tpl::make('admin-product-status');
			}
			tpl::set('verified-checked', $product['verified'] ? ' checked' : '');
			tpl::make('admin-product-categories-link');
			tpl::make('admin-product');
			tpl::clear('admin-product-vlink', 'quantity-input', 'quantity-text', 'reserve-input', 'admin-product-barcode', 'admin-product-categories-link', 'admin-product-size', 'admin-product-status', 'admin-product-rel-option', 'admin-product-prices-empty', 'admin-product-prices', 'admin-product-add2waybill', 'admin-product-filters', 'admin-product-stock-status');
		}
		$yml_offers = shop::count_yml_offers();
		tpl::set('yml-offers-count', sprintf('%d, в наличии: %d', $yml_offers[2], $yml_offers[1]));
		admin::makeCatalogMenu('left-column-content');
		tpl::make('left-column', 'main');
		shop::paginator($total_products, $params['limit']);
		tpl::make('admin-products', 'right-column-content');
		tpl::make('right-column', 'main');
	}
	
	static function updateProducts() {
		$action = REQUEST('action');
		if($action == 'change-view') {
			$_SESSION['table-view'] = absint(REQUEST('view'));
		}
		elseif($action == 'group-check') {
			if(!$idList = POST('idList')) die('Не отмечены товары для группировки!');
			// cache::set('product-group-select', $idList);
			$_SESSION['product-group-select'] = $idList;
			die('ok');
		}
		elseif($action == 'instock-discount-apply') {
			$instock_discount_check = intval(POST('instock_discount_check'));
			$instock_discount_pc = intval(POST('instock_discount_pc'));
			if($instock_discount_check) {
				if(!$instock_discount_pc || $instock_discount_pc > 100) {
					die('Процент скидки должен быть от 1 до 100');
				}
				else {
					promo::instock_discount_reset();
					promo::instock_discount_apply($instock_discount_pc);
					cache::flush();
				}
			}
			else {
				promo::instock_discount_reset();
				cache::flush();
			}
			config::set('instock_discount_check', $instock_discount_check);
			config::set('instock_discount_pc', $instock_discount_pc);
			die('ok');
		}
		elseif($action == 'autodiscount-check') {
			$autodiscount_check = absint(POST('autodiscount_check'));
			config::set('autodiscount_check', $autodiscount_check);
			die('ok');
		}
		elseif($action) {
			self::productGroupAction($action);
		}
		else {
			$barcode = $stock_status = false;
			foreach($_POST as $column => $values) {
				if($column == 'barcode') $barcode = true;
				if($column == 'stock_status') $stock_status = true;
				$sql = 'update product set '.$column.' = :value where id = :id';
				db::prepare($sql);
				foreach($values as $id => $value) {
					if(!$value && !is_numeric($value)) $value = NULL;
					db::set('id', $id);
					db::set('value', $value);
					db::execute();
				}
				if($column == 'text' && strlen($value) >= 500) {
					if($text_id = yandex::post_text($value)) {
						db::query('update product set yandex_text_id = ? where id = ?', $text_id, $id);
					}
				}
			}
			if($barcode || $stock_status) {
				shop::update_stock_from_import();
				product::update_quantity();
			}
		}
		if(AJAX) die('ok');
		return redirect(SESSION('referer'));
	}
	
	static function productGroupAction($action) {
		$idList = SESSION('product-group-select') ?: [];
		if($action == 'group-apply') {
			$idList = implode(', ', $idList);
			db::query('update product set rel = ? where id in('.$idList.')', SERVER('REQUEST_TIME'));
		}
		elseif($action == 'discount-apply') {
			if(!$discount_pc = absint(REQUEST('discount_pc'))) die('Процент скидки должен быть больше 0');
			self::group_discount_apply($idList, $discount_pc);
		}
		elseif($action == 'discount-reset') {
			self::group_discount_reset($idList);
		}
		elseif($action == 'max-bonus-apply') {
			$max_bonus = absint(REQUEST('max_bonus'));
			self::group_max_bonus_apply($idList, $max_bonus);
		}
		elseif($action == 'filters-apply') {
			$filters = REQUEST('filters') ?: [];
			if(!$filters) die('Ничего не выбрано');
			self::group_filters_apply($idList, $filters);
		}
		elseif($action == 'utime-apply') {
			self::group_utime_apply($idList);
		}
		elseif($action == 'outofp-apply') {
			self::group_outofp_apply($idList, 1);
		}
		elseif($action == 'outofp-reset') {
			self::group_outofp_apply($idList, 0);
		}
		elseif($action == 'status-apply') {
			$status = absint(REQUEST('status'));
			self::group_status_apply($idList, $status);
		}
		elseif($action == 'add-to-order') {
			$orderId = absint(REQUEST('order_id'));
			self::group_add_to_order($idList, $orderId);
		}
		else die('Действие не определено!');
	}
	
	static function group_discount_apply($idList, $discount_pc) {
		$idList = implode(', ', $idList);
		$koeff = (100 - $discount_pc) / 100;
		// update products
		db::query('update product p set price_old = price, price = ceiling(price * ?) where id in('.$idList.') and (select count(*) from product_option where product_id = p.id) = 0', $koeff);
		// update options
		db::query('update product_option set price_old = price, price = ceiling(price * ?) where product_id in('.$idList.')', $koeff);
	}
	
	static function group_discount_reset($idList) {
		$pid = $oid = [];
		foreach($idList as $id) {
			preg_match('#(\d+)?\-?(\d+)?#', $id, $match);
			$pid[] = $match[1];
			if(isset($match[2]) && $match[2] > 0) {
				$oid[] = $match[2];
			}
		}
		if($pid) {
			// products
			$pid = implode(',', $pid);
			db::query('update product p set price = price_old, price_old = NULL where price_old > 0 and id in('.$pid.')');
			db::query('update product_option set price = price_old, price_old = NULL where price_old > 0 and product_id in ('.$pid.')');
		}
		if($oid) {
			// options
			$oid = implode(',', $oid);
			db::query('update product_option po set price = price_old, price_old = NULL where price_old > 0 and id in('.$oid.')');
		}
	}
	
	static function group_filters_apply($idList, $filters) {
		$idList = implode(',', $idList);
		db::query('select id from product p where id in('.$idList.') and not exists(select 1 from product_option where product_id = p.id)');
		$products = db::fetchAll();
		db::query('select id, product_id from product_option where product_id in('.$idList.')');
		$options = db::fetchAll();
		db::prepare('insert ignore into product_filters values(:product_id, :option_id, :filter_id)');
		foreach($products as $product) {
			db::set('product_id', $product['id']);
			db::set('option_id', 0);
			foreach($filters as $filter_id) {
				db::set('filter_id', $filter_id);
				db::execute();
			}
		}
		foreach($options as $option) {
			db::set('product_id', $option['product_id']);
			db::set('option_id', $option['id']);
			foreach($filters as $filter_id) {
				db::set('filter_id', $filter_id);
				db::execute();
			}
		}
	}
	
	static function group_max_bonus_apply($idList, $max_bonus) {
		$idList = implode(',', $idList);
		db::query('update product set max_bonus = ? where id in('.$idList.')', $max_bonus);
	}
	
	static function group_utime_apply($idList) {
		$idList = implode(',', $idList);
		db::query('update product set utime = CURRENT_TIMESTAMP where id in('.$idList.')');
	}
	
	static function group_outofp_apply($idList, $val) {
		$idList = implode(',', $idList);
		db::query('update product set outofp = ? where id in('.$idList.')', $val);
	}
	
	static function group_status_apply($idList, $status) {
		$idList = implode(',', $idList);
		db::query('update product set status = ? where id in('.$idList.')', $status);
	}
	
	static function group_add_to_order($idList, $orderId) {
		if(!$idList) die('Товары не выбраны');
		if(!$order = db::querySingle('select * from orders where id = '.$orderId, true)) {
			die('ЗАКАЗ НЕ НАЙДЕН!');
		}
		cart::clear();
		foreach($idList as $item_id) {
			if(strpos($item_id, '-')) {
				list($pid, $oid) = explode('-', $item_id);
				cart::addOption($oid);
			}
			else {
				cart::addProduct($item_id);
			}
		}
		$order_items = json::decode($order['cart_items']);
		$cart_items = json::decode(cart::getOrderJSON());
		foreach($cart_items as $ck => $ci) {
			foreach($order_items as $ok => $oi) {
				// обновляем количество для существующих
				if(($oi['option_id'] && $ci['option_id'] == $oi['option_id']) || (!$oi['option_id'] && !$ci['option_id'] && $ci['id'] == $oi['id'])) {
					$order_items[$ok]['quantity'] += $ci['quantity'];
					$order['total_sum'] += $ci['quantity'] * $ci['price'];
					// исключаем добавленные товары...
					unset($cart_items[$ck]);
				}
			}
		}
		// ...и добавляем остатки как отдельные товары
		foreach($cart_items as $ci) {
			$order_items[] = $ci;
			$order['total_sum'] += $ci['quantity'] * $ci['price'];
		}
		// обновляем заказ
		$order['cart_items'] = json::encode(array_values($order_items));
		order::save($order);
		// вычитаем товары из склада
		// cart::updateStock();
		// очищаем корзину
		cart::clear();
		cart::save();
		die('ok');
	}
	
	static function updateProduct($id) {
		$sql = 'update product set ';
		$params = [];
		array_walk($_POST, 'trim');
		$barcode = false;
		foreach($_POST as $key => $val) {
			$params[] = $key.' = :'.$key;
			if($val == '') $_POST[$key] = NULL;
			if($key == 'barcode') $barcode = true;
		}
		if(!$params) die('parameters required');
		$sql .= implode(', ', $params);
		$sql .= ' where id = :id';
		db::prepare($sql);
		db::set($_POST);
		db::set('id', $id);
		db::execute();
		if($barcode) {
			shop::update_stock_from_import();
			product::update_quantity();
		}
		if(($text = POST('text')) && strlen($text) >= 500) {
			if($text_id = yandex::post_text($text)) {
				db::query('update product set yandex_text_id = ? where id = ?', $text_id, $id);
			}
		}
		if(AJAX) die('ok');
		return redirect(SESSION('referer'));
	}
	
	static function addProduct() {
		$sql = 'insert into product set ';
		array_walk($_POST, 'trim');
		foreach($_POST as $key => $val) {
			$params[] = $key.' = :'.$key;
			if($val == '') $_POST[$key] = NULL;
		}
		if(count($params) < 2) die('parameters required');
		$sql .= implode(', ', $params);
		db::prepare($sql);
		db::set($_POST);
		db::execute();
		$productId = db::lastInsertId();
		shop::update_stock_from_import(); // update stock
		if(AJAX) die('ok');
		return redirect('/admin/products/'.$productId);
	}
	
	static function products_print($products) {
		$export_xls = GET('xls');
		tpl::load('print-products');
		// собираем список ID товаров с опциями
		$option_pid = [];
		foreach($products as $product) {
			if($product['vcount'] > 1) $option_pid[] = $product['id'];
		}
		// выдираем опции из БД
		$options = [];
		if($option_pid) {
			db::query('select * from product_option where status = 1 and product_id in('.implode(',', $option_pid).') order by name');
			$options = array_group(db::fetchAll(), 'product_id');
		}
		$xls_data = [['Наименование','Опция','Штриход','Остаток','Запас','Склад','Цена']];
		foreach($products as $product) {
			tpl::set('name', $product['name']);
			tpl::set('option-name', '');
			tpl::set('barcode', str::startsWith('000', $product['barcode']) ? '' : $product['barcode']);
			tpl::set('quantity', $product['quantity']);
			tpl::set('reserve', $product['reserve']);
			tpl::set('stock-quantity', $product['stock_quantity'] ?: '--');
			tpl::set('price', $product['price']);
			if(isset($options[$product['id']])) {
				foreach($options[$product['id']] as $option) {
					tpl::set('name', $product['name']);
					tpl::set('option-name', $option['name']);
					tpl::set('barcode', $option['barcode'] ?: '--');
					tpl::set('quantity', $option['quantity']);
					tpl::set('reserve', $option['reserve']);
					tpl::set('stock-quantity', $option['stock_quantity'] ?: '--');
					tpl::set('price', $option['price']);
					tpl::make('print-product');
					$xls_data[] = [
						$product['name'], $option['name'], $option['barcode'], $option['quantity'], $option['reserve'], $option['stock_quantity'], $option['price']
					];
				}
			}
			else {
				tpl::make('print-product');
				$xls_data[] = [
					$product['name'], '', $product['barcode'], $product['quantity'], $product['reserve'], $product['stock_quantity'], $product['price']
				];
			}
		}
		if($export_xls) {
			$excel = new phpexcel();
			$excel->getProperties()->setCreator("Yamibox.ru")
				->setLastModifiedBy("PHP5")
				->setTitle($title)
				->setSubject($title);
			$excel->setActiveSheetIndex(0);
			$excel->getActiveSheet()->getStyle('C')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
			$excel->getActiveSheet()->fromArray($xls_data, NULL, 'A1');
			foreach(range('A', 'G') as $col) {
				$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			}
			$filename = FILES.'/products-invent.xls';
			$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
			$objWriter->save($filename);
			return redirect('/files/products-invent.xls');
		}
		tpl::set('products-xls-href', qs::set('xls', 'true'));
		$html = tpl::make('print-products');
		exit($html);
	}
	
	static function products_copypaste($products) {
		echo "<pre>\r\n";
		foreach($products as $product) {
			$line = [];
			$line['name'] = $product['name'];
			$line['quantity'] = $product['quantity'];
			$line['reserve'] = $product['reserve'];
			$line['supplier_quantity'] = $product['supplier_quantity'];
			$line['url'] = x::config('SITE_URL').'/product/'.$product['url_name'];
			echo implode("\t", $line)."\r\n";
		}
		echo "</pre>\r\n";
		exit;
	}
	
	static function products_import() {
		make_title('Импорт товаров');
		$brand_selected = REQUEST('brand');
		if(!empty($_FILES)) {
			ob_start();
			$action = REQUEST('action');
			if(!$file = uploadFile(reset($_FILES), FILES.'/temp/')) {
				echo "Ошибка! Не удалось загрузить файл на сервер!\r\n";
				goto VIEW;
			}
			if($action == 'import') {
				if(!$brand_selected) {
					echo "Бренд не выбран!\r\n";
					goto VIEW;
				}
				str::file_to_utf8($file);
				$result = product::import_csv($file, $brand_selected);
				printf("Обработано строк: %d\r\n", $result['add']);
				printf("Пропущено: %d\r\n", $result['skip']);
			}
			elseif($action == 'update') {
				$result = product::update_from_csv($file);
				printf("Обновлено товаров: %d\r\n", $result['products']);
				printf("Обновлено опций: %d\r\n", $result['options']);
			}
			elseif($action == 'update-filters') {
				$result = product::update_filters_from_csv($file);
				printf("Обновлено товаров: %d\r\n", $result['products']);
				printf("Обновлено опций: %d\r\n", $result['options']);
			}
			elseif($action == 'import-invent') {
				$result = product::update_invent($file);
				printf("Обновлено товаров: %d, Опций: %d\r\n", $result['products'], $result['options']);
			}
			tpl::set('response', "<pre>".ob_get_clean()."</pre>");
		}
		VIEW:
		$brands = shop::get_brands();
		foreach($brands as $brand) {
			tpl::set('option-value', $brand['name']);
			tpl::set('option-text', $brand['name']);
			tpl::set('option-selected', $brand['name'] == $brand_selected ? ' selected' : '');
			tpl::make('option', 'admin-products-import-brand');
		}
		tpl::make('admin-products-import', 'main');
	}
	
	static function products_export($products) {
		if(!$products) {
			alert("Нет товаров для выгрузки", 'error');
			return false;
		}
		$idList = [];
		$options = [];
		foreach($products as $product) {
			if($product['vcount'] > 1) $idList[] = $product['id'];
		}
		if($idList) {
			db::query('select *, (select group_concat(filter_id) from product_filters where product_id = po.product_id and option_id = po.id) as filters from product_option po where product_id in ('.implode(',', $idList).') order by option_id');
			$options = db::fetchAll();
			$options = array_group($options, 'product_id');
		}
		$file = FILES.'/temp/products-export.csv';
		$columns = ['ID','Товар','Опция','Штриход','Артикул','Опт','Розница','Старая цена'];
		$f = fopen($file, 'w+');
		fputcsv($f, $columns, "\t");
		foreach($products as $product) {
			if($po = @$options[$product['id']]) {
				foreach($po as $option) {
					$line = [
						$option['option_id'],
						$product['name'],
						$option['name'],
						$option['barcode'],
						$option['artikul'],
						$option['price_buyin'],
						$option['price'],
						$option['price_old']
					];
					fputcsv($f, $line, "\t");
				}
			}
			else {
				$line = [
					$product['id'],
					$product['name'],
					'',
					$product['barcode'],
					$product['artikul'],
					$product['price_buyin'],
					$product['price'],
					$product['price_old']
				];
				fputcsv($f, $line, "\t");
			}
		}
		fclose($f);
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="'.basename($file).'"');
		readfile($file);
		exit;
	}
	
	static function products_export_filters() {
		$idList = [];
		$options = [];
		$cat_ids = shop::concat_categories(1);
		$cat_ids .= ','. shop::concat_categories(36);
		db::query('select '.CATALOG_SELECT.', (select count(*) from product_option where product_id = p.id) as vcount, (select sum(quantity) from product_option where product_id = p.id) as options_quantity, (select ifnull(max(quantity), \'?\') from product_import where barcode = p.barcode) as supplier_quantity, (select group_concat(filter_id) from product_filters where product_id = p.id and option_id = 0) as filters from product p where status > -1 and id in(select distinct product_id from product_category where category_id in(select id from categories where use_filters = 1)) order by manufacturer, name');
		$products = db::fetchAll();
		foreach($products as $product) {
			if($product['vcount'] > 1) $idList[] = $product['id'];
		}
		if($idList) {
			db::query('select *, (select group_concat(filter_id) from product_filters where product_id = po.product_id and option_id = po.id) as filters from product_option po where product_id in ('.implode(',', $idList).') order by option_id');
			$options = db::fetchAll();
			$options = array_group($options, 'product_id');
		}
		$columns = ['id', 'краткое название', 'название', 'сух', 'нор', 'жир', 'ком', 'обе', 'чув', 'про', 'увя', '<25', '>25', '>35', '>45'];
		$file = FILES.'/temp/products-export-filters.csv';
		$f = fopen($file, 'w+');
		fputcsv($f, $columns, "\t");
		$filters = [
			'сух' => 11,
			'нор' => 12,
			'жир' => 13,
			'ком' => 15,
			'обе' => 61,
			'чув' => 62,
			'про' => 63,
			'увя' => 64,
			'<25' => 21,
			'>25' => 22,
			'>35' => 23,
			'>45' => 24
		];
		foreach($products as $product) {
			if($po = @$options[$product['id']]) {
				foreach($po as $option) {
					$option_filters = explode(',', $option['filters']);
					$line = [
						$option['option_id'],
						$product['short_text'],
						$product['name'] . ' - ' . $option['name'],
						in_array($filters['сух'], $option_filters) ? 1 : '',
						in_array($filters['нор'], $option_filters) ? 1 : '',
						in_array($filters['жир'], $option_filters) ? 1 : '',
						in_array($filters['ком'], $option_filters) ? 1 : '',
						in_array($filters['обе'], $option_filters) ? 1 : '',
						in_array($filters['чув'], $option_filters) ? 1 : '',
						in_array($filters['про'], $option_filters) ? 1 : '',
						in_array($filters['увя'], $option_filters) ? 1 : '',
						in_array($filters['<25'], $option_filters) ? 1 : '',
						in_array($filters['>25'], $option_filters) ? 1 : '',
						in_array($filters['>35'], $option_filters) ? 1 : '',
						in_array($filters['>45'], $option_filters) ? 1 : ''
					];
					fputcsv($f, $line, "\t");
				}
			}
			else {
				$product_filters = explode(',', $product['filters']);
				$line = [
					$product['id'],
					$product['short_text'],
					$product['name'],
					in_array($filters['сух'], $product_filters) ? 1 : '',
					in_array($filters['нор'], $product_filters) ? 1 : '',
					in_array($filters['жир'], $product_filters) ? 1 : '',
					in_array($filters['ком'], $product_filters) ? 1 : '',
					in_array($filters['обе'], $product_filters) ? 1 : '',
					in_array($filters['чув'], $product_filters) ? 1 : '',
					in_array($filters['про'], $product_filters) ? 1 : '',
					in_array($filters['увя'], $product_filters) ? 1 : '',
					in_array($filters['<25'], $product_filters) ? 1 : '',
					in_array($filters['>25'], $product_filters) ? 1 : '',
					in_array($filters['>35'], $product_filters) ? 1 : '',
					in_array($filters['>45'], $product_filters) ? 1 : ''
				];
				fputcsv($f, $line, "\t");
			}
		}
		fclose($f);
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="'.basename($file).'"');
		readfile($file);
		exit;
	}
	
	static function products_export_1c() {
		product::export_for_1c();
		$file = FILES.'/1c/goods.csv';
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="'.basename($file).'"');
		readfile($file);
		exit;
	}
	
	static function products_export_invent() {
		product::export_for_invent();
		$file = FILES.'/temp/moysklad-goods.csv';
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="'.basename($file).'"');
		readfile($file);
		exit;
	}
	
	static function product($id) {
		$action = GET('action');
		if($action == 'delete') {
			db::query('update product set status = -1 where id = ?', $id);
			if(AJAX) die('ok');
			return redirect(SESSION('referer'));
		}
		if($action == 'attach-video') {
			if(!$url = trim(GET('url'))) die('Пустой УРЛ');
			db::query('insert ignore into product_video values(?, ?)', $id, $url);
			die('ok');
		}
		if($action == 'delete-video') {
			if(!$videoId = trim(GET('video-id'))) die('Пустой ID');
			db::query('delete from product_video where product_id = ? and video_url like ?', $id, '%'.$videoId.'%');
			if(AJAX) die('ok');
			return redirect(SESSION('referer'));
		}
		if($action == 'autofill') {
			header('Content-Type: application/json');
			$response = self::autofill_product_name(GET('name'), GET('short_text'));
			die($response);
		}
		if(REQUEST_METHOD == 'POST') {
			if($id == 'new') self::addProduct();
			else self::updateProduct($id);
		}
		if($id == 'new') {
			$tpl_product_form = 'admin-product-add';
		}
		else {
			$id = intval($id);
			db::query('select *, (select group_concat(filter_id) from product_filters where product_id = p.id and option_id = 0) as filters from product p where id = ?', $id);
			if(!$product = db::fetchArray()) {
				return alert('Товар не найден!', 'error', 'main');
			}
			$tpl_product_form = 'admin-product-editor';
			tpl::push($product);
			tpl::set('form-product-action', '/admin/products/'.$product['id']);
			tpl::walk('text', 'html2chars');
			tpl::make('admin-product-categories-link');
			// product images
			$images = get_files(FILES.'/product/thumbs/', '#^('.$product['id'].')?(\-[0-9]+)?\.jpg$#i');
			if($images) {
				sort($images);
				foreach($images as $key => $image) {
					$filename = basename($image);
					tpl::set('product-img-href', '/files/product/'.$filename);
					tpl::set('product-img-src', '/files/product/thumbs/'.$filename);
					tpl::set('product-img-filename', $filename);
					tpl::make('admin-product-editor-img');
				}
				tpl::make('admin-product-editor-images');
			}
			$video = product::getVideo($id);
			foreach($video as $url) {
				tpl::set('video-url', $url);
				$query = parse_url($url, PHP_URL_QUERY);
				parse_str($query, $parse_url);
				tpl::set('video-id', $parse_url['v']);
				tpl::make('admin-product-video-url');
			}
			tpl::make('admin-product-video');
			// OPTIONS AND FILTERS
			if($options = product::getOptions($product['id'])) {
				tpl::set('product-options-json', json::encode($options));
				tpl::set('description-colspan', 2);
			}
			else {
				tpl::set('product-options-json', 'null');
				tpl::set('description-colspan', 3);
				$filters = json::get('filters');
				$product_filters = $product['filters'] ? explode(',', $product['filters']) : [];
				tpl::set('checkbox-name', 'filter');
				tpl::set('checkbox-class', 'product-filter');
				tpl::set('checkbox-disabled', ' data-pid="'.$product['id'].'" data-oid="0"');
				foreach($filters as $filter => $text) {
					if($filter > 15 && ($filter < 61 || $filter >= 70)) continue;
					tpl::set('checkbox-checked', in_array($filter, $product_filters) ? ' checked' : '');
					tpl::set('checkbox-text', $text);
					tpl::set('checkbox-value', $filter);
					tpl::make('checkbox', 'admin-product-filters');
					tpl::append('admin-product-filters', '<br>');
				}
				foreach($filters as $filter => $text) {
					if($filter > 24 || $filter < 21) continue;
					tpl::set('checkbox-checked', in_array($filter, $product_filters) ? ' checked' : '');
					tpl::set('checkbox-text', $text);
					tpl::set('checkbox-value', $filter);
					tpl::make('checkbox', 'admin-product-filters');
					tpl::append('admin-product-filters', '<br>');
				}
				tpl::make('product-editor-filters');
			}
			// next/previous product links
			$prev_id = db::querySingle('select id from product where status > -1 and id < '.$id.' order by id desc limit 1');
			if($prev_id) {
				tpl::set('prev-product-href', '/admin/products/'.$prev_id);
				tpl::make('prev-product-link');
			}
			$next_id = db::querySingle('select id from product where status > -1 and id > '.$id.' order by id asc limit 1');
			if($next_id) {
				tpl::set('next-product-href', '/admin/products/'.$next_id);
				tpl::make('next-product-link');
			}
			if($prev_id || $next_id) {
				tpl::make('next-prev-nav');
			}
		}
		$brands = shop::get_brands();
		foreach($brands as $brand) {
			tpl::set('option-value', $brand['name']);
			tpl::set('option-text', $brand['name']);
			tpl::set('option-selected', @$product['manufacturer'] == $brand['name'] ? ' selected' : '');
			tpl::make('option', 'option-brand');
		}
		tpl::make($tpl_product_form, 'main');
	}
	
	static function productOption($path, $id) {
		$id = intval($id);
		if(REQUEST_METHOD == 'POST') {
			self::updateProductOptions($id);
		}
		$status = [-1 => 'DEL', 0 => 'OFF', 1 => 'ON'];
		$brands = shop::get_brands();
		$filters = json::get('filters');
		$age_filters = [21 => '<25', 22 => '>25', 23 => '>35', 24 => '>45'];
		$brand_stock = [];
		foreach($brands as $brand) {
			$brand_stock[$brand['name']] = $brand['stock'];
		}
		db::query('select po.*, p.id as product_id, p.name as product_name, p.manufacturer as manufacturer, (select ifnull(max(quantity), \'?\') from product_import where barcode = po.barcode) as supplier_quantity, (select group_concat(filter_id) from product_filters where product_id = p.id and option_id = po.id) as filters from product_option po, product p where po.product_id = p.id and po.product_id = ? order by option_id', $id);
		foreach(db::fetchAll() as $option) {
			if(!$option['quantity']) $option['quantity'] = 0;
			tpl::push($option);
			if(!is_numeric($option['stock_quantity'])) tpl::set('stock_quantity', '?');
			tpl::set('stock-status-checked', $option['stock_status'] ? ' checked="checked"' : '');
			tpl::set('barcode', $option['barcode'] ?: '');
			$stock_id = $brand_stock[$option['manufacturer']];
			if($stock_id > 2) {
				tpl::make('admin-product-option-add2waybill');
			}
			$option_filters = $option['filters'] ? explode(',', $option['filters']) : [];
			tpl::set('checkbox-name', 'filter');
			tpl::set('checkbox-class', 'product-option-filter');
			tpl::set('checkbox-disabled', ' data-pid="'.$option['product_id'].'" data-oid="'.$option['id'].'"');
			foreach($filters as $filter => $text) {
				if($filter > 15 && ($filter < 61 || $filter >= 70)) continue;
				tpl::set('checkbox-checked', in_array($filter, $option_filters) ? ' checked' : '');
				$text = mb_substr($text, 0, 1, 'utf-8');
				tpl::set('checkbox-text', $text);
				tpl::set('checkbox-value', $filter);
				tpl::make('checkbox', 'admin-product-option-filters');
			}
			foreach($filters as $filter => $text) {
				if($filter > 24 || $filter < 21) continue;
				tpl::set('checkbox-checked', in_array($filter, $option_filters) ? ' checked' : '');
				$text = $age_filters[$filter];
				tpl::set('checkbox-text', $text);
				tpl::set('checkbox-value', $filter);
				tpl::make('checkbox', 'admin-product-option-filters');
			}
			foreach($status as $value => $text) {
				tpl::set('status-value', $value);
				tpl::set('status-text', $text);
				tpl::set('status-selected', $option['status'] == $value ? ' selected' : '');
				tpl::make('admin-option-status');
			}
			tpl::make('admin-product-option');
			tpl::clear('admin-product-option-add2waybill', 'admin-product-option-filters', 'admin-option-status');
		}
		// end list
		$html = tpl::make('admin-product-options', 'main');
		if(AJAX) die($html);
	}
	
	static function updateProductOptions($id) {
		if(POST('name') == 'filter') {
			$result = self::updateProductFilter(POST('pid'), POST('oid'), POST('fid'), POST('value'));
			die($result ? 'ok' : 'ignore');
		}
		$refresh = false;
		foreach($_POST as $column => $values) {
			if(in_array($column, ['barcode', 'stock_status'])) $refresh = true;
			$sql = 'update product_option set '.$column.' = :value where id = :id';
			db::prepare($sql);
			foreach($values as $id => $value) {
				if(!$value) $value = in_array($column, ['quantity','price','price_buyin', 'stock_status']) ? 0 : NULL;
				db::set('id', $id);
				db::set('value', $value);
				db::execute();
			}
		}
		if($refresh) {
			shop::update_stock_from_import();
			product::update_quantity();
		}
		die('ok');
	}
	
	static function updateProductFilter($product_id, $option_id, $filter_id, $value) {
		if($value) {
			db::query('insert ignore into product_filters values(?, ?, ?)', $product_id, $option_id, $filter_id);
		}
		else {
			db::query('delete from product_filters where product_id = ? and option_id = ? and filter_id = ?', $product_id, $option_id, $filter_id);
		}
		return db::count();
	}
	
	static function clients($path, $id = NULL) {
		if($id = intval($id)) {
			if(REQUEST_METHOD == 'POST') {
				// if(POST('action') == 'order') self::addClientOrder($id);
				// else self::updateClient($id);
				self::updateClient($id);
			}
			return self::client($id);
		}
		$cumulative = json::get('cumulative');
		// минимальная сумма, на которую предоставляется указанная скидка (%)
		$cumulative_min_sum = function($pc) use($cumulative) {
			foreach($cumulative as $cum) {
				if($cum['discount'] == $pc) return $cum['sum'];
			}
			return false;
		};
		$sort = [
			0 => 'ctime desc',
			1 => 'ctime asc',
			2 => 'email asc',
			3 => 'email desc',
			4 => 'orders_count desc',
			5 => 'orders_count asc',
			6 => 'orders_complete desc',
			7 => 'orders_complete asc',
			8 => 'orders_canceled desc',
			9 => 'orders_canceled asc',
			10 => 'orders_complete_sum desc',
			11 => 'orders_complete_sum asc',
			12 => 'bonus desc',
			13 => 'bonus asc',
			14 => 'cumulative desc',
			15 => 'cumulative asc',
			16 => 'bdate desc',
			17 => 'bdate asc',
			18 => 'ltime desc',
			19 => 'ltime asc',
			20 => 'firstname, lastname',
			21 => 'firstname desc, lastname desc',
			22 => 'orders_complete_avg desc',
			23 => 'orders_complete_avg asc',
			24 => 'status desc',
			25 => 'status asc',
			26 => 'last_order_time desc',
			27 => 'last_order_time asc'
		];
		db::query('select distinct status from user where status > 0 order by status asc');
		$status_list = [];
		while($user_status = db::fetchSingle()) {
			$status_list[] = $user_status;
		}
		$sort_select = absint(GET('sort'));
		$EXPORT = (bool)GET('export');
		$LIMIT = 40;
		$params = [];
		$sql = 'select count(id) from user u where true';
		array_walk($_GET, 'trim');
		tpl::push($_GET);
		// form input
		$search = GET('search');
		$cdate_min = GET('cdate_min');
		$cdate_max = GET('cdate_max');
		$ltime_min = intval(GET('ltime_min'));
		$ltime_max = intval(GET('ltime_max'));
		$bonus_min = intval(GET('bonus_min'));
		$bonus_max = intval(GET('bonus_max'));
		$order_sum_min = intval(GET('order_sum_min'));
		$order_sum_max = intval(GET('order_sum_max'));
		$orders_min = intval(GET('orders_min'));
		$orders_max = intval(GET('orders_max'));
		$order_avg_min = intval(GET('order_avg_min'));
		$order_avg_max = intval(GET('order_avg_max'));
		$discount_pc_min = intval(GET('discount_pc_min'));
		$discount_pc_max = intval(GET('discount_pc_max'));
		$status = intval(GET('user_status'));
		// end form input
		if($cdate_min) {
			$sql .= ' and ctime >= :cdate_min';
			$params['cdate_min'] = $cdate_min;
		}
		if($cdate_max) {
			$sql .= ' and ctime <= :cdate_max';
			$params['cdate_max'] = $cdate_max;
		}
		if($ltime_min) {
			$sql .= ' and (dayofyear(CURRENT_TIMESTAMP) - dayofyear(ltime)) >= :ltime_min';
			$params['ltime_min'] = $ltime_min;
		}
		if($ltime_max) {
			$sql .= ' and (dayofyear(CURRENT_TIMESTAMP) - dayofyear(ltime)) <= :ltime_max';
			$params['ltime_max'] = $ltime_max;
		}
		if($bonus_min) {
			$sql .= ' and bonus >= :bonus_min';
			$params['bonus_min'] = $bonus_min;
		}
		if($bonus_max) {
			$sql .= ' and bonus <= :bonus_max';
			$params['bonus_max'] = $bonus_max;
		}
		if($order_sum_min) {
			$sql .= ' and (select ifnull(sum(total_sum) + sum(delivery_price) - sum(bonus), 0) from orders where user_id = u.id and status = 4) >= :order_sum_min';
			$params['order_sum_min'] = $order_sum_min;
		}
		if($order_sum_max) {
			$sql .= ' and (select ifnull(sum(total_sum) + sum(delivery_price) - sum(bonus), 0) from orders where user_id = u.id and status = 4) <= :order_sum_max';
			$params['order_sum_max'] = $order_sum_max;
		}
		if($orders_min) {
			$sql .= ' and (select count(*) from orders where user_id = u.id and status = 4) >= :orders_min';
			$params['orders_min'] = $orders_min;
		}
		if($orders_max) {
			$sql .= ' and (select count(*) from orders where user_id = u.id and status = 4) <= :orders_max';
			$params['orders_max'] = $orders_max;
		}
		if($order_avg_min) {
			$sql .= ' and (select round(ifnull(avg(total_sum) + avg(delivery_price) - avg(bonus), 0)) from orders where user_id = u.id and status = 4) >= :order_avg_min';
			$params['order_avg_min'] = $order_avg_min;
		}
		if($order_avg_max) {
			$sql .= ' and (select round(ifnull(avg(total_sum) + avg(delivery_price) - avg(bonus), 0)) from orders where user_id = u.id and status = 4) <= :order_avg_max';
			$params['order_avg_max'] = $order_avg_max;
		}
		if($discount_pc_min) {
			if($discount_pc_min = $cumulative_min_sum($discount_pc_min)) {
				$sql .= ' and (select sum(total_sum) - sum(bonus) from orders where user_id = u.id and status = 4 and paid = 1) >= :discount_pc_min';
				$params['discount_pc_min'] = $discount_pc_min;
			}
		}
		if($discount_pc_max) {
			if($discount_pc_max = $cumulative_min_sum($discount_pc_max)) {
				$sql .= ' and (select sum(total_sum) - sum(bonus) from orders where user_id = u.id and status = 4 and paid = 1) <= :discount_pc_max';
				$params['discount_pc_max'] = $discount_pc_max;
			}
		}
		if($status) {
			$sql .= ' and status = :status';
			$params['status'] = $status;
		}
		// search
		if(preg_match_all('/[\w\@\.]+/u', $search, $match)) {
			$words = array();
			$wcount = count($match[0]);
			foreach($match[0] as $word) {
				$word = db::escape('%'.$word.'%');
				$words[] = ' email LIKE '.$word;
				$words[] = ' firstname LIKE '.$word;
				$words[] = ' lastname LIKE '.$word;
			}
			if(empty($words))
				return alert('Недопустимые параметры поиска.', 'warning');
			$cnd = implode(' or ', $words);
			$sql .= ' and ('.$cnd.')';
		}
		if(GET('filter') == 'birthday') {
			$sql .= ' and ((dayofyear(bdate) - dayofyear(current_date) between 0 and 30) or (dayofyear(bdate) + 365 - dayofyear(current_date) between 0 and 30))';
		}
		db::query($sql, $params);
		$total_clients = db::fetchSingle();
		$sql = str_replace('count(id)', '*, (select count(id) from orders where user_id = u.id) as orders_count, (select count(*) from orders where user_id = u.id and status = 4) as orders_complete, (select ifnull(sum(total_sum) + sum(delivery_price) - sum(bonus), 0) from orders where user_id = u.id and status = 4) as orders_complete_sum, (select round(ifnull(avg(total_sum) + avg(delivery_price) - avg(bonus), 0)) from orders where user_id = u.id and status = 4) as orders_complete_avg, (select count(*) from orders where user_id = u.id and status = 5) as orders_canceled, (select sum(total_sum) - sum(bonus) from orders where user_id = u.id and status = 4 and paid = 1) as cumulative', $sql);
		$sql .= ' order by '.$sort[$sort_select];
		if($EXPORT) {
			$clients_file = FILES.'/clients-export.csv';
			$f = fopen($clients_file, 'w+');
			$headers = ['id', 'Email', 'Имя', 'Телефон', 'Заказов', 'Доставлено', 'Отменено', 'Сумма доставленных', 'Средний чек', 'Бонусы', 'Накопит. скидка', 'Дата регистрации', 'Дата рождения', 'Последний визит'];
			fputcsv($f, $headers);
		}
		else {
			$params += ['offset' => offset($LIMIT), 'limit' => $LIMIT];
			$sql .= ' limit :offset, :limit';
		}
		db::query($sql, $params);
		// var_dump($sql);
		// var_dump($params);
		foreach(db::fetchAll() as $client) {
			tpl::push($client);
			if($client['ltime']) {
				$ltime = ts2date('Y-m-d H:i', $client['ltime']);
				tpl::set('ltime', $ltime);
				if($_SERVER['REQUEST_TIME'] - strtotime($client['ltime']) < 600) {
					tpl::set('ltime', '<span class="green" title="'.$ltime.'">онлайн</span>');
				}
			}
			$discount = 0;
			foreach($cumulative as $cum) {
				if($client['orders_complete_sum'] >= $cum['sum']) $discount = $cum['discount'];
			}
			tpl::set('cumulative', $discount);
			tpl::set('cdate', date('Y-m-d', strtotime($client['ctime'])));
			if($client['orders_count']) {
				tpl::make('admin-client-orders-link');
			}
			tpl::make('admin-client');
			tpl::clear('admin-client-orders-link');
			if($EXPORT) {
				$data = [
					$client['id'], $client['email'], $client['firstname'].' '.$client['lastname'], $client['phone'], $client['orders_count'], $client['orders_complete'], $client['orders_canceled'], $client['orders_complete_sum'], $client['orders_complete_avg'], $client['bonus'], $client['cumulative'], tpl::get('cdate'), $client['bdate'], $ltime
				];
				fputcsv($f, $data);
			}
		}
		if($EXPORT) {
			fclose($f);
			header('Content-Type: text/csv', true);
			header('Content-Transfer-Encoding: Binary', true);
			header('Content-Disposition: attachment; filename="'.basename($clients_file).'"');
			readfile($clients_file);
			exit;
		}
		make_title('Зарегистрированные пользователи');
		foreach($status_list as $status) {
			tpl::set('option-value', $status);
			tpl::set('option-text', $status);
			tpl::make('option', 'option-status');
		}
		$qs_args = qs::$args;
		tpl::set('cdate-sort-href', qs::set('sort', $sort_select == 0 ? 1 : 0));
		tpl::set('email-sort-href', qs::set('sort', $sort_select == 2 ? 3 : 2));
		tpl::set('orders-total-sort-href', qs::set('sort', $sort_select == 4 ? 5 : 4));
		tpl::set('orders-done-sort-href', qs::set('sort', $sort_select == 6 ? 7 : 6));
		tpl::set('orders-canceled-sort-href', qs::set('sort', $sort_select == 8 ? 9 : 8));
		tpl::set('sum-sort-href', qs::set('sort', $sort_select == 10 ? 11 : 10));
		tpl::set('bonus-sort-href', qs::set('sort', $sort_select == 12 ? 13 : 12));
		tpl::set('cumulative-sort-href', qs::set('sort', $sort_select == 14 ? 15 : 14));
		tpl::set('bdate-sort-href', qs::set('sort', $sort_select == 16 ? 17 : 16));
		tpl::set('ltime-sort-href', qs::set('sort', $sort_select == 18 ? 19 : 18));
		tpl::set('fio-sort-href', qs::set('sort', $sort_select == 20 ? 21 : 20));
		tpl::set('avg-sort-href', qs::set('sort', $sort_select == 22 ? 23 : 22));
		tpl::set('status-sort-href', qs::set('sort', $sort_select == 24 ? 25 : 24));
		tpl::set('last-order-sort-href', qs::set('sort', $sort_select == 26 ? 27 : 26));
		qs::remove('sort');
		tpl::set('clients-export-href', qs::set('export', 'true'));
		qs::$args = $qs_args;
		shop::paginator($total_clients, $LIMIT);
		tpl::make('admin-clients', 'main');
	}
	
	static function client($id) {
		$action = REQUEST('action');
		db::query('select * from user where id = ?', $id);
		if(!$client = db::fetchArray()) return alert('Аккаунт не найден.', 'error');
		if($action == 'admin_login') {
			addCookie('admin', account::$cookie);
			addCookie('cookie', $client['cookie']);
			return redirect('/account');
		}
		make_title($client['email']);
		tpl::push($client);
		if($client['address']) {
			if($address = json::decode($client['address'])) {
				tpl::push($address);
			}
		}
		if(!$client['ltime']) tpl::set('ltime', '--');
		tpl::set('password', $client['password'] ? '<span class="green">есть</span>' : '<span class="red">нет</span>');
		tpl::set('admin_comment', html2chars($client['admin_comment']));
		tpl::make('admin-client-details', 'main');
	}
	
	static function updateClient($id) {
		if(!$column = trim(POST('column'))) die('column is empty');
		$value = trim(POST('value'));
		if($column == 'bdate' && $value == '') $value = NULL;
		if(in_array($column, ['region','city','zipcode','street','building','building-add','apt'])) {
			if($address = db::querySingle('select address from user where id = '.$id)) {
				if($address = json::decode($address)) {
					$address[$column] = $value;
				}
			}
			else {
				$address = [$column => $value];
			}
			$column = 'address';
			$value = json::encode($address);
		}
		account::update(['id' => $id, $column => $value]);
		die('ok');
	}
	
	static function makeCatalogMenu($appendTo = NULL, $href_template = '/admin/products?cid=') {
		
		$categories = self::getCategories();
		$cat_by_parent = array_group($categories, 'parent_id');
		
		foreach($categories as $cat) {
			if($cat['parent_id'] == 0) {
				tpl::set('a-class', 'block');
				tpl::set('a-href', $href_template.$cat['id']);
				tpl::set('a-content', $cat['name'].' '.$cat['id']);
				tpl::make('a', 'categories-level1-content');
				if(isset($cat_by_parent[$cat['id']])) {
					foreach($cat_by_parent[$cat['id']] as $level2) {
						tpl::set('a-href', $href_template.$level2['id']);
						tpl::set('a-content', $level2['name'].' '.$level2['id']);
						tpl::make('a', 'categories-level2-content');
						if(isset($cat_by_parent[$level2['id']])) {
							foreach($cat_by_parent[$level2['id']] as $level3) {
								tpl::set('a-href', $href_template.$level3['id']);
								tpl::set('a-content', $level3['name'].' '.$level3['id']);
								tpl::make('a', 'categories-level3-content');
							}
							tpl::make('categories-level3', 'categories-level2-content');
							tpl::clear('categories-level3-content');
						}
					}
					tpl::make('categories-level2', 'categories-level1-content');
					tpl::clear('categories-level2-content');
				}
				tpl::make('categories-level1');
				tpl::clear('categories-level1-content');
			}
		}
		tpl::make('categories', $appendTo);
	}
	
	static function productCategories($path, $productId = 0) {
		if(!$productId) {
			$html = alert('Товар не выбран!', 'error');
			if(AJAX) die($html);
			return $html;
		}
		if(REQUEST_METHOD == 'POST') {
			$cid = absint(POST('cid'));
			$value = absint(POST('value'));
			if(!$cid) {
				$html = alert('Ошибка! Не передан ID категории', 'error');
				if(AJAX) die($html);
				return $html;
			}
			if(self::updateProductCategory($productId, $cid, $value)) {
				die('ok');
			}
			else die('Неверное значение переключателя!'); // вряд ли такое возможно :/
		}
		tpl::set('product-id', $productId);
		
		db::query('select * from product_category where product_id = ?', $productId);
		$product_categories = [];
		while($cat = db::fetchArray()) {
			$product_categories[$cat['category_id']] = $cat;
		}
		$idList = array_keys($product_categories);
		
		$categories = self::getCategories();
		$cat_by_parent = array_group($categories, 'parent_id');
		
		foreach($categories as $cat) {
			if($cat['parent_id'] > 0) continue;
			tpl::set('id', $cat['id']);
			tpl::set('name', $cat['name']);
			tpl::set('category-class', '');
			tpl::set('category-checked', in_array($cat['id'], $idList) ? ' checked' : '');
			tpl::make('product-category');
			if(isset($cat_by_parent[$cat['id']])) {
				foreach($cat_by_parent[$cat['id']] as $level2) {
					tpl::set('id', $level2['id']);
					tpl::set('name', $level2['name']);
					tpl::set('category-class', 'm1');
					tpl::set('category-checked', in_array($level2['id'], $idList) ? ' checked' : '');
					tpl::make('product-category');
					if(isset($cat_by_parent[$level2['id']])) {
						foreach($cat_by_parent[$level2['id']] as $level3) {
							tpl::set('id', $level3['id']);
							tpl::set('name', $level3['name']);
							tpl::set('category-class', 'm2');
							tpl::set('category-checked', in_array($level3['id'], $idList) ? ' checked' : '');
							tpl::make('product-category');
						}
					}
				}
			}
		}
		
		$html = tpl::make('product-categories', 'main');
		if(AJAX) die($html);
	}
	
	static function updateProductCategory($productId, $cid, $value) {
		if($value == 0) {
			return db::query('delete from product_category where product_id = ? and category_id = ?', $productId, $cid);
		}
		elseif($value == 1) {
			return db::query('insert ignore into product_category values(?, ?)', $productId, $cid);
		}
		else return false;
	}
	
	static function dropCache() {
		cache::flush();
		return redirect(SESSION('referer'));
	}
	
	static function get_orders_params() {
		$sort_types = json::get('admin-orders-sort');
		$params['sort'] = absint(REQUEST('sort'));
		$params['region'] = absint(GET('region'));
		$params['dtype'] = absint(GET('dtype'));
		$params['dservice'] = absint(GET('dservice'));
		$params['status'] = isset($_GET['status']) ? absint(GET('status')) : NULL;
		$params['cstatus'] = absint(GET('cstatus'));
		$params['paid'] = isset($_GET['paid']) ? absint(GET('paid')) : NULL;
		$params['search'] = trim(GET('search'));
		$params['user_id'] = absint(GET('user_id'));
		$params['limit'] = 50;
		$params['order_by'] = $sort_types[0]['sql'];
		if(isset($sort_types[$params['sort']])) {
			$params['order_by'] = $sort_types[$params['sort']]['sql'];
		}
		return $params;
	}
	
	static function orders($path, $id = 0) {
		if($id) return self::order($id);
		make_title('Заказы');
		$sort_types = json::get('admin-orders-sort');
		
		$params = self::get_orders_params();
		
		$action = trim(REQUEST('action'));
		
		if($action == 'apply-all') {
			$order_ids = REQUEST('order_ids');
			$column = trim(REQUEST('column'));
			$value = absint(REQUEST('value'));
			db::query('update orders set '.$column.' = ? where id in ('.implode(',', $order_ids).')', $value);
			die(db::count() ? 'ok' : 'Без изменений');
		}
		
		$status = json::get('order-status');
		$cstatus = json::get('order-cstatus');
		$payment_status = [0 => 'не оплачен', 1 => 'оплачен'];
		$dtype = json::get('dtype');
		$ptype = json::get('ptype');
		$dservice = json::get('dservice');
		
		$waybill_start = json::decode(file_get_contents(FILES.'/waybill-start.json'));
		$wb_sent = end($waybill_start);
		$wb_sent = strtotime($wb_sent);
		
		foreach($sort_types as $id => $sort) {
			tpl::set('option-value', $id ? qs::set('sort', $id) : qs::remove('sort'));
			tpl::set('option-text', $sort['text']);
			tpl::set('option-selected', $id == $params['sort'] ? ' selected' : '');
			tpl::make('option');
		}
		tpl::set('select-name', 'sort');
		tpl::set('select-class', 'filter');
		tpl::make('select', 'admin-orders-sort');
		qs::reset();
		qs::remove('p');
		// region filter select
		$regions = ['-- Все --', 'Москва', 'Регионы'];
		foreach($regions as $id => $region) {
			tpl::set('region-value', $id ? qs::set('region', $id) : qs::remove('region'));
			tpl::set('region-name', $region);
			tpl::set('region-selected', $params['region'] == $id ? ' selected' : '');
			tpl::make('admin-orders-region-option');
		}
		qs::reset();
		qs::remove('p');
		// dtype filter select
		tpl::set('option-value', qs::remove('dtype'));
		tpl::set('option-text', '-- Все --');
		tpl::set('option-selected', '');
		tpl::make('option', 'option-dtype');
		foreach($dtype as $id => $d) {
			tpl::set('option-value', qs::set('dtype', $id));
			tpl::set('option-text', $d['text']);
			tpl::set('option-selected', $id == $params['dtype'] ? ' selected' : '');
			tpl::make('option', 'option-dtype');
		}
		qs::reset();
		qs::remove('p');
		// status filter select
		tpl::set('option-value', qs::remove('status'));
		tpl::set('option-text', 'Все');
		tpl::set('option-selected', '');
		tpl::make('option', 'option-status');
		foreach($status as $id => $text) {
			if(in_array($id, [6,7,9,10])) continue;
			tpl::set('option-value', qs::set('status', $id));
			tpl::set('option-text', $text);
			tpl::set('option-selected', !is_null($params['status']) && $id == $params['status'] ? ' selected' : '');
			tpl::make('option', 'option-status');
		}
		// separator
			tpl::set('option-selected', ' disabled class="center"');
			tpl::set('option-text', '──────────');
			tpl::set('option-value', '');
			tpl::make('option', 'option-status');
		foreach($status as $id => $text) {
			if(!in_array($id, [6,7,9,10])) continue;
			tpl::set('option-value', qs::set('status', $id));
			tpl::set('option-text', $text);
			tpl::set('option-selected', !is_null($params['status']) && $id == $params['status'] ? ' selected' : '');
			tpl::make('option', 'option-status');
		}
		qs::reset();
		qs::remove('p');
		// collecting status filter select
		tpl::set('option-value', qs::remove('cstatus'));
		tpl::set('option-text', 'Все');
		tpl::set('option-selected', '');
		tpl::make('option', 'option-cstatus');
		foreach($cstatus as $id => $text) {
			tpl::set('option-value', qs::set('cstatus', $id));
			tpl::set('option-text', $text);
			tpl::set('option-selected', !is_null($params['cstatus']) && $id == $params['cstatus'] ? ' selected' : '');
			tpl::make('option', 'option-cstatus');
			tpl::set('option-value', $id);
			tpl::set('option-selected', '');
			tpl::make('option', 'option-cstatus-apply');
		}
		qs::reset();
		qs::remove('p');
		// payment status select
		tpl::set('option-value', qs::remove('paid'));
		tpl::set('option-text', 'Все');
		tpl::set('option-selected', '');
		tpl::make('option', 'option-paid');
		foreach($payment_status as $id => $text) {
			tpl::set('option-value', qs::set('paid', $id));
			tpl::set('option-text', $text);
			tpl::set('option-selected', !is_null($params['paid']) && $id == $params['paid'] ? ' selected' : '');
			tpl::make('option', 'option-paid');
			tpl::set('option-value', $id);
			tpl::set('option-selected', '');
			tpl::make('option', 'option-paid-apply');
		}
		if(account::$admin) {
			tpl::make('admin-orders-status-apply');
		}
		if(account::$employee) {
			tpl::set('paid-attr', ' disabled');
			tpl::set('cstatus-attr', ' disabled');
		}
		qs::reset();
		qs::remove('p');
		// delivery service select
		tpl::set('option-value', qs::remove('dservice'));
		tpl::set('option-text', 'Все');
		tpl::set('option-selected', '');
		tpl::make('option', 'option-dservice');
		uksort($dservice, function($a, $b) use($dservice) {
			$a = $dservice[$a];
			$b = $dservice[$b];
			return $a['name'] > $b['name'] ? 1 : 0;
		});
		foreach($dservice as $id => $service) {
			if(!in_array($id, [4,5,6,8,9,10,13,14,18,19])) continue;
			tpl::set('option-value', qs::set('dservice', $id));
			tpl::set('option-text', $service['name']);
			tpl::set('option-selected', $id == $params['dservice'] ? ' selected' : '');
			tpl::make('option', 'option-dservice');
		}
		// separator
			tpl::set('option-selected', ' disabled class="center"');
			tpl::set('option-text', '───────────────');
			tpl::set('option-value', '');
			tpl::make('option', 'option-dservice');
		foreach($dservice as $id => $service) {
			if(in_array($id, [4,5,6,8,9,10,13,14,18,19])) continue;
			tpl::set('option-value', qs::set('dservice', $id));
			tpl::set('option-text', $service['name']);
			tpl::set('option-selected', $id == $params['dservice'] ? ' selected' : '');
			tpl::make('option', 'option-dservice');
		}
		qs::reset();
		tpl::clear('option');
		tpl::set('search', html2chars($params['search']));
		list($total_orders, $orders) = order::getList($params);
		// get noitems
		$noitems = [];
		db::query('select order_id, sum(quantity) as quantity from order_noitem group by order_id');
		while($noitem = db::fetchArray()) {
			$noitems[$noitem['order_id']] = $noitem['quantity'];
		}
		$order_ids = [];
		foreach($orders as $order) {
			if($order['delivery_info']) $order['delivery_info'] = $order['city'].', '.$order['delivery_info'];
			tpl::push($order);
			tpl::set('order-sum', $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon']);
			tpl::set('cdate', ts2date('d.m.Y - H:i', $order['ctime']));
			tpl::set('ddate', $order['dtime'] ? ts2date('Y-m-d', $order['dtime']) : '--');
			tpl::set('dtype-text', @$dtype[$order['dtype']]['text']);
			if($order['dtype'] == 3) {
				tpl::set('dtype-text', $dtype[5]['text'].' (Почта России)');
			}
			tpl::set('ptype-text', @$ptype[$order['ptype']]['text']);
			tpl::set('address', '--');
			$phone = str_replace([' ','-','(',')'], '', $order['phone']);
			tpl::set('phone', $phone);
			list($total_positions, $in_stock) = order::get_semafor($order);
			tpl::set('positions-total', $total_positions);
			tpl::set('positions-stock', $in_stock);
			tpl::set('semafor-class', !$in_stock ? 'red' : '');
			if(isset($noitems[$order['id']])) {
				tpl::set('order-noitems-quantity', $noitems[$order['id']]);
				tpl::make('order-noitems-mark');
			}
			if($order['address'] && ($address = json::decode($order['address']))) {
				$address['region'] = $order['region'];
				$address['city'] = $order['city'];
				tpl::set('address', str::address2str($address));
			}
			if($order['user_id']) {
				tpl::make('client-link');
			}
			else {
				tpl::make('client-name');
			}
			foreach($status as $id => $text) {
				tpl::set('option-value', $id);
				tpl::set('option-text', $text);
				tpl::set('option-selected', $id == $order['status'] ? ' selected' : '');
				// оператору доступны только конкретные статусы
				if(account::$employee && !in_array($id, self::$employee_allowed_status)) {
					tpl::append('option-selected', ' disabled');
				}
				tpl::make('option', 'status-option');
			}
			foreach($cstatus as $id => $text) {
				tpl::set('option-value', $id);
				tpl::set('option-text', $text);
				tpl::set('option-selected', $id == $order['cstatus'] ? ' selected' : '');
				tpl::make('option', 'cstatus-option');
			}
			foreach($payment_status as $value => $text) {
				tpl::set('option-value', $value);
				tpl::set('option-text', $text);
				tpl::set('option-selected', $value == $order['paid'] ? ' selected' : '');
				tpl::make('option', 'paid-option');
			}
			if($order['status'] == 0) {
				tpl::make('admin-call-request-link');
			}
			$ctime = strtotime($order['ctime']);
			tpl::set('order-tr-class', $ctime > $wb_sent ? 'wb-new' : '');
			tpl::make('admin-order');
			tpl::clear('status-option', 'cstatus-option', 'client-link', 'client-name', 'paid-option', 'admin-order-pr-input', 'admin-call-request-link', 'order-noitems-mark');
			$order_ids[] = $order['id'];
		}
		if($total_orders) {
			tpl::set('order-ids', json::encode($order_ids));
			shop::paginator($total_orders, $params['limit']);
			qs::$params['path'] = '/admin/orders/print';
			tpl::set('print-list-href', qs::set('type', 'courier'));
			tpl::set('print-pr-href', qs::set('type', 'pr'));
			qs::$params['path'] = '/admin/quittance/all';
			tpl::set('print-quittance-href', qs::remove('type'));
			qs::$params['path'] = '/admin/orders/send';
			tpl::set('send-sl-href', qs::set('to', 'sl'));
			tpl::set('send-pp-href', qs::set('to', 'pp'));
			if(account::$admin) tpl::make('print-links');
			qs::reset();
		}
		else {
			tpl::set('order-ids', '[]');
			alert('Заказы не найдены!', 'notice', 'main');
		}
		tpl::set('orders-count', $total_orders);
		tpl::make('admin-orders', 'main');
	}
	
	static function suppliers() {
		$stock_list = shop::get_stock_list();
		if(REQUEST_POST) {
			$action = POST('action');
			if($action == 'toggle') {
				$status = intval(POST('status')) ? 1 : 0;
				$stock_id = intval(POST('sid'));
				if(!isset($stock_list[$stock_id])) die('Нет такого поставщика');
				db::query('update product_stock set status = ? where id = ?', $status, $stock_id);
				shop::update_stock_from_import(); // обновляем наличие товаров
				cache::flush(); // для обновления предложений на главной
				die('ok');
			}
			if($action == 'update-price-rate') {
				$stock_id = intval(POST('sid'));
				if(!isset($stock_list[$stock_id])) die('Нет такого поставщика');
				$price_rate = correct_float(POST('price_rate'));
				if($price_rate <= 1) die('Коэффициент должен быть больше 1');
				db::query('update product_stock set price_rate = ? where id = ?', $price_rate, $stock_id);
				cache::clear('stock_list');
				die('ok');
			}
			if($action == 'price-rate-apply') {
				$result = product::auto_price();
				$n = reset($result);
				if($n) cache::flush();
				die('Обновлено '.$n.' товаров');
			}
			if($action == 'lanixm-upload') {
				import::upload_lanixm();
				die('ok');
			}
			if($action == 'update-min-sum') {
				$stock_id = intval(POST('sid'));
				$min_sum = intval(POST('min_sum'));
				if(!isset($stock_list[$stock_id])) die('Нет такого поставщика');
				db::query('update product_stock set min_sum = ? where id = ?', $min_sum, $stock_id);
				die('ok');
			}
		}
		make_title('Поставщики');
		foreach($stock_list as $id => $stock) {
			if($id < 3) continue;
			tpl::push($stock);
			tpl::set('sid', $id);
			tpl::set('stock-checked', $stock['status'] ? ' checked' : '');
			tpl::set('stock-status-label', $stock['status'] ? 'Включено' : 'Выключено');
			tpl::set('status-class', $stock['status'] ? 'green' : 'red');
			if($id == 8) {
				tpl::make('admin-suppliers-lanixm-form');
			}
			else tpl::clear('admin-suppliers-lanixm-form');
			tpl::make('admin-suppliers-row');
		}
		tpl::make('admin-suppliers', 'main');
	}
	
	static function order($id) {
		if($action = REQUEST('action')) {
			if($action == 'delete-item') return self::deleteOrderItem($id);
			if($action == 'change-quantity') return order::update_products($id);
			if($action == 'addfromcart') return self::addProductsFromCartToOrder($id);
			if($action == 'sl-send-order') return self::sl_send_order($id);
			if($action == 'dguru-send-order') return self::dguru_send_order($id);
			if($action == 'create-pp') return self::createPPOrder($id);
			if($action == 'call-request') return self::callRequest($id);
			if($action == 'replace-select') return self::orderProductReplacement($id);
			if($action == 'replace') return self::replaceOrderItem($id);
			if($action == 'merge') return self::add_from_order($id);
			if($action == 'noitem') return self::tag_noitem($id);
		}
		elseif(REQUEST_METHOD == 'POST') {
			return order::update($id);
		}
		make_title('Заказ №'.$id);
		db::query('select * from orders where id = ?', $id);
		if(!$order = db::fetchArray()) {
			return alert('Такого заказа нет', 'warning', 'main');
		}
		tpl::push($order);
		if($order['comment']) tpl::walk('comment', 'html2chars');
		if($order['comment_admin']) tpl::walk('comment_admin', 'html2chars');
		tpl::set('order-id', $order['id']);
		$phone = str_replace([' ','-','(',')'], '', $order['phone']);
		tpl::set('phone', $phone);
		$status = json::get('order-status');
		$cstatus = json::get('order-cstatus');
		$dtypes = json::get('dtype');
		$ptypes = json::get('ptype');
		foreach($ptypes as $id => $ptype) {
			tpl::set('option-value', $id);
			tpl::set('option-text', $ptype['text']);
			tpl::set('option-selected', $id == $order['ptype'] ? ' selected' : '');
			tpl::make('option', 'option-ptype');
		}
		foreach($dtypes as $id => $dtype) {
			tpl::set('option-value', $id);
			tpl::set('option-text', $dtype['text']);
			tpl::set('option-selected', $id == $order['dtype'] ? ' selected' : '');
			tpl::make('option', 'option-dtype');
		}
		$payment_status = [
			0 => 'не оплачен', 1 => 'оплачен'
		];
		foreach($status as $id => $text) {
			tpl::set('option-value', $id);
			tpl::set('option-text', $text);
			tpl::set('option-selected', $id == $order['status'] ? ' selected' : '');
			// оператору доступны только конкретные статусы
			if(account::$employee && !in_array($id, self::$employee_allowed_status)) {
				tpl::append('option-selected', ' disabled');
			}
			tpl::make('option', 'status-option');
		}
		foreach($cstatus as $id => $text) {
			tpl::set('option-value', $id);
			tpl::set('option-text', $text);
			tpl::set('option-selected', $id == $order['cstatus'] ? ' selected' : '');
			tpl::make('option', 'cstatus-option');
		}
		foreach($payment_status as $value => $text) {
			tpl::set('option-value', $value);
			tpl::set('option-text', $text);
			tpl::set('option-selected', $value == $order['paid'] ? ' selected' : '');
			tpl::make('option', 'paid-option');
		}
		// order details here
		$products = json::decode($order['cart_items']);
		usort($products, function($a, $b) {
			if($a['name'] > $b['name']) return 1;
		});
		// fetch current stock quantity
		$barcodes = $stock_quantity = [];
		foreach($products as $product) {
			$barcodes[] = $product['barcode'];
		}
		db::query('select barcode, quantity from product_import where barcode in (\''.implode('\',\'', $barcodes).'\')');
		while($p = db::fetchArray()) {
			$stock_quantity[$p['barcode']] = $p['quantity'];
		}
		// noitems
		db::query('select * from order_noitem where order_id = ?', $order['id']);
		$noitems = db::fetchAll();
		$i = 1;
		foreach($products as $product) {
			$product['option_id'] = intval($product['option_id']);
			$has_noitem = false;
			foreach($noitems as $noitem) {
				if($noitem['product_id'] == $product['id'] && $noitem['option_id'] == $product['option_id'] && $noitem['quantity'] > 0) {
					$has_noitem = true;
					break;
				}
			}
			tpl::push($product);
			tpl::set('stock_quantity', @$stock_quantity[strval($product['barcode'])] ?: '?');
			tpl::set('wbid', $product['id']);
			if($product['option_id']) {
				tpl::append('wbid', '/'.$product['option_id']);
			}
			tpl::set('product-sum', $product['price'] * $product['quantity']);
			tpl::set('option-name', $product['option_id'] ? ' - '.$product['option_name'] : '');
			tpl::set('n', $i); $i++;
			tpl::set('noitem-checked', $has_noitem ? ' checked' : '');
			tpl::set('noitem-quantity', $has_noitem ? ' '.$noitem['quantity'] : '');
			if(account::$admin) {
				tpl::make('restricted-2');
			}
			tpl::make('admin-order-product');
			tpl::clear('restricted-2');
		}
		if(account::$employee) {
			tpl::set('cstatus-attr', ' disabled');
			tpl::set('paid-attr', ' disabled');
		}
		else {
			tpl::make('restricted-1');
			tpl::make('restricted-4');
		}
		if($order['user_id']) {
			db::query('select ltime, bdate from user where id = ?', $order['user_id']);
			tpl::push(db::fetchArray());
		}
		if($order['address']) {
			$address = json::decode($order['address']);
			if($address) {
				tpl::push($address);
			}
		}
		if($order['delivery_data']) {
			$delivery_data = json::decode($order['delivery_data']);
			ob_start();
			print_r($delivery_data);
			tpl::set('delivery_data', ob_get_clean());
			tpl::make('admin-order-pp-info');
		}
		tpl::set('cart-sum-with-discounts', $order['total_sum'] - $order['bonus'] - $order['cumulative'] - $order['coupon']);
		tpl::set('order-total', $order['delivery_price'] + $order['total_sum'] - $order['bonus'] - $order['cumulative'] - $order['coupon']);
		tpl::make('admin-order-details', 'main');
	}
	
	static function order_delivery($path, $order_id) {
		$order = db::querySingle('select * from orders where id = '.$order_id, true);
		if(!$order) die('<h1>Заказ не найден</h1>');
		if(REQUEST_METHOD == 'POST') {
			return self::order_delivery_save($order);
		}
		tpl::load('order-form2');
		$order_sum = $order['total_sum'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
		if(!$order['region']) $order['region'] = 'Москва';
		$address = $order['address'] ? json::decode($order['address']) : null;
		foreach(shoplogistics::get_regions() as $oblast) {
			tpl::set('option-value', $oblast['name']);
			tpl::set('option-text', $oblast['name']);
			tpl::set('option-selected', $oblast['name'] == $order['region'] ? ' selected' : '');
			tpl::make('option', 'region-option');
		}
		$cities = shoplogistics::get_city_list($order['region']);
		if(!$cities) $cities = shoplogistics::get_city_list('Москва');
		foreach($cities as $c) {
			tpl::set('option-value', $c['name']);
			tpl::set('option-text', $c['name']);
			tpl::set('option-selected', $c['name'] == $order['city'] ? ' selected' : '');
			tpl::make('option', 'city-option');
		}
		if($address) {
			tpl::push($address);
			if(!@$address['region']) {
				tpl::set('region', $order['region']);
			}
		}
		if($session_zipcode = SESSION('zipcode')) {
			tpl::set('zipcode', $session_zipcode);
		}
		tpl::set('order-id', $order_id);
		tpl::set('cart-cost', $order_sum);
		tpl::set('orderform-script-mtime', filemtime(ROOT.'/ui/js/orderform.js'));
		tpl::make('order-form-delivery');
		echo tpl::make('admin-order-delivery');
		exit;
	}
	
	static function order_delivery_save($order) {
		array_walk($_POST, function(&$val, $key) {
			$val = html2chars(trim($val));
		});
		$manual_input =  intval(POST('manual_input'));
		$dtype = absint(REQUEST('dtype'));
		$dservice = absint(REQUEST('dservice'));
		$delivery_info = trim(REQUEST('delivery_info'));
		$delivery_price = absint(REQUEST('delivery_price'));
		$delivery_price_real = absint(REQUEST('delivery_price_real'));
		$pickuppoint = trim(REQUEST('pickuppoint'));
		$region = $manual_input ? POST('region_m') : POST('region');
		$city = $manual_input ? POST('city_m') : POST('city');
		$address = json::decode($order['address']);
		$address['region'] = $region;
		$address['city'] = $city;
		$error = false;
		if(!$dtype) {
			$error = 'dtype';
		}
		// Курьеры (наши, SL или ПР), адрес обязателен
		elseif(($dtype == 5 && !$pickuppoint) || in_array($dtype, [1,3])) {
			$region = POST('region');
			$zipcode = POST('zipcode');
			$metro = POST('metro');
			$street = POST('street');
			$building = POST('building');
			$building_add = POST('building-add');
			$apt = POST('apt');
			if($delivery_price === '' || $delivery_info === '') {
				$error = 'delivery-type-required';
			}
			elseif(!$city || $street == '' || $building == '') {
				$error = 'address-required';
			}
			else {
				$address = array(
					'region' => $region,
					'city' => $city,
					'zipcode' => $zipcode,
					'metro' => $metro,
					'street' => $street,
					'building' => $building,
					'building-add' => $building_add,
					'apt' => $apt
				);
			}
		}
		if($error) {
			$errors = json::get('errors');
			$response = [
				'status' => 'error',
				'message' => $errors[$error]
			];
			if(AJAX) die(json::encode($response));
			alert($errors[$error], 'error', 'main');
		}
		else {
			$data = array(
				'region' => $region,
				'city' => $city,
				'address' => json::encode($address),
				'dtype' => $dtype,
				'delivery_info' => $delivery_info ?: NULL,
				'delivery_price' => $delivery_price,
				'delivery_price_real' => $delivery_price_real,
				'dservice' => $dservice,
				'delivery_data' => NULL
			);
			if($pickuppoint) {
				// ПВЗ PickPoint
				if($dservice == 5) {
					db::query('select * from points_pp where id = ?', $pickuppoint);
					if($delivery_data = db::fetchArray())
						$data['delivery_data'] = json::encode($delivery_data);
				}
				else {
					db::query('select * from sl_pickups where code_id = ?', $pickuppoint);
					if($delivery_data = db::fetchArray()) {
						$data['delivery_data'] = json::encode($delivery_data);
					}
				}
			}
			if($order_id = order::save($data, $order['id'])) {
				$response = [
					'status' => 'ok',
					'order_uid' => $order['uid'],
					'redirect' => '/admin/orders/'.$order_id
				];
				if(AJAX) die(json::encode($response));
				return redirect($response['redirect']);
			}
			else {
				$response = [
					'status' => 'ok',
					'message' => 'Извините, что-то пошло не так.'
				];
				if(AJAX) die(json::encode($response));
				return alert('Извините, что-то пошло не так.', 'error', 'main');
			}
		}
	}
	
	static function addProductsFromCartToOrder($orderId) {
		if(!$order = db::querySingle('select * from orders where id = '.$orderId, true)) die('ЗАКАЗ НЕ НАЙДЕН!');
		if(!cart::count()) die('Корзина пуста');
		$order_items = json::decode($order['cart_items']);
		$cart_items = json::decode(cart::getOrderJSON());
		foreach($cart_items as $ck => $ci) {
			foreach($order_items as $ok => $oi) {
				// обновляем количество для существующих
				if(($oi['option_id'] && $ci['option_id'] == $oi['option_id']) || (!$oi['option_id'] && !$ci['option_id'] && $ci['id'] == $oi['id'])) {
					$order_items[$ok]['quantity'] += $ci['quantity'];
					$order['total_sum'] += $ci['quantity'] * $ci['price'];
					// исключаем добавленные товары...
					unset($cart_items[$ck]);
				}
			}
		}
		// ...и добавляем остатки как отдельные товары
		foreach($cart_items as $ci) {
			$order_items[] = $ci;
			$order['total_sum'] += $ci['quantity'] * $ci['price'];
		}
		// обновляем заказ
		$order['cart_items'] = json::encode(array_values($order_items));
		order::save($order);
		// вычитаем товары из склада
		cart::updateStock();
		// очищаем корзину
		cart::clear();
		cart::save();
		die('ok');
	}
	
	static function deleteOrderItem($orderId) {
		if(!$productId = absint(REQUEST('id'))) die('ID товара не указан!');
		if(!$order = db::querySingle('select * from orders where id = '.$orderId, true)) die('ЗАКАЗ НЕ НАЙДЕН!');
		$optionId = absint(REQUEST('option_id'));
		$items = json::decode($order['cart_items']);
		$price_diff = 0;
		$deleted = NULL;
		foreach($items as $key => $item) {
			if(($optionId && $item['option_id'] == $optionId) || (!$optionId && $item['id'] == $productId)) {
				$price_diff += $item['price'] * $item['quantity'];
				$deleted = $items[$key];
				unset($items[$key]);
			}
		}
		if($deleted) {
			db::query('delete from order_noitem where order_id = ? and product_id = ? and option_id = ?', 
				$orderId, $deleted['id'], intval($deleted['option_id'])
			);
		}
		$order['cart_items'] = json::encode(array_values($items));
		$order['total_sum'] -= $price_diff;
		if(!order::save($order)) die('DB ERROR!');
		die('ok');
	}
	
	static function callRequest($orderId) {
		if(!$order = db::querySingle('select * from orders where id = '.$orderId, true)) die('Заказ не найден!');
		if(!$order['email']) die('Email не указан в заказе');
		if(shop::sendCallRequest($order)) die('ok');
		die('Не удалось отправить письмо');
	}
	
	static function import_product_options() {
		$dir = FILES.'/variations';
		$files = get_files($dir);
		db::prepare('insert ignore into product_option (option_id, barcode, product_id, quantity, price, price_old, price_buyin) (select :name, barcode, id, ifnull(quantity,0), price, price_old, price_buyin from product where id = :id)');
		foreach($files as $file) {
			$filename = finfo($file, 'filename');
			$product_id = preg_match('#^(\d+)-(\d+)$#', $filename, $match) ? intval($match[1]) : NULL;
			db::set('name', $filename);
			db::set('id', $product_id);
			db::execute();
		}
		// если у товара есть опции, у него не должно быть своего шрих-кода
		db::query('update product p set barcode = \'0000000000000\', price_old = null where (select count(*) from product_option where product_id = p.id) > 1');
		if(AJAX) die('ok');
		redirect(SESSION('referer'));
	}
	
	static function categories($path, $cid = 0) {
		make_title('Категории');
		tpl::set('admin-category-form-title', '<h3>Добавить новую категорию</h3>');
		if($delete = absint(GET('delete'))) {
			return self::deleteCategory($delete);
		}
		if($cid) return self::category($cid);
		if(REQUEST_METHOD == 'POST') {
			self::saveCategory();
		}
		self::makeCatalogMenu('left-column-content', '/admin/categories/');
		foreach(admin::getCategories() as $cat) {
			if(substr_count($cat['url'], '/') > 2) continue;
			tpl::set('option-value', $cat['id']);
			tpl::set('option-text', $cat['name']);
			tpl::make('option', 'option-parent');
		}
		// sort type select
		$sort_types = json::get('sort-types');
		foreach($sort_types as $id => $type) {
			tpl::set('option-value', $id);
			tpl::set('option-text', $type['text']);
			tpl::make('option', 'option-sort');
		}
		tpl::make('admin-category', 'right-column-content');
		tpl::make('left-column', 'main');
		tpl::make('right-column', 'main');
	}
	
	static function category($cid) {
		tpl::set('admin-category-form-title', '<h3>Редактировать категорию</h3>');
		// parent category select
		$categories = admin::getCategories();
		if(!isset($categories[$cid])) {
			return alert('Такой категории нет!', 'error', 'main');
		}
		$category = $categories[$cid];
		if(REQUEST_METHOD == 'POST') {
			self::saveCategory($cid);
		}
		foreach($categories as $cat) {
			tpl::set('option-value', $cat['id']);
			tpl::set('option-text', $cat['name']);
			tpl::set('option-selected', $cat['id'] == $category['parent_id'] ? ' selected' : '');
			tpl::make('option', 'option-parent');
		}
		// sort type select
		$sort_types = json::get('sort-types');
		foreach($sort_types as $id => $type) {
			tpl::set('option-value', $id);
			tpl::set('option-text', $type['text']);
			tpl::set('option-selected', $id == $category['product_sort'] ? ' selected' : '');
			tpl::make('option', 'option-sort');
		}
		$category['seo'] = html2chars($category['seo']);
		tpl::push($category);
		tpl::append('parent_url', '/');
		tpl::set('status-checked', $category['status'] ? ' checked' : '');
		tpl::set('use-filters-checked', $category['use_filters'] ? ' checked' : '');
		self::makeCatalogMenu('left-column-content', '/admin/categories/');
		tpl::make('admin-category-delete');
		tpl::make('admin-category-js');
		tpl::make('admin-category', 'right-column-content');
		tpl::make('left-column', 'main');
		tpl::make('right-column', 'main');
	}
	
	static function saveCategory($cid = 0) {
		array_walk($_POST, 'trim');
		$url = '/' . $_POST['url_name'];
		$categories = admin::getCategories();
		if($parentId = absint(POST('parent_id'))) {
			$parent = $categories[$parentId];
			$url = $parent['url'] . $url;
		}
		$_POST['status'] = absint(POST('status'));
		$_POST['sort'] = absint(POST('sort'));
		$_POST['product_sort'] = absint(POST('product_sort'));
		$_POST['url'] = $url;
		$_POST['use_filters'] = POST('use_filters') ? 1 : 0;
		if($cid) {
			foreach($_POST as $column => $value) {
				db::query('update categories set `'.$column.'` = ? where id = ?', $value, $cid);
			}
		}
		else {
			$sql = 'insert into categories set ';
			$parent_id = absint(POST('parent_id'));
			if(isset($categories[$parent_id])) $_POST['url'] = $categories[$parent_id]['url'] . '/' . POST('url_name');
			else $_POST['url'] = '/'.POST('url_name');
			foreach($_POST as $key => $value) {
				$params[] = $key.' = :'.$key;
			}
			$sql .= implode(',', $params);
			db::query($sql, $_POST);
		}
		cache::clear(['categories', 'admin-categories']);
		if(!$cid) redirect('/admin/categories/'.db::lastInsertId());
		if(AJAX) die('ok');
		redirect(SESSION('referer'));
	}
	
	static function deleteCategory($cid) {
		db::query('delete from categories where id = ?', $cid);
		cache::clear(['categories', 'admin-categories']);
		return redirect('/admin/categories');
	}
	
	static function getCategories() {
		if(!$categories = cache::get('admin-categories')) {
			$categories = [];
			db::query('select *, (select url from categories where id = c.parent_id) as parent_url, (select count(*) from product_category pc where category_id = c.id and (select `status` = 1 from product where id = pc.product_id)) as product_count from categories c order by sort, id');
			while($cat = db::fetchArray()) {
				$categories[$cat['id']] = $cat;
			}
			cache::set('admin-categories', $categories, 120);
		}
		return $categories;
	}
	
	static function reviews($path, $id = 0) {
		if($id = intval($id)) return self::review($id);
		$action = REQUEST('action');
		if($action == 'publish-all') {
			return self::publish_all_reviews();
		}
		if($action == 'date-apply') {
			return self::spread_reviews();
		}
		make_title('Отзывы покупателей');
		$LIMIT = 20;
		$review_bonus = [0, 1, 3, 5, 7, 10];
		$sql = 'select count(*) from product_review pr';
		if(GET('filter') == 'inactive') {
			$sql .= ' where status = 0';
		}
		$total_reviews = db::querySingle($sql);
		$sql = str_replace('count(*)', '*, (select name from product where id = pr.product_id) as product_name, (select url_name from product where id = pr.product_id) as product_url', $sql);
		$sql .= ' order by id desc limit :offset, :limit';
		db::query($sql, ['offset' => offset($LIMIT), 'limit' => $LIMIT]);
		foreach(db::fetchAll() as $review) {
			tpl::push($review);
			tpl::set('recommend', $review['recommend'] ? 'Да' : 'Нет');
			tpl::set('cdate', ts2date('d M H:i', $review['ctime']));
			tpl::set('product-url', '/product/'.$review['product_url']);
			foreach($review_bonus as $value) {
				tpl::set('option-value', $value);
				tpl::set('option-text', $value);
				tpl::set('option-selected', $value == $review['bonus'] ? ' selected' : '');
				tpl::make('option', 'bonus-option');
			}
			tpl::set('status-checked', $review['status'] ? ' checked' : '');
			tpl::walk('content', 'nl2br');
			tpl::make('admin-review');
			tpl::clear('bonus-option');
		}
		foreach($review_bonus as $value) {
			tpl::set('option-value', $value);
			tpl::set('option-text', $value);
			tpl::make('option', 'bonus-option');
		}
		tpl::set('date1', date('Y-m-d', strtotime('-3 days')));
		tpl::set('date2', date('Y-m-d'));
		shop::paginator($total_reviews, $LIMIT);
		tpl::make('admin-reviews', 'main');
	}
	
	static function review($id) {
		if($action = REQUEST('action')) {
			if($action == 'bonus') {
				$bonus = absint(REQUEST('bonus'));
				$status = $bonus ? 1 : 0;
				db::query('update product_review set status = ?, bonus = ? where id = ?', $status, $bonus, $id);
				if($bonus) {
					// add bonus for accepted review
					db::query('update user set bonus = bonus + ? where id = (select user_id from product_review where id = ?)', $bonus, $id);
					db::query('select cookie from user where id = (select user_id from product_review where id = ?)', $id);
					if($cookie = db::fetchSingle()) {
						cache::clear('user-'.$cookie);
					}
				}
				die('ok');
			}
			elseif($action == 'delete') {
				db::query('delete from product_review where id = ?', $id);
				die('ok');
			}
			elseif($action == 'edit') {
				$content = trim(POST('content'));
				db::query('update product_review set content = ? where id = ?', $content, $id);
				die('ok');
			}
		}
		die('wat');
	}
	
	static function publish_all_reviews() {
		$bonus = absint(REQUEST('bonus'));
		$uids = db::querySingle('select group_concat(user_id) from product_review where status = 0');
		if(!$uids) {
			if(AJAX) die('Нет новых отзывов');
			return false;
		}
		$id_arr = explode(',', $uids);
		db::query('update product_review set status = 1, bonus = ? where status = 0', $bonus);
		if($bonus) {
			foreach($id_arr as $uid) {
				db::query('update user set bonus = bonus + ? where id = ?', $bonus, $uid);
			}
			db::query('select cookie from user where id in('.$uids.')');
			while($cookie = db::fetchSingle()) {
				cache::clear('user-'.$cookie);
			}
		}
		if(AJAX) die('ok');
		return true;
	}
	
	static function spread_reviews() {
		$date1 = POST('date1');
		$date2 = POST('date2');
		$date1 = DateTime::createFromFormat('Y-m-d', $date1);
		$date2 = DateTime::createFromFormat('Y-m-d', $date2);
		$time1 = $date1->getTimestamp();
		$time2 = $date2->getTimestamp();
		db::query('select id from product_review where status = 0 order by ctime asc');
		if(!db::count()) die('Нет новых отзывов');
		$reviews = db::fetchAll();
		db::prepare('update product_review set ctime = ? where id = ?');
		foreach($reviews as $review) {
			$random = mt_rand($time1, $time2);
			$h = rand(8,23); $m = rand(0,60); $s = rand(0,60);
			$new_date = new DateTime();
			$ctime = $new_date->setTimestamp($random)->setTime($h,$m,$s)->format('Y-m-d H:i:s');
			db::set([$ctime, $review['id']]);
			db::execute();
		}
		exit('ok');
	}
	
	static function posts($path, $id = NULL) {
		if($id) return self::post($id);
		
		make_title('Блог');
		
		$LIMIT = 20;
		$phone = GET('phone');
		$types = json::get('blog-type');
		
		$sql = 'select count(*) from blog';
		if($phone) {
			$sql .= ' where content like \'%телефон%\' or preview like \'%телефон%\' or content like \'%звон%\' or preview like \'%звон%\'';
		}
		$count = db::querySingle($sql);
		$sql = str_replace('count(*)', '*', $sql);
		$sql .= ' order by id desc limit :offset, :limit';
		db::query($sql, ['offset' => offset($LIMIT), 'limit' => $LIMIT]);
		foreach(db::fetchAll() as $post) {
			tpl::push($post);
			tpl::set('cdate', ts2date('d.m.Y -- H:i', $post['ctime']));
			
			tpl::make('admin-post');
			tpl::clear('post-status');
		}
		shop::paginator($count, $LIMIT);
		tpl::make('admin-posts', 'main');
	}
	
	static function post($id) {
		if(REQUEST_METHOD == 'POST') {
			return self::upsertPost($id);
		}
		if($action = REQUEST('action')) {
			if($action == 'delete') {
				db::query('delete from blog where id = ?', $id);
				redirect('/admin/posts');
			}
			if($action == 'delimg') {
				db::query('update blog set image = NULL where id = ?', $id);
				redirect('/admin/posts/'.$id);
			}
		}
		make_title('Редактор');
		if($id == 'new') {
			tpl::make('post-js-new');
		}
		elseif($id = intval($id)) {
			db::query('select * from blog where id = ?', $id);
			if(!$blog = db::fetchArray()) return alert('Пост не найден', 'error');
			array_walk($blog, 'html2chars');
			tpl::push($blog);
			tpl::make('post-js-update');
			if($blog['image']) tpl::make('admin-post-image');
		}
		tpl::set('id', $id);
		tpl::make('admin-post-editor', 'main');
	}
	
	static function upsertPost($id) {
		$data = [];
		if(!$data['title'] = POST('title')) die('Нужен заголовок');
		if(!$data['title_url'] = POST('title_url')) die('Нужен URL заголовка');
		if(!$data['content'] = POST('content')) die('Текст не может быть пустым');
		$sql = 'replace into blog (id, user_id, author, title, title_url, content, image, preview, meta_description, meta_keywords) values (:id, :user_id, :author, :title, :title_url, :content, :image, :preview, :meta_description, :meta_keywords)';
		$data['id'] = intval($id) ?: NULL;
		$data['user_id'] = account::$info['id'];
		$data['author'] = trim(POST('author')) ?: NULL;
		if(empty($_FILES)) {
			$data['image'] = db::querySingle('select image from blog where id = '.$data['id']);
		}
		else {
			$img_src = uploadFile($_FILES['image'], FILES.'/blog/');
			$data['image'] = basename($img_src);
		}
		$data['preview'] = trim(POST('preview')) ?: NULL;
		$data['meta_description'] = trim(POST('meta_description')) ?: NULL;
		$data['meta_keywords'] = trim(POST('meta_keywords')) ?: NULL;
		db::query($sql, $data);
		if(strlen($data['content']) >= 500) {
			yandex::post_text($data['content']);
		}
		if(AJAX) die('ok');
		redirect('/admin/posts');
	}
	
	static function coupons($path, $couponId = NULL) {
		if($couponId = intval($couponId)) return self::coupon($couponId);
		elseif(REQUEST_METHOD == 'POST') return self::createCoupon();
		make_title('Купоны');
		$sql = 'select *, (select firstname from user where id = c.user_id) as firstname, (select lastname from user where id = c.user_id) as lastname from coupon c where true';
		$params = [];
		if($search = trim(GET('search'))) {
			$sql .= ' and code like ?';
			$params[] = '%'.$search.'%';
			tpl::set('coupon-search', html2chars($search));
		}
		$sql .= ' order by id desc';
		db::query($sql, $params);
		$coupons = db::fetchAll();
		foreach($coupons as $coupon) {
			tpl::push($coupon);
			tpl::set('status-checked', $coupon['status'] ? ' checked' : '');
			tpl::set('pc-checked', $coupon['pc'] ? ' checked' : '');
			if($coupon['order_id']) tpl::make('admin-coupon-order-link');
			else tpl::set('admin-coupon-order-link', '--');
			if($coupon['user_id']) tpl::make('admin-coupon-user');
			else tpl::set('admin-coupon-user', 0);
			tpl::make('admin-coupon');
			tpl::clear('admin-coupon-order-link', 'admin-coupon-user');
		}
		tpl::make('admin-coupons', 'main');
	}
	
	static function coupon($couponId) {
		if(REQUEST('action') == 'delete') {
			db::query('delete from coupon where id = ?', $couponId);
			exit('ok');
		}
		if(REQUEST_METHOD == 'POST') {
			return self::updateCoupon($couponId);
		}
		make_title('Купон №'.$couponId);
		tpl::append('main', 'В разработке...');
	}
	
	static function updateCoupon($couponId) {
		if(!$column = POST('column')) die('Поле не указано');
		if(($value = POST('value')) == '') die('Значение не указано');
		db::query('update coupon set `'.$column.'` = ? where id = ?', $value, $couponId);
		exit('ok');
	}
	
	static function createCoupon() {
		if(!$code = trim(REQUEST('code'))) die('Нужно имя купона!');
		if(strlen($code) < 4) die('Название не менее 4 символов!');
		db::query('insert ignore into coupon set code = ?', $code);
		if(!db::count()) die('Ошибка! Вероятно, код не уникален');
		die('ok');
	}
	
	static function quittance($path, $orderId) {
		if($orderId == 'all') {
			$params = self::get_orders_params();
			list($total_orders, $orders) = order::getList($params);
		}
		else {
			db::query('select * from orders where id = ?', $orderId);
			if(!$order = db::fetchArray()) return alert('ЗАКАЗ НЕ НАЙДЕН!');
			$total_orders = 1;
			$orders = [$order];
		}
		$ptypes = json::get('ptype');
		tpl::load('quittance');
		foreach($orders as $order) {
			tpl::push($order);
			// check for coupon
			if($coupon = db::querySingle('select * from coupon where order_id = '.$order['id'], true)) {
				tpl::set('coupon-code', $coupon['code']);
				tpl::set('coupon-amount', $coupon['amount']);
				tpl::make('quittance-coupon');
			}
			if($order['comment']) {
				tpl::walk('comment', 'html2chars');
				tpl::walk('comment', 'nl2br');
				tpl::make('quittance-comment');
			}
			tpl::set('order-id', $order['id']);
			tpl::set('cart-sum', $order['total_sum']);
			//$address = $order['address'] ? str::address2str(json::decode($order['address'])) : '';
			if($order['address']) {
				if($address = json::decode($order['address'])) {
					// workaround for old orders with no city in address
					if(!isset($address['city']) || !$address['city']) {
						$address = array_merge(['city'=>$order['city']], $address);
					}
					$address = str::address2str($address);
				}
			}
			else $address = $order['city'];
			tpl::set('address', $address);
			tpl::set('date', ts2date('d.m.Y H:i', $order['ctime']));
			tpl::set('ptype-text', @$ptypes[$order['ptype']]['text'] ?: '--');
			$products = json::decode($order['cart_items']);
			usort($products, function($a, $b) {
				return $a['name'] > $b['name'] ? 1 : 0;
			});
			$n = 0;
			foreach($products as $product) {
				$n++;
				tpl::push($product);
				if($product['id'] == 540) tpl::set('manufacturer', '');
				tpl::set('n', $n);
				tpl::set('product-sum', $product['price'] * $product['quantity']);
				tpl::set('stock', $product['stock'] > 2 ? 'С' : '');
				if($product['option_name']) {
					tpl::set('option_name', ' - '.$product['option_name']);
				}
				tpl::make('quittance-product');
			}
			tpl::set('total-sum', $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon']);
			// barcode
			$order_code = str_pad($order['id'], 5, 0, STR_PAD_LEFT);
			$date_code = date('Ymd', strtotime($order['ctime']));
			$barcode = $date_code . $order_code;
			tpl::set('order-barcode', $barcode);
			tpl::make('quittance-order');
			tpl::clear('quittance-coupon', 'quittance-comment', 'quittance-product');
		}
		echo tpl::make('quittance');
		exit;
	}
	
	static function analytics() {
		$sort = [
			0 => function($a, $b) {
				return $a['sell_sum'] > $b['sell_sum'] ? -1 : 1;
			},
			1 => function($a, $b) {
				return $a['sell_sum'] < $b['sell_sum'] ? -1 : 1;
			},
			2 => function($a, $b) {
				return $a['quantity'] > $b['quantity'] ? -1 : 1;
			},
			3 => function($a, $b) {
				return $a['quantity'] < $b['quantity'] ? -1 : 1;
			},
			4 => function($a, $b) {
				return $a['name'] > $b['name'] ? -1 : 1;
			},
			5 => function($a, $b) {
				return $a['name'] < $b['name'] ? -1 : 1;
			}
		];
		$sort_select = absint(GET('sort'));
		$paid_select = GET('paid') ?: [];
		$status_select = GET('status') ?: [];
		array_walk($paid_select, 'intval');
		array_walk($status_select, 'intval');
		$start_date = GET('start_date');
		$end_date = GET('end_date');
		$sql = 'select cart_items, bonus, coupon, cumulative from orders where user_id != 1';
		$params = [];
		if(!$start_date) {
			$start_date = date('Y-m-d', strtotime('today - 1 month'));
		}
		$datetime = date('Y-m-d H:i:s', strtotime($start_date));
		$sql .= ' and ctime >= ?';
		$params[] = $datetime;
		tpl::set('start_date', $start_date);
		if($end_date) {
			$datetime = date('Y-m-d H:i:s', strtotime($end_date));
			$sql .= ' and ctime <= ?';
			$params[] = $datetime;
			tpl::set('end_date', $end_date);
		}
		if($status_select) {
			$sql .= ' and status in('.implode(',', $status_select).')';
		}
		if($paid_select) {
			$sql .= ' and paid in('.implode(',', $paid_select).')';
		}
		db::query($sql, $params);
		$items = [];
		// $orders = db::fetchAll();
		$bonus_sum = $coupon_sum = $cumulative_sum = 0;
		while($order = db::fetchArray()) {
			// print_r($order);
			$cart_items = json::decode($order['cart_items']);
			if(!$cart_items) continue;
			$bonus_sum += $order['bonus'];
			$coupon_sum += $order['coupon'];
			$cumulative_sum += $order['cumulative'];
			foreach($cart_items as $item) {
				$key = $item['id'];
				if($item['option_id']) $key .= '-'.$item['option_id'];
				if(isset($items[$key])) {
					$items[$key]['quantity'] += $item['quantity'];
					$items[$key]['sell_sum'] += $item['price'] * $item['quantity'];
				}
				else {
					$items[$key] = $item;
					$items[$key]['sell_sum'] = $item['price'] * $item['quantity'];
				}
			}
		}
		usort($items, $sort[$sort_select]);
		$total_sell_sum = 0;
		$total_quantity_sum = 0;
		foreach($items as $product) {
			tpl::push($product);
			tpl::set('product-href', '/product/'.$product['url_name']);
			if($product['option_id']) {
				tpl::append('product-href', '?option_id='.$product['option_id']);
			}
			if($product['option_name']) tpl::set('option_name', '- '.$product['option_name']);
			tpl::make('admin-analytics-product');
			$total_sell_sum += $product['sell_sum'];
			$total_quantity_sum += $product['quantity'];
		}
		
		make_title('Аналитика продаж');
		$status = json::get('order-status');
		$paid = [0 => 'Не оплачен', 1 => 'Оплачен'];
		foreach($status as $id => $name) {
			tpl::set('checkbox-class', 'cb-status');
			tpl::set('checkbox-name', 'status[]');
			tpl::set('checkbox-text', $name);
			tpl::set('checkbox-value', $id);
			tpl::set('checkbox-checked', in_array($id, $status_select) ? ' checked' : '');
			tpl::make('checkbox');
		}
		tpl::set('checkbox-fieldset-class', 'status-fieldset');
		tpl::make('checkbox-fieldset', 'status-fieldset');
		tpl::clear('checkbox');
		foreach($paid as $id => $name) {
			tpl::set('checkbox-class', 'cb-paid');
			tpl::set('checkbox-name', 'paid[]');
			tpl::set('checkbox-text', $name);
			tpl::set('checkbox-value', $id);
			tpl::set('checkbox-checked', in_array($id, $paid_select) ? ' checked' : '');
			tpl::make('checkbox');
		}
		tpl::set('checkbox-fieldset-class', 'paid-fieldset');
		tpl::make('checkbox-fieldset', 'paid-fieldset');
		tpl::clear('checkbox');
		
		$qs_args = qs::$args;
		tpl::set('product-sort-href', qs::set('sort', $sort_select == 4 ? 5 : 4));
		tpl::set('quantity-sort-href', qs::set('sort', $sort_select == 2 ? 3 : 2));
		tpl::set('price-sort-href', qs::set('sort', $sort_select == 0 ? 1 : 0));
		qs::$args = $qs_args;
		tpl::set('total_bonus_sum', $bonus_sum);
		tpl::set('total_coupon_sum', $coupon_sum);
		tpl::set('total_cumulative_sum', $cumulative_sum);
		tpl::set('total_diff_sum', $total_sell_sum - $cumulative_sum - $bonus_sum - $coupon_sum);
		tpl::set('date-form-action', $_SERVER['REQUEST_URI']);
		tpl::set('total_sell_sum', $total_sell_sum);
		tpl::set('total_quantity_sum', $total_quantity_sum);
		tpl::make('admin-analytics', 'main');
	}
	
	static function upload_akt_vozvrata() {
		if(!isset($_FILES['act_file'])) die('No file!');
		if(!$file = uploadFile($_FILES['act_file'], FILES.'/temp/', 'sl_act_vozvrata.xls')) die('Error');
		include 'PHPExcel/IOFactory.php';
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($file);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($file);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();
		db::prepare('update orders set delivery_price_real = :delivery_price_real where id = :id');
		//  Loop through each row of the worksheet in turn
		for ($row = 1; $row <= $highestRow; $row++){ 
			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE)[0];
			// print_r($rowData);
			if(!isset($rowData[2]) || !isset($rowData[20])) continue;
			if(!$order_id = intval($rowData[2])) continue;
			if(strlen($order_id) !== 4) continue;
			if(!$delivery_price_real = floatval($rowData[20])) continue;
			db::set(['id' => $order_id, 'delivery_price_real' => $delivery_price_real]);
			db::execute();
		}
		die('ok');
	}
	
	static function ordersStat() {
		if(REQUEST_METHOD == 'POST') {
			return self::upload_akt_vozvrata();
		}
		$sort = [
			0 => 'ctime desc',
			1 => 'ctime asc',
			2 => 'dtime desc',
			3 => 'dtime asc',
			4 => 'user_id desc',
			5 => 'user_id asc',
			6 => 'total_sum desc',
			7 => 'total_sum asc',
			8 => 'bonus desc',
			9 => 'bonus asc',
			10 => 'coupon desc',
			11 => 'coupon asc',
			12 => 'cumulative desc',
			13 => 'cumulative asc',
			14 => 'city asc',
			15 => 'city desc',
			16 => 'delivery_price_real desc',
			17 => 'delivery_price_real asc',
			18 => 'order_sum desc',
			19 => 'order_sum asc',
			20 => 'id desc',
			21 => 'id asc',
			22 => 'dtype asc',
			23 => 'dtype desc',
			24 => 'rk_fee desc',
			25 => 'rk_fee asc'
		];
		
		$EXPORT = (bool)GET('export');
		
		$sql = 'select count(*) from orders o where true';
		$select_sql = '*, (select sum(sum) from robokassa_ex where order_id = o.id) as rk_fee';
		$params = [];
		$sort_select = absint(GET('sort'));
		$paid_select = GET('paid') ?: [];
		$status_select = GET('status') ?: [];
		$dtype_select = GET('dtype') ?: [];
		array_walk($paid_select, 'intval');
		array_walk($status_select, 'intval');
		array_walk($dtype_select, 'intval');
		if($status_select) {
			$sql .= ' and status in('.implode(',', $status_select).')';
		}
		if($paid_select) {
			$sql .= ' and paid in('.implode(',', $paid_select).')';
		}
		if($dtype_select) {
			$sql .= ' and dtype in('.implode(',', $dtype_select).')';
		}
		$start_date = GET('start_date') ?: date('Y-m-d', strtotime('now - 7 days'));
		$datetime = date('Y-m-d H:i:s', strtotime($start_date));
		$sql .= ' and ctime >= ?';
		$params[] = $datetime;
		tpl::set('start_date', date('Y-m-d', strtotime($start_date)));
		if($end_date = GET('end_date')) {
			$datetime = date('Y-m-d H:i:s', strtotime($end_date));
			$sql .= ' and ctime <= ?';
			$params[] = $datetime;
			tpl::set('end_date', date('Y-m-d', strtotime($end_date)));
		}
		db::query($sql, $params);
		$num_orders = db::fetchSingle();
		tpl::set('num_orders', $num_orders);
		
		$sql = str_replace('count(*)', $select_sql, $sql);
		$sql .= ' order by '.$sort[$sort_select];
		db::query($sql, $params);
		$orders = db::fetchAll();
		
		$bonus_sum = $coupon_sum = $cumulative_sum = $delivery_sum = 0;
		
		$total_sum_sum = 0;
		$total_items_count = 0;
		$total_order_sum = 0;
		$total_buyin_sum = 0;
		$total_rk_fee_sum = 0;
		
		if($EXPORT) {
			$statfile = FILES.'/orders-stat.csv';
			$f = fopen($statfile, 'w+');
			fputcsv($f, ['id', 'Дата', 'Доставлен', 'Покупатель', 'Единиц товаров', 'Заказ', 'Закуп товаров', 'Бонусы', 'Купон', 'Накоп. скидка', 'Доставка', 'Комиссия РК', 'Итого', 'Город']);
		}
		
		foreach($orders as $order) {
			if($order['dtype'] == 1 && !$order['delivery_price_real']) {
				$cityname = mb_strtolower($order['city'], 'utf-8');
				$strpos = strpos($cityname, 'москва');
				if($strpos !== false && $strpos < 3) {
					$order['delivery_price_real'] = 250;
				}
			}
			tpl::push($order);
			if(!$order['delivery_price_real']) tpl::set('delivery_price_real', '');
			tpl::set('client', $order['user_id'] ? '<a href="/admin/clients/'.$order['user_id'].'">'.$order['firstname'].' '.$order['lastname'].'</a>' : $order['firstname'].' '.$order['lastname']);
			tpl::set('bonus', $order['bonus'] ? $order['bonus'].'р.' : '--');
			tpl::set('coupon', $order['coupon'] ? $order['coupon'].'р.' : '--');
			tpl::set('cumulative', $order['cumulative'] ? $order['cumulative'].'р.' : '--');
			$dtype_text = '--';
			if($order['dtype'] == 1) {
				$delivery_info = mb_strtolower($order['delivery_info'], 'utf8');
				$dtype_text = false !== strpos($delivery_info, 'самовывоз') ? 'ПВЗ' : 'Курьером';
			}
			elseif($order['dtype'] == 2) {
				$dtype_text = 'Самовывоз';
			}
			elseif($order['dtype'] == 3) {
				$delivery_info = mb_strtolower($order['delivery_info'], 'utf8');
				$dtype_text = false !== strpos($delivery_info, 'посылка') ? 'ПР посылка' : 'ПР 1 класс';
			}
			tpl::set('dtype-text', $dtype_text);
			$cart_items = json::decode($order['cart_items']);
			$items_count = 0;
			foreach($cart_items as $item) {
				$items_count += $item['quantity'];
			}
			tpl::set('items_count', $items_count);
			$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['coupon'] - $order['cumulative'];
			$buyin_sum = order::get_buyin_sum($order['cart_items']);
			$total_sum = $order_sum - $order['delivery_price_real'] - $buyin_sum - $order['rk_fee'];
			tpl::set('order_sum', $order_sum);
			tpl::set('buyin_sum', $buyin_sum);
			tpl::set('total_sum', $total_sum);
			tpl::make('admin-order-stat');
			$bonus_sum += $order['bonus'];
			$coupon_sum += $order['coupon'];
			$cumulative_sum += $order['cumulative'];
			$total_sum_sum += $total_sum;
			$total_items_count += $items_count;
			if($order['status'] != 5) $delivery_sum += $order['delivery_price_real'];
			$total_order_sum += $order_sum;
			$total_buyin_sum += $buyin_sum;
			$total_rk_fee_sum += $order['rk_fee'];
			$data = [
				$order['id'], $order['ctime'], $order['dtime'], $order['firstname'].' '.$order['lastname'], $items_count, $order_sum, '-'.$buyin_sum, '-'.$order['bonus'], '-'.$order['coupon'], '-'.$order['cumulative'], '-'.$order['delivery_price_real'], '-'.$order['rk_fee'], $total_sum, $order['city']
			];
			if($EXPORT) fputcsv($f, $data);
		}
		
		tpl::set('avg-sum', round($total_order_sum / $num_orders));
		
		if($EXPORT) {
			fclose($f);
			header('Content-Type: text/csv', true);
			header('Content-Transfer-Encoding: Binary', true);
			header('Content-Disposition: attachment; filename="'.basename($statfile).'"');
			readfile($statfile);
			exit;
		}
		
		make_title('Статистика заказов');
		$status = json::get('order-status');
		$paid = [0 => 'Не оплачен', 1 => 'Оплачен'];
		$dtypes = [1 => 'Курьер', 3 => 'Почта России', 2 => 'Офис', 4 => 'PickPoint', 5 => 'ShopLogistics'];
		foreach($status as $id => $name) {
			tpl::set('checkbox-class', 'cb-status');
			tpl::set('checkbox-name', 'status[]');
			tpl::set('checkbox-text', $name);
			tpl::set('checkbox-value', $id);
			tpl::set('checkbox-checked', in_array($id, $status_select) ? ' checked' : '');
			tpl::make('checkbox');
		}
		tpl::set('checkbox-fieldset-class', 'status-fieldset');
		tpl::make('checkbox-fieldset', 'status-fieldset');
		tpl::clear('checkbox');
		foreach($paid as $id => $name) {
			tpl::set('checkbox-class', 'cb-paid');
			tpl::set('checkbox-name', 'paid[]');
			tpl::set('checkbox-text', $name);
			tpl::set('checkbox-value', $id);
			tpl::set('checkbox-checked', in_array($id, $paid_select) ? ' checked' : '');
			tpl::make('checkbox');
		}
		tpl::set('checkbox-fieldset-class', 'paid-fieldset');
		tpl::make('checkbox-fieldset', 'paid-fieldset');
		tpl::clear('checkbox');
		foreach($dtypes as $dtype => $name) {
			tpl::set('checkbox-class', 'cb-dtype');
			tpl::set('checkbox-name', 'dtype[]');
			tpl::set('checkbox-text', $name);
			tpl::set('checkbox-value', $dtype);
			tpl::set('checkbox-checked', in_array($dtype, $dtype_select) ? ' checked' : '');
			tpl::make('checkbox');
		}
		tpl::set('checkbox-fieldset-class', 'dtype-fieldset');
		tpl::make('checkbox-fieldset', 'dtype-fieldset');
		tpl::clear('checkbox');
		
		tpl::set('ctime-sort-href', qs::set('sort', $sort_select == 0 ? 1 : 0));
		tpl::set('dtime-sort-href', qs::set('sort', $sort_select == 2 ? 3 : 2));
		tpl::set('user-sort-href', qs::set('sort', $sort_select == 4 ? 5 : 4));
		tpl::set('sum-sort-href', qs::set('sort', $sort_select == 6 ? 7 : 6));
		tpl::set('bonus-sort-href', qs::set('sort', $sort_select == 8 ? 9 : 8));
		tpl::set('coupon-sort-href', qs::set('sort', $sort_select == 10 ? 11 : 10));
		tpl::set('cumulative-sort-href', qs::set('sort', $sort_select == 12 ? 13 : 12));
		tpl::set('city-sort-href', qs::set('sort', $sort_select == 14 ? 15 : 14));
		tpl::set('dprice-sort-href', qs::set('sort', $sort_select == 16 ? 17 : 16));
		tpl::set('ordersum-sort-href', qs::set('sort', $sort_select == 18 ? 19 : 18));
		tpl::set('id-sort-href', qs::set('sort', $sort_select == 20 ? 21 : 20));
		tpl::set('dtype-sort-href', qs::set('sort', $sort_select == 22 ? 23 : 22));
		tpl::set('rk_fee-sort-href', qs::set('sort', $sort_select == 24 ? 25 : 24));
		qs::reset();
		
		tpl::set('total_bonus_sum', $bonus_sum);
		tpl::set('total_coupon_sum', $coupon_sum);
		tpl::set('total_cumulative_sum', $cumulative_sum);
		tpl::set('total_sum_sum', $total_sum_sum);
		tpl::set('total_items_count', $total_items_count);
		tpl::set('total_delivery_price', $delivery_sum);
		tpl::set('total_order_sum', $total_order_sum);
		tpl::set('total_buyin_sum', $total_buyin_sum);
		tpl::set('total_rk_fee_sum', $total_rk_fee_sum);
		
		tpl::set('export-href', qs::set('export', 'true'));
		
		qs::reset();
		
		tpl::make('admin-order-stats', 'main');
	}
	
	static function ordersStat2() {
		make_title('Краткая статистика заказов');
		$sql = 'select *, date(dtime) as ddate from orders where status = 4 and paid = 1';
		$start_date = GET('start_date') ?: date('Y-m-d', strtotime('now - 7 days'));
		$datetime = date('Y-m-d H:i:s', strtotime($start_date));
		$sql .= ' and dtime >= ?';
		$params[] = $datetime;
		tpl::set('start_date', date('Y-m-d', strtotime($start_date)));
		if($end_date = GET('end_date')) {
			$datetime = date('Y-m-d H:i:s', strtotime($end_date));
			$sql .= ' and dtime <= ?';
			$params[] = $datetime;
			tpl::set('end_date', date('Y-m-d', strtotime($end_date)));
		}
		$dtypes = [1 => 'Курьер', 2 => 'Офис', 3 => 'ПР', 4 => 'PickPoint', 5 => 'ShopLogistics'];
		$dtype_selected = GET('dtype') ?: [];
		foreach($dtypes as $dtype => $text) {
			tpl::set('checkbox-name', 'dtype[]');
			tpl::set('checkbox-text', $text);
			tpl::set('checkbox-value', $dtype);
			tpl::set('checkbox-checked', in_array($dtype, $dtype_selected) ? ' checked' : '');
			tpl::make('checkbox', 'dtype-checkbox');
		}
		if($dtype_selected) {
			$sql .= ' and dtype in('.implode(',', $dtype_selected).')';
			foreach($dtype_selected as $dtype) {
				tpl::set('dtype-'.$dtype.'-checked', ' checked');
			}
		}
		$sql .= ' order by ifnull(dtime, ctime) desc';
		db::query($sql, $params);
		tpl::set('orders-count', db::count());
		$totals = ['order-sum-total' => 0, 'delivery-real-total' => 0, 'orders-total' => 0, 'buyin-total' => 0];
		while($order = db::fetchArray()) {
			$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['coupon'] - $order['cumulative'];
			$buyin_sum = order::get_buyin_sum($order['cart_items']);
			$order_total = $order_sum - $order['delivery_price_real'] - $buyin_sum;
			tpl::set('id', $order['id']);
			tpl::set('ddate', $order['ddate']);
			tpl::set('order-sum', $order_sum);
			tpl::set('buyin-sum', $buyin_sum);
			tpl::set('delivery-real', $order['delivery_price_real'] ?: '');
			tpl::set('delivery-info', $order['delivery_info']);
			tpl::set('order-total', $order_total);
			tpl::set('dtype-text', $order['dtype'] ? $dtypes[$order['dtype']] : '');
			$totals['order-sum-total'] += $order_sum;
			$totals['delivery-real-total'] += $order['delivery_price_real'];
			$totals['orders-total'] += $order_total;
			$totals['buyin-total'] += $buyin_sum;
			tpl::make('admin-order-stats2-row');
		}
		tpl::push($totals);
		tpl::make('admin-order-stats2', 'main');
	}
	
	static function orders_stat_graph() {
		$date_start = GET('date_start') ?: date('Y-m-d', strtotime('1 week ago'));
		$date_end = GET('date_end') ?: date('Y-m-d');
		$group_by = absint(GET('group_by'));
		$group_params = [
			0 => 'день',
			1 => 'неделя',
			2 => 'месяц'
		];
		foreach($group_params as $value => $text) {
			tpl::set('option-text', $text);
			tpl::set('option-value', $value);
			tpl::set('option-selected', $value == $group_by ? ' selected' : '');
			tpl::make('option', 'option-group-by');
		}
		tpl::set('date_start', $date_start);
		tpl::set('date_end', $date_end);
		tpl::make('admin-analytics-graph', 'main');
	}
	
	static function graph_json() {
		$date_start = GET('date_start') ?: date('Y-m-d', strtotime('1 week ago'));
		$date_end = GET('date_end') ?: date('Y-m-d');
		$group_by = absint(GET('group_by'));
		$group_params = [
			0 => 'date(ctime)',
			1 => 'concat(weekofyear(ctime),", ",year(ctime))',
			2 => 'concat(monthname(ctime)," ", year(ctime))'
		];
		$buyin_select = [
			0 => '(select sum(sum) from bills where date(ctime) = date(o.ctime))',
			1 => '(select sum(sum) from bills where year(ctime) = year(CURRENT_TIMESTAMP) and weekofyear(rtime) = weekofyear(o.ctime))',
			2 => '(select sum(sum) from bills where year(ctime) = year(CURRENT_TIMESTAMP) and month(ctime) = month(o.ctime))'
		];
		$sql = 'select '.$group_params[$group_by].' as group_by, dayofweek(ctime) as dayofweek, total_sum, delivery_price, delivery_price_real, bonus, coupon, cumulative, '.$buyin_select[$group_by].' as price_buyin from orders o where status in(0,2,3,8,4) and (date(ctime) between :date_start and :date_end) order by ctime';
		$params = ['date_start' => $date_start, 'date_end' => $date_end];
		db::query($sql, $params);
		$orders = db::fetchAll();
		$orders = array_group($orders, 'group_by');
		// $style = new stdClass();
		// $style->role = 'style';
		// $data = [['День', 'Заказы', 'Отгружено', 'Факторинг', 'Закуп', 'Доставка', 'Прибыль']];
		$data = [['День', 'Заказы', 'Закуп', 'Доставка']];
		foreach($orders as $group => $list) {
			$sum = $outbound = $factoring = $buyin = $delivery = $income = 0;
			foreach($list as $order) {
				$sum += ($order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['coupon'] - $order['cumulative']);
				$delivery += $order['delivery_price_real'];
				$buyin = $order['price_buyin'];
			}
			$style = '';
			if($group_by == 0) {
				$dt = DateTime::createFromFormat('Y-m-d', $group);
				$group = $dt->format('D, j M');
				// $style = in_array($order['dayofweek'], [7,1]) ? 'color: #ff0000' : 'color: #0000ff';
			}
			elseif($group_by == 1) {
				$group = 'Нед '.$group;
			}
			elseif($group_by == 2) {
				$group = ''.$group;
			}
			$data[] = [$group, $sum, intval($buyin), $delivery];
		}
		header('Content-Type: application/json');
		exit(json::encode($data));
	}
	
	static function attachOrder($path, $orderId) {
		$orderId = absint($orderId);
		$userId = absint(REQUEST('user_id'));
		tpl::set('order-id', $orderId);
		if($orderId && $userId) {
			db::query('update orders set user_id = ? where id = ?', $userId, $orderId);
			db::query('update orders o inner join user u on u.id = o.user_id set o.firstname = u.firstname, o.lastname = u.lastname, o.email = u.email, o.phone = u.phone where o.id = ?', $orderId);
			die('ok');
		}
		// show user list and user search form
		if($search = trim(GET('search'))) {
			$str = '%'.$search.'%';
			$sql = 'select * from user where email like ? or firstname like ? or lastname like ?';
			db::query($sql, $str, $str, $str);
			while($user = db::fetchArray()) {
				tpl::push($user);
				tpl::make('admin-order-attach-user');
			}
			die(tpl::make('admin-order-attach-users'));
		}
		die(tpl::make('admin-order-attach-search-form'));
	}
	
	static function wishlist() {
		make_title('Товары в Wishlist');
		$sort_select = absint(GET('sort'));
		$LIMIT = 40;
		$sort = [
			0 => 'added desc',
			1 => 'added asc',
			2 => 'product_quantity asc, option_quantity asc',
			3 => 'product_quantity desc, option_quantity desc',
			4 => 'product_name asc, option_name asc',
			5 => 'product_name desc, option_name desc'
		];
		$params = ['offset'=>offset($LIMIT), 'limit' => $LIMIT];
		$sql = 'select count(*) from wishlist w';
		$numRows = db::querySingle($sql);
		$select = '*, count(*) as added, (select name from product where id = w.product_id) as product_name, (select url_name from product where id = w.product_id) as product_url, (select name from product_option where id = w.option_id) as option_name, (select quantity from product where id = w.product_id) as product_quantity, (select quantity from product_option where id = w.option_id) as option_quantity';
		$sql = str_replace('count(*)', $select, $sql);
		$sql .= ' group by product_id, option_id order by '.$sort[$sort_select].' limit :offset, :limit';
		db::query($sql, $params);
		while($wish = db::fetchArray()) {
			tpl::push($wish);
			if($wish['option_id']) {
				tpl::set('option-name', ' - '.$wish['option_name']);
			}
			else {
				tpl::set('option_quantity', '-');
				tpl::clear('option-name');
			}
			tpl::make('admin-wishlist-item');
		}
		$qs_args = qs::$args;
		tpl::set('added-sort-href', qs::set('sort', $sort_select == 0 ? 1 : 0));
		tpl::set('qty-sort-href', qs::set('sort', $sort_select == 2 ? 3 : 2));
		tpl::set('name-sort-href', qs::set('sort', $sort_select == 4 ? 5 : 4));
		qs::$args = $qs_args;
		shop::paginator($numRows, $LIMIT, false, 'paginator left');
		tpl::make('admin-wishlist-items');
		tpl::make('admin-wishlist', 'main');
	}
	
	static function createMSOrder($orderId = 0) {
		if(!$orderId) die('ORDER ID REQUIRED');
		if(!$order = db::querySingle('select * from orders where id = '.$orderId, true)) die('ORDER NOT FOUND');
		$address = $order['address'] ? json::decode($order['address']) : [];
		if(!isset($address['street']) || !$address['street']) $address['street'] = '';
		if(!isset($address['building']) || !$address['building']) $address['building'] = '';
		$order_sum = $order['total_sum'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
		$data = [
			'recipient_first_name' => $order['firstname'],
			'recipient_last_name' => $order['lastname'],
			'recipient_phone' => $order['phone'],
			'deliverypoint_street' => $address['street'],
			'deliverypoint_house' => $address['building'],
			'order_payment_method' => 1,
			'order_items' => [[
					'orderitem_name' => 'Косметика и парфюмерия',
					'orderitem_quantity' => 1,
					'orderitem_cost' => $order_sum
				]
			],
			'order_weight' => 1,
			'order_length' => 15,
			'order_width' => 10,
			'order_height' => 10,
			'deliverypoint_city' => $order['city'],
			'order_delivery_cost' => 0,
			'delivery_delivery' => 152,
			'delivery_direction' => 40,
			'delivery_price' => 37814,
			'delivery_pickuppoint' => 0,
			'delivery_to_ms_warehouse' => '0'
		];
		$order = json::encode($data, JSON_UNESCAPED_UNICODE);
		// die($order);
		tpl::set('order-data', $order);
		tpl::set('id', $orderId);
		$html = tpl::make('admin-ms-order');
		$html .= '<div class="bold center" style="padding:20px;width:300px;height:1em;" id="ms-order-status">Отправляем данные...</div>';
		die(MOBILE ? 'ok' : $html);
	}
	
	static function createPPOrder($order_id = 0) {
		if(!$order_id) die('ORDER ID REQUIRED');
		if(!$order = db::querySingle('select * from orders where id = '.$order_id, true)) die('ORDER NOT FOUND');
		$response_data = ppapi::send_order($order);
		if(!empty($response_data['CreatedSendings'])) {
			$invoice_number = $response_data['CreatedSendings'][0]['InvoiceNumber'];
			$result = 'Заказ успешно зарегистрирован. Код отправления '.$invoice_number.' записан.';
		}
		elseif(!empty($response_data['RejectedSendings'])) {
			$result = $response_data['RejectedSendings'][0]['ErrorMessage'];
		}
		else {
			$result = 'Неизвестная ошибка. Не удалось разобрать ответ сервера.';
		}
		die($result);
	}
	
	static function supplier_products() {
		$action = REQUEST('action');
		if($action == 'missha-xls') {
			return self::upload_missha_xls();
		}
		make_title('Товары поставщика');
		$sort = [
			0 => 'filename asc, quantity desc',
			1 => 'filename desc, quantity desc',
			2 => 'barcode asc, quantity desc',
			3 => 'barcode desc, quantity desc',
			4 => 'quantity desc, filename',
			5 => 'quantity asc, filename',
			6 => 'name asc',
			7 => 'name desc',
			8 => 'price_usd desc',
			9 => 'price_usd asc'
		];
		$sort_select = absint(GET('sort'));
		$min_quantity = absint(GET('min_quantity'));
		
		tpl::set('filename-sort-href', qs::set('sort', $sort_select == 0 ? 1 : 0));
		tpl::set('barcode-sort-href', qs::set('sort', $sort_select == 2 ? 3 : 2));
		tpl::set('quantity-sort-href', qs::set('sort', $sort_select == 4 ? 5 : 4));
		tpl::set('name-sort-href', qs::set('sort', $sort_select == 6 ? 7 : 6));
		tpl::set('price-sort-href', qs::set('sort', $sort_select == 8 ? 9 : 8));
		qs::reset();
		
		$sql = 'select count(*) from product_import pi where not exists(select 1 from product p where barcode = pi.barcode) and not exists(select 1 from product_option where barcode = pi.barcode)';
		if($min_quantity) {
			tpl::set('min-quantity-checked', ' checked');
			$sql .= ' and quantity >= '.$min_quantity;
		}
		else {
			$min_quantity = 5;
		}
		tpl::set('min-quantity', $min_quantity);
		$count = db::querySingle($sql);
		$sql = str_replace('count(*)', '*', $sql);
		$sql .= ' order by ' . $sort[$sort_select];
		db::query($sql);
		foreach(db::fetchAll() as $product) {
			tpl::push($product);
			tpl::make('admin-supplier-product');
		}
		tpl::set('row-count', $count);
		tpl::make('admin-supplier-products', 'main');
	}
	
	static function upload_missha_xls() {
		if(!isset($_FILES['missha_file'])) die('Нет файла для загрузки');
		if(!$file = uploadFile($_FILES['missha_file'], FILES.'/import/', 'missha.xlsx')) {
			die('Не удалось загрузить файл');
		}
		exit('ok');
	}
	
	static function edit_free_shipping() {
		if(!account::$admin) die('ACCESS DENIED');
		$html = trim(POST('html'));
		file_put_contents(FILES.'/free-shipping.html', $html);
		die('ok');
	}
	
	static function autofill_product_name($name, $short_text) {
		$data['url_name'] = str::str2url($short_text.' '.$name);
		$data['page_title'] = $short_text.' '.$name;
		return json::encode($data);
	}
	
	static function mysqldump() {
		if(!in_array(account::$info['id'], [1,3])) die('ACCESS DENIED');
		$db_config = x::config('DB');
		$cmd = 'mysqldump -u'.$db_config['user'].' -p'.$db_config['password'].' '.$db_config['dbname'].' > /home/yamibox/www/files/yamibox.sql';
		exec($cmd);
		$file = '/home/yamibox/www/files/yamibox.sql.bz2';
		exec('bzip2 -f /home/yamibox/www/files/yamibox.sql');
		header('Content-Type: application/x-bzip2');
		header('Content-Transfer-Encoding: Binary', true);
		header('Content-Disposition: attachment; filename="'.basename($file).'"');
		readfile($file);
	}
	
	static function slider($path, $action = NULL) {
		if($action == 'update')
			return slider::update();
		if($action == 'remove')
			return slider::remove();
		if($action == 'add')
			return slider::add();
		make_title('Слайдер');
		$slides = slider::get();
		foreach($slides as $slide) {
			tpl::push($slide);
			tpl::make('admin-slider-item');
		}
		tpl::make('admin-slider', 'main');
	}
	
	static function print_orders() {
		$ptype = json::get('ptype');
		$status = json::get('order-status');
		$print_type = GET('type');
		
		if($print_type == 'pr') {
			$tpl_orders = 'print-orders-pr';
			$tpl_order = 'print-order-pr';
		}
		elseif($print_type == 'courier') {
			$tpl_orders = 'print-orders';
			$tpl_order = 'print-order';
		}
		else {
			die('WRONG PRINT TYPE');
		}
		tpl::load($tpl_orders);
		
		$params = self::get_orders_params();
		$params['limit'] = 0;
		list($total_orders, $orders) = order::getList($params);
		$n = 1;
		foreach($orders as $order) {
			tpl::push($order);
			tpl::set('n', $n);
			$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
			tpl::set('order-sum', $order_sum);
			tpl::set('order-sum-pay', $order['paid'] ? 0 : $order_sum);
			tpl::set('phone', str::filter_phone_number($order['phone']) ?: '');
			if(strpos($order['city'], ',')) {
				list($city, $region) = explode(',', $order['city']);
				tpl::set('city', trim($city));
				tpl::set('region', trim($region));
			}
			if(mb_strtolower($order['city'], 'utf-8') == 'москва') {
				tpl::set('region', 'Московская область');
			}
			$address = json::decode($order['address']);
			tpl::set('address', str::address2str($address, false));
			tpl::set('zipcode', @$address['zipcode']);
			tpl::set('order-status', @$status[$order['status']] ?: '???');
			tpl::set('order-paid', $order['paid'] ? 'Оплачен' : 'Не оплачен');
			tpl::set('ptype-text', @$ptype[$order['ptype']]['text'] ?: '???');
			tpl::make($tpl_order);
			$n++;
		}
		exit(tpl::make($tpl_orders));
	}
	
	static function download_example_csv($path, $filename) {
		$file = FILES.'/temp/'.$filename;
		if(!file_exists($file)) {
			status('404 Page Not Found');
			die('404 Page Not Found');
		}
		header('Content-Type: text/csv', true);
		header('Content-Transfer-Encoding: Binary', true);
		header('Content-Disposition: attachment; filename="'.basename($filename).'"');
		readfile($file);
		exit;
	}
	
	static function product_groups() {
		if(REQUEST_METHOD == 'POST') {
			$rel = trim(POST('rel'));
			$group_name = trim(POST('group_name'));
			if(!$rel) die('Не указан идентификатор группы!');
			if($group_name) db::query('replace into product_groups set rel = ?, name = ?', $rel, $group_name);
			else db::query('delete from product_groups where rel = ?', $rel);
			die('ok');
		}
		make_title('Группы товаров');
		db::query('select rel, (select name from product_groups where rel = p.rel) as group_name, group_concat(id) as id, group_concat(name) as product_name, group_concat(url_name) as url_name from product p where rel > 0 and status >= 0 group by rel order by rel desc');
		while($group = db::fetchArray()) {
			tpl::set('group_name', $group['group_name']);
			tpl::set('rel', $group['rel']);
			$ids = explode(',', $group['id']);
			$product_names = explode(',', $group['product_name']);
			$url_names = explode(',', $group['url_name']);
			foreach($url_names as $key => $url_name) {
				tpl::set('product_name', $product_names[$key]);
				tpl::set('url_name', $url_name);
				tpl::make('admin-product-group-item');
				if(!isset($product_names[$key])) print_r($product_name);
			}
			tpl::make('admin-product-group');
			tpl::clear('admin-product-group-item');
		}
		tpl::make('admin-product-groups', 'main');
	}
	
	static function product_image() {
		ob_start();
		$action = REQUEST('action');
		if($action == 'delete') {
			if(!$filename = POST('filename')) die('Пустое имя файла!');
			@unlink(FILES.'/product/'.$filename);
			@unlink(FILES.'/product/thumbs/'.$filename);
		}
		if($action == 'add') {
			if(!$product_id = absint(POST('product_id'))) {
				$response = [
					'status' => 'error',
					'message' => 'Не указан ID товара'
				];
				die(json::encode($response));
			}
			$files = get_files(FILES.'/product/', '#^('.$product_id.')?(\-[0-9]+)?\.\w+$#i');
			$num = 0;
			foreach($files as $file) {
				$filename = finfo($file, 'filename');
				list($pid, $n) = explode('-', $filename);
				if($n > $num) $num = $n;
			}
			$img = new SimpleImage();
			foreach($_FILES['file']['tmp_name'] as $i => $filename) {
				$num++;
				$imgsize = getimagesize($filename);
				if(!preg_match('#image/(jpg|jpeg)#', $imgsize['mime'])) continue;
				//print_r($filename);
				//print_r($imgsize);
				$temp = explode(".", $_FILES["file"]["name"][$i]);
				$extension = end($temp);
				$new_name = $product_id.'-'.$num.'.'.$extension;
				$dest = FILES.'/product/'.$new_name;
				if(move_uploaded_file($_FILES["file"]["tmp_name"][$i], $dest)) {
					$img->load($dest)->resize(550, 550)->save($dest);
					$thumb = str_replace('/product/', '/product/thumbs/', $dest);
					$img->load($dest)->resize(200, 200)->save($thumb);
				}
				else {
					sprintf("Ошибка загрузки: %s\n", $_FILES["file"]["name"][$i]);
				}
			}
		}
		$out = ob_get_clean();
		die($out ?: 'ok');
	}
	
	static function sl_send_order($order_id) {
		if(!$order = db::querySingle('select * from orders where id = '.$order_id, true)) die('ORDER NOT FOUND');
		$result = shoplogistics::send_order($order);
		if(is_numeric($result)) die('ok');
		if(is_string($result)) die($result);
		$error = $result['answer']['error'] == 0 ? 'ok' : $result;
		die($error);
	}
	
	static function dguru_send_order($order_id) {
		if(!$order = db::querySingle('select * from orders where id = '.$order_id, true)) die('ORDER NOT FOUND');
		$result = dguru::send_order($order);
		if(preg_match('#^Error#', $result)) {
			die($result);
		}
		die('ok');
	}
	
	static function orderProductReplacement($order_id) {
		$pid = absint(REQUEST('pid'));
		$oid = absint(REQUEST('oid'));
		function make_product($product, $option = NULL) {
			tpl::set('product-href', '/product/'.$product['url_name']);
			tpl::set('product-img-src', product::getImage($product));
			tpl::set('pid', $product['id']);
			tpl::set('oid', 0);
			tpl::set('name', $product['name']);
			tpl::set('price', $product['price']);
			tpl::set('stock-text', $product['stock'] == 2 ? 'В офисе' : 'На складе');
			$text = br2nl($product['text']);
			$text = preg_replace('#<.+?>#s', '', $text);
			$text = str_replace('&nbsp;', ' ', $text);
			if(strlen($text) > 1000) {
				$text = substr($text, 0, 1000);
				$text .= ' [...]';
			}
			$text = nl2br($text);
			tpl::set('text', $text);
			if($product['price_old']) {
				tpl::set('price_old', $product['price_old']);
				tpl::set('price-old-class', '');
			}
			else {
				tpl::set('price-old-class', 'hidden');
			}
			if($option) {
				tpl::set('option-name', $option['name']);
				tpl::set('product-img-src', '/files/options/thumbs/'.$option['option_id'].'.jpg');
				tpl::append('product-href', '?option_id='.$option['id']);
				tpl::set('price', $option['price']);
				tpl::set('oid', $option['id']);
				tpl::set('stock-text', $option['stock'] == 2 ? 'В офисе' : 'На складе');
				if($option['price_old']) {
					tpl::set('price_old', $option['price_old']);
					tpl::set('price-old-class', '');
				}
				else {
					tpl::set('price-old-class', 'hidden');
				}
			}
			tpl::make('product-replace');
			tpl::clear('product-href');
		}
		db::query('select category_id from product_category where category_id not in(68,71,83) and product_id = ? order by category_id desc', $pid);
		$cats = $products = [];
		while($cid = db::fetchSingle()) {
			$cats[] = $cid;
		}
		foreach($cats as $cid) {
			db::query('select *, (select group_concat(id) from product_option where product_id = p.id) as option_ids from product p where status = 1 and stock > 1 and id in(select product_id from product_category where category_id = ?) and id != ? order by quantity desc, stock_quantity desc limit 5', [$cid, $pid]);
			while($product = db::fetchArray()) {
				$products[] = $product;
			}
			if(count($products) == 5) break;
		}
		$options_count = $pout = 0;
		$option_ids = [];
		foreach($products as $product) {
			if($product['option_ids']) {
				db::query('select * from product_option where status = 1 and stock > 1 and id in ('.$product['option_ids'].') order by quantity desc, stock_quantity desc limit 5');
				$options = db::fetchAll();
				foreach($options as $option) {
					make_product($product, $option);
					$pout++;
					if($pout == 5) break 2;
				}
			}
			else {
				// make product
				make_product($product);
				$pout++;
				if($pout == 5) break;
			}
		}
		$product = db::querySingle('select * from product where id = '.$pid, true);
		tpl::push($product);
		tpl::set('pid', $pid);
		tpl::set('oid', $oid);
		tpl::set('order-id', $order_id);
		tpl::set('colspan', $pout);
		$html = $pout ? tpl::make('order-product-replace', 'main') : '<span class="s3 bold">Не найдены товары для замены</span>';
		exit($html);
	}
	
	static function replaceOrderItem($order_id) {
		$order = db::querySingle('select cart_items, total_sum from orders where id = '.$order_id, true);
		$items = json::decode($order['cart_items']);
		$origin = POST('origin');
		$replacement = POST('replacement');
		$total_sum = 0;
		foreach($items as $key => $item) {
			$item['option_id'] = intval($item['option_id']);
			if($item['id'] == $origin['pid'] && $item['option_id'] == $origin['oid']) {
				$product = db::querySingle('select * from product where id = '.$replacement['pid'], true);
				if($replacement['oid'] > 0) {
					$option = db::querySingle('select * from product_option where id = '.$replacement['oid'], true);
				}
				else {
					$option = false;
				}
				$item = [
					'id' => $product['id'],
					'name' => $product['name'],
					'short_text' => $product['short_text'],
					'barcode' => $option ? $option['barcode'] : $product['barcode'],
					'price' => $option ? $option['price'] : $product['price'],
					'price_old' => $option ? $option['price_old'] : $product['price_old'],
					'quantity' => 1,
					'manufacturer' => $product['manufacturer'],
					'url_name' => $product['url_name'],
					'weight' => $option ? $option['weight'] : $product['weight'],
					'volume' => $option ? $option['volume'] : $product['volume'],
					'option_name' => $option ? $option['name'] : NULL,
					'option_id' => $option ? $option['id'] : NULL,
					'stock' => $option ? $option['stock'] : $product['stock']
				];
				$items[] = $item;
				unset($items[$key]);
				db::query('delete from order_noitem where order_id = ? and product_id = ? and option_id = ?', 
					$order_id, $origin['pid'], $origin['oid']
				);
			}
			$total_sum += $item['price'] * $item['quantity'];
		}
		$cart_items = json::encode($items);
		db::query('update orders set cart_items = ?, total_sum = ? where id = ?', $cart_items, $total_sum, $order_id);
		if(!db::count()) {
			echo "Ошибка!";
		}
		if(AJAX) die('ok');
		return redirect('/admin/orders/'.$order_id);
	}
	
	static function collecting() {
		$action = trim(REQUEST('action'));
		if($action == 'list') return self::collecting_order_list();
		if($action == 'add') return self::collecting_order_add();
		make_title('Сбор');
		tpl::make('admin-collecting', 'main');
	}
	
	static function collecting_order_add() {
		$code = trim(REQUEST('code'));
		// add order
		if(preg_match('#^201[456]\d{9,9}$#', $code)) {
			$order_num = substr($code, -5, 5);
			$order_id = intval($order_num);
			db::query('select 1 from collecting_orders where order_id = ?', $order_id);
			if(!$exists = db::fetchSingle()) {
				db::query('insert into collecting_orders set order_id = ?', $order_id);
			}
			exit('<p>Заказ '.$order_id.' добавлен</p>');
		}
		// add item
		elseif(preg_match('#^\d{13,13}$#', $code)) {
			db::query('select quantity from collecting_items where barcode = ?', $code);
			if($quantity = db::fetchSingle()) {
				$quantity++;
				db::query('update collecting_items set quantity = ? where barcode = ?', $quantity, $code);
			}
			else {
				db::query('insert into collecting_items set barcode = ?, quantity = 1', $code);
			}
			$_SESSION['last_code'] = ''.$code;
			exit('ok');
		}
		exit('???');
	}
	
	static function collecting_order_list() {
		db::query('select *, (case when dtype in(1,2) then dtype when dtype = 3 then 5 when dtype = 4 then 3 when dtype = 5 then 4 else 0 end) as sort_dtype from orders where (status = 2 and cstatus = 2) or id in(select order_id from collecting_orders) order by sort_dtype asc, total_sum desc');
		$orders = db::fetchAll();
		$citems = [];
		db::query('select * from collecting_items order by barcode');
		while($citem = db::fetchArray()) {
			$citems[$citem['barcode']] = $citem['quantity'];
		}
		$last_code = SESSION('last_code'); // последний добавленный шрих-код (товар)
		$order_add = NULL; // заказ, к которому был добавлен товар
		foreach($orders as $order) {
			$cart_items = json::decode($order['cart_items']);
			$order_collected = true;
			foreach($cart_items as $item) {
				tpl::push($item);
				tpl::set('stock', $item['stock'] > 2 ? 'С' : '');
				tpl::set('indicator-class', 'red');
				if($item['option_name']) tpl::append('name', ' - '.$item['option_name']);
				if($item['stock'] == 1) tpl::set('stock', '<span class="red">--</span>');
				$collected = 0;
				$barcode = ''.$item['barcode'];
				if(isset($citems[$barcode])) {
					$collected = $citems[$barcode];
					if($collected >= $item['quantity']) {
						$collected = $item['quantity'];
						tpl::set('indicator-class', 'green');
					}
					else {
						$order_collected = false;
					}
					if($citems[$barcode] > 0) {
						$citems[$barcode] -= $item['quantity'];
						if($barcode == $last_code && $citems[$barcode] <= 0) $order_add = $order;
					}
				}
				if($collected < 0) $collected = 0;
				tpl::make('collected-indicator');
				tpl::set('collected', $collected);
				tpl::make('collecting-order-item');
				tpl::clear('collected-indicator');
			}
			if($order_collected) {
				// заказ собран
				if(order::complete_collecting($order)) continue;
			}
			$order_sum = $order['total_sum'] + $order['delivery_price'] - $order['bonus'] - $order['cumulative'] - $order['coupon'];
			tpl::set('order-sum', $order_sum);
			tpl::set('order-id', $order['id']);
			tpl::make('collecting-order');
			tpl::clear('collecting-order-item');
		}
		foreach($citems as $barcode => $quantity) {
			if($quantity < 1) continue;
			tpl::set('barcode', $barcode);
			tpl::set('quantity', $quantity);
			tpl::make('collecting-item-undefined');
		}
		if(tpl::get('collecting-order')) {
			tpl::set('order_id', 'null');
			if($order_add) {
				$dtype = json::get('dtype');
				tpl::set('order_id', $order_add['id']);
				tpl::set('dservice', @$dtype[$order_add['dtype']]['text'] ?: '');
			}
			if($last_code) {
				tpl::set('last_code', $last_code);
				$_SESSION['last_code'] = '';
			}
			$html = tpl::make('collecting-orders');
			exit($html);
		}
		exit('empty');
	}
	
	static function send_orders() {
		if(!$to = GET('to')) {
			alert('Ошибка! Не указано назначение', 'error');
			return false;
		}
		ob_start();
		$params = self::get_orders_params();
		list($total_orders, $orders) = order::getList($params);
		foreach($orders as $order) {
			if($to == 'sl') {
				$response = shoplogistics::send_order($order);
			}
			if($to == 'pp') {
				$response = ppapi::send_order($order);
			}
			print_r($response);
		}
		$out = '<pre>'.ob_get_clean().'</pre>';
		tpl::set('main', $out);
	}
	
	static function brands() {
		$action = REQUEST('action');
		if($action == 'update') return self::update_brand();
		if($action == 'add') return self::add_brand();
		make_title('Бренды');
		$brands = shop::get_brands();
		$stocks = shop::get_stock_list();
		foreach($brands as $brand) {
			tpl::push($brand);
			foreach($stocks as $id => $stock) {
				tpl::set('option-value', $id);
				tpl::set('option-text', $stock['name']);
				tpl::set('option-selected', $id == $brand['stock'] ? ' selected' : '');
				tpl::make('option', 'option-stock');
			}
			tpl::make('admin-brand');
			tpl::clear('option-stock');
		}
		foreach($stocks as $id => $stock) {
			tpl::set('option-value', $id);
			tpl::set('option-text', $stock['name']);
			tpl::set('option-selected', '');
			tpl::make('option', 'option-stock');
		}
		tpl::make('admin-brands', 'main');
	}
	
	static function add_brand() {
		$data['name'] = trim(REQUEST('name'));
		$data['url'] = trim(REQUEST('url'));
		$data['banner'] = trim(REQUEST('banner'));
		$data['logo'] = trim(REQUEST('logo'));
		$data['stock'] = absint(REQUEST('stock'));
		$data['filter_id'] = absint(REQUEST('filter_id')) ?: NULL;
		db::query('insert ignore into brands set name = :name, url = :url, banner = :banner, logo = :logo, stock = :stock, filter_id = :filter_id', $data);
		if(db::count()) {
			cache::clear('brands');
			die('ok');
		}
		else abort(db::error());
	}
	
	static function update_brand() {
		$bid = absint(REQUEST('bid'));
		$column = trim(REQUEST('column'));
		$value = trim(REQUEST('value'));
		$brands = shop::get_brands();
		if($column == 'filter_id' && !$value) $value = NULL;
		db::query('update brands set '.$column.' = ? where id = ?', $value, $bid);
		if(db::count()) {
			// update manufacturer in products
			if($column == 'name') {
				$old_name = $brands[$bid]['name'];
				db::query('update product set name = replace(name, ?, ?) where manufacturer = ?', $old_name, $value, $old_name);
				db::query('update product set manufacturer = ? where manufacturer = ?', $value, $old_name);
			}
			// update product url
			if($column == 'url') {
				$old_url = $brands[$bid]['url'];
				$name = $brands[$bid]['name'];
				db::query('update product set url_name = replace(url_name, ?, ?) where manufacturer = ?', $old_url, $value, $name);
			}
			cache::flush();
			die('ok');
		}
		else abort(db::error());
	}
	
	static function products_sticker() {
		tpl::load('print-stickers');
		db::query('select name, price, price_old from product p where status > -1 and id not in(18,19,35,36,490) and (quantity > 0 or exists(select 1 from product_option where product_id = p.id and status = 1 and quantity > 0)) order by manufacturer, name');
		$products = db::fetchAll();
		db::query('select product_id, price, price_old from product_option where status = 1 and quantity > 0 order by product_id, name');
		$options = db::fetchAll();
		$options = array_group($options, 'product_id');
		$i = 0;
		foreach($products as $product) {
			tpl::set('name', $product['name']);
			$product_options = @$options[$product['id']];
			if($product_options) {
				foreach($product_options as $po) {
					tpl::set('price', $po['price']);
					if($po['price_old'] > $po['price']) {
						tpl::set('price_old', $po['price_old']);
						tpl::make('sticker-price-old');
					}
					tpl::make('product-sticker');
					$i++;
					if($i == 75) {
						tpl::append('product-sticker', '<div class="page-break"></div>');
						$i = 0;
					}
					tpl::clear('sticker-price-old');
				}
			}
			else {
				tpl::set('price', $product['price']);
				if($product['price_old'] > $product['price']) {
					tpl::set('price_old', $product['price_old']);
					tpl::make('sticker-price-old');
				}
				tpl::make('product-sticker');
				$i++;
				if($i == 75) {
					tpl::append('product-sticker', '<div class="page-break"></div>');
					$i = 0;
				}
				tpl::clear('sticker-price-old');
			}
		}
		$html = tpl::make('print-stickers');
		echo $html;
		exit;
	}
	
	static function add_from_order($to_order) {
		$from_order = absint(REQUEST('from_order'));
		$result = self::merge_orders($from_order, $to_order);
		if($result == 1) $response = 'Заказ '.$from_order.' не найден';
		elseif($result == 2) $response = 'Заказ '.$to_order.' не найден';
		elseif($result == 3) $response = 'Один и тот же заказ';
		else $response = 'ok';
		die($response);
	}
	
	static function tag_noitem($order_id) {
		$product_id = absint(REQUEST('product_id'));
		$option_id = absint(REQUEST('option_id'));
		db::query('select quantity from order_noitem where order_id = ? and product_id = ? and option_id = ?', 
			$order_id, $product_id, $option_id
		);
		$quantity = intval(db::fetchSingle());
		db::query('select cart_items from orders where id = ?', $order_id);
		$cart_items = db::fetchSingle();
		$ordered = 0;
		foreach(json::decode($cart_items) as $item) {
			$item['option_id'] = intval($item['option_id']);
			if($item['id'] == $product_id && $item['option_id'] == $option_id) {
				$ordered = $item['quantity'];
				break;
			}
		}
		// усли уже отмечено сколько заказано, вместо добавления сбрасываем счет и начинаем заново
		if($quantity == $ordered) {
			$quantity = 0;
			db::query('delete from order_noitem where order_id = ? and product_id = ? and option_id = ?', 
				$order_id, $product_id, $option_id
			);
		}
		else {
			$quantity += 1;
			db::query('replace into order_noitem set order_id = ?, product_id = ?, option_id = ?, quantity = ?', 
				$order_id, $product_id, $option_id, $quantity
			);
		}
		die((string)$quantity);
	}
	
	static function merge_orders($from_order, $to_order) {
		if($from_order == $to_order) return 3;
		$ids = $from_order . ',' . $to_order;
		$order1 = db::querySingle('select id, cart_items, total_sum, status from orders where id = '.$from_order, true);
		$order2 = db::querySingle('select id, cart_items from orders where id = '.$to_order, true);
		if(!$order1) return 1;
		if(!$order2) return 2;
		$cart_items1 = json::decode($order1['cart_items']);
		$cart_items2 = json::decode($order2['cart_items']);
		foreach($cart_items1 as $key => $item1) {
			$match = false;
			foreach($cart_items2 as &$item2) {
				if($item2['id'] == $item1['id'] && $item2['option_id'] == $item1['option_id'] && $item2['price'] == $item1['price']) {
					$match = true;
					$item2['quantity'] += $item1['quantity'];
				}
			}
			if(!$match) {
				$cart_items2[] = $item1;
			}
		}
		$cart_items2 = json::encode($cart_items2);
		db::query('update orders set cart_items = :cart_items, total_sum = total_sum + :order_sum where id = :id', [
			'cart_items' => $cart_items2,
			'order_sum' => $order1['total_sum'],
			'id' => $order2['id']
		]);
		if($order1['status'] != 5) {
			db::query('update orders set status = 5 where id = ?', $order1['id']);
			order::returnItems($order1['id']);
		}
		return 0;
	}
	
	static function pnl() {
		pnl::main();
	}
	
	static function bills() {
		bill::main();
	}
	
	static function sl_sverka() {
		if(REQUEST_POST) {
			if(!isset($_FILES['act_file'])) die('No file!');
			if(!$file = uploadFile($_FILES['act_file'], FILES.'/shoplogistics/sverka/')) die('Error');
			include 'PHPExcel/IOFactory.php';
			try {
				$inputFileType = PHPExcel_IOFactory::identify($file);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($file);
			} catch(Exception $e) {
				die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
			}
			$sheet = $objPHPExcel->getSheet(0); 
			$highestRow = $sheet->getHighestRow(); 
			$highestColumn = $sheet->getHighestColumn();
			$act_id = $sheet->getCell('B4')->getValue();
			db::query('delete from sl_sverka where act_id = ?', $act_id);
			db::query('delete from sl_sverka_summary where act_id = ?', $act_id);
			db::prepare('insert into sl_sverka set order_id = :order_id, act_id = :act_id, ddate = :ddate, weight = :weight, cost = :cost, sum_client = :sum_client, sum_return = :sum_return, delivery_price_real = :delivery_price_real');
			$current_row = 9;
			for($row = $current_row; $row <= $highestRow; $row++) {
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE)[0];
				if(!$row_num = intval($rowData[1])) continue;
				$data = ['act_id' => $act_id];
				$data['order_id']= trim($rowData[2]);
				$data['delivery_price_real'] = correct_float($rowData[20]);
				$data['sum_return'] = intval($rowData[18]);
				$data['sum_client'] = intval($rowData[16]);
				$data['cost'] = intval($rowData[12]);
				$data['weight'] = correct_float($rowData[10]);
				$data['ddate'] = trim($rowData[7]);
				db::set($data);
				db::execute();
				$current_row++;
			}
			db::prepare('insert into sl_sverka_summary set act_id = :act_id, summary = :summary');
			while($current_row <= $highestRow) {
				$summary = $sheet->getCell('B'.$current_row)->getValue();
				if(str::startsWith($summary, 'Общая сумма к возврату')) {
					db::set([
						'act_id' => $act_id,
						'summary' => $summary
					]);
					db::execute();
					break;
				}
				$current_row++;
			}
			db::query('update sl_sverka ss set delivery_price = (select delivery_price from orders where id = ss.order_id)');
			die('ok');
		}
		make_title('Сверка с ШЛ');
		db::query('select * from sl_sverka order by act_id');
		while($row = db::fetchArray()) {
			tpl::push($row);
			tpl::make('admin-sl-sverka-row');
		}
		db::query('select * from sl_sverka_summary order by act_id');
		while($row = db::fetchArray()) {
			tpl::push($row);
			tpl::make('admin-sl-sverka-summary-row');
		}
		tpl::make('admin-sl-sverka', 'main');
	}
	
	static function layout() {
		tpl::load('admin-layout');
		if(account::$admin) {
			tpl::make('admin-menu-employee');
			tpl::make('admin-menu-owner');
			tpl::make('admin-submenu-owner');
		}
		if(account::$employee) {
			tpl::make('admin-menu-employee');
		}
		echo tpl::make('admin-layout');
	}
	
}

if(!admin::checkAuth()) {
	status('403 Forbidden');
	die('ACCESS DENIED');
}

tpl::load('admin');
tpl::load('shop');

?>