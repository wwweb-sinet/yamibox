<?php
$w = 100; $h = 30;

$font = dirname(__FILE__) . '/font/fregat.ttf';

$img = imagecreatetruecolor($w, $h);
$bg = imagecolorallocate($img, 255,255,255);
imagefill($img, 0, 0, $bg);

// ������ �� ���
/* for($i=1; $i<=40; $i++) {
	$color = array();
	while(count($color)<3) {
		$color[] = rand(127,255);
	}
	$color = imagecolorallocate($img, $color[0], $color[1], $color[2]);
	$x = rand(5,$w-5); $y = rand(5, $h-5);
	imageline($img, $x, $y, $x+rand(-12,12), $y+rand(-12,12), $color);
}
unset($x); */

$code = '';
// $num_chars = rand(4,5);
// $x_offset = ($num_chars == 4) ? 22 : 18;
// $chars = str_shuffle('AaBbCcDdEeFfGgHhJjKkMmNnPpQRrTUuVWXxYyZz123456789');
// $chars = substr($chars, 0, $num_chars);
// $chars = str_split($chars);
$x_offset = 22;
for($i = 0; $i < 4; $i++) {
	$char = mt_rand(0, 9);
	$code .= $char;
	$x = !isset($x) ? mt_rand(6, 8) : $x;
	$y = mt_rand(24, 26);
	// $c = mt_rand(0, 64);
	$color = imagecolorallocate($img, rand(0, 64), rand(0, 64), rand(0,64));
	imagettftext($img, mt_rand(18,20), $char == 7 ? 0 : mt_rand(-15, 15), $x, $y, $color, $font, $char);
	$x += $x_offset;
}

session_start();
$_SESSION['captcha'] = $code;

header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Content-Type: image/png");
imagepng($img);
imagedestroy($img);
exit();

?>