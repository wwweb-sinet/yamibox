/*
PickPoint.ru
Date: 21:13 30.09.2014
Mail: "Levi Mikhail" <mlevi@mindonline.ru>
Use: isolated jQuery, iframe, hash data transfer
 */
var pp_w_onload = null;
if (typeof window.jQQ !== 'object') {
	(function () {
		var callbacks = [],
		jq;
		function loadScript(url, callback) {
			var script = document.createElement("script")
				script.type = "text/javascript";
			if (typeof script.readyState === 'undefined') {
				script.onload = function () {
					callback();
				};
			} else {
				if (typeof script.onreadystatechange === 'undefined')
					alert("Error load pickpoint widget");
				script.onreadystatechange = function () {
					if (script.readyState === "loaded" || script.readyState === "complete") {
						script.onreadystatechange = null;
						callback();
					}
				};
			};
			script.src = url;
			document.getElementsByTagName("head")[0].appendChild(script);
			return script;
		};
		function validateCallback(callback) {
			if (typeof callback === 'undefined')
				throw "Cannot validate callback: undefined";
			if (callback && callback.length < 1)
				throw "Callback missing at least 1 placeholder argument";
			return callback;
		};
		function fillArray(data, qty) {
			var array = [];
			for (var i = qty; i > 0; i--)
				array.push(data);
			return array;
		};
		window.jQQ = {
			isReady : false,
			isolate : function () {
				var callback = validateCallback(arguments[0]);
				if (!window.jQQ.isReady)
					return callbacks.push(callback);
				return callback.apply(this, fillArray(jq, callback.length));
			},
			setup : function (version, cb) {
				if (!document.body) {
					var doLoad = function () {
						window.jQQ.setup(version)
					};
					if (window.addEventListener) {
						window.addEventListener("load", doLoad, false);
					} else
						if (window.attachEvent) {
							window.attachEvent("onload", doLoad);
						} else if (window.onLoad) {
							if (window.onload)
								pp_w_onload = window.onload;
							window.onload = function () {
								if (pp_w_onload)
									pp_w_onload();
								window.jQQ.setup(version)
							};
						} else {
								if ("console" in window) window.console.log("Pickpoint.ru: jQQ onload fail");
							}
					return;
				}
				var url = '//ajax.googleapis.com/ajax/libs/jquery/' + (version || '1.4.2') + '/jquery.min.js';
				loadScript(url, function () {
					window.jQQ.isReady = true;
					jq = jQuery.noConflict(true);
					if (typeof callbacks.forEach !== 'undefined')
						callbacks.forEach(window.jQQ.isolate);
					delete(callbacks);
					PickPointInit();
				});
			}
		};
	})()
}
function PickPointInit() {
	PickPoint.init();
	if  ("onhashchange" in window) {
		if(window.addEventListener) {
			window.addEventListener("hashchange", PickPoint.hashChangeEvent, false);
		}
		/* else if (window.attachEvent) {
			window.attachEvent("hashchange", PickPoint.hashChangeEvent);    
		} */else 
			window.setInterval("PickPoint.checkHash()", 20);
	} else 
		window.setInterval("PickPoint.checkHash()", 20);
}
jQQ.setup("1.4.2");
PickPoint = {
	ie : (navigator.userAgent.indexOf("MSIE") != -1) || (navigator.userAgent.indexOf("Trident") != -1),    
	opera : navigator.userAgent.indexOf('Opera') != -1 ? true : false,
	styles : "#pickpoint_ushadeoverlay {position:fixed;top:0;left:0;bottom:0;right:0;background:rgba(0,0,0,0.3);display:none;z-index:200;}.pickpoint_ucontainer {background:#EFEFEF;border:1px solid #aaa;box-shadow:0 3px 7px #333;-moz-box-shadow:0 3px 7px #333;-webkit-box-shadow:0 3px 7px #333;-moz-border-radius:4px;-webkit-border-radius:4px; border-radius:4px;-khtml-border-radius:4px; min-width:200px;min-height:250px;max-width:100%;position:fixed; color:#333;text-shadow:0 1px 0 #fff; padding:3px; display:none;z-index:99999999;}.pickpoint_ucontainer iframe{border:0px; border-collapse:collapse;}.pickpoint_frmnav{border-bottom:1px solid #bbb;background:#EFEFEF;height:22px;} .pickpoint_frmclose{line-height:22px;vertical-align:middle;position:absolute; right:0; padding-right:15px; left:auto; z-index:10000000; text-decoration: none; color:#3366EE !important; font-weight:normal; font-size:12px; font-family:Arial;}#pickpoint_widget_ifcs{padding:0; margin:0; border:0px;}",
	_container : null,
	realHash : "",
	scrollTop : 0,
	ref : "",
	_callback : function () {
		alert('Pickpoint: Укажите callback функцию');
	},
    
   
	container_api : {
		uShadeOverlay : function (show) {
			var overlayname = "pickpoint_ushadeoverlay";
			jQQ.isolate(function (jQuery) {
				if (jQuery("#" + overlayname).length <= 0)
					jQuery('body').append("<div id=\"pickpoint_ushadeoverlay\">&nbsp;</div>");
				if (show)
					jQuery("#" + overlayname).fadeIn('fast');
				else
					jQuery("#" + overlayname).fadeOut('fast');
			});
		},
		uCreateContainer : function () {
			var name = "pickpoint_ucc_" + (new Date()).getTime();
			jQQ.isolate(function (jQuery) {
				jQuery('body').append("<div  id=\"" + name + "\" class=\"pickpoint_ucontainer\">&nbsp;</div>");
				PickPoint.container_api.uContainerCenter(name);
			});
			return name;
		},
		uContainerCenter : function (name) {
			jQQ.isolate(function (jq) {
				var obj = jq('#' + name);
				obj.css("position", "absolute");
				obj.css("z-index", "2147483647");
				oh = obj.outerHeight();
				ow = obj.outerWidth();
				if (oh == 0)
					oh = 800;
				if (ow == 0)
					ow = 1060;
				var elem = (document.compatMode === "CSS1Compat") ? document.documentElement : document.body;
				var height = elem.clientHeight;
				var width = elem.clientWidth;
				obj.css("top", Math.max(5, ((height - oh) / 2) + jq(window).scrollTop()) + "px");
				obj.css("left", Math.max(5, ((width - ow) / 2) + jq(window).scrollLeft()) + "px");
				obj.css("width", "1090px");
			});
		},
		uContainerShow : function (name) {
			jQQ.isolate(function (jQuery) {
				jQuery('#' + name).fadeIn();
			});
		},
		uContainerHide : function (name) {
			jQQ.isolate(function (jQuery) {
				jQuery('#' + name).fadeOut();
			});
		},
		uContainerHTML : function (name, html) {
			jQQ.isolate(function (jQuery) {
				jQuery('#' + name).html("<div><a href=\"javascript:PickPoint.container_api.ucClose('" + name + "')\" class=\"pickpoint_frmclose\">Закрыть</a></div><div    class=\"pickpoint_frmnav\">&nbsp;</div><div style=\"overflow:hidden; width:1090px;\">" + html + "</div>");
			});
		},
		ucClose : function (name) {
			jQQ.isolate(function (jQuery) {
				PickPoint.container_api.uContainerHide(name);
				PickPoint.container_api.uShadeOverlay(false);
			});
		}
	},
	__js : function () {
		this.scrollTop = self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
		this._container = this.container_api.uCreateContainer();
		this.ref = document.location.toString();
		if (this.ref.indexOf('#') > 0)
			this.ref = this.ref.substr(0, this.ref.indexOf('#'));
		jQQ.isolate(function (jQuery) {
			if (window.onresize != undefined)
				window.onresize = function () {
					PickPoint.container_api.uContainerCenter(PickPoint._container);
				};
		});
	},
	__loadCSS : function (src) {
		var text = PickPoint.styles;
		var head = document.getElementsByTagName('head')[0];
		var style = document.createElement('style');
		style.type = 'text/css';
		if (style.styleSheet) {
			style.styleSheet.cssText = text;
		} else {
			if (style.innerText == '') {
				style.innerText = text;
			} else {
				style.innerHTML = text;
			}
		}
		head.appendChild(style);
	},
	init : function () {
		jQQ.isolate(function (jQuery) {
			PickPoint.__js();
			PickPoint.__loadCSS();
		});
	},
	open : function (callback, options) {
		if (typeof(callback) == 'function')
			this._callback = callback;
		this.scrollTop = self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
		
		this.realHash = document.location.hash;

		var setting = {
			city : '',
			ids : null,
			numbers : null,			
            mode : null,
            flags1 : null,
            flags2 : null,
            flags3 : null,
            cities : null,
            fromcity : null,
			notLink : false
		}
		if (options) {
			if (typeof(options.city) == 'string')
				setting.city = options.city;
			if (options.ids != undefined && typeof(options.ids.length) == 'number' && options.ids.length > 0)
				setting.ids = options.ids;
			if (options.numbers != undefined && typeof(options.numbers.length) == 'number' && options.numbers.length > 0)
				setting.numbers = options.numbers;
			if (options.cities != undefined && typeof(options.cities.length) == 'number' && options.cities.length > 0)
				setting.cities = options.cities;            
			if (typeof(options.notLink) == 'boolean')
				setting.notLink = options.notLink;
            if (options.flags1 != undefined && typeof(options.flags1.length) == 'number' && options.flags1.length > 0)
				setting.flags1 = options.flags1;       
            if (options.flags2 != undefined && typeof(options.flags2.length) == 'number' && options.flags2.length > 0)
				setting.flags2 = options.flags2;  
            if (options.flags3 != undefined && typeof(options.flags3.length) == 'number' && options.flags3.length > 0)
				setting.flags3 = options.flags3;      
            if (options.mode != undefined)
				setting.mode = options.mode;       
            if (typeof(options.fromcity) == 'string')
                setting.fromcity = options.fromcity;       
		}
        
        
		var uri = "";
		if (setting.ids) {
			var sIds = '';
			for (var i = 0; i < setting.ids.length; i++)
				sIds += setting.ids[i] + ',';
			uri += '&ids=' + sIds;
		}
		if (setting.numbers) {
			var sNumbers = '';
			for (var i = 0; i < setting.numbers.length; i++)
				sNumbers += setting.numbers[i] + ',';
			uri += '&numbers=' + sNumbers;
		}
		if (setting.cities) {
			var sCities = '';
			for (var i = 0; i < setting.cities.length; i++)
				sCities += setting.cities[i] + ',';
			uri += '&cities=' + sCities;
		}
        if (setting.flags1) {
			var sflags = '';
			for (var i = 0; i < setting.flags1.length; i++)
				sflags += setting.flags1[i] + (i<setting.flags1.length-1?',':'');
			uri += '&flags1=' + sflags;
		}
        if (setting.flags2) {
			var sflags = '';
			for (var i = 0; i < setting.flags2.length; i++)
				sflags += setting.flags2[i] + (i<setting.flags2.length-1?',':'');
			uri += '&flags2=' + sflags;
		}
        if (setting.flags3) {
			var sflags = '';
			for (var i = 0; i < setting.flags3.length; i++)
				sflags += setting.flags3[i] + (i<setting.flags3.length-1?',':'');
			uri += '&flags3=' + sflags;
		}        
        
		if (setting.notLink)
			uri += '&n_l=' + setting.notLink;
            
        if (setting.fromcity)
			uri += '&fromcity=' + encodeURIComponent(setting.fromcity);
            
            
        if (setting.mode)
			uri += '&mode=' + setting.mode;            
            
		var url = "//pickpoint.ru/select/";
		if (setting.city != '') {
			url += setting.city + '/';
		}
		url += "?ref=" + encodeURIComponent(this.ref) + "&scrollTop=" + this.scrollTop + uri;
		url += "&action=city_select";
		var ww = PickPoint.ie ? "1160" : "100%";
		this.container_api.uContainerHTML(this._container, "<iframe src=\"" + url + "\" frameborder=\"0\" style=\"overflow-x: hidden; overflow-y: scroll; \"  height=\"700\" width=\"" + ww + "\" id=\"pickpoint_widget_ifcs\">&nbsp;</iframe>");
		this.container_api.uShadeOverlay(true);
		PickPoint.container_api.uContainerCenter(PickPoint._container);
		this.container_api.uContainerShow(this._container);
	},
	getSetting : function (hash) {
		var setting = new Array();
		var fields = hash.split('&');
		for (var i = 0; i < fields.length; i++) {
			var value = fields[i].split('=');
			setting[value[0]] = decodeURIComponent(value[1]);
		}
		return setting;
	},
	checkHash : function () {
		var hash = document.location.hash.toString();
		if (hash.indexOf('#') == 0)
			hash = hash.substr(1);
		if (hash != '') {
			PickPoint.processHash(hash);
		}
	},
	processHash:function(hash){
			if (hash.indexOf('PickpointAction') == -1)
				PickPoint.realHash = hash;
			else
				document.location.hash = PickPoint.realHash;
			var request = PickPoint.getSetting(hash);
			switch (request['PickpointAction']) {
			case 'set':
				PickPoint.goToScroll(parseInt(request['scrollTop']));
				PickPoint._callback(request);
				this.container_api.uShadeOverlay(false);
				this.container_api.uContainerHide(this._container);
				return true;
				break;
			}
			return false;
	},
	
	hashChangeEvent:function(e){
		try{
			var hash = PickPoint.ie ? window.location.hash : e.newURL;
			if (PickPoint.processHash(hash.split("#")[1])){		
				//
			}
			
		}  catch(err) {}
	},
	
	goToScroll : function (offset) {
		if (!offset > 0)
			return;
		if (document.documentElement)
			document.documentElement.scrollTop = offset;
		if (document.body)
			document.body.scrollTop = offset;
	}
};