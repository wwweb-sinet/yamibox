var $form = $('#order-form');
var $city = $form.find('select[name="city"]'), $loadgif = $('#loadgif');
var $region = $form.find('select[name="region"]');
var $city_m = $form.find('.city.kladr');
var $region_m = $form.find('.region.kladr');
var $dtype = $('#dtype');
var $ptype = $form.find('input[name="ptype"]');
// var $ptype1 = $form.find('input[name="ptype"][value="1"]');
// var $ptype2 = $form.find('input[name="ptype"][value="2"]');
var $ptype3 = $form.find('input[name="ptype"][value="3"]');
var $addressForm = $('#delivery-address-form');
var $zipcode = $form.find('input[name="zipcode"]');
var $zipcode_loadgif = $zipcode.next('.loadgif');
var $metro_select = $('#metro-select');
var $yamimap = $('#yamimap');
var $pickup_select = $('#delivery-pickup-select');
var $courier_select = $('#delivery-courier-select');
var $pr_select = $('#delivery-pr-select');
var $pr_apply = $pr_select.next('.select-change');
var $input_toggle = $('.input-toggle');

var $time_from = $form.find('select[name="time_from"]');
var $time_to = $form.find('select[name="time_to"]');

var selected_delivery = null, delivery_type = 0;
var yamimap;
var deliveries = {};

var zipcode = $zipcode.val();

var manual_input = false;

$input_toggle.click(function(e) {
	e.preventDefault();
	manual_input = !manual_input;
	$(this).html(manual_input ? 'Выбрать из списка' : 'Не нашли свой населенный пункт в списке?<br>Нет доставки Почтой России?<br>Попробуйте текстовый ввод');
	input_toggle();
});

$('.input-apply').click(function() {
	var cityname = $city_m.val();
	if(!cityname) {
		return false;
	}
	drop_delivery();
	$zipcode.val('');
	cityname += ', ' + $region_m.val();
	go_to_location(cityname);
});

$(document).ready(function() {
	var $form_loadgif = $form.find('[type="submit"]').next('.loadgif');
	$('#order-form form').ajaxForm({
		beforeSubmit: function() {
			$form_loadgif.show();
		},
		dataType: 'json',
		success: function(response) {
			dataLayer.push({'event':'GAevent', 'eventCategory':'order', 'eventAction':'order_form', 'eventLabel':'send'});
			$form_loadgif.hide();
			if(response.status == 'ok') {
				return redirect(response.redirect);
			}
			else {
				alert(response.message);
			}
		}
	});
	if($city.val()) $loadgif.show().find('.loadgif').show();
	$.kladr.setDefault({
		parentInput: '.personal',
		select: function (obj) {
			if(obj.contentType == 'region') {
				var region = obj.name + ' ' + obj.typeShort + '.';
				$region_m.val(region);
			}
			if(obj.contentType == 'city') {
				var city = obj.type + ' ' + obj.name;
				$city_m.val(city);
			}
		}
	});
	$('.kladr.city').kladr('type', $.kladr.type.city);
	$('.kladr.region').kladr('type', $.kladr.type.region);
});

function input_toggle() {
	if(manual_input) {
		$('#manual-input').val(1);
		$region.disable().hide();
		$city.disable().hide();
		$('.input.kladr').enable().show();
		$('.input-apply').show();
	}
	else {
		$('#manual-input').val(0);
		$region.enable().show();
		$city.enable().show().change();
		$('.input.kladr').disable().hide();
		$('.input-apply').hide();
	}
}

// загрузка вариантов доставки курьером
function load_deliveries(cityname) {
	$loadgif.show();
	var region = manual_input ? $region_m.val() : $region.val();
	$.get('/delivery/list/sl_courier', {'city':cityname,'region':region,'sum':cart.cost}, function(data) {
		deliveries.courier = data.list;
		deliveries.courier_free = data.free;
		$('#free2').text(data.free);
		make_deliveries_courier(deliveries.courier);
	}, 'json');
	load_deliveries_pr(cityname);
	$.get('/delivery/list/metro', {'city':cityname}, function(data) {
		make_metro_list(data);
		$loadgif.hide();
	}, 'json');
}

function load_deliveries_pr(cityname) {
	zipcode = $zipcode.val();
	$zipcode_loadgif.show();
	if(!cityname) cityname = manual_input ? $city_m.val() : $city.val();
	$.get('/delivery/list/pr', {'city':cityname, 'zipcode':zipcode, 'sum':cart.cost}, function(data) {
		deliveries.pr = data.list;
		deliveries.pr_free = data.free;
		$('#free3').text(data.free);
		make_deliveries_pr(deliveries.pr);
		$zipcode_loadgif.hide();
	}, 'json');
}

function pick_point(key, button) {
	$(button).text('Выбрано');
	var point = deliveries.pickup[key];
	// yamimap.balloon.close();
	var html = 'Способ доставки: самовывоз из' + (point.dservice == 10 ? ' офиса ' : ' пункта ') + point.delivery_name + '.<br>Адрес: ' + point.address;
	if(point.dservice != 10) {
		html += '.<br>Стоимость доставки: ' + point.delivery_price + ' р.';
		if(point.days) html += ' Срок доставки: ' + point.days + ' дн.';
	}
	$('#delivery-variant-info').html(html);
	$pickup_select.val(key);
	// console.log(point);
	var info = [];
	info.push(point.delivery_name);
	info.push('самовывоз с ' + point.address);
	if(point.days) info.push(point.days + ' дн.');
	info = info.join(', ');
	var delivery = {
		'type' : point.dtype,
		'dservice' : point.dservice,
		'price' : point.delivery_price,
		'price_real' : point.delivery_price_real,
		'pickuppoint' : point.code,
		'info' : info
	};
	set_delivery(delivery);
}

function pick_delivery(d) {
	var html = 'Способ доставки: ' + d.name;
	if(delivery_type == 2) html += ', доставка курьером до двери';
	html += '.<br>Стоимость доставки: ' + d.price + ' р.';
	if(d.days) html += ' Срок доставки: ' + d.days + ' раб. дн.';
	$('#delivery-variant-info').html(html);
	var delivery = {
		'type' : d.dtype,
		'dservice' : d.dservice,
		'price' : d.price,
		'price_real' : d.price_real,
		'pickuppoint' : 0,
		'info' : d.name + ', ' + d.days + ' дн.'
	};
	set_delivery(delivery);
}

function make_deliveries_courier(deliveries) {
	// courier
	if(deliveries.length) {
		$courier_select.find('option').remove();
		for(i in deliveries) {
			var delivery = deliveries[i];
			var delivery_info = '';
			delivery_info += delivery.name + ', курьером до двери';
			if(delivery.days) delivery_info += ', ' + delivery.days + ' дн.';
			delivery_info += ', ' + delivery.price + ' р.';
			$('<option />').val(i).text(delivery_info).appendTo($courier_select);
		}
		$('#delivery-variant-courier').show();
		$courier_select.change();
	}
	else {
		$('#delivery-variant-courier').hide();
	}
}

function make_deliveries_pr(deliveries) {
	if(isMoscow($city.val())) {
		$('#delivery-variant-pr').hide();
	}
	else {
		$('#delivery-variant-pr').show();
	}
	$pr_select.find('option').remove();
	if(deliveries.length) {
		for(i in deliveries) {
			var delivery = deliveries[i];
			var delivery_info = '';
			delivery_info += delivery.name;
			delivery_info += ', ' + delivery.days + ' дн.';
			delivery_info += ', ' + delivery.price + ' р.';
			$('<option />').val(i).text(delivery_info).appendTo($pr_select);
		}
		
		$pr_select.enable();
		$pr_apply.enable();
	}
	else {
		$('<option />').val('').text('Почта России, требуется почтовый индекс').appendTo($pr_select);
		$pr_select.disable();
		$pr_apply.disable();
	}
}

function make_metro_list(data) {
	if(data.length) {
		$metro_select.find('option').not(':first-child').remove();
		for(m in data) {
			var metro = data[m];
			var $option = $('<option />');
			$option.val(metro.name).text(metro.name);
			$option.appendTo($metro_select);
		}
		$metro_select.enable();
	}
	else {
		$metro_select.val('').disable();
	}
}

function drop_delivery() {
	selected_delivery = null;
	$('#delivery_info').val('');
	$('#delivery_price').val('');
	$('#delivery_price_real').val('');
	$('#delivery-price').text('0 р.');
	$('#pickuppoint').val('');
	$('#dservice').val('');
	$('#total-sum').text(cart.cost + ' р.');
	$('#delivery-variant-info').text('Способ доставки: не выбрано');
	$dtype.val(0).change();
	$('.delivery-variant.selected').removeClass('selected').find('.content').slideUp();
}

function set_delivery(delivery) {
	// console.log(delivery);
	selected_delivery = delivery;
	$('#delivery_info').val(delivery.info);
	$('#delivery_price').val(delivery.price);
	$('#delivery_price_real').val(delivery.price_real);
	$('#delivery-price').text(delivery.price + ' р.');
	$('#pickuppoint').val(delivery.pickuppoint);
	$('#dservice').val(delivery.dservice);
	$('#total-sum').text(cart.cost + delivery.price + ' р.');
	$dtype.val(delivery.type).change();
}

function isMoscow(city) {
	var regexp = /^(moscow|москва|moskva)/i;
	return city.match(regexp) !== null;
}

function isSPB(city) {
	var regexp = /^(санкт[-\s]+петербург)/i;
	return city.match(regexp) !== null;
}

function isObl(region) {
	var regexp = /^(московская обл)/i;
	return region.match(regexp) !== null;
}

function isCentral(city, region) {
	if(isMoscow(city)) return true;
	if(isSPB(city)) return true;
	if(isObl(region)) return true;
	return false;
}

function time_interval_notice() {
	var time_from = $time_from.val();
	var time_to = $time_to.val();
	var selected_interval = time_to - time_from;
	var city = manual_input ? $city_m.val() : $city.val();
	var region = manual_input ? $region_m.val() : $region.val();
	var day = new Date().getDay();
	var isWeekend = (day == 6) || (day == 0);
	var min_interval = 3;
	try {
		if(isMoscow(city)) min_interval = isWeekend ? 8 : 3;
		else if(isObl(region)) min_interval = isWeekend ? 8 : 5;
		if(selected_interval < min_interval) {
			$('#time-error').text('интервал должен быть не менее '+ min_interval +' часов');
		}
		else {
			$('#time-error').text('');
		}
	}
	catch(e) {}
}

function check_zipcode() {
	var zipcode = $zipcode.val();
	var regexp = /(\d{6})/;
	if(zipcode.match(regexp) !== null) {
		load_deliveries_pr();
	}
}

function go_to_location(cityname) {
	ymaps.geocode(cityname, {
		results: 1
	}).then(function (res) {
		var firstGeoObject = res.geoObjects.get(0),
			coords = firstGeoObject.geometry.getCoordinates(),
			bounds = firstGeoObject.properties.get('boundedBy');
		var location_name = firstGeoObject.properties.get('name');
		show_map_points(cityname);
		yamimap.setBounds(bounds, {
			checkZoomRange: true
		});
		load_deliveries(cityname);
	}, {
		autoFitToViewport: 'always'
	});
}

function show_map_points(cityname) {
	yamimap.geoObjects.removeAll();
	var region = manual_input ? $region_m.val() : $region.val();
	$.get('/delivery/list/sl_points', {'city':cityname,'region':region,'sum':cart.cost}, function(points) {
		deliveries.pickup = points.list;
		deliveries.pickup_free = points.free;
		// console.log(points.central);
		$('#free1').text(points.free);
		if(deliveries.pickup.length) {
			$pickup_select.find('option').remove();
			for(i in deliveries.pickup) {
				var point = deliveries.pickup[i];
				var html = '<span class="pink bold">'+point.delivery_name+'</span>';
				html += '<br>' + point.name + '<br>' + point.address;
				if(point.phone) html += '<br>Тел: ' + point.phone;
				if(point.worktime) html += '<br>' + point.worktime;
				if(point.metro) html += '<br>Метро: ' + point.metro;
				if(point.dservice != 10) {
					html += '<br>' + '<span class="bold">Доставка: ' + point.delivery_price + ' р.';
					if(point.days) html += ', ' + point.days + ' р. дн.';
					html += '</span>';
				}
				html += '<div class="center"><span class="pointer point-pick" onclick="pick_point('+i+', this)">Выбрать</span></div>';
				var customCircle = ymaps.templateLayoutFactory.createClass('<div class="yamimap-placemark" id="point-'+i+'"><span class="yamimap-delivery-icon delivery-'+point.dservice+'"></span></div>');
				var z = 650;
				if(point.dservice == 10) z = 651;
				if(point.dservice == 5) z = 648;
				if(point.dservice == 6) z = 649;
				var placemark = new ymaps.Placemark([point.latitude, point.longitude], {
					balloonContent : html,
				}, {
					iconLayout: customCircle,
					iconShape: {
						type: 'Circle',
						coordinates: [0, 0],
						radius: 12
					},
					zIndex: z
				});
				placemark.events.add(['click'], function(e) {
					//var placemarkPixelCoordinates = placemark.geometry.getPixelGeometry().getCoordinates();
					//yamimap.setGlobalPixelCenter([placemarkPixelCoordinates[0], placemarkPixelCoordinates[1]],
					//yamimap.getZoom(), {duration: 250});
				});
				yamimap.geoObjects.add(placemark);
				// Pickups list into selector
				var option_text = point.delivery_name + ', ' + point.name + ', ' + point.delivery_price + ' р.';
				if(point.days) option_text += ', ' + point.days + ' р. дн.';
				$('<option />').val(i).text(option_text).appendTo($pickup_select);
			}
			$('#delivery-variant-pickup').show();
		}
		else {
			$('#delivery-variant-pickup').hide();
		}
	}, 'json');
}

$('#yamimap-toggle').click(function(e) {
	e.preventDefault();
	$yamimap.toggle();
});

$('.delivery-variant .opener').click(function() {
	var $el = $(this).parent(), $content = $el.find('.content');
	var type = parseInt($el.data('type'));
	if(delivery_type != type) drop_delivery();
	delivery_type = type;
	if(type == 1) {
		// костыль, который зумит карту при первом открытии (в исходном состоянии почему-то показывает весь мир)
		if($yamimap.is(':hidden')) {
			// $yamimap.show();
			yamimap.setZoom(10);
		}
		$addressForm.find('input').disable();
	}
	else {
		$addressForm.appendTo($content).show().find('input').enable();
	}
	if(type == 3) {
		$zipcode.attr('required', true);
		$('#time-select-row').hide();
		$('#metro-select-row').hide();
	}
	else {
		$zipcode.removeAttr('required');
		$('#time-select-row').show();
		$('#metro-select-row').show();
	}
	if(!$el.is('.selected')) {
		$('.delivery-variant.selected').removeClass('selected').find('.content').slideUp();
		$el.addClass('selected');
		$content.slideDown();
	}
});

$('.select-change').click(function() {
	$(this).prev('select').trigger('apply');
});

$courier_select.on('apply', function() {
	var $el = $(this), key = $el.val();
	var delivery_courier = deliveries.courier[key];
	pick_delivery(delivery_courier);
});
$pickup_select.on('apply', function() {
	var $el = $(this), key = $el.val();
	pick_point(key);
	$('#point-'+key).click();
});
$pr_select.on('apply', function() {
	var $el = $(this), key = $el.val();
	var delivery_pr = deliveries.pr[key];
	pick_delivery(delivery_pr);
});

$dtype.change(function() {
	var val = parseInt($dtype.val());
	var city = $city.val();
	var moscow = city ? isMoscow(city) : false;
	if(val == 1 || val == 5 || val == 4) {
		// $ptype1.uncheck().disable().parent().addClass('disabled');
		if(moscow) {
			// if(selected_delivery.dservice == 10) $ptype2.enable().parent().removeClass('disabled');
			// else $ptype2.uncheck().disable().parent().addClass('disabled');
			if(selected_delivery.dservice == 10) $ptype3.uncheck().disable().parent().addClass('disabled');
			else $ptype3.check().enable().parent().removeClass('disabled');
		}
		else {
			// $ptype2.uncheck().disable().parent().addClass('disabled');
			$ptype3.enable().parent().removeClass('disabled');
		}
	}
	else {
		// $ptype2.uncheck().disable().parent().addClass('disabled');
		// if(val == 3) $ptype1.disable().parent().addClass('disabled');
		// else $ptype1.enable().parent().removeClass('disabled');
	}
	if(!moscow && cart.cost < 1000) {
		$ptype3.uncheck().disable().parent().addClass('disabled');
	}
	time_interval_notice();
});

ymaps.ready(function() {
	var cityname = $city.val();
	var center = [55.76, 37.64];
	ymaps.geocode(cityname, {
		results: 1
	}).then(function (res) {
		center = res.geoObjects.get(0).geometry.getCoordinates();
	});
	yamimap = new ymaps.Map("yamimap", {
		center : center,
		zoom : 11,
		controls: ['zoomControl']
	});
	if(cityname) go_to_location(cityname);
});

$city.change(function() {
	var cityname = $city.val();
	if(!cityname) {
		return false;
	}
	drop_delivery();
	$zipcode.val('');
	$.cookie('city_to', cityname);
	go_to_location(cityname);
});

$region.change(function() {
	var region = $region.val();
	$.cookie('region_to', region);
	$city.html('');
	drop_delivery();
	$('.delivery-variant').hide().find('.content').hide();
	$.get('/delivery/list/locations', {'region':region}, function(list) {
		$city.append('<option value="">Населенный пункт</option>');
		for(c in list) {
			var $option = $('<option value="'+list[c].name+'">'+list[c].name+'</option>');
			$option.appendTo($city);
		}
	}, 'json');
});

$zipcode.bind('keyup', check_zipcode);
$zipcode.bind('change', check_zipcode);

$time_from.change(time_interval_notice);
$time_to.change(time_interval_notice);

