Host: yamibox.ru

User-agent: *
Sitemap: http://yamibox.ru/sitemap.xml
Disallow: /*.php
Disallow: /account
Disallow: /add2cart
Disallow: /admin
Disallow: /cart
Disallow: /remove
Disallow: /collection
Disallow: /wishlist
Disallow: /add2wishlist
Disallow: /client
Disallow: /files/
Allow: /files/*/
